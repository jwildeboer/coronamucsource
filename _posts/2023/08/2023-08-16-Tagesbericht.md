---
layout: default
title: 2023-08-16 Tagesbericht
category: Tagesberichte
---

![2023-08-16 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, steigende Zahlen. Zweite zeigt R(t) Wert für München, der auf 1,18 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests kaum noch aussagekräftig ist.](/images/2023/08/20230816.png)

Das RKI meldet eine Inzidenz von 4,1 für München. 22 Neue, Wegen Feiertag in Bayern gestern keine Meldungen. Die gestern genannte Inzidenz von 4,6 bleibt unverändert. Dunkelziffer Faktor IFR: 11x ITS: 9x Abwasser: 72x. R(t) jetzt 1,18. 7 Tage Summe der Todesfälle bleibt 0.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 08.08. | 3,3 &rarr; 3,4 |
| 09.08. | 3,2 &rarr; 3,2 |
| 10.08. | 3,5 &rarr; 3,5 |
| 11.08. | 3,7 &rarr; 3,9 |
| 12.08. | 3,7 &rarr; 3,9 |
| 13.08. | 3,7 &rarr; 3,9 |
| 14.08. | 4,6 &rarr; 4,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 72x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 11 |
| ITS basiert für München | 2 |
| Abwasserbasiert für München | 72 |
| ITS basiert für Bayern | 9 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 0. Damit insgesamt bis jetzt 233 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) geht auf 1,18. Somit Tag 11 mit Inzidenz über 1.

| Datum | R(t) |
|---|---:|
| 09.08. | 1,12 |
| 10.08. | 1,12 |
| 11.08. | 1,14 |
| 12.08. | 1,12 |
| 13.08. | 1,15 |
| 14.08. | 1,19 |
| 15.08. | 1,18 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Insgesamt somit 2 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 1 | +/-0 | 
| Intensiv, beatmet | 1 | +/-0 | 
| SUMME | 2 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt im Vergleich zu Vorgestern mit 77 auf gestern 61.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern bleibt im Vergleich zu gestern mit 11 gleich (+/-0). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern mit 9 auf heute 10  (+1). Insgesamt somit 21 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 11 | +/-0 |
| Intensiv, beatmet | 10 | +1 |
| SUMME | 21 | +1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-08-11 mit Werten bis 2023-08-09**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sehen seit Juli ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/08/20230811A.png)

## Varianten München

**Letzte Aktualisierung: 2023-08-09 mit Werten bis 2023-07-10**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 3 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-07-10  wieder auf einem leidlich aktuellen Stand.

TL;DR: Omikron XBB hat alles übernommen. Die Omikron BA Varianten tauchen gar nicht mehr auf. An der Spitze ist jetzt XBB.1.5 mit 41,58%, dann kommt XBB.1.16 mit 33,33%, XBB.2 mit "nur" noch 22,08% nachdem es 2 Wochen zuvor noch fast 50% waren. XBB.1.9 hat nur noch 2,82% wo es vor 4 WOchen noch 47,42% waren.

| KW | 24 | 25 | 26 | 27 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 34,01% | 27,27% | 23,00% | 41,58% |
| Omikron XBB.1.9 | 47,42% | 22,67% | 10,17% | 2,82% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 19,69% | 33,33% |
| Omikron XBB.2 | 0,83% | 49,98% | 25,82% | 22,08% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Rekombinante Linien | 17,28% | 0,00% | 21,06% | 0,00% |
| Andere | 0,46% | 0,08% | 0,26% | 0,19% |

![VoC München](/images/2023/08/20230809V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-08-08**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 08.08.2023.

In Bayern wurden im Zeitraum zwischen dem 12. Juli 2023 und dem 8. August 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-07-11 | 2023-08-08 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.920 | 9.991.925 | +5 | 75,22% (+/-0%) |
| 2 | 9.901.631 | 9.901.645 | +14 | 75,14% (+/-0%) |
| 3 | 7.808.698 | 7.808.710 | +12 | 59,26% (+/-0%) |
| 4 | 1.584.729 | 1.584.799 | +70 | 12,03% (+/-0%) |
| 5 | 113.267 | 113.329 | +62 | 0,86% (+/-0%) |
| 6 | 5.626 | 5.635 | +9 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 172 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es noch 316 Impfungen insgesamt. 