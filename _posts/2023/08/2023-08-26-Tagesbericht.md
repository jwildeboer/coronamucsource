---
layout: default
title: 2023-08-26 Tagesbericht
category: Tagesberichte
---

![2023-08-26 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, seit 3 Tagen keine neuen Zahlen. Zweite zeigt R(t) Wert für München, der auf 0,91 sinkt, wegen fehlender Zahlen. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests kaum noch aussagekräftig ist.](/images/2023/08/20230826.png)

**Es wurden seit 3 Tagen keine neuen Zahlen vom RKI gemeldet. Inzidenz und R(t) sind daher momentan unrealistisch niedrig.**

Das RKI meldet eine (zu niedrige) Inzidenz von 2,4 für München. 0 Neue, 0 Nachmeldungen, 0 Korrekturen. Die gestern genannte Inzidenz von 3,5 bleibt gleich. Dunkelziffer Faktor IFR: 11x ITS: 32x Abwasser: 72x. R(t) sinkt auf (unrealsitische) 0,92. 7 Tage Summe der Todesfälle bleibt 0.
<!--more-->

## Inzidenzentwicklung

**Es wurden seit 3 Tagen keine neuen Zahlen vom RKI gemeldet. Inzidenz und R(t) sind daher momentan unrealistisch niedrig.**

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 18.08. | 4,8 &rarr; 5,0 |
| 19.08. | 4,8 &rarr; 5,0 |
| 20.08. | 4,8 &rarr; 5,0 |
| 21.08. | 4,8 &rarr; 5,1 |
| 22.08. | 5,8 &rarr; 5,8 |
| 23.08. | 4,6 &rarr; 4,6 |
| 24.08. | 3,5 &rarr; 3,5 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 72x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 11 |
| ITS basiert für München | 15 |
| Abwasserbasiert für München | 72 |
| ITS basiert für Bayern | 31 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 0. Damit insgesamt bis jetzt 233 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

**Es wurden seit 3 Tagen keine neuen Zahlen vom RKI gemeldet. Inzidenz und R(t) sind daher momentan unrealistisch niedrig.**

Unser 7 Tage R(t) sinkt wegen fehlender Zahlen vom RKI auf unrealistische 0,92.

| Datum | R(t) |
|---|---:|
| 19.08. | 1,19 |
| 20.08. | 1,16 |
| 21.08. | 1,13 |
| 22.08. | 1,14 |
| 23.08. | 1,09 |
| 24.08. | 1,02 |
| 25.08. | 0,92 |

Die scheinbare Reduzierung des R(t) Wertes heute liegt an den fehlenden Zahlen vom LGL.

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt im Vergleich zu gestern mit 3 auf heute 4 (+1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Insgesamt somit 5 Intensivbetten belegt (+1). 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | +1 | 
| Intensiv, beatmet | 1 | +/-0 | 
| SUMME | 5 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung wurde nicht aktualisiert.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern mit 29 auf heute 24 (-5). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 11 gleich (+/-0). Insgesamt somit 35 Intensivbetten belegt (-5).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 24 | -5 |
| Intensiv, beatmet | 11 | +/-0 |
| SUMME | 35 | -5 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-08-25 mit Werten bis 2023-08-23**

Wegen Mariä Himmelfahrt wurden die Werte für vorletzte Woche einfach "vergessen". Daher gibt es eine Lücke in den Zahlen.

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sehen seit Juli ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/08/20230826A.png)

## Varianten München

**Letzte Aktualisierung: 2023-08-22 mit Werten bis 2023-07-24**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 3 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-07-24  wieder auf einem leidlich aktuellen Stand.

TL;DR: Omikron EG.5 liegt vorne, aber verharrt bei 33,91%. Omikron XBB.1.16 liegt auf Platz 2 mit 19,08%, Omikron XBB.1.5 auf Platz 3 mit 16,97%. Die Ominkron BA, BN, BF und BQ Varianten kommen nicht mehr vor.

| KW |  26 |  27 |  28 |  29 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron BA.2.75 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron BN.1 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron XBB.1.5 |  23,00% |  26,19% |  14,00% |  16,97% |
| Omikron XBB.1.9 |  2,23% |  1,48% |  9,98% |  4,88% |
| Omikron XBB.1.16 |  19,69% |  26,55% |  10,39% |  19,08% |
| Omikron XBB.2 |  25,82% |  11,04% |  9,78% |  9,67% |
| Omikron BA.4 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron BA.5 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron BF.7 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron BQ.1 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron CH.1.1 |  0,00% |  0,00% |  0,00% |  0,00% |
| Omikron EG.5 |  7,94% |  18,54% |  32,83% |  33,91% |
| Rekombinante Linien |  21,06% |  15,88% |  22,67% |  14,94% |
| Andere |  0,26% |  0,32% |  0,35% |  0,55% |

![VoC München](/images/2023/08/20230822V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-08-08**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 08.08.2023.

In Bayern wurden im Zeitraum zwischen dem 12. Juli 2023 und dem 8. August 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-07-11 | 2023-08-08 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.920 | 9.991.925 | +5 | 75,22% (+/-0%) |
| 2 | 9.901.631 | 9.901.645 | +14 | 75,14% (+/-0%) |
| 3 | 7.808.698 | 7.808.710 | +12 | 59,26% (+/-0%) |
| 4 | 1.584.729 | 1.584.799 | +70 | 12,03% (+/-0%) |
| 5 | 113.267 | 113.329 | +62 | 0,86% (+/-0%) |
| 6 | 5.626 | 5.635 | +9 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 172 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es noch 316 Impfungen insgesamt. 