---
layout: default
title: 2023-09-21 Tagesbericht
category: Tagesberichte
---

![2023-09-21 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, klare Tendenz nach oben. Zweite zeigt R(t) Wert für München, der etwas auf 1,26 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests kaum noch aussagekräftig ist.](/images/2023/09/20230921.png)

Das RKI meldet eine Inzidenz von 15,9 für München. 54 Neue, 12 Nachmeldungen, 0 Korrekturen. Die gestern genannte Inzidenz von 13,6 erhöht sich auf 14,5.. Dunkelziffer Faktor IFR: 10x ITS: 9x Abwasser: 26x. R(t) sinkt auf 1,26. 7 Tage Summe der Todesfälle ist 0.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 13.09. | 11,4 &rarr; 11,5 |
| 14.09. | 12,2 &rarr; 12,2 |
| 15.09. | 12,8 &rarr; 13,2 |
| 16.09. | 12,8 &rarr; 13,2 |
| 17.09. | 12,8 &rarr; 13,2 |
| 18.09. | 14,8 &rarr; 15,3 |
| 19.09. | 13,6 &rarr; 14,5 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 42x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 5 |
| Abwasserbasiert für München | 26 |
| ITS basiert für Bayern | 9 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist wieder 0. Damit insgesamt bis jetzt 236 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 1,26. Tag 18 mit R(t) über 1 und damit exponenziell.

| Datum | R(t) |
|---|---:|
| 14.09. | 1,15 |
| 15.09. | 1,22 |
| 16.09. | 1,28 |
| 17.09. | 1,29 |
| 18.09. | 1,31 |
| 19.09. | 1,27 |
| 20.09. | 1,26 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern mit 5 auf heute 4 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 4 auf heute 3 (-1). Insgesamt somit 7 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | -1 | 
| Intensiv, beatmet | 3 | -1 | 
| SUMME | 7 | -2 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung steigt von vorgestern 269 auf gestern 302. Zum ersten mal seit dem 28.04.2023 ist die Tagesmeldung wieder über 300. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern mit 36 auf heute 33 (-3). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 12 gleich (+/-0). Insgesamt somit 45 Intensivbetten belegt (-3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 33 | -3 |
| Intensiv, beatmet | 12 | +/-0 |
| SUMME | 45 | -3 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-09-18 mit Werten bis 2023-09-13**

Die Abwasserzahlen wurden am Montag nach 10 Tagen aktualisiert, mit Zahlen bis zum 13.09.2023, also **vor** der Wiesneröffnung. Am Freitag müsste es dann die ersten Wiesnzahlen geben, aber, Überraschung, das BayVOC Projekt hat schonmal angekündigt, daß es in dieser Woche leider, leider keine neuen Zahlen geben wird.

Momentan sind die Einzelwerte höher als in der Vorwoche, von ca. 14 auf jetzt 20.. Die IAA lässt grüßen? Der Trendwert wird zweistellig.

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sahen im Juli/August ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/09/20230918A.png)

## Varianten München

**Letzte Aktualisierung: 2023-09-18 mit Werten bis 2023-08-21**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 3 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-08-21, "nur" knapp 1 Monat hinterher.

TL;DR: Omikron EG.5 liegt mit 43,48% klar vorne. Omikron XBB.1.16 mit 26,94% auf Platz 2 . Omikron XBB.2.3 liegt auf Platz 3 mit 26,94%, die Omikron BA, BN, BF und BQ Varianten kommen nicht mehr vor.

| KW | 29 | 30 | 32 | 33 |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,93% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,26% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 7,47% | 10,08% | 5,86% | 0,00% |
| Omikron XBB.1.9 | 39,61% | 21,65% | 12,76% | 9,49% |
| Omikron XBB.1.16 | 12,72% | 18,60% | 21,99% | 26,94% |
| Omikron XBB.2.3 | 3,16% | 9,56% | 9,69% | 20,06% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 2,44% | 0,00% | 0,00% |
| Omikron EG.5 | 36,67% | 35,85% | 33,85% | 43,48% |
| Rekombinante Linien | 0,00% | 0,31% | 15,60% | 0,00% |
| Andere | 0,37% | 0,32% | 0,25% | 0,03% |

![VoC München](/images/2023/09/20230918V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-09-12**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 08.08.2023.

In Bayern wurden im Zeitraum zwischen dem 12. Juli 2023 und dem 8. August 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-08-08 | 2023-09-12 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.925 | 9.991.927 | +2 | 75,22% (+/-0%) |
| 2 | 9.901.645 | 9.901.650 | +5 | 75,14% (+/-0%) |
| 3 | 7.808.710 | 7.808.720 | +10 | 59,26% (+/-0%) |
| 4 | 1.584.799 | 1.584.838 | +39 | 12,03% (+/-0%) |
| 5 | 113.329 | 113.448 | +119 | 0,86% (+/-0%) |
| 6 | 5.635 | 5.658 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 198 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es 178 Impfungen insgesamt. 