---
layout: default
title: 2023-09-16 Tagesbericht
category: Tagesberichte
---

![2023-09-16 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, klare Tendenz nach oben. Zweite zeigt R(t) Wert für München, der auf 1,21 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests kaum noch aussagekräftig ist.](/images/2023/09/20230916.png)

Das RKI meldet eine Inzidenz von 12,8 für München. 35 Neue, 0 Nachmeldungen, 1 Korrektur. Die gestern genannte Inzidenz von 12,2 bleibt gleich. Dunkelziffer Faktor IFR: 10x  ITS: 9x Abwasser: 38x. R(t) steigt weiter auf jetzt 1,21. 7 Tage Summe der Todesfälle ist 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 08.09. | 8,0 &rarr; 8,5 |
| 09.09. | 8,0 &rarr; 8,5 |
| 10.09. | 8,0 &rarr; 8,5 |
| 11.09. | 8,4 &rarr; 8,5 |
| 12.09. | 8,5 &rarr; 10,2 |
| 13.09. | 11,4 &rarr; 11,5 |
| 14.09. | 12,2 &rarr; 12,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 42x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 8 |
| Abwasserbasiert für München | 38 |
| ITS basiert für Bayern | 9 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 1. Damit insgesamt bis jetzt 236 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt auf 1,21.

| Datum | R(t) |
|---|---:|
| 09.09. | 1,13 |
| 10.09. | 1,11 |
| 11.09. | 1,09 |
| 12.09. | 1,08 |
| 13.09. | 1,10 |
| 14.09. | 1,15 |
| 15.09. | 1,21 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern mit 8 auf heute 7  (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 11 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 7 | -1 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 11 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung steigt von vorgestern 263 auf gestern 272. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern bleibt im Vergleich zu gestern mit 32 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 12 gleich (+/-0). Insgesamt somit 44 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 32 | +/-0 |
| Intensiv, beatmet | 12 | +/-0 |
| SUMME | 44 | +/-0 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-09-08 mit Werten bis 2023-09-06**

Leider keine Überraschung, wir kennen das aus dem Vorjahr: Die Abwasserwerte für Mümchen wurden auch heute NICHT aktualisiert. Wer einen Zusammenhang mit IAA, Schulanfag und Wiesn Eröffnung vermutet &mdash; nun, es liegt leider auf der Hand. 

Momentan sind die Werte deutlich höher als in der Vorwoche, ca Faktor 5. Die IAA lässt grüßen? Der Trendwert bleibt aber noch einstellig.

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sahen im Juli/August ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/09/20230908A.png)

## Varianten München

**Letzte Aktualisierung: 2023-09-08 mit Werten bis 2023-07-31**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 1 Woche wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-07-31, "nur" knapp 1.5 Monate hinterher.

TL;DR: Omikron EG.5 liegt mit 45,61% klar vorne. Omikron XBB.1.9 verdreifacht sich fast auf 28,17% und liegt damit auf Platz 2.Omikron XBB.1.16 liegt auf Platz 3 mit 16,13%, etwas weniger als in der KW 29,  Die Ominkron BA, BN, BF und BQ Varianten kommen nicht mehr vor.

| KW | 27 | 28 | 29 | 30 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 26,19% | 14,00% | 20,80% | 9,56% |
| Omikron XBB.1.9 | 1,48% | 9,98% | 9,92% | 28,17% |
| Omikron XBB.1.16 | 26,55% | 10,39% | 19,92% | 16,13% |
| Omikron XBB.2 | 11,04% | 9,78% | 4,92% | 0,00% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron EG.5 | 18,54% | 32,83% | 35,84% | 45,61% |
| Rekombinante Linien | 15,88% | 22,67% | 7,47% | 0,00% |
| Andere | 0,00% | 0,00% | 0,24% | 0,00% |

![VoC München](/images/2023/09/20230908V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-09-12**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 08.08.2023.

In Bayern wurden im Zeitraum zwischen dem 12. Juli 2023 und dem 8. August 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-08-08 | 2023-09-12 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.925 | 9.991.927 | +2 | 75,22% (+/-0%) |
| 2 | 9.901.645 | 9.901.650 | +5 | 75,14% (+/-0%) |
| 3 | 7.808.710 | 7.808.720 | +10 | 59,26% (+/-0%) |
| 4 | 1.584.799 | 1.584.838 | +39 | 12,03% (+/-0%) |
| 5 | 113.329 | 113.448 | +119 | 0,86% (+/-0%) |
| 6 | 5.635 | 5.658 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 198 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es 178 Impfungen insgesamt. 