---
layout: default
title: 2023-02-08 Tagesbericht
category: Tagesberichte
---

![2023-02-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230208.png)

Das RKI meldet eine Inzidenz von 85,5. 239 neue und 11 Nachmeldungen, davon 8 für vorgestern.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 31.01. | 78,1 &rarr; 82,3 |
| 01.02. | 84,0 &rarr; 84,9 |
| 02.02. | 85,7 &rarr; 87,8 |
| 03.02. | 88,1 &rarr; 91,6 |
| 04.02. | 88,1 &rarr; 91,6 |
| 05.02. | 88,1 &rarr; 91,6 |
| 06.02. | 89,9 &rarr; 90,9 |

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 5. Damit insgesamt jetzt 69 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 08.02. | 07.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 185,3 | 195,6 | -0,2% |
| LK Ebersberg | 74,0 | 88,5 | -16,4% |
| LK Freising | 71,2 | 70,7 | +0,7% |
| LK Fürstenfeldbruck | 122,6 | 120,8 | +1,5% |
| LK München | 103,8 | 96,3 | +7,8% |
| LK Starnberg | 89,2 | 83,4 | +7,0% |

Weiter steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt heute leicht auf 1,14.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 01.02. | 1,16 | --- |
| 02.02. | 1,20 | --- |
| 03.02. | 1,21 | --- |
| 04.02. | 1,21 | --- |
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |
| 07.02. | 1,14 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute wieder: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 18 von 247 auf 265. IMC steigt um 2 von gestern 14 auf heute 16. Intensiv bleibt auf 21. Damit insgesamt 302 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 265 | +18 | 
| IMC | 16 | +2 | 
| Intensiv | 21 | +/-0 |
| SUMME | 302 | +20 |

Damit sind 22 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.