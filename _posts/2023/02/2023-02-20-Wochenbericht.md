---
layout: default
title: 2023-02-20 Wochenbericht KW07
category: Wochenberichte
---

![2023-02-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 17 Tagen über 1, sinkt aber wieder. Zweite zeigt Krankenhausbetten, deren Zahl sich jetzt um 370 stabilisiert. Dritte zeigt Inzidenz, die seit 11 Tagen wieder über 100 liegt.](/images/2023/02/20230220.png)

In der letzten Woche gab es insgesamt 1.603 neue COVID Fälle in München. Das sind 1,2% **mehr** als in der Vorwoche (1.584). Leider auch 13 Tote mit COVID Nachweis in den letzten 7 Tagen, mehr als in der Vorwoche (9).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe minimal von 1.585 auf 1.603. Die um Nachmeldungen korrigierte Inzidenz bleibt am Ende stabil von 106,5 auf 107,7.  An 4 von 7 Tagen war die Inzidenz sogar über 110.

Diese Woche sind Faschingsferien in Bayern, daher ist ein leichter Rückgang der Zahlen zu erwarten.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 12.02. | 1 | 1.585 | 106,5 |
| 13.02. | 470 | 1.691 | 113,7 |
| 14.02. | 353 | 1.743 | 117,2 |
| 15.02. | 306 | 1.701 | 114,3 |
| 16.02. | 274 | 1.689 | 113.5 |
| 17.02. | 199 | 1.603 | 107,7 |
| 18.02. | 0 | 1.603 | 107,7 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 1,10 auf jetzt 1,02 und blieb damit knapp über 1.

| Datum | R(t) 7 Tage |
|---|---:|
| 13.02. | 1,10 |
| 14.02. | 1,12 |
| 15.02. | 1,12 |
| 16.02. | 1,11 |
| 17.02. | 1,08 |
| 18.02. | 1,04 | 
| 19.02. | 1,02 |

## Abwasserwerte für München

Es gibt jetzt auch endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München. Die Daten laufen ein wenig hinterher und gehen nur bis zum 7. Februar, aber immerhin. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten erkennt man aber auch einen steigenden Trend, den wir in unserer R(t) Berechnung auch sehen, aber NICHT so richtig in der Inzidenz.

![Abwasserwerte München](/images/2023/02/KW06Abwasser.png)

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München steigt die Zahl der belegten Normalbetten stetig weiter von 293 auf jetzt 336. IMC pendelt knapp unter 15, Intensiv sinkt von 23 auf jetzt 16. An 7 von 7 Tagen war die Summe zweistellig **höher** als in der Vorwoche.

Im Vergleich zum Vorjahr trotzdem niedrigere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 13.02. | 293 | 13 | 23 | 329 | +47 | 510 |
| 14.02. | 308 | 15 | 22 | 345 | +43 | 532 |
| 15.02. | 319 | 11 | 22 | 352 | +45 | 520 |
| 16.02. | 329 | 10 | 26 | 365 | +63 | 531 |
| 17.02. | 331 | 11 | 23 | 365 | +64 | 528 |
| 18.02. | 334 | 13 | 20 | 367 | +65 | 528 |
| 19.02. | 336 | 14 | 16 | 368 | +66 | 529 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, minimal ruhiger aus. Langsam sinkende Zahlen, die unter den Vorjahreswerten bleiben. Sieht nach einer saisonalen Entwicklung aus.

Bei den Intensivbetten steigen die Zahlen jedoch stetig von 161 auf jetzt 199.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 13.02. | 161 | 1.906 | 1.871 |
| 14.02. | 165 | 1.904 | 1.850 |
| 15.02. | 172 | 1.932 | 1.850 |
| 16.02. | 182 | 1.864 | 1.927 |
| 17.02. | 181 | 1.731 | 1.960 |
| 18.02. | 178 | 1.679 | 1.953 |
| 19.02. | 199 | 1.618 | 2.007 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche weiter weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 172 | 75,21% (+/-0%) |
| 2. | 88 | 75,13% (+/-0%) |
| 3. | 285 | 59,25% (+0,01%) |
| 4. | 2.067 | 11,96% (+0,01%) |
| 5. | 834 | 0,83% (+0,01%) |
| 6. | 59 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/02/KW07Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 10.389 Tests/Tag mit einer Positivrate von 17,38%. Gegenüber den Zahlen von letzter Woche (11.130 Tests/Tag mit 15,15% Positivrate) also etwas weniger Tests bei weiter steigender Positivrate.

10.389 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 10.389 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 17,38%.](/images/2023/02/20230216Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten etwas hinterherhinken.

Die Variante BA.5 bleibt in München erstaunlich stark vertreten, XBB.1.5 steigt stetig an. BA.2.75 und BQ.1 sind reativ stabil auf Platz 3 und 4. BA.4 ist mehr oder weniger verschwunden.

| Variante | KW 01 | KW 02 | KW 03 | KW 04 |
|---|--:|--:|--:|--:|
| BA,2 | 0,48% | 0,00% | 9,00% | 0,00% |
| BA,2,75 | 18,06% | 15,26% | 14,27% | 13,79% |
| BN,1 | 4,35% | 4,54% | 2,76% | 6,63% |
| XBB,1,5 | 9,73% | 13,01% | 16,77% | 20,58% |
| BA,4 | 0,00% | 0,00% | 0,00% | 0,00% |
| BA,5 | 43,92% | 42,03% | 34,41% | 38,67% |
| BF,7 | 2,59% | 0,46% | 1,41% | 0,26% |
| BQ,1 | 19,97% | 17,71% | 17,17% | 15,69% |
| Andere | 0,90% | 6,99% | 4,21% | 4,38% |

![VoC München](/images/2023/02/KW07VarMUC.png)

## Varianten Bayern

In Bayern: Der BA.5 Anteil sinkt weiter auf unter 10%, XBB.1.5 hat BQ.1 überholt. BF.7 fällt auf unter 2%.

Die Zeiten dominanter Varianten sind wohl vorbei. Die Grafik geht zwar bis KW 07, aber da liegen nur Ergebnisse für eine mickrige Sequenzierung zugrunde. Daher geht unsere Tabelle nur bis KW 06.

| Variante | KW 03 | KW 04 | KW 05 | KW06 |
|:--|--:|--:|--:|--:|
| BA.2 | 3.0% | 3.8% | 4.4% | 7.6% |
| BA.2.75 | 16.4% | 14.9% | 16,3% | 16.7% |
| BN.1 | 6.1% | 9.4% | 9.4% | 6.4% |
| XBB.1.5 | 10.2% | 11.1% | 19.8% | 28.2% |
| BA.4 | 0.0% | 0.2% | 0.0% | 0.0% |
| BA.5 | 15.5% | 11.5% | 8.7% | 7.1% |
| BF.7 | 9.3% | 9.0% | 4.7% | 1.7% |
| BQ.1 | 36.1% | 35.8% | 32.0% | 26.0% |
| Andere | 3.4% | 4.2% | 4.8% | 6.4% |
| Sequenzierungen | 510 | 477 | 748 | 408 |

![VoC Bayern](/images/2023/02/KW07Var.png)
