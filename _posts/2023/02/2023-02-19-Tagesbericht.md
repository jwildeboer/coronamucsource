---
layout: default
title: 2023-02-19 Tagesbericht
category: Tagesberichte
---

![2023-02-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe höher wie in der Vorwoche, aber die Wachstumsrate sinkt. Der R(t) Wert ist seit 22 Tagen über 1, sinkt aber seit 4 Tagen. Zweite zeigt Krankenhausbetten, deren Zahl weiter anzieht, bis Montag aber nicht aktualisiert wird. Dritte zeigt Inzidenz, die seit 10 Tagen wieder über 100 liegt.](/images/2023/02/20230219.png)

Das RKI meldet eine Inzidenz von 107,7. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

**Stand 2023-02-18, Aktualisierung erst wieder am Dienstag**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 10.02. | 101,1 &rarr; 106,5 |
| 11.02. | 101,1 &rarr; 106,5 |
| 12.02. | 101,1 &rarr; 106,5 |
| 13.02. | 109,1 &rarr; 113,7 |
| 14.02. | 112,6 &rarr; 117,2 |
| 15.02. | 111,6 &rarr; 114,3 |
| 16.02. | 111,1 &rarr; 113,5 |

Da bis Montag keine Meldungen an das RKI gehen und somit erst am Dienstag eine neue Inzidenz errechnet wird, können wir die Inzidenz für Montag schon heute vorhersagen:

| Montag | 107,7 | 

Somit 11 Tage mit Inzidenz über 100 schon sicher.

## COVID-positive Todesfälle

**Stand 2023-02-18, Aktualisierung erst wieder am Dienstag**

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 13. Damit insgesamt bis jetzt 91 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt momentan wohl bei ca. 7-8.

## Umland

**Stand 2023-02-18, Aktualisierung erst wieder am Dienstag**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 18.02. | 17.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 135,7 | 148,6 | -8,7% |
| LK Ebersberg | 63,6 | 69,9 | -9,0% |
| LK Freising | 82,8 | 91,6 | -9,6% |
| LK Fürstenfeldbruck | 120,3 | 116,2 | +3,5% |
| LK München | 103,8 | 105,8 | -1,9% |
| LK Starnberg | 79,7 | 85,6 | -6,9% |

Bis auf Fürstenfeldnruck überall sinkende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 1,04. 23 Tage über 1 und damit weiter exponenziell. Bis am Dienstag neue Meldungen vom RKI kommen wird R(t) weiter scheinbar sinken.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 12.02. | 1,09 |
| 13.02. | 1,10 |
| 14.02. | 1,12 |
| 15.02. | 1,12 |
| 16.02. | 1,11 |
| 17.02. | 1,08 |
| 18.02. | 1,04 |

## Krankenhaus

**Stand 2023-02-17, Aktualisierung erst wieder am Montag**

Die Zahl der belegten Krankenhausbetten steigt auch heute weiter an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 10 von 319 auf 329. IMC sinkt um 1 von gestern 11 auf heute 10. Intensiv steigt um 4 von gestern 22 auf heute 26. Damit insgesamt 365 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 329 | +10 | 
| IMC | 10 | -1 | 
| Intensiv | 26 | +4 |
| SUMME | 365 | +13 |

Damit sind 63 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.