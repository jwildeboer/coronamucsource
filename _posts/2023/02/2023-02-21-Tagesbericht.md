---
layout: default
title: 2023-02-21 Tagesbericht
category: Tagesberichte
---

![2023-02-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe höher wie in der Vorwoche, aber die Wachstumsrate sinkt. Der R(t) Wert ist nach 22 Tagen exponenziell heute wieder sehr knapp unter 1. Zweite zeigt Krankenhausbetten, deren Zahl heute kräftig anzieht. Dritte zeigt Inzidenz, die nach 11 über 100 jetz wieder drunter liegt.](/images/2023/02/20230221.png)

Das RKI meldet eine Inzidenz von 97,9. 306 Neue, 21 Nachmeldungen, davon 16 für letzten Freitag und keine Nachmeldungen für Samstag und Sonntag. Die Inzidenz für Sonntag geht von 107,7 auf 108,9, Samstag und Freitag von 107,7 auf 109,0. R(t) ist unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 13.02. | 109,1 &rarr; 113,7 |
| 14.02. | 112,6 &rarr; 117,2 |
| 15.02. | 111,6 &rarr; 114,3 |
| 16.02. | 111,1 &rarr; 113,7 |
| 17.02. | 107,7 &rarr; 109,0 |
| 18.02. | 107,7 &rarr; 109,0 |
| 19.02. | 107,7 &rarr; 108,9 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt bis jetzt 91 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt momentan wohl bei ca. 7.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 21.02 | 18.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 134,4 | 135,7 | -1,0% |
| LK Ebersberg | 75,4 | 63,6 | +18,6% |
| LK Freising | 76,2 | 82,8 | -8,0% |
| LK Fürstenfeldbruck | 114,8 | 120,3 | -4,6% |
| LK München | 105,2 | 103,8 | +1,3% |
| LK Starnberg | 71,7 | 79,7 | -10,0% |

Wieder uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,99. Nach 24 Tagen  über 1 jetzt also unter der exponenziellen Grenze. 

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 15.02. | 1,12 |
| 16.02. | 1,11 |
| 17.02. | 1,08 |
| 18.02. | 1,04 |
| 19.02. | 1,04 |
| 20.02. | 1,03 |
| 21.02. | 0,99 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute kräftig an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 32 von 336 auf 368. IMC sinkt um 3 von gestern 14 auf heute 11. Intensiv steigt um 3 von gestern 18 auf heute 21. Damit insgesamt 400 Betten belegt. So hohe Zahlen hatten wir zuletzt am 1.1.2023  mit 403 belegten Betten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 368 | +32 | 
| IMC | 11 | -3 | 
| Intensiv | 21 | +3 |
| SUMME | 400 | +32 |

Damit sind 71 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.