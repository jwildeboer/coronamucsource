---
layout: default
title: 2023-02-01 Tagesbericht
category: Tagesberichte
---

![2023-02-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230201.png)

Das RKI meldet eine Inzidenz von 78,1 für München. 258 neue und 38 Nachmeldungen, davon 36 für vorgestern.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 24.01. | 58,0 &rarr; 60,0 |
| 25.01. | 59,2 &rarr; 60,0 |
| 26.01. | 62,6 &rarr; 64,6 |
| 27.01. | 62,8 &rarr; 65,5 |
| 28.01. | 62,8 &rarr; 65,5 |
| 29.01. | 62,8 &rarr; 65,5 |
| 30.01. | 71,7 &rarr; 74,1 |

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt jetzt 64 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 01.02. | 31.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 139,0 | 70,1 | +98,3% |
| LK Ebersberg | 81,6 | 59,5 | +37,1% |
| LK Freising | 66,8 | 69,6 | -4,0% |
| LK Fürstenfeldbruck | 87,4 | 90,6 | -3,5% |
| LK München | 90,3 | 74,6 | +21,0% |
| LK Starnberg | 68,0 | 60,0 | +13,3% |

Dachau fast verdoppelt.

## Reproduktionswert R(t) für München

Unser R(t) ist heute weiter auf 1,12 gestiegen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.01. | 0,86 | --- |
| 26.01. | 0,95 | --- |
| 27.01. | 1,00 | --- |
| 28.01. | 1,04 | --- |
| 29.01. | 1,06 | --- |
| 30.01. | 1,08 | --- |
| 31.01. | 1,12 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute minimal: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 245 auf 243. IMC steigt um 3 von gestern 10 auf heute 13. Intensiv sinkt um 5 von gestern 29 auf heute 24. Damit insgesamt 280 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 243 | -2 | 
| IMC | 13 | +3 | 
| Intensiv | 24 | -5 |
| SUMME | 280 | -4 |

Damit sind 11 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.