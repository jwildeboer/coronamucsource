---
layout: default
title: 2023-02-24 Tagesbericht
category: Tagesberichte
---

![2023-02-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe ähnlich wie in der Vorwoche, aber Meldeverzögerungen wegen Faschingsdienstag. Der R(t) Wert ist seit 3 Tagen wieder sehr knapp unter 1. Zweite zeigt Krankenhausbetten, die gesunken sind. Dritte zeigt Inzidenz, die seit 15 Tagen über 100 liegt.](/images/2023/02/20230224.png)

Das RKI meldet eine Inzidenz von 106,9. 257 Neue, 50 Nachmeldungen, davon 47 für vorgestern. Die gestern genannte Inzidenz von 105,5 springt daher auf 108,4. 15 Tage mit Inzidenz über 100, Tag 3 mit R(t) unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 16.02. | 111,1 &rarr; 114,3 |
| 17.02. | 107,7 &rarr; 109,6 |
| 18.02. | 107,7 &rarr; 109,6 |
| 19.02. | 107,7 &rarr; 109,5 |
| 20.02. | 97,7 &rarr; 110,8 |
| 21.02. | 88,7 &rarr; 102,8 |
| 22.02. | 105,5 &rarr; 108,4 |

## COVID-positive Todesfälle

Es wurden 7 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 13. Damit insgesamt bis jetzt 101 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 8 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 24.02. | 23.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 107,4 | 100,4 | +7,0% |
| LK Ebersberg | 80,9 | 83,0 | -2,5% |
| LK Freising | 93,8 | 86,1 | +8,9% |
| LK Fürstenfeldbruck | 111,2 | 117,6 | -5,4% |
| LK München | 97,8 | 100,6 | -2,8% |
| LK Starnberg | 62,9 | 62,2 | +1,1% |

Nur geringe Schwankungen, keine klare Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,96. Nach 25 Tagen über 1 jetzt also wieder seit 3 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 17.02. | 1,08 |
| 18.02. | 1,05 |
| 19.02. | 1,03 |
| 20.02. | 1,01 |
| 21.02. | 0,98 |
| 22.02. | 0,97 |
| 23.02. | 0,96 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten ändert sich gegenüber gestern kaum: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 15 von 364 auf 349. IMC bleibt mit 5 gleich. Intensiv steigt um 3 von gestern 25 auf heute 28. Damit insgesamt 382 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 349 | -15 | 
| IMC | 5 | +/-0 | 
| Intensiv | 28 | +3 |
| SUMME | 382 | -12 |

Damit sind 17 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.