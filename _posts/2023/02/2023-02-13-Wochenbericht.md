---
layout: default
title: 2023-02-13 Wochenbericht KW06
category: Wochenberichte
---

![2023-02-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 17 Tagen über 1. Zweite zeigt Krankenhausbetten, deren Zahl sich stabilisiert um 300. Dritte zeigt Inzidenz, die seit 4 Tagen wieder über 100 liegt.](/images/2023/02/20230213.png)

In der letzten Woche gab es insgesamt 1.504 neue COVID Fälle in München. Das sind 10,1% **mehr** als in der Vorwoche (1.366). Leider auch 9 Tote mit COVID Nachweis in den letzten 7 Tagen, wie in der Vorwoche (9).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe von 1.366 auf 1.504. Die um Nachmeldungen korrigierte Inzidenz stieg von 91,8 auf 101,1 und stand an 3 von 7 Tagen über 100.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 05.02. | 0 | 1.366 | 91,8 |
| 06.02. | 364 | 1.352 | 90,9 |
| 07.02. | 298 | 1.336 | 89,8 |
| 08.02. | 346 | 1.456 | 97,9 |
| 09.02. | 283 | 1.506 | 101,2 |
| 10.02. | 213 | 1.504 | 101,1 |
| 11.02. | 0 | 1.504 | 101,1 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 1,18 auf jetzt 1,06 und blieb damit über 1.

| Datum | R(t) 7 Tage |
|---|---:|
| 06.02. | 1,18 |
| 07.02. | 1,14 |
| 08.02. | 1,11 |
| 09.02. | 1,08 |
| 10.02. | 1,07 |
| 11.02. | 1,07 | 
| 12.02. | 1,06 |

## Abwasserwerte für München

Es gibt jetzt auch endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München. Die Daten laufen ein wenig hinterher und gehen nur bis zum 08. Februar, aber immerhin. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf.

In den Abwasserwerten erkennt man aber auch einen steigenden Trend, den wir in unserer R(t) Berechnung auch sehen.

![Abwasserwerte München](/images/2023/02/KW06Abwasser.png)

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München steigt die Zahl der belegten Normalbetten ein wenig von 247 auf jetzt 266. IMC pendelt klar über 10, Intensiv unter 30. An 6 von 7 Tagen war die Summe **höher** als in der Vorwoche.

Im Vergleich zum Vorjahr niedrigere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 06.02. | 247 | 14 | 21 | 282 | -2 | 487 |
| 07.02. | 265 | 16 | 21 | 302 | +22 | 492 |
| 08.02. | 270 | 13 | 24 | 307 | +20 | 509 |
| 09.02. | 265 | 11 | 26 | 302 | +33 | 516 |
| 10.02. | 265 | 11 | 25 | 301 | +33 | 512 |
| 11.02. | 266 | 12 | 24 | 301 | +36 | 511 |
| 12.02. | 266 | 12 | 24 | 302 | +37 | 510 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, etwas ruhiger aus. Langsam sinkende Zahlen, die unter den Vorjahreswerten bleiben. Sieht nach einer saisonalen Entwicklung aus.

Bei den Intensivbetten steigen die Zahlen jedochlangsam aber stetig von 140 auf jetzt 164.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 06.02. | 141 | 1.565 | 1.872 |
| 07.02. | 130 | 1.604 | 1.909 |
| 08.02. | 142 | 1.590 | 1.970 |
| 09.02. | 143 | 1.603 | 1.953 |
| 10.02. | 145 | 1.484 | 1.926 |
| 11.02. | 154 | 1.448 | 1.907 |
| 12.02. | 162 | 1.408 | 1.884 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche weiter weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 198 | 75,21% (+/-0%) |
| 2. | 91 | 75,13% (+/-0%) |
| 3. | 385 | 59,24% (+/-0%) |
| 4. | 3.516 | 11,93% (+0,03%) |
| 5. | 1.137 | 0,81% (+/-0%) |
| 6. | 39 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/02/KW06Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 10.891 Tests/Tag mit einer Positivrate von 14,63%. Gegenüber den Zahlen von letzter Woche (10.966 Tests/Tag mit 11,87% Positivrate) also ähnlich viele Tests bei steigender Positivrate.

10.891 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 10.891 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 14,63%.](/images/2023/02/20230209Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten nicht gerade brandaktuell sind.

Die Variante BA.5 setzt sich Anfang Januar in München wieder durch, BA.2.75 und BQ.1 sind abgeschlagen. XBB.1.5 bleibt unter 8%. Allerdings gehen dei Zahlen nur bis zum 9. Januar 2023, somit KW 01.

| Variante | KW 50 | KW 51 | KW 52 | KW 01 |
|---|--:|--:|--:|--:|
| BA.2 | 2.04% | 5.48% | 3.25% | 0.95% |
| BA.2.75 | 13.73% | 15.15% | 15.32% | 17.28% |
| BN.1 | 3.08% | 0.18% | 8.98% | 3.39% |
| XBB.1.5 | 3.65% | 0% | 5.16% | 7.94% |
| BA.4 | 0% | 0% | 0% | 0% |
| BA.5 | 24.42% | 26.08% | 18.25% | 46.72% |
| BF.7 | 10.06% | 5.32% | 8.68% | 5.19% |
| BQ.1 | 40.88% | 36.02% | 37.65% | 17.92% |
| Andere | 2.14% | 11.77% | 2.71% | 0.61% |

![VoC München](/images/2023/02/KW06VarMUC.png)

In Bayern: Der BA.5 Anteil sinkt weiter auf unter 15%, BQ.1 scheint mit 33.7% an der Spitze, verliert aber auch. BF.7 fällt auf unter 5%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben. Die Variante XBB.1.5 scheint langsam Fuss zu fassen aber bleibt unter 20%.

Die Zeiten dominanter Varianten sind wohl vorbei. Die Grafik geht zwar bis KW 06, aber da liegen nur Ergebnisse für mickrige 3 Sequenzierungen zugrunde. Daher geht unsere Tabelle nur bis KW 05.

| Variante | KW 02 | KW 03 | KW 04 | KW 05 |
|:--|--:|--:|--:|--:|
| BA.2 | 2.5% | 3.0% | 3.8% | 4.0% |
| BA.2.75 | 13.7% | 16.4% | 14.2% | 15.5% |
| BN.1 | 5.7% | 6.1% | 10.4% | 10.9% |
| XBB.1.5 | 7.1% | 10.2% | 9.4% | 16.1% |
| BA.4 | 0.2% | 0.0% | 0.3% | 0.0% |
| BA.5 | 15.1% | 15.5% | 12.2% | 10.3% |
| BF.7 | 11.8% | 9.3% | 8.4% | 4.2% |
| BQ.1 | 42.2% | 36.1% | 37.0% | 33.7% |
| Andere | 1.8% | 3.4% | 4.6% | 5.4% |
| Sequenzierungen | 510 | 440 | 395 | 478 |

![VoC Bayern](/images/2023/02/KW06Var.png)

