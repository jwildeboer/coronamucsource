---
layout: default
title: 2023-02-26 Tagesbericht
category: Tagesberichte
---

![2023-02-26 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe ähnlich wie in der Vorwoche. Der R(t) Wert ist seit 4 Tagen wieder knapp unter 1. Zweite zeigt Krankenhausbetten, die zwar leicht gesunken sind, dennoch höher als in der Vorwoche. Bis Montag keine Aktualisierung. Dritte zeigt Inzidenz, die seit 17 Tagen über 100 liegt.](/images/2023/02/20230226.png)

Das RKI meldet eine Inzidenz von 107,3. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

**Stand 2023-02-25, erst am Dienstag wieder neue Zahlen**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 17.02. | 107,7 &rarr; 109,6 |
| 18.02. | 107,7 &rarr; 109,6 |
| 19.02. | 107,7 &rarr; 109,5 |
| 20.02. | 97,7 &rarr; 110,8 |
| 21.02. | 88,7 &rarr; 102,8 |
| 22.02. | 105,5 &rarr; 108,4 |
| 23.02. | 106,9 &rarr; 107,5 |

Da am Wochenende keine Zahlen vom RKI gemeldet werden, können wir die Inzidenz für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 107,3 |
| Montag | 107,3 |

Damit bleiben wir bis Dienstag klar über Inzidenz 100. Und danach eventuell eine neue Spitze wegen Urlaubsrückkehrer.

## COVID-positive Todesfälle

**Stand 2023-02-25, erst am Dienstag wieder neue Zahlen**

Es wurden 8 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 18. Damit insgesamt bis jetzt 109 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 7 |


## Umland

**Stand 2023-02-25, erst am Dienstag wieder neue Zahlen**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 25.02 | 24.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 108,1 | 107,4 | +0,6% |
| LK Ebersberg | 78,9 | 80,9 | -2,5% |
| LK Freising | 93,3 | 93,8 | -0,5% |
| LK Fürstenfeldbruck | 108,0 | 111,2 | -2,9% |
| LK München | 94,3 | 97,8 | -3,6% |
| LK Starnberg | 63,6 | 62,9 | +1,1% |

Nur geringe Schwankungen, keine klare Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,96. Nach 25 Tagen über 1 jetzt also wieder seit 3 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 19.02. | 1,03 |
| 20.02. | 1,01 |
| 21.02. | 0,98 |
| 22.02. | 0,97 |
| 23.02. | 0,96 |
| 24.02. | 0,96 |
| 25.02. | 0,98 |

## Krankenhaus

**Stand 2023-02-24, erst am Montag wieder neue Zahlen**

Die Zahl der belegten Krankenhausbetten ändert sich kaum: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 15 von 364 auf 349. IMC bleibt mit 5 gleich. Intensiv steigt um 3 von gestern 25 auf heute 28. Damit insgesamt 382 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 349 | -15 | 
| IMC | 5 | +/-0 | 
| Intensiv | 28 | +3 |
| SUMME | 382 | -12 |

Damit sind 17 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.