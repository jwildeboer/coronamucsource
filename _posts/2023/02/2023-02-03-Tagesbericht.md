---
layout: default
title: 2023-02-03 Tagesbericht
category: Tagesberichte
---

![2023-02-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230203.png)

Das RKI meldet eine Inzidenz von 85,7 für München. 210 neue und 5 Nachmeldungen für die letzten 7 Tage.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 26.01. | 62,6 &rarr; 64,7 |
| 27.01. | 62,8 &rarr; 65,7 |
| 28.01. | 62,8 &rarr; 65,7 |
| 29.01. | 62,8 &rarr; 65,7 |
| 30.01. | 71,7 &rarr; 74,5 |
| 31.01. | 78,1 &rarr; 81,9 |
| 01.02. | 84,0 &rarr; 84,3 |

Es wurden kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 9. Damit insgesamt jetzt 68 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 03.02. | 02.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 178,2 | 160,8 | +10,8% |
| LK Ebersberg | 96,8 | 92,0 | +5,2% |
| LK Freising | 74,5 | 70,1 | +6,3% |
| LK Fürstenfeldbruck | 110,3 | 100,7 | +9,5% |
| LK München | 99,8 | 96,0 | +4,0% |
| LK Starnberg | 73,9 | 68,0 | +8,7% |

Weiter steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) ist heute weiter auf 1,20 gestiegen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 27.01. | 1,00 | --- |
| 28.01. | 1,04 | --- |
| 29.01. | 1,06 | --- |
| 30.01. | 1,08 | --- |
| 31.01. | 1,12 | --- |
| 01.02. | 1,16 | --- |
| 02.02. | 1,20 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute wieder, wie so häufig am Freitag: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 11 von 248 auf 237. IMC sinkt um 2 von gestern 14 auf heute 12. Intensiv sinkt um 5 von gestern 25 auf heute 20. Damit insgesamt 269 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 237 | -11 | 
| IMC | 12 | -2 | 
| Intensiv | 20 | -5 |
| SUMME | 269 | -18 |

Damit sind 20 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.