---
layout: default
title: 2023-02-28 Tagesbericht
category: Tagesberichte
---

![2023-02-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich niedriger wie in der Vorwoche. Der R(t) Wert ist seit 7 Tagen wieder knapp unter 1. Zweite zeigt Krankenhausbetten, die leicht gesunken sind. Dritte zeigt Inzidenz, die nach 18 Tagen über 100 heute zum ersten mal wieder drunter liegt.](/images/2023/02/20230228.png)

Das RKI meldet eine Inzidenz von 91,8. 243 Neue, 17 Nachmeldungen, davon 13 für letzten Freitag. Die für Freitag, Samstag und Sonntag genannte Inzidenz von 107,3 erhöht sich durch Nachmeldungen auf 108,4. 
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 20.02. | 97,7 &rarr; 110,8 |
| 21.02. | 88,7 &rarr; 102,8 |
| 22.02. | 105,5 &rarr; 108,4 |
| 23.02. | 106,9 &rarr; 107,5 |
| 24.02. | 107,3 &rarr; 108,4 |
| 25.02. | 107,3 &rarr; 108,4 |
| 26.02. | 107,3 &rarr; 108,4 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 18. Damit insgesamt bis jetzt 109 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 7 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 28.02. | 25.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 97,1 | 108,1 | -10,2% |
| LK Ebersberg | 65,0 | 78,9 | -17,6% |
| LK Freising | 93,3 | 93,3 | +/-0% |
| LK Fürstenfeldbruck | 100,7 | 108,0 | -6,8% |
| LK München | 81,2 | 94,3 | -13,9% |
| LK Starnberg | 64,4 | 63,6 | +1,3% |

Noch fallende Zahlen, kann sich im Laufe der Woche durch Urlaubsrückkehrer ändern.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,96. Nach 25 Tagen über 1 jetzt also wieder seit 3 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 21.02. | 0,98 |
| 22.02. | 0,97 |
| 23.02. | 0,96 |
| 24.02. | 0,96 |
| 25.02. | 0,98 |
| 26.02. | 0,99 |
| 27.02. | 0,97 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 10 von 361 auf 351. IMC sinkt leicht von gestern 8 auf jetzt 7.Intensiv sinkt um 3 von gestern 30 auf heute 27. Damit insgesamt 385 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 351 | -10 | 
| IMC | 7 | -1 | 
| Intensiv | 27 | -3 |
| SUMME | 385 | -14 |

Damit sind 15 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.