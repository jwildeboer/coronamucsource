---
layout: default
title: 2023-02-06 Wochenbericht KW05
category: Wochenberichte
---

![2023-01-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die langsamer sinkt und erst morgen neue Zahlen enthält.](/images/2023/02/20230207.png)

In der letzten Woche gab es insgesamt 1.366 neue COVID Fälle in München. Das sind satte 39,7% **mehr** als in der Vorwoche (978). Leider auch 9 Tote mit COVID Nachweis in den letzten 7 Tagen, mehr als in der Vorwoche (5).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe von 978 auf 1.366. Die um Nachmeldungen korrigierte Inzidenz stieg von 65,7 auf 91,8.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 29.01. | 0 | 978 | 65,7 |
| 30.01. | 378 | 1.110 | 74,6 |
| 31.01. | 314 | 1.224 | 82,3 |
| 01.02. | 226 | 1.263 | 84,9 |
| 02.02. | 233 | 1.307 | 87,9 |
| 03.02. | 215 | 1.366 | 91.8 |
| 04.02. | 0 | 1.366 | 91,8 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche von 1,08 auf jetzt 1,20 und ist damit wieder im exponenziellem Bereich.

| Datum | R(t) 7 Tage |
|---|---:|
| 30.01. | 1,08 |
| 31.01. | 1,12 |
| 01.02. | 1,16 |
| 02.02. | 1,20 |
| 03.02. | 1,21 |
| 04.02. | 1,21 | 
| 05.02. | 1,20 |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München sinkt die Zahl der belegten Normalbetten relativ ein wenig. IMC pendelt klar über 10, Intensiv um 30. An 7 von 7 Tagen war die Summe niedriger als in der Vorwoche.

Im Vergleich zum Vorjahr klar niedrigere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 30.01. | 245 | 10 | 29 | 284 | -15 | 468 |
| 31.01. | 243 | 13 | 24 | 280 | -11 | 460 |
| 01.02. | 248 | 14 | 25 | 287 | -6 | 482 |
| 02.02. | 237 | 12 | 20 | 269 | -20 | 492 |
| 03.02. | 235 | 12 | 21 | 268 | -32 | 487 |
| 04.02. | 232 | 11 | 23 | 266 | -44 | 488 |
| 05.02. | 230 | 11 | 24 | 265 | -27 | 487 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, ähnlich wie im Vorjahr aus. Klare Steigung der Zahlen. Aber unter den Vorjahreszahlen. Sieht lkangsam nach einer saisonalen Entwicklung aus.

Bei den Intensivbetten bleibt es eher stabil um 130.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 30.01. | 1.330 | 1.668 |
| 31.01. | 1.365 | 1.694 |
| 01.02. | 1.461 | 1.734 |
| 02.02. | 1.453 | 1.842 |
| 03.02. | 1.465 | 1.848 |
| 04.02. | 1.463 | 1.850 |
| 05.02. | 1.460 | 1.872 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche weiter weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 198 | 75,21% (+/-0%) |
| 2. | 91 | 75,13% (+/-0%) |
| 3. | 385 | 59,24% (+/-0%) |
| 4. | 3.516 | 11,93% (+0,03%) |
| 5. | 1.137 | 0,81% (+/-0%) |
| 6. | 39 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/01/KW04Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 10.966 Tests/Tag mit einer Positivrate von 11,87%. Gegenüber den Zahlen von letzter Woche (11.262 Tests/Tag mit 9,91% Positivrate) also etwas weniger Tests bei steigender Positivrate.

10.966 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 11.929 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 10,63%.](/images/2023/01/20230126Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter auf unter 15%, BQ.1 scheint mit 37.3% an der Spitze, verliert aber auch. BF.7 fällt auf unter 10%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

Es gibt nun auch Zahlen zur Variante XBB.1.5, die aber noch unter 10% Anteil hat.

Die Zeiten dominanter Varianten ist wohl vorbei.

| Variante | KW 01 | KW 02 | KW 03 | KW 04 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 0.0% | 2.5% | 3.0% | 3.8% |
| Omikron BA.2.75 | 10.4% | 13.7% | 16.4% | 14.1% |
| Omikron BN.1 | 5.7% | 5.7% | 6.1% | 10.5% |
| Omikron XBB.1.5 | 7.5% | 7.1% | 10.2% | 9.5% |
| Omikron BA.4 | 0.4% | 0.2% | 0.0% | 0.3% |
| Omikron BA.5 | 17.6% | 15.1% | 15.5% | 12.0% |
| Omikron BF.7 | 14.7% | 11.8% | 9.3% | 7.9% |
| Omikron BQ.1 | 43.4% | 42.2% | 36.1% | 37.3% |
| Andere | 0.4% | 1.8% | 3.4% | 4.6% |
| Sequenzierungen | 279 | 510 | 440 | 391 |

![VoC](/images/2023/02/KW04Var.png)