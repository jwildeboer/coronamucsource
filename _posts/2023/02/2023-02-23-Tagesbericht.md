---
layout: default
title: 2023-02-23 Tagesbericht
category: Tagesberichte
---

![2023-02-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe ähnlich wie in der Vorwoche, aber Meldeverzögerungen wegen Faschingsdienstag. Der R(t) Wert ist seit 2 Tagen wieder sehr knapp unter 1. Zweite zeigt Krankenhausbetten, in der Summe unevrändert gegenüber gestern. Dritte zeigt Inzidenz, die seit 14 Tagen über 100 liegt.](/images/2023/02/20230223.png)

Das RKI meldet eine Inzidenz von 105,5. 346 Neue, 218 Nachmeldungen, davon 205(!) für vorgestern. Die gestern genannte Inzidenz von 88,7 springt daher auf 103,0. 14 Tage mit Inzidenz über 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 15.02. | 111,6 &rarr; 114,7 |
| 16.02. | 111,1 &rarr; 114,3 |
| 17.02. | 107,7 &rarr; 109,6 |
| 18.02. | 107,7 &rarr; 109,6 |
| 19.02. | 107,7 &rarr; 109,6 |
| 20.02. | 97,7 &rarr; 111,0 |
| 21.02. | 88,7 &rarr; 103,0 |

## COVID-positive Todesfälle

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 6. Damit insgesamt bis jetzt 94 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz vor einem Jahr können wir eine zweite Dunkelziffer abschätzen. Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen.

| Berechnung (Art) | Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 8 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 23.02. | 22.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 100,4 | 90,1 | +11,4% |
| LK Ebersberg | 83,0 | 72,6 | +14,3% |
| LK Freising | 86,1 | 58,5 | +47,2% |
| LK Fürstenfeldbruck | 117,6 | 99,3 | +18,4% |
| LK München | 100,6 | 102,3 | -1,7% |
| LK Starnberg | 62,2 | 65,1 | -4,5% |

Nachmeldungen wegen Faschingsdienstag, bis auf LK München und Fürstenfeldbruck, wo die Zahlen auch am Mittwoch vernünftig gemeldet wurden.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,97. Nach 25 Tagen  über 1 jetzt also wieder unter der exponenziellen Grenze. Aber durch Faschingsdienstag verzerrt.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 16.02. | 1,11 |
| 17.02. | 1,08 |
| 18.02. | 1,05 |
| 19.02. | 1,03 |
| 20.02. | 1,01 |
| 21.02. | 0,99 |
| 22.02. | 0,97 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten ändert sich gegenüber gestern kaum: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 366 auf 364. IMC sinkt um 3 von gestern 8 auf heute 5. Intensiv sinkt um 1 von gestern 26 auf heute 25. Damit insgesamt 394 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 364 | -2 | 
| IMC | 5 | -3 | 
| Intensiv | 25 | -1 |
| SUMME | 394 | -6 |

Damit sind 42 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 9.863 Tests/Tag mit einer Positivrate von 20,22%. Gegenüber den Zahlen von letzter Woche (10.628 Tests/Tag mit 17,13% Positivrate) also wieder weniger Tests bei weiter steigender Positivrate.

9.863 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom möglichen Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 9.863 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 20,22%.](/images/2023/02/20230223Test.png)

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.