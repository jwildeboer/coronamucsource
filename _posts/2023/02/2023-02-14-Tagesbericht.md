---
layout: default
title: 2023-02-14 Tagesbericht
category: Tagesberichte
---

![2023-02-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 17 Tagen über 1. Zweite zeigt Krankenhausbetten, deren Zahl deutlich anzieht. Dritte zeigt Inzidenz, die seit 5 Tagen wieder über 100 liegt.](/images/2023/02/20230214.png)

Das RKI meldet eine Inzidenz von 109,1. 405 Neue, 77 Nachmeldungen, davon 1 für Sonntag und 72 für Freitag. Die Inzidenzen von Freitag, Samstag und Sonntag erhöhen sich durch diese Nachmeldungen von 101,6 auf 106,3. Tag 5 Mit Inzidenz über 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 06.02. | 89,9 &rarr; 90,9 |
| 07.02. | 85,5 &rarr; 89,8 |
| 08.02. | 96,1 &rarr; 97,9 |
| 09.02. | 98,2 &rarr; 101,1 |
| 10.02. | 101,1 &rarr; 106,3 |
| 11.02. | 101,1 &rarr; 106,3 |
| 12.02. | 101,1 &rarr; 106,3 |

## COVID-positive Todesfälle

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 14. Damit insgesamt jetzt 83 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt wohl bei ca. 7-8.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 14.02 | 11.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 158,9 | 167,9 | -5,4% |
| LK Ebersberg | 70,6 | 75,4 | -6,4% |
| LK Freising | 97,2 | 93,8 | +3,6% |
| LK Fürstenfeldbruck | 107,1 | 108,4 | -1,2% |
| LK München | 113,5 | 119,2 | -4,8% |
| LK Starnberg | 101,6 | 98,7 | +2,9% |

Uneinheitliches Bild, leicht fallende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steigt, wie erwartet, wieder auf 1,09. 18 Tage über 1 und damit weiter exponenziell.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |
| 07.02. | 1,14 | --- |
| 08.02. | 1,11 | --- |
| 09.02. | 1,08 | --- |
| 10.02. | 1,07 | --- |
| 11.02. | 1,07 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute wieder deutlich an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 27 von 266 auf 293. IMC steigt um 1 von gestern 12 auf heute 13. Intensiv sinkt um 1 von gestern 24 auf heute 23. Damit insgesamt 329 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 293 | +27 | 
| IMC | 13 | +1 | 
| Intensiv | 23 | -1 |
| SUMME | 329 | +27 |

Damit sind 47 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.