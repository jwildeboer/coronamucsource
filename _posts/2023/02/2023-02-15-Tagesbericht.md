---
layout: default
title: 2023-02-15 Tagesbericht
category: Tagesberichte
---

![2023-02-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 19 Tagen über 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter anzieht. Dritte zeigt Inzidenz, die seit 6 Tagen wieder über 100 liegt.](/images/2023/02/20230215.png)

Das RKI meldet eine Inzidenz von 112,6. 293 Neue, 61 Nachmeldungen, davon 57 für vorgestern. Die gestern genannte Inzidenz von 109,1 erhöht sich durch die Nachmeldungen auf 113,1. Tag 6 mit Inzidenz über 100. Auch der R(t) Wert steigt wieder.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 07.02. | 85,5 &rarr; 89,9 |
| 08.02. | 96,1 &rarr; 98,1 |
| 09.02. | 98,2 &rarr; 101,7 |
| 10.02. | 101,1 &rarr; 106,4 |
| 11.02. | 101,1 &rarr; 106,4 |
| 12.02. | 101,1 &rarr; 106,5 |
| 13.02. | 109,1 &rarr; 113,1 |

## COVID-positive Todesfälle

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 18. Damit insgesamt bis jetzt 87 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt wohl bei ca. 7-8.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 15.02. | 14.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 156,3 | 158,9 | -1,6% |
| LK Ebersberg | 70,6 | 70,6 | +/-0% |
| LK Freising | 87,2 | 97,2 | -10,3% |
| LK Fürstenfeldbruck | 104,8 | 107,1 | -2,1% |
| LK München | 114,1 | 113,5 | +0,5% |
| LK Starnberg | 94,3 | 101,6 | -7,2% |

Weiter leicht fallende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steigt, wie erwartet, weiter auf 1,11. 19 Tage über 1 und damit weiter exponenziell.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 08.02. | 1,11 |
| 09.02. | 1,08 |
| 10.02. | 1,08 |
| 11.02. | 1,09 |
| 12.02. | 1,09 |
| 13.02. | 1,10 |
| 14.02. | 1,11 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt auch heute wieder an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 15 von 293 auf 308. IMC steigt um 2 von gestern 13 auf heute 15. Intensiv sinkt um 1 von gestern 23 auf heute 22. Damit insgesamt 345 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 308 | +15 | 
| IMC | 15 | +2 | 
| Intensiv | 22 | -1 |
| SUMME | 345 | +16 |

Damit sind 43 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.