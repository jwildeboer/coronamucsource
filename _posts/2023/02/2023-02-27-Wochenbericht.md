---
layout: default
title: 2023-02-27 Wochenbericht KW08
category: Wochenberichte
---

![2023-02-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ähnlich wie in der Vorwoche, eher Plateau. Keine Tendenz erkennbar. Zweite zeigt Krankenhausbetten, deren Zahl sich knapp unter 400 einpendelt. Dritte zeigt Inzidenz, die seit 18 Tagen wieder über 100 liegt.](/images/2023/02/20230227.png)

In der letzten Woche (Faschingsferien) gab es insgesamt 1.597 neue COVID Fälle in München. Das sind 2,0% **weniger** als in der Vorwoche (1.629). Leider auch 18 Tote mit COVID Nachweis in den letzten 7 Tagen, mehr als in der Vorwoche (13).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe minimal von 1.629 auf 1.597. Die um Nachmeldungen korrigierte Inzidenz sank am Ende ein klein wenig von 109,5 auf 107,3, blieb aber immer über 100.

Die Faschingsferien sind vorbei, daher in dieser Woche eventuell wieder etwas höhere Zahlen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 19.02. | 0 | 1.628 | 109,4 |
| 20.02. | 491 | 1.647 | 110,7 |
| 21.02. | 237 | 1.530 | 102,8 |
| 22.02. | 394 | 1.616 | 108,6 |
| 23.02. | 263 | 1.600 | 107,5 |
| 24.02. | 212 | 1.597 | 107,3 |
| 25.02. | 0 | 1.597 | 107,3 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 1,01 auf jetzt 0,98 und ist damit sehr knapp unter 1.

| Datum | R(t) 7 Tage |
|---|---:|
| 20.02. | 1,01 |
| 21.02. | 0,98 |
| 22.02. | 0,97 |
| 23.02. | 0,96 |
| 24.02. | 0,96 |
| 25.02. | 0,98 | 
| 26.02. | 0,98 |

## Abwasserwerte für München

Es gibt jetzt auch endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München. Die Daten laufen ein wenig hinterher und gehen nur bis zum 14. Februar, aber immerhin. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend an, daß wir die Spitze überschritten haben - aber wie gesagt - die Daten laufen 14 Tage hinterher.

![Abwasserwerte München](/images/2023/02/KW08Abwasser.png)

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München schwankte die Zahl der belegten Normalbetten zwischen 350-370, heute 361. IMC pendelt unter 10, Intensiv stieg von 21 auf jetzt 30. An 7 von 7 Tagen war die Summe zweistellig **höher** als in der Vorwoche.

Im Vergleich zum Vorjahr trotzdem niedrigere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 20.02. | 368 | 11 | 21 | 400 | +71 | 529 |
| 21.02. | 368 | 8 | 26 | 400 | +55 | 544 |
| 22.02. | 364 | 5 | 25 | 394 | +42 | 552 |
| 23.02. | 349 | 5 | 28 | 382 | +17 | 563 |
| 24.02. | 353 | 6 | 28 | 387 | +22 | 541 |
| 25.02. | 357 | 7 | 29 | 393 | +26 | 543 |
| 26.02. | 361 | 8 | 30 | 399 | +31 | 544 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche eine stetige Zunaheme der belegten Betten, fast so hohe Zahlen wie im Vorjahr. Weit über der alten Grenze der gelben Ampel von 1.200, eher um und über 2.000. Warum das einfch so hingenommen wird - unbegreiflich.

Bei den Intensivbetten steigen die Zahlen auch stetig von 205 auf jetzt 219.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 20.02. | 205 | 2.148 | 2.038 |
| 21.02. | 206 | 2.050 | 2.081 |
| 22.02. | 222 | 2.160 | 2.103 |
| 23.02. | 216 | 2.081 | 2.080 |
| 24.02. | 218 | 1.876 | 2.011 |
| 25.02. | 218 | 1.824 | 2.028 |
| 26.02. | 219 | 1.759 | 2.015 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche wieder weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 53 | 75,21% (+/-0%) |
| 2. | 137 | 75,13% (+/-0%) |
| 3. | 172 | 59,25% (+/-0%) |
| 4. | 1.297 | 11,97% (+0,01%) |
| 5. | 486 | 0,83% (+/-0%) |
| 6. | 34 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/02/KW08Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 9.863 Tests/Tag mit einer Positivrate von 20,22%. Gegenüber den Zahlen von letzter Woche (10.628 Tests/Tag mit 17,13% Positivrate) also wieder weniger Tests bei weiter steigender Positivrate.

9.863 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom möglichen Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 9.863 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 20,22%.](/images/2023/02/20230223Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten etwas hinterherhinken.

Leider sind die Daten für München weiterhin auf dem Stand vom 30. Januar und wurden heute im Vergleich zur Vorwoche nicht aktualisiert.

Die Variante BA.5 bleibt in München erstaunlich stark vertreten, XBB.1.5 steigt stetig an. BA.2.75 und BQ.1 sind reativ stabil auf Platz 3 und 4. BA.4 ist mehr oder weniger verschwunden.

| Variante | KW 01 | KW 02 | KW 03 | KW 04 |
|---|--:|--:|--:|--:|
| BA,2 | 0,48% | 0,00% | 9,00% | 0,00% |
| BA,2,75 | 18,06% | 15,26% | 14,27% | 13,79% |
| BN,1 | 4,35% | 4,54% | 2,76% | 6,63% |
| XBB,1,5 | 9,73% | 13,01% | 16,77% | 20,58% |
| BA,4 | 0,00% | 0,00% | 0,00% | 0,00% |
| BA,5 | 43,92% | 42,03% | 34,41% | 38,67% |
| BF,7 | 2,59% | 0,46% | 1,41% | 0,26% |
| BQ,1 | 19,97% | 17,71% | 17,17% | 15,69% |
| Andere | 0,90% | 6,99% | 4,21% | 4,38% |

![VoC München](/images/2023/02/KW07VarMUC.png)

## Varianten Bayern

In Bayern: Der BA.5 Anteil sinkt weiter auf unter 10%, XBB.1.5 führt zwar kalr, ist aber mit 36,9% nicht wirklich dominant. BQ.1 ist mit 24,0% auf Platz zwei, BA.2.75 mit 14,9% auf Platz drei. Die Zeiten dominanter Varianten sind wohl vorbei.

| Variante | KW 04 | KW 05 | KW06 | KW07 |
|:--|--:|--:|--:|--:|
| BA.2 | 3.7% | 4.7% | 7.1% | 8.2% |
| BA.2.75 | 14.6% | 16,4% | 16.3% | 14.9% |
| BN.1 | 9.4% | 9.6% | 6.3% | 4.8% |
| XBB.1.5 | 10.5% | 19.5% | 30.3% | 36.9% |
| BA.4 | 0.2% | 0.0% | 0.0% | 0.0% |
| BA.5 | 10.7% | 8.7% | 6.3% | 3.5% |
| BQ.1 | 36.8% | 31,7% | 26.3% | 24.0% |
| BF.7 | 9.6% | 4.5% | 2.1% | 2.2% |
| Rekombinante | 3.5% | 4.5% | 4.2% | 2.2% |
| Andere | 1,0% | 0,4% | 1,1% | 3.3% |
| Sequenzierungen | 513 | 773 | 575 | 463 |

![VoC Bayern](/images/2023/02/KW08Var.png)
