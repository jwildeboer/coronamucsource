---
layout: default
title: 2023-02-09 Tagesbericht
category: Tagesberichte
---

![2023-02-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert ist seit 13 Tagen über 1, sinkt aber leicht. Zweite zeigt Krankenhausbetten, deren Zahl weiter steigt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230209.png)

Das RKI meldet eine Inzidenz von 96,1. 320 Neue,  73 Nachmeldungen, davon 58 für vorgestern. Die gestern genannte Inzidenz von 85,5 erhöht sich dadurch auf 89,7. R(t) bleibt klar über 1, sinkt aber wieder. 3 neue Todesfälle.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 01.02. | 84,0 &rarr; 84,9 |
| 02.02. | 85,7 &rarr; 87,9 |
| 03.02. | 88,1 &rarr; 91,8 |
| 04.02. | 88,1 &rarr; 91,8 |
| 05.02. | 88,1 &rarr; 91,8 |
| 06.02. | 89,9 &rarr; 90,9 |
| 07.02. | 85,5 &rarr; 89,7 |

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 4. Damit insgesamt jetzt 72 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 09.02 | 08.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 187,2 | 185,3 | -1,0% |
| LK Ebersberg | 71,9 | 74,0 | -2,8% |
| LK Freising | 80,6 | 71,2 | +13,2% |
| LK Fürstenfeldbruck | 114,4 | 122,6 | -6,7% |
| LK München | 112,6 | 103,8 | +8,5% |
| LK Starnberg | 97,3 | 89,2 | +9,1% |

Gemischtes Bild.

## Reproduktionswert R(t) für München

Unser R(t) sinkt heute weiter leicht auf 1,11.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.02. | 1,20 | --- |
| 03.02. | 1,21 | --- |
| 04.02. | 1,21 | --- |
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |
| 07.02. | 1,14 | --- |
| 08.02. | 1,11 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 5 von 265 auf 270. IMC sinkt um 3 von gestern 16 auf heute 13. Intensiv steigt um 3 von gestern 21 auf heute 24. Damit insgesamt 307 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 270 | +5 | 
| IMC | 13 | -3 | 
| Intensiv | 24 | +3 |
| SUMME | 307 | +5 |

Damit sind 20 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 10.891 Tests/Tag mit einer Positivrate von 14,63%. Gegenüber den Zahlen von letzter Woche (10.966 Tests/Tag mit 11,87% Positivrate) also ähnlich viele Tests bei steigender Positivrate.

10.891 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 10.891 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 14,63%.](/images/2023/02/20230209Test.png)

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.