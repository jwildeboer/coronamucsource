---
layout: default
title: 2023-02-22 Tagesbericht
category: Tagesberichte
---

![2023-02-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, in Summe höher wie in der Vorwoche, aber Meldeverzögerungen wegen Faschingsdienstag. Der R(t) Wert ist nach 24 Tagen exponenziell heute wieder sehr knapp unter 1. Zweite zeigt Krankenhausbetten, in der Summe unevrändert gegenüber gestern. Dritte zeigt Inzidenz, die heute wegen Meldeverzögerungen zu niedrig sein dürfte.](/images/2023/02/20230222.png)

Das RKI meldet eine Inzidenz von 88,7. Nur 31 Neue, dafür 190 Nachmeldungen, davon 183 für vorgestern. Die gestern genannte Inzidenz von 97,9 springt daher auf 110,4. Faschingseffekt oder Meldeverzögerungen?
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 14.02. | 112,6 &rarr; 117,2 |
| 15.02. | 111,6 &rarr; 114,5 |
| 16.02. | 111,1 &rarr; 113,9 |
| 17.02. | 107,7 &rarr; 109,2 |
| 18.02. | 107,7 &rarr; 109,2 |
| 19.02. | 107,7 &rarr; 109,1 |
| 20.02. | 97,7 &rarr; 110,4 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 5. Damit insgesamt bis jetzt 92 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt momentan wohl bei ca. 7.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 22.02. | 21.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 90,1 | 134,4 | -33,0% |
| LK Ebersberg | 72,6 | 75,4 | -3,7% |
| LK Freising | 58,5 | 76,2 | -23,2% |
| LK Fürstenfeldbruck | 99,3 | 114,8 | -13,5% |
| LK München | 102,3 | 105,2 | -2,8% |
| LK Starnberg | 65,1 | 71,7 | -9,2% |

Meldeverzögerungen wegen Faschingsdienstag. Das ist **kein** offizieller Feiertag, trotzdem haben Dachau, Freising und Fürstenfeldbruck einfach mal gar keine Zahlen für gestern an das RKI übermittelt. Das verzerrt die Inzidenzberechnung natürlich deutlich. 

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,97. Nach 25 Tagen  über 1 jetzt also wieder unter der exponenziellen Grenze. Aber durch Faschingsdienstag verzerrt.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 15.02. | 1,12 |
| 16.02. | 1,11 |
| 17.02. | 1,08 |
| 18.02. | 1,04 |
| 19.02. | 1,03 |
| 20.02. | 1,01 |
| 21.02. | 0,97 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten bleibt gegenüber gestern unverändert, mit leichten Verschiebungen: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 368 auf 366. IMC sinkt um 3 von gestern 11 auf heute 8. Intensiv steigt um 5 von gestern 21 auf heute 26. Damit insgesamt 400 Betten belegt. So hohe Zahlen hatten wir zuletzt am 1.1.2023  mit 403 belegten Betten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 366 | -2 | 
| IMC | 8 | -3 | 
| Intensiv | 26 | +5 |
| SUMME | 400 | +/-0 |

Damit sind 55 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.