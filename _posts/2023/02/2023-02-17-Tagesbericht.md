---
layout: default
title: 2023-02-17 Tagesbericht
category: Tagesberichte
---

![2023-02-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 21 Tagen über 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter anzieht. Dritte zeigt Inzidenz, die seit 8 Tagen wieder über 100 liegt.](/images/2023/02/20230217.png)

Das RKI meldet eine Inzidenz von 111,6. 241 Neue, 37 Nachmeldungen, davon 31 für vorgestern. Die gestern genannte Inzidenz von 111,6 erhöht sich durch die Nachmeldungen auf 114,1. Tag 8 mit Inzidenz über 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 09.02. | 98,2 &rarr; 101,7 |
| 10.02. | 101,1 &rarr; 106,3 |
| 11.02. | 101,1 &rarr; 106,3 |
| 12.02. | 101,1 &rarr; 106,4 |
| 13.02. | 109,1 &rarr; 113,4 |
| 14.02. | 112,6 &rarr; 117,0 |
| 15.02. | 111,6 &rarr; 114,1 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt bis jetzt 88 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt momentan wohl bei ca. 7-8.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 17.02. | 16.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 148,6 | 157,6 | -5,7% |
| LK Ebersberg | 69,9 | 63,6 | +9,9% |
| LK Freising | 91,6 | 87,8 | +4,3% |
| LK Fürstenfeldbruck | 116,2 | 115,3 | +0,8% |
| LK München | 105,8 | 111,5 | -5,1% |
| LK Starnberg | 85,6 | 91,4 | -6,3% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) bleibt weiter auf 1,11. 21 Tage über 1 und damit weiter exponenziell.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 10.02. | 1,08 |
| 11.02. | 1,09 |
| 12.02. | 1,09 |
| 13.02. | 1,10 |
| 14.02. | 1,12 |
| 15.02. | 1,12 |
| 16.02. | 1,11 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt auch heute weiter an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 10 von 319 auf 329. IMC sinkt um 1 von gestern 11 auf heute 10. Intensiv steigt um 4 von gestern 22 auf heute 26. Damit insgesamt 365 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 329 | +10 | 
| IMC | 10 | -1 | 
| Intensiv | 26 | +4 |
| SUMME | 365 | +13 |

Damit sind 63 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.