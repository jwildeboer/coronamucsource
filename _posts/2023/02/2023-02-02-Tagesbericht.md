---
layout: default
title: 2023-02-02 Tagesbericht
category: Tagesberichte
---

![2023-02-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230202.png)

Das RKI meldet eine Inzidenz von 84,0 für München. 220 neue und 55 Nachmeldungen für die letzten 7 Tage, davon 52 für vorgestern.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 25.01. | 59,2 &rarr; 60,1 |
| 26.01. | 62,6 &rarr; 64,7 |
| 27.01. | 62,8 &rarr; 65,7 |
| 28.01. | 62,8 &rarr; 65,7 |
| 29.01. | 62,8 &rarr; 65,7 |
| 30.01. | 71,7 &rarr; 74,4 |
| 31.01. | 78,1 &rarr; 81,7 |

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 11. Damit insgesamt jetzt 68 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 02.02. | 01.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 160,8 | 139,0 | +15,7% |
| LK Ebersberg | 92,0 | 81,6 | +12,7% |
| LK Freising | 70,1 | 66,8 | +4,9% |
| LK Fürstenfeldbruck | 100,7 | 87,4 | +15,2% |
| LK München | 96,0 | 90,3 | +6,3% |
| LK Starnberg | 68,0 | 68,0 | +/-0% |

Insgesamt klar steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) ist heute weiter auf 1,16 gestiegen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.01. | 0,95 | --- |
| 27.01. | 1,00 | --- |
| 28.01. | 1,04 | --- |
| 29.01. | 1,06 | --- |
| 30.01. | 1,08 | --- |
| 31.01. | 1,12 | --- |
| 01.02. | 1,16 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 5 von 243 auf 248. IMC steigt um 1 von gestern 13 auf heute 14. Intensiv steigt um 1 von gestern 24 auf heute 25. Damit insgesamt 287 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 248 | +5 | 
| IMC | 14 | +1 | 
| Intensiv | 25 | +1 |
| SUMME | 287 | +7 |

Damit sind 6 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.