---
layout: default
title: 2023-02-12 Tagesbericht
category: Tagesberichte
---

![2023-02-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 16 Tagen über 1, seit 2 Tagen 1,07. Zweite zeigt Krankenhausbetten, deren Zahl etwas sinkt aber bis Montag nicht aktualisert wird. Dritte zeigt Inzidenz, die seit 3 Tagen wieder über 100 liegt.](/images/2023/02/20230212.png)

Das RKI meldet, wie vorausgesagt, eine Inzidenz von 101,1. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

**Stand 2023-02-11, wird erst wieder am Dienstag aktualisiert**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 03.02. | 88,1 &rarr; 91,8 |
| 04.02. | 88,1 &rarr; 91,8 |
| 05.02. | 88,1 &rarr; 91,8 |
| 06.02. | 89,9 &rarr; 90,9 |
| 07.02. | 85,5 &rarr; 89,8 |
| 08.02. | 96,1 &rarr; 97,9 |
| 09.02. | 98,2 &rarr; 101,1 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, gibt es erst am Dienstag eine neue Inzidenz. Die Inzidenz für Montag können wir schon jetzt berechnen:

| Montag | 101,1 |

Am Montag somit Tag 4 mit Inzidenz über 100.

## COVID-positive Todesfälle

**Stand 2023-02-11, wird erst wieder am Dienstag aktualisiert**

Es wurden 6 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 9. Damit insgesamt jetzt 78 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt wohl bei ca. 7-8.

## Umland

**Stand 2023-02-11, wird erst wieder am Dienstag aktualisiert**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 11.02 | 10.02 | Änderung |
|:---|---:|---:|---:|
| LK Dachau |167,9 | 175,6 | -4,4% |
| LK Ebersberg | 75,4 | 67,1 | +12,4% |
| LK Freising | 93,8 | 87,8 | +6,8% |
| LK Fürstenfeldbruck | 108,4 | 107,5 | +0,8% |
| LK München | 119,2 | 116,1 | +2,7% |
| LK Starnberg | 98,7 | 103,1 | -4,3% |

Uneinheitliches Bild, leicht steigende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) bleibt heute wegen fehlender Meldungen auf 1,07 und könnte bald wieder steigen. 16 Tage über 1 und damit exponenziell.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |
| 07.02. | 1,14 | --- |
| 08.02. | 1,11 | --- |
| 09.02. | 1,08 | --- |
| 10.02. | 1,07 | --- |
| 11.02. | 1,07 | --- |

## Krankenhaus

**Stand 2023-02-10, wird erst wieder am Montag aktualisiert**

Die Zahl der belegten Krankenhausbetten sinkt heute ein wenig - wie so häufig am Freitag: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 5 von 270 auf 265. IMC sinkt um 2 von gestern 13 auf heute 11. Intensiv steigt um 2 von gestern 24 auf heute 26. Damit insgesamt 302 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 265 | -5 | 
| IMC | 11 | -2 | 
| Intensiv | 26 | +2 |
| SUMME | 302 | -5 |

Damit sind 33 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.