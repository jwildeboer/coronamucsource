---
layout: default
title: 2023-02-07 Tagesbericht
category: Tagesberichte
---

![2023-02-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230207.png)

Das RKI meldet eine Inzidenz von 89,9. 353 Neue und 53 Nachmeldungen für die letzten 7 Tage, davon 0 fürs Samstag und Sonntag und 48 für Freitag.
Die fürs Wochenende gemeldete Inzidenz steigt von 88,1 auf 91,6.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 30.01. | 71,7 &rarr; 74,5 |
| 31.01. | 78,1 &rarr; 82,1 |
| 01.02. | 84,0 &rarr; 84,6 |
| 02.02. | 85,7 &rarr; 87,4 |
| 03.02. | 88,1 &rarr; 91,6 |
| 04.02. | 88,1 &rarr; 91,6 |
| 05.02. | 88,1 &rarr; 91,6 |

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 6. Damit insgesamt jetzt 69 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 07.02. | 06.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 195,6 | 189,1 | +3,4% |
| LK Ebersberg | 88,5 | 93,4 | -5,2% |
| LK Freising | 70,7 | 64,6 | +9,4% |
| LK Fürstenfeldbruck | 120,8 | 113,9 | +6,1% |
| LK München | 96,3 | 93,8 | +2,7% |
| LK Starnberg | 83,4 | 80,4 | +3,7% |

Weiter steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt heute leicht auf 1,18.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 31.01. | 1,12 | --- |
| 01.02. | 1,16 | --- |
| 02.02. | 1,20 | --- |
| 03.02. | 1,21 | --- |
| 04.02. | 1,21 | --- |
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute wieder: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 17 von 230 auf 247. IMC steigt um 3 von gestern 11 auf heute 14. Intensiv sinkt um 3 von gestern 24 auf heute 21. Damit insgesamt 282 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 247 | +17 | 
| IMC | 14 | +3 | 
| Intensiv | 21 | -3 |
| SUMME | 282 | +17 |

Damit sind 2 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.