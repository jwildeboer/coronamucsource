---
layout: default
title: 2023-02-10 Tagesbericht
category: Tagesberichte
---

![2023-02-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert ist seit 14 Tagen über 1, sinkt aber leicht. Zweite zeigt Krankenhausbetten, deren Zahl etwas sinkt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/02/20230210.png)

Das RKI meldet eine Inzidenz von 98,2. 240 Neue, 26 Nachmeldungen, davon 24 für vorgestern. Die gestern genannte Inzidenz von 96,1 erhöht sich dadurch auf 97,7. R(t) bleibt klar über 1, sinkt aber wieder.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 02.02. | 85,7 &rarr; 87,9 |
| 03.02. | 88,1 &rarr; 91,8 |
| 04.02. | 88,1 &rarr; 91,8 |
| 05.02. | 88,1 &rarr; 91,8 |
| 06.02. | 89,9 &rarr; 90,9 |
| 07.02. | 85,5 &rarr; 89,8 |
| 08.02. | 96,1 &rarr; 97,7 |

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 4. Damit insgesamt jetzt 72 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt wohl bei ca. 7-8.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 10.02 | 09.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 175,6 | 187,2 | -6,2% |
| LK Ebersberg | 67,1 | 71,9 | -6,7% |
| LK Freising | 87,8 | 80,6 | +8,9% |
| LK Fürstenfeldbruck | 107,5 | 114,4 | -6,0% |
| LK München | 116,1 | 112,6 | +3,1% |
| LK Starnberg | 103,1 | 97,3 | +6,0% |

Uneinheitliches Bild, aber nur leichte Schwankungen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt heute weiter leicht auf 1,08.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.02. | 1,21 | --- |
| 04.02. | 1,21 | --- |
| 05.02. | 1,20 | --- |
| 06.02. | 1,18 | --- |
| 07.02. | 1,14 | --- |
| 08.02. | 1,11 | --- |
| 09.02. | 1,08 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute ein wenig - wie so häufig am Freitag: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 5 von 270 auf 265. IMC sinkt um 2 von gestern 13 auf heute 11. Intensiv steigt um 2 von gestern 24 auf heute 26. Damit insgesamt 302 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 265 | -5 | 
| IMC | 11 | -2 | 
| Intensiv | 26 | +2 |
| SUMME | 302 | -5 |

Damit sind 33 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.