---
layout: default
title: 2023-02-16 Tagesbericht
category: Tagesberichte
---

![2023-02-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher wie in der Vorwoche, der R(t) Wert ist seit 20 Tagen über 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter anzieht. Dritte zeigt Inzidenz, die seit 7 Tagen wieder über 100 liegt.](/images/2023/02/20230216.png)

Das RKI meldet eine Inzidenz von 111,6. 274 Neue, 62 Nachmeldungen, davon 58 für vorgestern. Die gestern genannte Inzidenz von 112,6 erhöht sich durch die Nachmeldungen auf 116,6. Tag 7 mit Inzidenz über 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 08.02. | 96,1 &rarr; 98,1 |
| 09.02. | 98,2 &rarr; 101,7 |
| 10.02. | 101,1 &rarr; 106,3 |
| 11.02. | 101,1 &rarr; 106,3 |
| 12.02. | 101,1 &rarr; 106,4 |
| 13.02. | 109,1 &rarr; 113,2 |
| 14.02. | 112,6 &rarr; 116,6 |

## COVID-positive Todesfälle

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt bis jetzt 88 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer - Faktor 7-8 geschätzt

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen. Sie liegt momentan wohl bei ca. 7-8.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 16.02 | 15.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 157,6 | 156,3 | +0,8% |
| LK Ebersberg | 63,6 | 70,6 | -9,9% |
| LK Freising | 87,8 | 87,2 | +0,7% |
| LK Fürstenfeldbruck | 115,3 | 104,8 | +10,0% |
| LK München | 111,5 | 114,1 | -2,3% |
| LK Starnberg | 91,4 | 94,3 | -3,1% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) bleibt weiter auf 1,11. 20 Tage über 1 und damit weiter exponenziell.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) |
|---|---:|
| 09.02. | 1,08 |
| 10.02. | 1,08 |
| 11.02. | 1,09 |
| 12.02. | 1,09 |
| 13.02. | 1,10 |
| 14.02. | 1,12 |
| 15.02. | 1,11 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt auch heute weiter an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 11 von 308 auf 319. IMC sinkt um 4 von gestern 15 auf heute 11. Intensiv bleibt mit 22 unverändert gegenüber gestern. Damit insgesamt 352 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 319 | +11 | 
| IMC | 11 | -4 | 
| Intensiv | 22 | +/-0 |
| SUMME | 352 | +7 |

Damit sind 45 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 10.389 Tests/Tag mit einer Positivrate von 17,38%. Gegenüber den Zahlen von letzter Woche (11.130 Tests/Tag mit 15,15% Positivrate) also etwas weniger Tests bei weiter steigender Positivrate.

10.389 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 10.389 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 17,38%.](/images/2023/02/20230216Test.png)

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.