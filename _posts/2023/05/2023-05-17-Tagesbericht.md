---
layout: default
title: 2023-05-17 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-17 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, etwas leichter. Zweite zeigt R(t) Wert für München, der auf 0,83 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230517.png)

Das RKI meldet eine Inzidenz von 7,8 für München. 21 Neue, 5 Nachmeldungen, alle für vorgestern, 1 Korrektur. Die gestern genannte Inzidenz von 7,4 erhöht sich dadurch auf 7,7. Dunkelziffer Faktor 10-25. Keine neuen Todesmeldungen. 7 Tage Summe der Todesfälle 3. R(t) jetzt 0,83.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 09.05. | 10,7 &rarr; 11,0 |
| 10.05. | 10,4 &rarr; 10,4 |
| 11.05. | 9,3 &rarr; 9,3 |
| 12.05. | 8,5 &rarr; 8,6 |
| 13.05. | 8,5 &rarr; 8,6 |
| 14.05. | 8,5 &rarr; 8,6 |
| 15.05. | 7,4 &rarr; 7,7 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 21 |
| ITS basiert für Bayern | 25 |

## COVID-positive Todesfälle

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle steht auf 3. Damit insgesamt bis jetzt 219 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 17.05. | 16.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 4,5 | 5,1 | -11,8% |
| LK Ebersberg | 4,8 | 3,5 | +37,1% |
| LK Freising* | 2,2 | 3,3 | -33,3% |
| LK Fürstenfeldbruck | 6,9 | 4,1 | +68,3% |
| LK München | 5,7 | 7,1 | -19,7% |
| LK Starnberg | 6,6 | 5,9 | +11,9% |

* Kreis hat keine neuen Zahlen gemeldet.

Wenig Veränderung. Dunkelziffer auch hier Faktor 10&ndash;25. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt wieder auf 0,95 und wird bis Montag auf 0,92 gehen.

| Datum | R(t) |
|---|---:|
| 10.05. | 1,13 |
| 11.05. | 1,13 |
| 12.05. | 1,02 |
| 13.05. | 0,96 |
| 14.05. | 0,92 |
| 15.05. | 0,86 |
| 16.05. | 0,83 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 3 gleich (+/-0). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 7 auf heute 6 (-1). Insgesamt somit 9 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | +/-0 | 
| Intensiv, beatmet | 6 | -1 | 
| SUMME | 9 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 153 wieder minimal höher als vorgestern (147).

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 29 auf heute 28 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern deutlich von 19 auf heute 15 (-4). Insgesamt somit 43 Intensivbetten belegt (-5).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 28 | -1 |
| Intensiv, beatmet | 15 | -4 |
| SUMME | 43 | -5 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)