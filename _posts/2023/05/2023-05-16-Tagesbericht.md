---
layout: default
title: 2023-05-16 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-16 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, wieder leichter. Zweite zeigt R(t) Wert für München, der auf 0,85 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230516.png)

Das RKI meldet eine Inzidenz von 7,4 für München. 23 Neue, 2 Nachmeldungen, beide für letzten Freitag, 1 Korrektur. Die Wochenendinzidenz erhöht sich dadurch von 8,5 auf 8,6. Dunkelziffer Faktor 10-25. R(t) fällt weiter auf jetzt 0,85. Keine neuen Todesmeldungen. 7 Tage Summe der Todesfälle 3.
<!--more-->

## Inzidenzentwicklung


Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 08.05. | 13,1 &rarr; 13,2 |
| 09.05. | 10,7 &rarr; 11,0 |
| 10.05. | 10,4 &rarr; 10,4 |
| 11.05. | 9,3 &rarr; 9,3 |
| 12.05. | 8,5 &rarr; 8,6 |
| 13.05. | 8,5 &rarr; 8,6 |
| 14.05. | 8,5 &rarr; 8,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 29 |
| ITS basiert für Bayern | 25 |

## COVID-positive Todesfälle

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle steht auf 3. Damit insgesamt bis jetzt 219 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 16.05. | 13.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 5,1 | 5,8 | -12,1% |
| LK Ebersberg | 3,5 | 3,5 | +/-0% |
| LK Freising | 3,3 | 4,4 | -25,0% |
| LK Fürstenfeldbruck* | 4,1 | 5,0 | -18,0% |
| LK München | 7,1 | 7,4 | -4,1% |
| LK Starnberg* | 5,9 | 9,5 | -37,9% |

* Kreis hat keine neuen Zahlen gemeldet.

Wenig Veränderung. Dunkelziffer auch hier Faktor 10&ndash;25. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt wieder auf 0,95 und wird bis Montag auf 0,92 gehen.

| Datum | R(t) |
|---|---:|
| 07.05. | 0,96 |
| 08.05. | 1,06 |
| 09.05. | 1,11 |
| 10.05. | 1,13 |
| 11.05. | 1,13 |
| 12.05. | 1,01 |
| 13.05. | 0,95 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 2 auf heute 3 (+1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 7 gleich (+/-0). Insgesamt somit 10 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | +1 | 
| Intensiv, beatmet | 7 | +/-0 | 
| SUMME | 10 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 147 wieder klar niedriger als vorgestern (195). Wie häufig am Montag. Dann kommen aber noch Nachmeldungen.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 30 auf heute 29 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 21 auf heute 19 (-2). Insgesamt somit 48 Intensivbetten belegt (-3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 29 | -1 |
| Intensiv, beatmet | 19 | -2 |
| SUMME | 48 | -3 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)