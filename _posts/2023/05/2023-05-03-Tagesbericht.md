---
layout: default
title: 2023-05-03 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-03 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der weiter sinkt und jetzt bei 0,85 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230503.png)

Das RKI meldet eine Inzidenz von 8,5 für München. 52 Neue, 1 Nachmeldung, 9 Korrekturen. Die gestern genannte Inzidenz von 7,6 reduziert sich dadurch auf 7,5. Dunkelziffer Faktor 10-19. R(t) sinkt auf 0,85. 1 neue Todesmeldung. 7 Tage Summe der Todesfälle 5.
<!--more-->

## Inzidenzentwicklung

Aufgrund der hohen Zahl an Korrekturen sinkt die korrigierte Inzidenz. Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 25.04. | 12,0 &rarr; 12,0 |
| 26.04. | 11,0 &rarr; 11,0 |
| 27.04. | 11,2 &rarr; 11,0 |
| 28.04. | 11,0 &rarr; 10,9 |
| 29.04. | 11,0 &rarr; 10,9 |
| 30.04. | 11,0 &rarr; 10,9 |
| 01.05. | 7,6 &rarr; 7,5 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 21 |
| ITS basiert für Bayern | 19 |

## COVID-positive Todesfälle

Es wurde heute 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht auf 5. Damit insgesamt bis jetzt 215 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten Samstag in %.

| Kreis | 03.05. | 29.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 4,5 | 5,8 | -22,4% |
| LK Ebersberg* | 1,4 | 4,2 | -66,7% |
| LK Freising | 4,4 | 5,0 | -12% |
| LK Fürstenfeldbruck | 4,1 | 7,3 | -43,8% |
| LK München | 6,6 | 8,9 | -25,8% |
| LK Starnberg | 8,0 | 11,7 | -31,6% |

* Diese Kreise haben keine neuen Fälle gemeldet.

Fallende Inzidenzen im einstelligen Bereich. Dunkelziffer auch hier Faktor 10&ndash;19. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt weiter auf 0,85 und bleibt damit stabil unter 1.

| Datum | R(t) |
|---|---:|
| 26.04. | 0,85 |
| 27.04. | 0,84 |
| 28.04. | 0,88 |
| 29.04. | 0,89 |
| 30.04. | 0,89 |
| 01.05. | 0,87 |
| 02.05. | 0,85 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 5 auf heute 6 (+1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 5 auf heute 4 (-1). Insgesamt somit weiterhin 10 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 6 | +1 | 
| Intensiv, beatmet | 4 | -1 | 
| SUMME | 10 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 228 wieder höher als gestern (168). Wegen Feiertag war diese Zahl aber eh deutlich zu niedrig und hat sich durch Nachmeldungen schon wieder auf 223 korrigiert.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 41 auf heute 39 (-2). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 18 auf heute 17 (-1). Insgesamt somit 56 Intensivbetten belegt (-3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 39 | -2 |
| Intensiv, beatmet | 17 | -1 |
| SUMME | 56 | -3 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-03 mit Werten bis 2023-04-25**

Die Abwasserbelastung sinkt wieder deutlich. Heute wurden die Werte endlich aktualisiert. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230503A.png)