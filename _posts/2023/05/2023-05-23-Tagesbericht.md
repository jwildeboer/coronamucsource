---
layout: default
title: 2023-05-23 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-23 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, kaum Veränderungen gegenüber Vorwoche. Zweite zeigt R(t) Wert für München, der auf 0,95 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230523.png)

Das RKI meldet eine Inzidenz von 7,7 für München. 29 Neue, 4 Nachmeldungen, alle für letzten Freitag. Die Wochenendinzidenzen von 7,3 erhöhen sich durch Nachmeldungen auf 7,6. Dunkelziffer Faktor 10-22. Ein neuer Todesfall gemeldet, 7 Tage Summe der Todesfälle 2.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 15.05. | 7,4 &rarr; 7,7 |
| 16.05. | 7,8 &rarr; 8,1 |
| 17.05. | 8,0 &rarr; 8,4 |
| 18.05. | 6,7 &rarr; 7,1 |
| 19.05. | 7,3 &rarr; 7,6 |
| 20.05. | 7,3 &rarr; 7,6 |
| 21.05. | 7,3 &rarr; 7,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 15 |
| ITS basiert für Bayern | 22 |

## COVID-positive Todesfälle

Es wurde heute ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 2. Damit insgesamt bis jetzt 221 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 23.05. | 20.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau* | 2,6 | 4,5 | -42,2% |
| LK Ebersberg | 2,8 | 2,8 | +/-0% |
| LK Freising | 5,0 | 2,8 | +78,6% |
| LK Fürstenfeldbruck | 2,3 | 2,3 | +/-0% |
| LK München | 3,7 | 2,3 | +60,9% |
| LK Starnberg | 5,9 | 3,7 | +59,5% |

* Kreis hat keine neuen Zahlen gemeldet.

Wegen sehr niedriger Fallzahlen prozentual scheinbar heftige Schwankungen. Dunkelziffer auch hier Faktor 10&ndash;23. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt auf 0,89 und wird bis Montag auf 0,90 gehen.

| Datum | R(t) |
|---|---:|
| 16.05. | 0,84 |
| 17.05. | 0,83 |
| 18.05. | 0,82 |
| 19.05. | 0,87 |
| 20.05. | 0,90 |
| 21.05. | 0,91 |
| 22.05. | 0,95 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 5 gleich (+/-0). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 5 auf 4 (-1). Insgesamt somit 9 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 5 | +/-0 | 
| Intensiv, beatmet | 4 | -1 | 
| SUMME | 9 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt auf 134.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 26 auf heute 30 (+4). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 13 auf heute 11 (-2). Insgesamt somit 41 Intensivbetten belegt (+2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 30 | +4 |
| Intensiv, beatmet | 11 | -2 |
| SUMME | 41 | +2 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)