---
layout: default
title: 2023-05-22 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-22 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, etwas leichter, bis Dienstag aber keine neuen Zahlen. Zweite zeigt R(t) Wert für München, der auf 0,90 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230522.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 7,3 für München. Montag, daher keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. R(t) steigt auf 0,90. Dunkelziffer Faktor 10-22. 7 Tage Summe der Todesfälle 1.
<!--more-->

## Inzidenzentwicklung

**Wegen Meldepause am Wochenende erst am Dienstag neue Zahlen vom RKI**

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 12.05. | 8,5 &rarr; 8,6 |
| 13.05. | 8,5 &rarr; 8,6 |
| 14.05. | 8,5 &rarr; 8,6 |
| 15.05. | 7,4 &rarr; 7,7 |
| 16.05. | 7,8 &rarr; 8,1 |
| 17.05. | 8,0 &rarr; 8,4 |
| 18.05. | 6,7 &rarr; 7,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 16 |
| ITS basiert für Bayern | 22 |

## COVID-positive Todesfälle

**Wegen Meldepause am Wochenende erst am Dienstag neue Zahlen vom RKI**

Es wurde am Samstag ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 1. Damit insgesamt bis jetzt 220 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Wegen Meldepause am Wochenende erst am Dienstag neue Zahlen vom RKI**

Inzidenz im Umland von München. Jeweils mit Änderung zum Mittwoch in %.

| Kreis | 20.05. | 17.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 4,5 | 5.1 | -11,8% |
| LK Ebersberg* | 2,8 | 2,8 | +/-0% |
| LK Freising | 2,8 | 2,8 | +/-0% |
| LK Fürstenfeldbruck* | 2,3 | 5,0 | -54,0% |
| LK München | 2,3 | 4,0 | -42,5% |
| LK Starnberg | 3,7 | 5,1 | -27,5% |

* Kreis hat keine neuen Zahlen gemeldet.

Feiertagseffekt. Dunkelziffer auch hier Faktor 10&ndash;23. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt auf 0,89 und wird bis Montag auf 0,90 gehen.

| Datum | R(t) |
|---|---:|
| 15.05. | 0,86 |
| 16.05. | 0,84 |
| 17.05. | 0,83 |
| 18.05. | 0,82 |
| 19.05. | 0,87 |
| 20.05. | 0,89 |
| 21.05. | 0,90 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 4 auf heute 5 (+1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 5 gleich (+/-0). Insgesamt somit 10 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 5 | +1 | 
| Intensiv, beatmet | 5 | +/-0 | 
| SUMME | 10 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt auf 134.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern bleibt im Vergleich zu gestern mit 26 gleich (+/-0). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 14 auf heute 13 (-1). Insgesamt somit 39 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 26 | +/-0 |
| Intensiv, beatmet | 13 | -1 |
| SUMME | 39 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)