---
layout: default
title: 2023-05-09 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-09 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell eher stabil. Heute mit Sondereffekt wegen Feiertag letzte Woche Montag. Zweite zeigt R(t) Wert für München, der auf 1,06 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230509.png)

Das RKI meldet eine Inzidenz von 13,1 für München. 40 Neue, 1 Nachmeldung für letzten Freitag. Die Wochenendinzidenzen bleiben daher unverändert bei 10,4. Dunkelziffer Faktor 10-22. Keine neuen Todesfälle seit 5 Tagen. 7 Tage Summe der Todesfälle 2. R(t) geht über 1. Ausgleichsbewegung wegen Feiertag in der letzten Woche.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 01.05. | 7,6 &rarr; 7,5 |
| 02.05. | 8,5 &rarr; 8,5 |
| 03.05. | 9,5 &rarr; 9,5 |
| 04.05. | 9,8 &rarr; 9,9 |
| 05.05. | 10,4 &rarr; 10,4 |
| 06.05. | 10,4 &rarr; 10,4 |
| 07.05. | 10,4 &rarr; 10,4 |

Der erwartete Sprung nach oben ist da, Nachholeffekt wegen Maifeiertag. Ab Mittwoch sollten sich die Zahlen dann wieder beruhigen.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 10 |
| ITS basiert für Bayern | 22 |

## COVID-positive Todesfälle

Seit 5 Tagen wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 2. Damit insgesamt bis jetzt 216 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 09.05. | 06.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 7,1 | 4,5 | +57,8% |
| LK Ebersberg | 5,5 | 4,8 | +14,6% |
| LK Freising | 5,0 | 3,9 | +28,2% |
| LK Fürstenfeldbruck | 3,7 | 2,7 | +37,0% |
| LK München | 7,7 | 8,6 | -10,5% |
| LK Starnberg | 11,7 | 8,0 | +46,3% |

Fast überall ist der Nachholeffekt wegen Feiertag deutlich sichtbar. Dunkelziffer auch hier Faktor 10&ndash;22. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 1,06 und geht damit über 1.

| Datum | R(t) |
|---|---:|
| 02.05. | 0,85 |
| 03.05. | 0,86 |
| 04.05. | 0,87 |
| 05.05. | 0,91 |
| 06.05. | 0,95 |
| 07.05. | 0,96 |
| 08.05. | 1,06 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 3 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 7 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | +/-0 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 7 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 225 wieder etwas niedriger als vorgestern (232). Das RKI scheint diese Zahl nicht mehr so zuverlässig täglich zu aktualisieren. Die Zahlen für heute fehlen z.B. noch.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 41 auf heute 40 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 17 gleich (+/-0). Insgesamt somit 57 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 40 | -1 |
| Intensiv, beatmet | 17 | +/-0 |
| SUMME | 57 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-09 mit Werten bis 2023-05-03**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230509A.png)