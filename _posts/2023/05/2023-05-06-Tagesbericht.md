---
layout: default
title: 2023-05-06 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-06 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell eher stabil. Zweite zeigt R(t) Wert für München, der auf 0,91 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230506.png)

Das RKI meldet eine Inzidenz von 10,4 für München. 30 Neue, 2 Nachmeldungen, 2 Korrekturen. Die gestern genannte Inzidenz von 9,8 erhöht sich auf 9,9. Dunkelziffer Faktor 10–20. R(t) steigt auf 0,91. Keine neue Todesmeldung. 7 Tage Summe der Todesfälle 2.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 28.04. | 11,0 &rarr; 10,9 |
| 29.04. | 11,0 &rarr; 10,9 |
| 30.04. | 11,0 &rarr; 10,9 |
| 01.05. | 7,6 &rarr; 7,5 |
| 02.05. | 8,5 &rarr; 8,5 |
| 03.05. | 9,5 &rarr; 9,5 |
| 04.05. | 9,8 &rarr; 9,9 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, bzw dor tnicht ausgewertet werden, können wir schon heute die Inzidenzen für Sonntag und Montag vorhersagen:

| Sonntag | 10,4 |
| Montag | 10,4 |

An Dienstag wird es dann wohl einen Sprung nach oben geben, Nachholeffekt wegen Maifeiertag. Ab Mittwoch sollten sich die Zahlen dann wieder beruhigen.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 10 |
| ITS basiert für Bayern | 20 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 2. Damit insgesamt bis jetzt 216 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum vortag in %.

| Kreis | 06.05. | 05.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau* | 4,5 | 5,1 | -11,8% |
| LK Ebersberg | 4,8 | 3,5 | +37,1% |
| LK Freising* | 3,9 | 4,4 | -11,4% |
| LK Fürstenfeldbruck | 2,7 | 3,2 | -15,6% |
| LK München | 8,6 | 8,3 | +3,6% |
| LK Starnberg | 8,0 | 8,0 | +/-0% |

* Diese Kreise haben keine neuen Fälle gemeldet.

Scheinbar kräftige Sprünge, aber bei sehr kleinen Fallzahlen. Dunkelziffer auch hier Faktor 10&ndash;20. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 0,91 und bleibt noch knapp unter 1.

| Datum | R(t) |
|---|---:|
| 29.04. | 0,89 |
| 30.04. | 0,89 |
| 01.05. | 0,87 |
| 02.05. | 0,85 |
| 03.05. | 0,86 |
| 04.05. | 0,87 |
| 05.05. | 0,91 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 4 auf heute 3 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 7 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | -1 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 7 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 259 wieder etwas höher als gestern (255). Die Tageszahl ist aber eh deutlich zu niedrig und hat sich durch Nachmeldungen schon wieder auf 286 korrigiert.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 38 auf heute 43 (+5). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 20 auf heute 19 (-1). Insgesamt somit 62 Intensivbetten belegt (+4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 43 | +5 |
| Intensiv, beatmet | 19 | -1 |
| SUMME | 62 | +4 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-03 mit Werten bis 2023-04-25**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230503A.png)