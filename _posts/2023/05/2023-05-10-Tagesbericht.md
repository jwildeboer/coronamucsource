---
layout: default
title: 2023-05-10 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-10 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell wieder leichter. Auch heute noch Sondereffekt wegen Feiertag letzte Woche Montag. Zweite zeigt R(t) Wert für München, der auf 1,10 steigt, aber morgen wieder sinken sollte. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230510.png)

Das RKI meldet eine Inzidenz von 10,7 für München. 16 Neue, keine Nachmeldungen, 1 Korrektur. Die Inzidenz von gestern bleibt daher unverändert bei 13,1. Dunkelziffer Faktor 10-23. Keine neuen Todesfälle seit 6 Tagen. 7 Tage Summe der Todesfälle 1. R(t) bleibt über 1. Ausgleichsbewegung wegen Feiertag in der letzten Woche.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 02.05. | 8,5 &rarr; 8,5 |
| 03.05. | 9,5 &rarr; 9,5 |
| 04.05. | 9,8 &rarr; 9,9 |
| 05.05. | 10,4 &rarr; 10,4 |
| 06.05. | 10,4 &rarr; 10,4 |
| 07.05. | 10,4 &rarr; 10,4 |
| 08.05. | 13,1 &rarr; 13,1 |

Der erwartete Sprung nach oben ist da, Nachholeffekt wegen Maifeiertag. Ab morgen sollten sich die Zahlen dann wieder beruhigen.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 18 |
| ITS basiert für Bayern | 23 |

## COVID-positive Todesfälle

Seit 6 Tagen wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 1. Damit insgesamt bis jetzt 216 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 10.05. | 09.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 6,4 | 7,1 | -9,9% |
| LK Ebersberg | 6,2 | 5,5 | +12,7% |
| LK Freising | 6,1 | 5,0 | +22,0% |
| LK Fürstenfeldbruck | 4,1 | 3,7 | +10,8% |
| LK München | 9,1 | 7,7 | +18,2% |
| LK Starnberg | 9,5 | 11,7 | -18,8% |

Fast überall ist der Nachholeffekt wegen Feiertag deutlich sichtbar. Dunkelziffer auch hier Faktor 10&ndash;23. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 1,10 und bleibt damit über 1.

| Datum | R(t) |
|---|---:|
| 03.05. | 0,86 |
| 04.05. | 0,87 |
| 05.05. | 0,91 |
| 06.05. | 0,95 |
| 07.05. | 0,96 |
| 08.05. | 1,06 |
| 09.05. | 1,10 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 3 auf heute 6 (+3). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 10 Intensivbetten belegt (+3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 6 | +3 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 10 | +3 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 225 wieder etwas niedriger als vorgestern (232). Das RKI scheint diese Zahl nicht mehr so zuverlässig täglich zu aktualisieren. Die Zahlen für heute fehlen z.B. noch.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 40 auf heute 42 (+2). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 17 auf heute 20 (+3). Insgesamt somit 62 Intensivbetten belegt (+5).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 42 | +2 |
| Intensiv, beatmet | 20 | +3 |
| SUMME | 62 | +5 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-09 mit Werten bis 2023-05-03**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230509A.png)