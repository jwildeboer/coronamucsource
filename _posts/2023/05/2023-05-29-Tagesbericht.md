---
layout: default
title: 2023-05-29 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-29 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, die Zahlen sinken weiter, auch weil heute wieder Feiertag ist. Zweite zeigt R(t) Wert für München, der auf 0,88 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230529.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 5,6 für München. Montag (und Feiertag), daher heute und morgen keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. R(t) sinkt auf 0,88. Dunkelziffer Faktor 10-12. 7 Tage Summe der Todesfälle 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 21.05. | 7,3 &rarr; 7,6 |
| 22.05. | 7,7 &rarr; 7,7 |
| 23.05. | 7,0 &rarr; 7,0 |
| 24.05. | 5,8 &rarr; 6,2 |
| 25.05. | 7,1 &rarr; 7,1 |
| 26.05. | 5,6 &rarr; 5,6 |
| 27.05. | 5,6 &rarr; 5,6 |

Und da heute mal wieder Feiertag ist, am Wochenende auch keine Zahlen gemeldet werden, können wir die Inzidenz für Dienstag schon heute berechnen:

| Dienstag| 3,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 21 |
| ITS basiert für Bayern | 12 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 1. Damit insgesamt bis jetzt 221 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Da am 02.06. das Corona Dashboard des RKI eingestellt wird, können wir diesen Teil des Tagesberichtes in Zukunft nicht nehr fortführen.**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 27.05. | 26.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau* | 3,9 | 5,1 | -23,5% |
| LK Ebersberg* | 4,2 |  4,8 | -12,5% |
| LK Freising* | 5,0 | 5,0 | +/-0% |
| LK Fürstenfeldbruck* | 2,7 | 3,2 | -15,6% |
| LK München | 5,4 | 6,0 | -10,0% |
| LK Starnberg* | 2,9 | 3,7 | -21,6% |

* Kreis hat keine neuen Zahlen gemeldet.

Wegen sehr niedriger Fallzahlen prozentual scheinbar heftige Schwankungen. Dunkelziffer auch hier Faktor 10&ndash;12. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) bleibt auf 0,88.

| Datum | R(t) |
|---|---:|
| 22.05. | 0,95 |
| 23.05. | 0,94 |
| 24.05. | 0,92 |
| 25.05. | 0,94 |
| 26.05. | 0,90 |
| 27.05. | 0,88 |
| 28.05. | 0,88 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 6 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 3 auch gleich (+/-0). Insgesamt somit 9 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 6 | +/-0 | 
| Intensiv, beatmet | 3 | +/-0 | 
| SUMME | 9 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt von vorgestern 162 auf gestern 155.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 22 auf 20 (-2). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 11 auf heute 9 (-2). Insgesamt somit 29 Intensivbetten belegt (-4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 20 | -2 |
| Intensiv, beatmet | 9 | -2 |
| SUMME | 29 | -4 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)

## Varianten München

**Letzte Aktualisierung: 2023-05-23 mit Werten bis 2023-05-01**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)