---
layout: default
title: 2023-05-31 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-31 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, die Zahlen sinken weiter, auch weil vorgestern Feiertag war. Zweite zeigt R(t) Wert für München, der auf 0,79 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230531.png)

Das RKI meldet eine Inzidenz von 4,6 für München. 27 Neue, 2 Nachmeldungen für letzten Freitag, 3 Korrekturen. Die Wochenendinzidenz von 5,6 erhöht sich dadurch auf 5,7. Die gestern genannte Inzidenz von 3,6 korrigiert sich auf 3,8. Dunkelziffer Faktor 10-15. 2 neue Todesfälle gemeldet, 7 Tage Summe der Todesfälle 2.
<!--more-->

Seit nunmehr 15 Tagen keine Aktualisierung der Abwasserdaten für München. Langsam wird es peinlich. Die Daten sind da, werden aber aufgrund "notwendiger technicher Umstellungen" nicht veröffentlicht. "In Kürze" sollen die Daten wieder fliessen. Das scheint ein langes "in Kürze" zu sein.

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 23.05. | 7,0 &rarr; 6,9 |
| 24.05. | 5,8 &rarr; 6,1 |
| 25.05. | 7,1 &rarr; 7,0 |
| 26.05. | 5,6 &rarr; 5,7 |
| 27.05. | 5,6 &rarr; 5,7 |
| 28.05. | 5,6 &rarr; 5,7 |
| 29.05. | 3,6 &rarr; 3,8 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 24 |
| ITS basiert für Bayern | 15 |

## COVID-positive Todesfälle

Es wurden heute zwei neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle steht auf 2. Damit insgesamt bis jetzt 223 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Da am 02.06. das Corona Dashboard des RKI eingestellt wird, können wir diesen Teil des Tagesberichtes in Zukunft nicht nehr fortführen.**

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 31.05. | 27.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 2,6 | 3,9 | -33,3% |
| LK Ebersberg | 4,2 | 4,2 | +/-0% |
| LK Freising | 3,3 | 5,0 | -34,0% |
| LK Fürstenfeldbruck | 1,4 | 2,7 | -48,1% |
| LK München | 4,0 | 5,4 | -25,9% |
| LK Starnberg | 1,5 | 2,9 | -48,3% |

* Kreis hat keine neuen Zahlen gemeldet.

Wegen sehr niedriger Fallzahlen prozentual scheinbar heftige Schwankungen. Dunkelziffer auch hier Faktor 10&ndash;15. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 0,79.

| Datum | R(t) |
|---|---:|
| 24.05. | 0,92 |
| 25.05. | 0,93 |
| 26.05. | 0,90 |
| 27.05. | 0,88 |
| 28.05. | 0,89 |
| 29.05. | 0,81 |
| 30.05. | 0,79 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 6 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 3 auch gleich (+/-0). Insgesamt somit 9 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 6 | +/-0 | 
| Intensiv, beatmet | 3 | +/-0 | 
| SUMME | 9 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt von vorgestern 95 auf gestern 106. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 20 auf 23 (+3). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 9 auch gleich (+/-0). Insgesamt somit 32 Intensivbetten belegt (+3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 23 | +3 |
| Intensiv, beatmet | 9 | +/-0 |
| SUMME | 32 | +3 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-31 mit Werten bis 2023-05-23**

Die Abwasserbelastung steigt ein wenig, bleibt aber klar unter den Werten im März/April.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230531A.png)

## Varianten München

**Letzte Aktualisierung: 2023-05-23 mit Werten bis 2023-05-01**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)