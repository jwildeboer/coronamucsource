---
layout: default
title: 2023-05-24 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-24 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, kaum Veränderungen gegenüber Vorwoche. Zweite zeigt R(t) Wert für München, der auf 0,94 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230524.png)

Das RKI meldet eine Inzidenz von 7,0 für München. 15 Neue, 1 Nachmeldung für vorgestern. Die gestern genannte Inzidenz bleibt unverändert bei 7,1. Dunkelziffer Faktor 10-20. Kein neuer Todesfall gemeldet, 7 Tage Summe der Todesfälle 2.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 16.05. | 7,8 &rarr; 8,1 |
| 17.05. | 8,0 &rarr; 8,4 |
| 18.05. | 6,7 &rarr; 7,1 |
| 19.05. | 7,3 &rarr; 7,6 |
| 20.05. | 7,3 &rarr; 7,6 |
| 21.05. | 7,3 &rarr; 7,6 |
| 22.05. | 7,7 &rarr; 7,7 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 15 |
| ITS basiert für Bayern | 20 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 2. Damit insgesamt bis jetzt 221 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 24.05. | 23.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 4,5 | 2,6 | +73,1% |
| LK Ebersberg* | 0,7 | 2,8 | -75,0% |
| LK Freising* | 3,9 | 5,0 | -22,0% |
| LK Fürstenfeldbruck | 2,7 | 2,3 | +17,4% |
| LK München | 4,3 | 3,7 | +16,2% |
| LK Starnberg* | 3,7 | 5,9 | -37,3% |

* Kreis hat keine neuen Zahlen gemeldet.

Wegen sehr niedriger Fallzahlen prozentual scheinbar heftige Schwankungen. Dunkelziffer auch hier Faktor 10&ndash;20. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 0,94.

| Datum | R(t) |
|---|---:|
| 17.05. | 0,83 |
| 18.05. | 0,82 |
| 19.05. | 0,87 |
| 20.05. | 0,90 |
| 21.05. | 0,91 |
| 22.05. | 0,95 |
| 23.05. | 0,94 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 5 gleich (+/-0). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 4 auf 3 (-1). Insgesamt somit 8 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 5 | +/-0 | 
| Intensiv, beatmet | 3 | -1 | 
| SUMME | 8 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung steigt von gestern 134 auf heute 176.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 30 auf heute 29 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 11 auf heute 7 (-4). Insgesamt somit 36 Intensivbetten belegt (-5).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 29 | -1 |
| Intensiv, beatmet | 7 | -4 |
| SUMME | 36 | -5 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-16 mit Werten bis 2023-05-09**

Die Abwasserbelastung sinkt wieder deutlich.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230516A.png)

## Varianten München

**Letzte Aktualisierung: 2023-05-23 mit Werten bis 2023-05-01**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)