---
layout: default
title: 2023-05-04 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-04 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der minimal steigt und jetzt bei 0,86 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230504.png)

Das RKI meldet eine Inzidenz von 9,5 für München. 37 Neue, keine Nachmeldungen, 7(!) Korrekturen. Die gestern genannte Inzidenz von 8,5 bleibt unverändert. Dunkelziffer Faktor 10–20. R(t) steigt minimal auf 0,86. 1 neue Todesmeldung. 7 Tage Summe der Todesfälle 6.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 26.04. | 11,0 &rarr; 11,0 |
| 27.04. | 11,2 &rarr; 11,0 |
| 28.04. | 11,0 &rarr; 10,9 |
| 29.04. | 11,0 &rarr; 10,9 |
| 30.04. | 11,0 &rarr; 10,9 |
| 01.05. | 7,6 &rarr; 7,5 |
| 02.05. | 8,5 &rarr; 8,5 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 13 |
| ITS basiert für Bayern | 19 |

## COVID-positive Todesfälle

Es wurde heute 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht auf 6. Damit insgesamt bis jetzt 216 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum vortag in %.

| Kreis | 04.05. | 03.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 5,1 | 4,5 | +13,3% |
| LK Ebersberg | 3,5 | 1,4 | +250% |
| LK Freising* | 3,3 | 4,4 | -25,0% |
| LK Fürstenfeldbruck | 3,2 | 4,1 | -22,0% |
| LK München | 6,9 | 6,6 | +4,5% |
| LK Starnberg | 8,8 | 8,0 | +10,0% |

* Diese Kreise haben keine neuen Fälle gemeldet.

Fallende Inzidenzen im einstelligen Bereich. Dunkelziffer auch hier Faktor 10&ndash;20. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt weiter auf 0,85 und bleibt damit stabil unter 1.

| Datum | R(t) |
|---|---:|
| 27.04. | 0,84 |
| 28.04. | 0,88 |
| 29.04. | 0,89 |
| 30.04. | 0,89 |
| 01.05. | 0,87 |
| 02.05. | 0,85 |
| 03.05. | 0,86 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 6 auf heute 4 (-2). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 8 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | -2 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 8 | -2 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 255 wieder klar höher als gestern (228). Wegen Feiertag war diese Zahl aber eh deutlich zu niedrig und hat sich durch Nachmeldungen schon wieder auf 275 korrigiert.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 39 auf heute 38 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 17 auf heute 16 (-1). Insgesamt somit 54 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 38 | -1 |
| Intensiv, beatmet | 16 | -1 |
| SUMME | 54 | -2 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-03 mit Werten bis 2023-04-25**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230503A.png)