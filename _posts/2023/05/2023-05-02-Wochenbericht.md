---
layout: default
title: 2023-04-24 Wochenbericht KW17
category: Wochenberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110185462711835256
---

![2023-05-02 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Wegen Feiertag erst am Mittwoch wieder neue Zahlen.  Zweite zeigt R(t) Wert für München, der etwas sinkt und jetzt bei 0,87 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230502.png)

In der letzten Woche gab es offiziell insgesamt nur 164 neue COVID Fälle in München. Das sind 16,8% **weniger** als in der Vorwoche (197) - aber wegen Ostern und jetzt Maifeiertag sind die Zahlen ein wenig verzerrt. Die Dunkelziffer schätzen wir auf Faktor 10 bis 18. Es wurden leider aber auch 6 Todesfälle in der letzten Woche gemeldet &mdash deutlich weniger als in der Vorwoche (24).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 197 auf 164. Aber wegen der sehr hohen Dunkelziffer ist diese Zahl alles andere als realistisch. Die um Nachmeldungen korrigierte Inzidenz sank auch ein wenig von 13,2 auf jetzt um 11, aber wegen der Abschaffung vieler Maßnahmen sind diese Zahlen kaum realistisch.

Dunkelziffer Faktor 10 bis 18 sollte man aktuell im Kopf gleich mitrechnen. Damit liegen wir wohl bei einer Basisinzidenz um 200.

Getestet werden eigentlich nur noch Krankenhausneuaufnahmen. Daher ergibt sich eine scheinbare Verschiebung der Inzidenz auf ältere Bevölkerungsgruppen. Wie sich die Infektionen in der allgemeinen Bevölkerung verteilen ist schlicht nicht mehr nachvollziehbar.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 23.04. | 0 | 197 | 13,2 |
| 24.04. | 51 | 181 | 12,1 |
| 25.04. | 39 | 181 | 12,0 |
| 26.04. | 21 | 165 | 11,0 |
| 27.04. | 31 | 164 | 11,0 |
| 28.04. | 22 | 164 | 11,0 |
| 29.04. | 0 | 164 | 11,0 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 18 |
| ITS basiert für Bayern | 18 |

## Reproduktionswert R(t) für München

Unser R(t) ist in der letzen Woche weiter gesunken. Der Ostereffekt zeigte sich noch kurz mit einem R(t) über 1, aber jetzt geht es klar zurück.

| Datum | R(t) |
|---|---:|
| 25.04. | 0,87 |
| 26.04. | 0,85 |
| 27.04. | 0,84 |
| 28.04. | 0,88 |
| 29.04. | 0,89 |
| 30.04. | 0,90 |
| 01.05. | 0,87 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 18. April. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München ein wenig aufwärts, aber es fehlen Zahlen für die letzten 2 Wochen.

![Abwasserwerte München](/images/2023/04/20230425A.png)

## Krankenhauszahlen München

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München sank die Zahl der belegten Intensivbetten weiter und pendelt jetzt knapp über 10. 
Im Vergleich zum Vorjahr klar weniger &mdash; gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 24.04. | 7 | 4 | 11 | -1 | 34 |
| 25.04. | 8 | 2 | 10 | -1 | 37 |
| 26.04. | 8 | 3 | 11 | +1 | 33 |
| 27.04. | 6 | 2 | 8 | -3 | 27 |
| 28.04. | 6 | 3 | 9 | +1 | 24 |
| 29.04. | 7 | 4 | 11 | +2 | 26 |
| 30.04. | 7 | 4 | 11 | +/-0 | 28 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister und die 7 Tage Hospitalisierung.**

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Die (um Nachmeldungen korrigierte) 7 Tage Hospitaliserung ist wieder klar unter dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel) und sinkt weiter. Nur noch kanpp ein Viertel im Vergleich zum Vorjahr.

Bei den Intensivbetten sinken die Zahlen auch &mdash; von 73 auf jetzt 58 &mdash; damit stabil unter 100 Intensivbetten belegt, das hatten wir zuletzt im August 2021!

| Datum | Invasiv beatmet | ICU | Summe | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|---:|
| 24.04. | 29 | 44 | 73 | 414 | 1.365 |
| 25.04. | 27 | 35 | 63 | 391 | 1.489 |
| 26.04. | 27 | 34 | 61 | 354 | 1.442 |
| 27.04. | 22 | 35 | 57 | 330 | 1.368 |
| 28.04. | 19 | 39 | 58 | 309 | 1.297 |
| 29.04. | 19 | 38 | 57 | 299 | 1.279 |
| 30.04. | 17 | 41 | 58 | 293 | 1.239 |

## Impfungen

**Seit Freitag, den 07.04.2023 ist die bisherige Impfregelung ausgelaufen. Damit werden die Impfzahlen vom RKI wohl auch nicht mehr täglich aktualisiert. Damit verharren die Impfzahlen momentan auf Stand 08.04.2023**

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Der Wegfall der Impfregelung scheint zu einem kleinen Run aif die Impfung zu führen, an den Quoten ändert das leider nichts. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 39 | 75,22% (+/-0%) |
| 2. | 79 | 75,14% (+/-0%) |
| 3. | 105 | 59,26% (+/-0%) |
| 4. | 483 | 12,02% (+0,01%) |
| 5. | 347 | 0,85% (+/-0%) |
| 6. | 125 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/04/KW13Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 4 Wochen wurden nachgeliefert. Somit sind wir wieder auf einem halbwegs aktuellen Stand.

**Stand 2023-04-17**

Vor einem Monat führte XBB.1.5 in München noch klar mit 56,61%. Heute liegt sie zwar weiterhin auf Platz 1, aber mit nur noch 38,68%. Auf Platz 2 findet sich die Sammlung der rekombinanten Linien mit 32,96%, wo sich wohl auch die XBB.1.1.6 Variante versteckt, die (noch) nicht separat ausgewiesen wird. Auf Platz 3 ist BA.5 mit einem überraschend starkem Comeback.

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei.

| KW | 13 | 14 | 15 | 16 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,83% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 4,02% | 0,00% | 0,00% | 2,83% |
| Omikron BN.1 | 3,53% | 0,13% | 1,50% | 0,14% |
| Omikron XBB.1.5 | 57,88% | 39,78% | 34,84% | 38,68% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 1,37% | 19,48% | 27,38% | 22,17% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 3,68% | 3,40% | 0,00% | 0,00% |
| Omikron CH.1.1 | 3,42% | 0,00% | 4,13% | 2,57% |
| Rekombinante Linien | 24,44% | 36,55% | 31,46% | 32,96% |
| Andere | 0,83% | 0,66% | 0,69% | 0,65% |

![VoC München](/images/2023/05/KW17VarMUC.png)

## Varianten Bayern

**Aufgrund der sehr geringen Zahl an Sequenzierungen (von 600/Woche jetzt weniger als 20) können wir diesen Teil des Wochenberichts nicht mehr mit gutem Gewissen fortführen.**

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 70,0% nicht wirklich dominant. "Andere" stehen mit 22,2% auf Platz zwei &mdash; man darf vermuten, daß da schon viel XBB.1.6 enthalten ist. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt grad mal 6. Daher haben wir die Zahlen für KW14 nicht in die Tabelle aufgenommen.

| KW% | 10 | 11 | 12 | 13 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,7% | 0,6% | 0,0% | 1,1% |
| Omikron BA.2.75 | 0,9% | 0,0% | 0,5% | 0,0% |
| Omikron BN.1 | 1,3% | 1,4% | 1,4% | 0,0% |
| Omikron XBB.1.5 | 51,5% | 60,8% | 58,4% | 70,0% |
| Omikron BA.4 | 0,0% | 0,0% | 0,0% | 0,0% |
| Omikron BA.5 | 2,5% | 0,9% | 2,4% | 0,0% |
| Omikron BF.7 | 0,0% | 0,3% | 0,5% | 0,0% |
| Omikron BQ.1 | 15,2% | 10,1% | 6,2% | 3,3% |
| Omikron CH 1.1 | 14,3% | 10,1% | 8,6% | 2,2% |
| Rekombinante | 13,6% | 15,3% | 22,0% | 22,2% |
| Andere | 0,0% | 0,5% | 0,0% | 1,2% |
| Sequenzierungen | 356 | 332 | 207 | 90 |

![VoC Bayern](/images/2023/04/KW15Var.png)