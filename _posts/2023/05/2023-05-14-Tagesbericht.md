---
layout: default
title: 2023-05-14 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-14 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell wieder leichter. Zweite zeigt R(t) Wert für München, der auf 0,95 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230514.png)

Das RKI meldet, wie vorhergesagt, eine Inzidenz von 8,5 für München. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. R(t) sinkt auf 0,95. Dunkelziffer Faktor 10-24. 7 Tage Summe der Todesfälle 3.
<!--more-->

## Inzidenzentwicklung

**Wegen Meldepause am Wochenende erst am Dienstag wieder neue Zahlen vom RKI**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 05.05. | 10,4 &rarr; 10,4 |
| 06.05. | 10,4 &rarr; 10,4 |
| 07.05. | 10,4 &rarr; 10,4 |
| 08.05. | 13,1 &rarr; 13,2 |
| 09.05. | 10,7 &rarr; 11,0 |
| 10.05. | 10,4 &rarr; 10,4 |
| 11.05. | 9,3 &rarr; 9,3 |

Der erwartete Sprung nach oben war da, Nachholeffekt wegen Maifeiertag.

Da am Wochenende keine Zahlen an das RKI gemeldet werden, können wir die Inzidenz für Montag schon jetzt berechnen:

| Montag | 8,5 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 23 |
| ITS basiert für Bayern | 24 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 3. Damit insgesamt bis jetzt 219 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Wegen Meldepause am Wochenende erst am Dienstag wieder neue Zahlen vom RKI**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 13.05. | 12.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 5,8 | 5,1 | +13,7% |
| LK Ebersberg* | 3,5 | 6,2 | -43,5% |
| LK Freising | 4,4 | 3,9 | +12,8% |
| LK Fürstenfeldbruck | 5,0 | 5,5 | -9,1% |
| LK München* | 7,4 | 8,6 | -14,0% |
| LK Starnberg | 9,5 | 9,5 | +/-0% |

* Kreis hat keine neuen Zahlen gemeldet.

Wenig Veränderung. Dunkelziffer auch hier Faktor 10&ndash;22. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt wieder auf 0,95 und wird bis Montag auf 0,92 gehen.

| Datum | R(t) |
|---|---:|
| 07.05. | 0,96 |
| 08.05. | 1,06 |
| 09.05. | 1,11 |
| 10.05. | 1,13 |
| 11.05. | 1,13 |
| 12.05. | 1,01 |
| 13.05. | 0,95 |


## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 2 gleich (+/-0). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 6 auf 7  (+1). Insgesamt somit 9 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | -/-0 | 
| Intensiv, beatmet | 7 | +1 | 
| SUMME | 9 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 199 wieder klar niedriger als vorgestern (205) &mdash; der Freitagseffekt. Das RKI hat nach [unserem Hinweis](https://github.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland/issues/24) die Veröffentlichung der Zahlen wieder repariert. Vielen Dank!

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 33 auf heute 31 (-2). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 18 auf heute 19 (+1). Insgesamt somit 50 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 31 | -2 |
| Intensiv, beatmet | 19 | +1 |
| SUMME | 50 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-09 mit Werten bis 2023-05-03**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230509A.png)