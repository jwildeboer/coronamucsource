---
layout: default
title: 2023-05-11 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-05-11 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell wieder leichter. Auch heute noch Sondereffekt wegen Feiertag letzte Woche Montag. Zweite zeigt R(t) Wert für München, der auf 1,13 steigt, aber morgen wieder sinken sollte. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/05/20230511.png)

Das RKI meldet eine Inzidenz von 10,4 für München. 27 Neue, 5 Nachmeldungen, 1 Korrektur. Die Inzidenz von gestern korrigiert sich von 10,7 auf 11,0. Dunkelziffer Faktor 10-22. 1 neuer Todesfall . 7 Tage Summe der Todesfälle 1. R(t) bleibt mit 1,13 über 1. Immer noch Ausgleichsbewegung wegen Feiertag in der letzten Woche.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 03.05. | 9,5 &rarr; 9,5 |
| 04.05. | 9,8 &rarr; 9,9 |
| 05.05. | 10,4 &rarr; 10,4 |
| 06.05. | 10,4 &rarr; 10,4 |
| 07.05. | 10,4 &rarr; 10,4 |
| 08.05. | 13,1 &rarr; 13,2 |
| 09.05. | 10,7 &rarr; 11,0 |

Der erwartete Sprung nach oben ist da, Nachholeffekt wegen Maifeiertag.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 16 |
| ITS basiert für Bayern | 22 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 1. Damit insgesamt bis jetzt 217 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 11.05. | 10.05. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 5,8 | 6,4 | -9,4% |
| LK Ebersberg | 6,2 | 6,2 | +/-0% |
| LK Freising* | 5,5 | 6,1 | -9,8% |
| LK Fürstenfeldbruck | 5,0 | 4,1 | +22,0% |
| LK München | 7,4 | 9,1 | -18,7% |
| LK Starnberg | 9,5 | 9,5 | +/-0% |

* Kreis hat keine neuen Zahlen gemeldet.

Wenig Veränderung. Dunkelziffer auch hier Faktor 10&ndash;22. 

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 1,10 und bleibt damit über 1.

| Datum | R(t) |
|---|---:|
| 04.05. | 0,87 |
| 05.05. | 0,91 |
| 06.05. | 0,95 |
| 07.05. | 0,96 |
| 08.05. | 1,06 |
| 09.05. | 1,11 |
| 10.05. | 1,13 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 6 auf heute 3 (-3). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 4 auf heute 5 (+1). Insgesamt somit 8 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | -3 | 
| Intensiv, beatmet | 5 | +1 | 
| SUMME | 8 | -2 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung war gestern mit 216 wieder etwas niedriger als vorgestern (232). Das RKI hat nach [unserem Hinweis](https://github.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland/issues/24) die Veröffentlichung der Zahlen wieder repariert. Vielen Dank!

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 42 auf heute 35 (-7). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 20 auf heute 19 (-1). Insgesamt somit 54 Intensivbetten belegt (-8).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 35 | -7 |
| Intensiv, beatmet | 19 | -1 |
| SUMME | 54 | -8 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-05-09 mit Werten bis 2023-05-03**

Die Abwasserbelastung sinkt wieder deutlich. Die befürchtete Nach-Osterwelle bzw. Spitze durch Baumesse ist zwar sichtbar, fällt aber deutlich kleiner aus als wir dachten.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/05/20230509A.png)