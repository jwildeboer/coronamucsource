---
layout: default
title: 2023-04-30 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-30 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Wegen Feiertag erst am Mittwoch wieder neue Zahlen.  Zweite zeigt R(t) Wert für München, der wieder etwas steigt und jetzt bei 0,89 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230430.png)

Das RKI meldet, wie vorhergesagt, eine Inzidenz von 11,0 für Münchden. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. R(t) steht auf 0,89. Dunkelziffer Faktor 10-18. 7 Tage Summe der Todesfälle 6.
<!--more-->

## Inzidenzentwicklung

**Bis Mittwoch keine Aktualisierung möglich, da erst ab Dienstag wieder an das RKI gemeldet wird.**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 21.04. | 13,2 &rarr; 13,2 |
| 22.04. | 13,2 &rarr; 13,2 |
| 23.04. | 13,2 &rarr; 13,2 |
| 24.04. | 12,1 &rarr; 12,2 |
| 25.04. | 12,0 &rarr; 12,2 |
| 26.04. | 11,0 &rarr; 11,1 |
| 27.04. | 11,2 &rarr; 11,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 19 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

**Bis Mittwoch keine Aktualisierung möglich, da erst ab Dienstag wieder an das RKI gemeldet wird.**

Es wurden heute 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 6. Damit insgesamt bis jetzt 214 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Bis Mittwoch keine Aktualisierung möglich, da erst ab Dienstag wieder an das RKI gemeldet wird.**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 29.04. | 28.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau* | 5,8 | 7,1 | -18,3% |
| LK Ebersberg | 4,2 | 5,5 | -23,6% |
| LK Freising* | 5,0 | 6,6 | -24,2% |
| LK Fürstenfeldbruck | 7,3 | 8,7 | -16,1% |
| LK München | 8,9 | 9,4 | -5,3% |
| LK Starnberg | 11,7 | 15,4 | -24,0% |

Fallend. Dunkelziffer auch hier Faktor 10&ndash;18. * Die Kreise Dachau und Freising haben keine neuen Fälle gemeldet.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt wieder auf 0,89.

| Datum | R(t) |
|---|---:|
| 23.04. | 0,95 |
| 24.04. | 0,88 |
| 25.04. | 0,87 |
| 26.04. | 0,85 |
| 27.04. | 0,84 |
| 28.04. | 0,88 |
| 29.04. | 0,89 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 3 auf heute 4 (+1). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 6 auf heute 7 (+1). Insgesamt somit 11 Intensivbetten belegt (+2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | +1 | 
| Intensiv, beatmet | 7 | +1 | 
| SUMME | 11 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 299 wieder etwas niedriger als gestern (309). Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 39 auf heute 38 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 19 gleich (+/-0). Insgesamt somit 57 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 38 | -1 |
| Intensiv, beatmet | 19 | +/-0 |
| SUMME | 57 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-04-25 mit Werten bis 2023-04-18**

Die Abwasserbelastung sinkt nicht mehr. Das war zu erwarten. Neben Ferienrückkehrer gab es auch noch die Baumesse in München. Die Zahlen gehen jetzt bis 2023-04-18, laufen also um eine Woche hinterher.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230425A.png)