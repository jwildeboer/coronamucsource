---
layout: default
title: 2023-04-14 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110197195003261067
---

![2023-04-14 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, weniger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der jetzt auf 0,74 sinkt. Dritte zeigt Inzidenz, die zwar stabil weit unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230414.png)

Das RKI meldet eine Inzidenz von 12,2. 30 Neue, 15 Nachmeldungen. Davon 4 für vorgestern. Die gestern genannte Inzidenz von 12,0 steigt dadurch auf 12,3. R(t) fällt weiter auf 0,74. Dunkelziffer Faktor 9–33. 11 Tote, 7 Tage Summe der Toten somit 19.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 06.04. | 21,2 &rarr; 21,8 |
| 07.04. | 19,3 &rarr; 20,0 |
| 08.04. | 19,3 &rarr; 20,0 |
| 09.04. | 19,3 &rarr; 20,0 |
| 10.04. | 13,6 &rarr; 14,3 |
| 11.04. | 13,1 &rarr; 13,1 |
| 12.04. | 12,0 &rarr; 12,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 33 |
| ITS basiert für Bayern | 33 |

## COVID-positive Todesfälle

Es wurden heute wieder 11 neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle stegt damit auf 19. Damit insgesamt bis jetzt 178 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 14.04. | 13.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 7,1 | 6,4 | +10,9% |
| LK Ebersberg | 7,6 | 5,5 | +38,2% |
| LK Freising | 6,6 | 7,7 | -14,3% |
| LK Fürstenfeldbruck | 6,4 | 7,8 | -17,9% |
| LK München | 10,3 | 9,4 | +9,6% |
| LK Starnberg | 11,7 | 9,5 | +23,2% |

Sehr geringe Fallzahlen, daher heftigere Schwankungen. Kaum noch realistische Zahlen. Dunkelziffer auch hier Faktor 9&ndash;33.

## Reproduktionswert R(t) für München

Unser R(t) fällt weiter auf 0,74.

| Datum | R(t) |
|---|---:|
| 07.04. | 0,94 |
| 08.04. | 0,95 |
| 09.04. | 0,95 |
| 10.04. | 0,94 |
| 11.04. | 0,87 |
| 12.04. | 0,80 |
| 13.04. | 0,74 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern mit 14 auf jetzt 11 (-3). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 12 auf jetzt 9 (-3). Insgesamt somit 20 Intensivbetten belegt (-6).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 11 | -3 | 
| Intensiv, beatmet | 9 | -3 | 
| SUMME | 20 | -6 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung liegt heute bei 380, etwas höher als gestern (370), aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 66 auf heute 54 (-12). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 44 auf heute 42 (-2). Insgesamt somit 96 Intensivbetten belegt (-14).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 54 | -12 |
| Intensiv, beatmet | 42 | -2 |
| SUMME | 96 | -14 |

Zum ersten mal seit dem 22.08.2021(!) bayernweit unter 100 Intensivbetten mit Covid-positiven Patienten belegt. 601 Tage lang waren mehr als 100 Intensivbetten belegt. In der Spitze waren es 1.084 am 01.12.2021.

## Abwasserbelastung München

Eher stabile Tendenz, Daten gehen aber nur bis 2023-04-04. Auch heute keine Aktualisierung, obwohl man werktägliche Updates anstrebt. Jetzt 7 Tage ohne neue Zahlen. Schade.

![Abwasserwerte München](/images/2023/04/20230407A.png)