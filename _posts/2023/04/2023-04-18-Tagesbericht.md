---
layout: default
title: 2023-04-18 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110221238757839670
---

![2023-04-18 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, noch keine Bewertung möglich. Zweite zeigt R(t) Wert für München, der jetzt auf 0,92 steigt - immer noch Nachholeffket wegen Ostern. Dritte zeigt Inzidenz, die zwar weiter unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230418.png)

Das RKI meldet eine Inzidenz von 19,5 für Münchden. 64 Neue, 11 Nachmeldungen, davon 3 für letzten Freitag. Die Wochenendinzidenz von 14,9 erhöht sich durch Nachmeldungen auf 15,2. Leider wieder 5 neue Todesfälle, 7 Tage Summe 30. R(t) steigt auf 0,92.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 10.04. | 13,6 &rarr; 14,3 |
| 11.04. | 13,1 &rarr; 13,2 |
| 12.04. | 12,0 &rarr; 12,4 |
| 13.04. | 12,2 &rarr; 13,0 |
| 14.04. | 14,9 &rarr; 15,2 |
| 15.04. | 14,9 &rarr; 15,2 |
| 16.04. | 14,9 &rarr; 15,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 12 |
| ITS basiert für Bayern | 21 |

## COVID-positive Todesfälle

Es wurden heute wieder 5 neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle stegt damit auf 30. Damit insgesamt bis jetzt 189 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 18.04. | 15.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 16,7 | 10,9 | +53,2% |
| LK Ebersberg | 11,1 | 8,3 | +33,7% |
| LK Freising | 6,1 | 8,3 | -26,5% |
| LK Fürstenfeldbruck | 9,2 | 8,7 | +5,7% |
| LK München | 11,1 | 11,1 | +/-0% |
| LK Starnberg | 16,1 | 13,2 | +22,0% |

Sehr geringe Fallzahlen, daher heftigere Schwankungen. Nachholeffekt durch Ostern. Kaum noch realistische Zahlen. Dunkelziffer auch hier Faktor 9&ndash;21.

## Reproduktionswert R(t) für München

Unser R(t) steigt auf 0,77.

| Datum | R(t) |
|---|---:|
| 09.04. | 0,95 |
| 10.04. | 0,94 |
| 11.04. | 0,87 |
| 12.04. | 0,80 |
| 13.04. | 0,75 |
| 14.04. | 0,74 |
| 15.04. | 0,77 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 6 auf heute 5 (-1). Die Zahl der invasiv Beatmeten bleibtt im Vergleich zu gestern mit 10 gleich (+/-0). Insgesamt somit 15 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 5 | -1 | 
| Intensiv, beatmet | 10 | +/-0 | 
| SUMME | 15 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung springt heute auf 550, das sind 43 mehr als gestern (407), aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 63 auf heute 70 (+7). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 43 auf heute 41 (-2). Insgesamt somit 111 Intensivbetten belegt (+5).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 70 | +7 |
| Intensiv, beatmet | 41 | -2 |
| SUMME | 111 | +5 |

Damit seit 2 Tagen wieder mehr als 100 Intensivbetten bayernweit mit Corona-positive Patienten belegt.

## Abwasserbelastung München

Stabil fallende Tendenz, Daten gehen bis 2023-04-11.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niedrschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230415A.png)