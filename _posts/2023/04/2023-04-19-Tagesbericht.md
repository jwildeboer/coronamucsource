---
layout: default
title: 2023-04-19 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-19 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, noch keine Bewertung möglich, aber tendenziell weniger. Zweite zeigt R(t) Wert für München, der jetzt auf 0,97 steigt - immer noch Nachholeffekt wegen Ostern. Dritte zeigt Inzidenz, die zwar weiter unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230419.png)

Das RKI meldet eine Inzidenz von 14,5 für Münchden. 29 Neue, 8 Nachmeldungen, davon 3 für vorgestern. Die gestern genannte Inzidenz von 19,5 erhöht sich durch Nachmeldungen auf 19,7. Leider wieder 3 neue Todesfälle, 7 Tage Summe 33. R(t) steigt weiter auf 0,97.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 11.04. | 13,1 &rarr; 13,2 |
| 12.04. | 12,0 &rarr; 12,4 |
| 13.04. | 12,2 &rarr; 13,0 |
| 14.04. | 14,9 &rarr; 15,2 |
| 15.04. | 14,9 &rarr; 15,2 |
| 16.04. | 14,9 &rarr; 15,2 |
| 17.04. | 19,5 &rarr; 19,7 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 11 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurden heute wieder 3 neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle stegt damit auf 33. Damit insgesamt bis jetzt 192 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 19.04. | 18.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 14,2 | 16,7 | -15,0% |
| LK Ebersberg | 10,4 | 11,1 | -6,3% |
| LK Freising | 5,0 | 6,1 | -18,0% |
| LK Fürstenfeldbruck | 9,6 | 9,2 | +4,3% |
| LK München | 11,1 | 11,1 | +/-0% |
| LK Starnberg | 11,7 | 16,1 | -27,3% |

Eher fallende Tendenz. Dunkelziffer auch hier Faktor 9&ndash;18.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,97.

| Datum | R(t) |
|---|---:|
| 12.04. | 0,80 |
| 13.04. | 0,75 |
| 14.04. | 0,75 |
| 15.04. | 0,77 |
| 16.04. | 0,81 |
| 17.04. | 0,92 |
| 18.04. | 0,97 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 5 auf heute nur noch 2 (-3). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 10 auf 9 (-1). Insgesamt somit nur noch 11 Intensivbetten belegt (-4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | -3 | 
| Intensiv, beatmet | 9 | -1 | 
| SUMME | 11 | -4 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung fällt heute auf 432, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 70 auf heute 57 (-13). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 41 auf heute 39 (-2). Insgesamt somit 96 Intensivbetten belegt (-15).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 56 | -13 |
| Intensiv, beatmet | 39 | -2 |
| SUMME | 96 | -15 |

## Abwasserbelastung München

Stabil fallende Tendenz, Daten gehen bis 2023-04-11. Auch heute wieder keine Aktualisierung.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230415A.png)