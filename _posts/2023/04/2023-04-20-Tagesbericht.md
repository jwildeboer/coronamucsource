---
layout: default
title: 2023-04-20 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-20 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, noch keine Bewertung möglich, aber tendenziell weniger. Zweite zeigt R(t) Wert für München, der jetzt auf 1,04 steigt - immer noch Nachholeffekt wegen Ostern. Dritte zeigt Inzidenz, die zwar weiter unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230420.png)

Das RKI meldet eine Inzidenz von 14,5 für Münchden. 36 Neue, 13 Nachmeldungen, davon 12 für vorgestern. Die gestern genannte Inzidenz von 14,5 erhöht sich durch Nachmeldungen auf 15,1. Leider wieder 10(!) neue Todesfälle, 7 Tage Summe 35. R(t) steigt weiter auf 1,04. Dunkelziffer Faktor 10-18.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 12.04. | 12,0 &rarr; 12,4 |
| 13.04. | 12,2 &rarr; 13,0 |
| 14.04. | 14,9 &rarr; 15,2 |
| 15.04. | 14,9 &rarr; 15,2 |
| 16.04. | 14,9 &rarr; 15,2 |
| 17.04. | 19,5 &rarr; 19,7 |
| 18.04. | 14,5 &rarr; 15,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 10 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurden heute wieder 10(!) neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle stegt damit auf 35. Damit insgesamt bis jetzt 202 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 20.04. | 19.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 13,5 | 14,2 | -4,9% |
| LK Ebersberg | 8,3 | 10,4 | -20,2% |
| LK Freising | 7,2 | 5,0 | +44% |
| LK Fürstenfeldbruck | 11,4 | 9,6 | +18,8% |
| LK München | 10,3 | 11,1 | -7,2% |
| LK Starnberg | 10,2 | 11,7 | -12,8% |

Uneinheitlich. Dunkelziffer auch hier Faktor 10&ndash;18.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 1,04. Das 4 Tage R(t) sinkt aber bereits wieder.

| Datum | R(t) |
|---|---:|
| 13.04. | 0,75 |
| 14.04. | 0,75 |
| 15.04. | 0,77 |
| 16.04. | 0,81 |
| 17.04. | 0,92 |
| 18.04. | 0,98 |
| 19.04. | 1,04 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 2 gleich. Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 9 auch gleich.. Insgesamt somit weiter nur noch 11 Intensivbetten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | +/-0 | 
| Intensiv, beatmet | 9 | +/-0 | 
| SUMME | 11 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 434 nahezu identisch zi gestern (432), aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 57 auf heute 56 (-1). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 39 auf heute 42 (+3). Insgesamt somit 98 Intensivbetten belegt (+2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 56 | -1 |
| Intensiv, beatmet | 42 | +3 |
| SUMME | 98 | +2 |

## Abwasserbelastung München

Stabil fallende Tendenz, Daten gehen bis 2023-04-11. Auch heute wieder keine Aktualisierung.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230415A.png)