---
layout: default
title: 2023-04-22 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-22 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der wieder auf 1,03 sinkt - immer noch Nachholeffekt wegen Ostern. Dritte zeigt Inzidenz, die zwar weiter unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230422.png)

Das RKI meldet eine Inzidenz von 13,2 für Münchden. 23 Neue, keine Nachmeldungen. Die gestern genannte Inzidenz von 13,8 bleibt unverändert. Leider wieder 2 neue Todesfälle, 7 Tage Summe 24. R(t) sinkt wieder auf 1,03. Dunkelziffer bleibt Faktor 10-18.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 14.04. | 14,9 &rarr; 15,2 |
| 15.04. | 14,9 &rarr; 15,2 |
| 16.04. | 14,9 &rarr; 15,2 |
| 17.04. | 19,5 &rarr; 19,7 |
| 18.04. | 14,5 &rarr; 15,1 |
| 19.04. | 14,5 &rarr; 14,5 |
| 20.04. | 13,8 &rarr; 13,8 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 14 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurden heute wieder 2 neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle geht damit auf 24. Damit insgesamt bis jetzt 208 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 22.04. | 21.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 12,9 | 14,2 | -9,2% |
| LK Ebersberg | 11,1 | 9,0 | +23,3% |
| LK Freising | 8,8 | 7,2 | +22,2% |
| LK Fürstenfeldbruck | 13,7 | 12,4 | +10,5% |
| LK München | 10,0 | 9,1 | +9,9% |
| LK Starnberg | 13,9 | 10,2 | +36,3% |

Steigend. Dunkelziffer auch hier Faktor 10&ndash;18.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt wieder auf 1,03. Das 4 Tage R(t) sinkt kräftig.

| Datum | R(t) |
|---|---:|
| 15.04. | 0,77 |
| 16.04. | 0,81 |
| 17.04. | 0,92 |
| 18.04. | 0,98 |
| 19.04. | 1,04 |
| 20.04. | 1,10 |
| 21.04. | 1,03 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 2 auf heute 3 (+1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 9 gleich. Insgesamt somit weiter nur noch 12 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | +1 | 
| Intensiv, beatmet | 9 | +/-0 | 
| SUMME | 12 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 386 deutlich niedriger als gestern (416) &mdash; der Freitagseffekt. Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 54 auf heute 47 (-7). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 39 auf heute 38 (-1). Insgesamt somit 85 Intensivbetten belegt (-8).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 47 | -7 |
| Intensiv, beatmet | 38 | -1 |
| SUMME | 85 | -8 |

## Abwasserbelastung München

Stabil fallende Tendenz, Daten gehen aber weiterhin nur bis 2023-04-11. Auch heute wieder keine Aktualisierung.

Laut Auskunft des LGL gab es diese Woche Probleme mit den Labormeldungen, nächste Woche sollen aber wieder Daten fließen.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230415A.png)