---
layout: default
title: 2023-04-15 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110202366837867855
---

![2023-04-15 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, weniger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der jetzt bei 0,74 steht. Dritte zeigt Inzidenz, die zwar stabil weit unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230415.png)

Das RKI meldet eine Inzidenz von 14,9. 30 Neue, 11 Nachmeldungen. Davon 10 für vorgestern. Die gestern genannte Inzidenz von 12,2 steigt dadurch auf 12,9. R(t) bleibt bei 0,74. Dunkelziffer Faktor 9–30. 6 Tote, 7 Tage Summe der Toten somit 25.
<!--more-->

Das die Inzidenz wieder steigt ist eher ein Nachholeffekt wegen der fehlenden Zahlen durch Ostern. Bis mitte der nächsten Woche werden diese Effekte die Zahlen verzerren. Erst dann werden wir sehen wo die Reise hingeht.

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 07.04. | 19,3 &rarr; 20,0 |
| 08.04. | 19,3 &rarr; 20,0 |
| 09.04. | 19,3 &rarr; 20,0 |
| 10.04. | 13,6 &rarr; 14,3 |
| 11.04. | 13,1 &rarr; 13,2 |
| 12.04. | 12,0 &rarr; 12,4 |
| 13.04. | 12,2 &rarr; 12,9 |

Da heute und morgen keine Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 14,9 |
| Montag | 14,9 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 23 |
| ITS basiert für Bayern | 30 |

## COVID-positive Todesfälle

Es wurden heute wieder 6 neue Todesfälle (nach)gemeldet. Die 7 Tage Summe der Todesfälle stegt damit auf 25. Damit insgesamt bis jetzt 184 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 15.04. | 14.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 10,9 | 7,1 | +53,5% |
| LK Ebersberg | 8,3 | 7,6 | +9,2% |
| LK Freising | 8,3 | 6,6 | +25,8% |
| LK Fürstenfeldbruck | 8,7 | 6,4 | +35,9% |
| LK München | 11,1 | 10,3 | +7,8% |
| LK Starnberg | 13,2 | 11,7 | +12,8% |

Sehr geringe Fallzahlen, daher heftigere Schwankungen. Nachholeffekt durch Ostern. Kaum noch realistische Zahlen. Dunkelziffer auch hier Faktor 9&ndash;30.

## Reproduktionswert R(t) für München

Unser R(t) bleibt bei 0,74.

| Datum | R(t) |
|---|---:|
| 08.04. | 0,95 |
| 09.04. | 0,95 |
| 10.04. | 0,94 |
| 11.04. | 0,87 |
| 12.04. | 0,80 |
| 13.04. | 0,75 |
| 14.04. | 0,74 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern mit 11 auf jetzt 8 (-3). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern mit 9 auf jetzt 10 (+1). Insgesamt somit 18 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 8 | -3 | 
| Intensiv, beatmet | 10 | +1 | 
| SUMME | 18 | -2 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung liegt heute bei 449, höher als gestern (380), aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 54 auf heute 50 (-4). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 42 auf heute 43 (+1). Insgesamt somit 93 Intensivbetten belegt (-3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 50 | -4 |
| Intensiv, beatmet | 43 | +1 |
| SUMME | 93 | -3 |

Zum ersten mal seit dem 22.08.2021(!) bayernweit unter 100 Intensivbetten mit Covid-positiven Patienten belegt. 601 Tage lang waren mehr als 100 Intensivbetten belegt. In der Spitze waren es 1.084 am 01.12.2021.

## Abwasserbelastung München

Stabil fallende Tendenz, Daten gehen bis 2023-04-11.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niedrschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230415A.png)