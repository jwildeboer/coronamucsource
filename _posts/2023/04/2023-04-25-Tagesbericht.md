---
layout: default
title: 2023-04-25 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-25 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der wieder klar unter 1 sinkt. Dritte zeigt Inzidenz, die zwar weiter unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230425.png)

Das RKI meldet eine Inzidenz von 12,1 für Münchden. 50 Neue, 1 Nachmeldung für Freitag. Keine Änderung der Inzidenzen von Freitag bis Sonntag. Dunkelziffer Faktor 10-18. R(t) fällt weiter auf 0,88. Eine neue Todesmeldung. 7 Tage Summe der Todesfälle 19.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 17.04. | 19,5 &rarr; 19,7 |
| 18.04. | 14,5 &rarr; 15,1 |
| 19.04. | 14,5 &rarr; 14,5 |
| 20.04. | 13,8 &rarr; 13,8 |
| 21.04. | 13,2 &rarr; 13,2 |
| 22.04. | 13,2 &rarr; 13,2 |
| 23.04. | 13,2 &rarr; 13,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 14 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurde heute wieder 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht damit auf 19. Damit insgesamt bis jetzt 209 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 25.04. | 22.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 10,3 | 12,9 | -20,2% |
| LK Ebersberg | 8,3 | 11,1 | -25,2% |
| LK Freising | 8,3 | 8,8 | -5,7% |
| LK Fürstenfeldbruck | 13,3 | 13,7 | -2,9% |
| LK München | 8,3 | 10,0 | -17% |
| LK Starnberg | 13,9 | 13,9 | +/-0% |

Fallende Zahlen. Dunkelziffer auch hier Faktor 10&ndash;18.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt mit 0,88 wieder klar unter 1.
| Datum | R(t) |
|---|---:|
| 18.04. | 0,98 |
| 19.04. | 1,04 |
| 20.04. | 1,10 |
| 21.04. | 1,03 |
| 22.04. | 0,99 |
| 23.04. | 0,95 |
| 24.04. | 0,88 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 4 gleich (+/-0). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 8 auf heute 7 (-1). Insgesamt somit nur noch 11 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | +/-0 | 
| Intensiv, beatmet | 7 | -1 | 
| SUMME | 11 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 336 wieder niedriger als gestern 363). Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 50 auf heute 44 (-6). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 34 auf heute 29 (-5). Insgesamt somit 73 Intensivbetten belegt (-11).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 44 | -6 |
| Intensiv, beatmet | 29 | -5 |
| SUMME | 73 | -11 |

## Abwasserbelastung München

Die Abwasserbelastung sinkt nicht mehr. Das war zu erwarten. Neben Ferienrückkehrer gab es auch noch doe Baumesse in München. Die Zahlen gehen jetzt bis 2023-04-18, laufen also um eine Woche hinterher.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230425A.png)