---
layout: default
title: 2023-04-26 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-26 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der wieder unter 1 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230426.png)

Das RKI meldet eine Inzidenz von 12,0 für Münchden. 36 Neue, 3 Nachmeldungen, davon 1 für Vorgestern. Die gestern genannte Inzidenz von 12,1 erhöht sich dadurch auf 12,2. Dunkelziffer Faktor 10-15. R(t) fällt weiter auf 0,86. Eine neue Todesmeldung. 7 Tage Summe der Todesfälle 18.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 18.04. | 14,5 &rarr; 15,1 |
| 19.04. | 14,5 &rarr; 14,5 |
| 20.04. | 13,8 &rarr; 13,8 |
| 21.04. | 13,2 &rarr; 13,2 |
| 22.04. | 13,2 &rarr; 13,2 |
| 23.04. | 13,2 &rarr; 13,2 |
| 24.04. | 12,1 &rarr; 12,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 14 |
| ITS basiert für Bayern | 15 |

## COVID-positive Todesfälle

Es wurde heute wieder 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht damit auf 18. Damit insgesamt bis jetzt 210 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 26.04. | 25.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 9,0 | 10,3 | -12,6% |
| LK Ebersberg | 10,4 | 8,3 | +25,3% |
| LK Freising | 7,2 | 8,3 | -13,3% |
| LK Fürstenfeldbruck | 12,4 | 13,3 | -6,8% |
| LK München | 10,0 | 8,3 | +20,5% |
| LK Starnberg | 16,1 | 13,9 | +15,8% |

Springt wild. Dunkelziffer auch hier Faktor 10&ndash;15.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt mit 0,88 wieder klar unter 1.
| Datum | R(t) |
|---|---:|
| 19.04. | 1,04 |
| 20.04. | 1,10 |
| 21.04. | 1,03 |
| 22.04. | 0,99 |
| 23.04. | 0,95 |
| 24.04. | 0,88 |
| 25.04. | 0,86 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern mit 4 auf heute 2 (-2). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 7 auf heute 8 (+1). Insgesamt somit nur noch 10 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | -2 | 
| Intensiv, beatmet | 8 | +1 | 
| SUMME | 10 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 336 wieder niedriger als gestern 363). Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 44 auf heute 39 (-9). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 29 auf heute 27 (-2). Insgesamt somit 62 Intensivbetten belegt (-11).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 39 | -9 |
| Intensiv, beatmet | 27 | -2 |
| SUMME | 62 | -11 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-04-25 mit Werten bis 2023-04-18**

Die Abwasserbelastung sinkt nicht mehr. Das war zu erwarten. Neben Ferienrückkehrer gab es auch noch doe Baumesse in München. Die Zahlen gehen jetzt bis 2023-04-18, laufen also um eine Woche hinterher.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der eidentifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230425A.png)