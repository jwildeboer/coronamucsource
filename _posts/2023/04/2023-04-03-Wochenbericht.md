---
layout: default
title: 2023-04-03 Wochenbericht KW13
category: Wochenberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110134987067266598
---

![2023-04-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert sinkt wieder und steht jetzt auf 0,76. Neue Zahlen vom RKI aber erst am Dienstag. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl weiter klar sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230403.png)

In der letzten Woche gab es offiziell insgesamt nur 313 neue COVID Fälle in München. Das sind 41,9% **weniger** als in der Vorwoche (539). Leider aber auch 7 Tote mit COVID Nachweis in den letzten 7 Tagen (alle am letzten Samstag gemeldet), wieder mehr als in der Vorwoche (3). Die Dunkelziffer schätzen wir zwischen Faktor 8 bis 23. Das LGL hat die IVENA Zahl heute NICHT aktualisiert.
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 539 auf 313. Die um Nachmeldungen korrigierte Inzidenz fiel von 36,2 auf 21,0, Aber wegen der Abschaffung der Maßnahmen sind diese Zahlen kaum realistisch. Dunkelziffer Faktor 8 bis 23 sollte man im Kopf gleich mitrechnen. Getestet werden eigentlich nur noch Krankenhausneuaufnahmen. Daher ergibt sich eine scheinbare Verschiebung der Inzidenz auf ältere Bevölkerungsgruppen. Wie sich die Infektionen in der allgemeinen Bevölkerung verteilen ist schlicht nicht mehr nachvollziehbar.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 26.03. | 0 | 539 | 36,2 |
| 27.03. | 105 | 478 | 32,1 |
| 28.03. | 53 | 404 | 27,2 |
| 29.03. | 61 | 396 | 26,6 |
| 30.03. | 67 | 376 | 25,3 |
| 31.03. | 27 | 313 | 21,0 |
| 01.04. | 0 | 313 | 21,0 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 23 |

## Reproduktionswert R(t) für München

Unser R(t) sin kt jetzt auch wieder stabil. Von 0,93 auf aktuell 0,76.

| Datum | R(t) |
|---|---:|
| 27.03. | 0,93 |
| 28.03. | 0,89 |
| 29.03. | 0,87 |
| 30.03. | 0,84 |
| 31.03. | 0,81 |
| 01.04. | 0,79 |
| 02.04. | 0,76 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 21. März. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München jetzt klar sinkende Zahlen.

![Abwasserwerte München](/images/2023/04/KW13Abwasser.png)

## Krankenhauszahlen München

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München blieb die Zahl der belegten Intensivbetten eher stabil um oder etwas über 30. 
Im Vergleich zum Vorjahr nur die Hälfte - gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 27.03. | 12 | 23 | 35 | +4 | 61 |
| 28.03. | 12 | 18 | 30 | -5 | 59 |
| 29.03. | 11 | 20 | 31 | +1 | 61 |
| 30.03. | 11 | 19 | 30 | -1 | 68 |
| 31.03. | 11 | 19 | 30 | -/-0 | 61 |
| 01.04. | 11 | 18 | 29 | -1 | 59 |
| 02.04. | 11 | 20 | 31 | +2 | 57 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche sank die IVENA Zahl der insgesamt belegten Krankenhausbetten mit COVID-positiv getesteten Patienten weiter, aber **leider hat das LGL heute keine neue Zahl genannt**, daher können wir nur bis zum Stand letzten Freitag (2023-03-31) berichten.

Die (um Nachmeldungen korrigierte) 7 Tage Hospitaliserung sinkt wieder unter dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel), es werden aber noch einige Nachmeldungen kommen. Trotzdem &mdash; klar bessere Zahlen als im Vorjahr.

Bei den Intensivbetten sinken die Zahlen zwar auch &ndash; von 167 auf jetzt 153 &mdash; dort aber langsamer.

| Datum | Intensiv | IVENA<br>Gesamt | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|
| 27.03. | 167 | 2.024 | 1.106 | 2.308 |
| 28.03. | 165 | 1.927 | 1.043 | 2.285 |
| 29.03. | 156 | 1.823 | 964 | 2.255 |
| 30.03. | 154 | 1.737 | 871 | 2.252 |
| 31.03. | 155 | --- | 779 | 2.205 |
| 01.04. | 156 | --- | 757 | 2.145 |
| 02.04. | 153 | --- | 725 | 2.119 |

## Impfungen

**Am Freitag, den 07.04.2023 läuft die bisherige Impfregelung aus. Danach wird nur noch in Arztpraxen geimpft. Die Kostenübernahme durch Krankenkassen ist noch NICHT abschliessend geregelt. Daher: diese Woche noch impfen lassen!**

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Der Wegfall der Impfregelung scheint zu einem kleinen Run aif die Impfung zu führen, an den Quoten ändert das leider nichts. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 537 | 75,22% (+/-0%) |
| 2. | 627 | 75,14% (+/-0%) |
| 3. | 589 | 59,26% (+0,01%) |
| 4. | 1.023 | 12,01% (+/-0%) |
| 5. | 531 | 0,85% (+/-0%) |
| 6. | 178 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/04/KW13Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München &mdash; wieder keine Aktualisierung

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München bleiben auf dem Stand der vorletzten Woche, eine Aktualisierung gab es auch in dieser Woche leider noch nicht. Die Daten gehen somit nur bis zum 20. Februar.

**Stand 2023-02-20, seit zwei Wochen keine Aktualisierung**

In München führt XBB.1.5 zwar klar mit 44,73%, so richtig dominant ist das aber auch nicht. BQ.1 liegt mit 20,16% auf Platz zwei, BA.2 mit 14,88% auf platz drei. CH.1.1 bleibt weiter unter 20%.

| KW  | 4  | 5  | 6  | 7 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 3,01% | 12,47% | 20,13% | 14,88% |
| Omikron BA.2.75 | 11,58% | 10,25% | 10,04% | 0,00% |
| Omikron BN.1 | 3,32% | 2,09% | 1,77% | 4,77% | 
| Omikron XBB.1.5 | 27,92% | 39,84% | 27,18% | 44,73% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 24,88% | 2,33% | 12,22% | 0,00% |
| Omikron BF.7 | 0,13% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 19,32% | 17,70% | 14,16% | 20,16% |
| Omikron CH.1.1 | 6,97% | 7,41% | 3,71% | 14,43% |
| Rekombinante Linien | 1,72% | 0,55% | 10,14% | 0,62% |
| Andere | 1,15% | 7,36% | 0,65% | 0,41% |

![VoC München](/images/2023/03/KW11VarMuc.png)

## Varianten Bayern

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 60,2% nicht wirklich dominant. BA.2 und CH 1.1 stehen mit 12,4% auf Platz zwei, BQ.1 mit 8% auf Platz drei. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt grad mal 113.

In der letzten Woche (KW12) wurden bisher nur 13 Sequenzierungen dokumentiert, daher geben wir die Zahlen nicht an &mdash; sie sind einfach nicht aussagekräftig genug.

| KW | 8 | 9 | 10 | 11 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 8,8% | 11,4% | 11,8% | 12,4% |
| Omikron BA.2.75 | 1,2% | 0,7% | 1,1% | 0% |
| Omikron BN.1 | 3,4% | 1,9% | 0,8% | 1,8% |
| Omikron XBB.1.5 | 40,6% | 48,6% | 52% | 60,2% |
| Omikron BA.4 | 0,2% | 0% | 0% | 0% |
| Omikron BA.5 | 4,3% | 2,2% | 2% | 0,9% |
| Omikron BF.7 | 1,1% | 0,4% | 0% | 0% |
| Omikron BQ.1 | 17,8% | 13,8% | 13,5% | 8% |
| Omikron CH 1.1 | 16,1% | 11,2% | 11,8% | 12,4% |
| Rekombinante | 4,3% | 3,9% | 4,5% | 4,4% |
| Andere | 2,20% | 5,90% | 2,50% | 0% |
| Sequenzierungen | 642 | 617 | 356 | 113 |

![VoC Bayern](/images/2023/04/KW13Var.png)
