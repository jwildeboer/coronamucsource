---
layout: default
title: 2023-04-27 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-27 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der weiter sinkt und jetzt bei 0,85 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230427.png)

Das RKI meldet eine Inzidenz von 12,0 für Münchden. 20 Neue, 3 Nachmeldungen, alle für Vorgestern. Die gestern genannte Inzidenz von 12,0 erhöht sich dadurch auf 12,2. Dunkelziffer Faktor 10-17. R(t) fällt weiter auf 0,85. Keine neue Todesmeldung. 7 Tage Summe der Todesfälle 8.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 19.04. | 14,5 &rarr; 14,5 |
| 20.04. | 13,8 &rarr; 13,8 |
| 21.04. | 13,2 &rarr; 13,2 |
| 22.04. | 13,2 &rarr; 13,2 |
| 23.04. | 13,2 &rarr; 13,2 |
| 24.04. | 12,1 &rarr; 12,2 |
| 25.04. | 12,0 &rarr; 12,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 17 |
| ITS basiert für Bayern | 17 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht damit auf 8. Damit insgesamt bis jetzt 210 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 27.04. | 26.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 7,7 | 9,0 | -14,4% |
| LK Ebersberg | 9,0 | 10,4 | -13,5% |
| LK Freising | 7,2 | 7,2 | +/-0% |
| LK Fürstenfeldbruck | 11,0 | 12,4 | -11,3% |
| LK München | 9,1 | 10,0 | -9% |
| LK Starnberg | 16,1 | 16,1 | +/-0% |

Fallend. Dunkelziffer auch hier Faktor 10&ndash;17.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt mit 0,85 wieder klar unter 1.

| Datum | R(t) |
|---|---:|
| 20.04. | 1,10 |
| 21.04. | 1,03 |
| 22.04. | 0,99 |
| 23.04. | 0,95 |
| 24.04. | 0,88 |
| 25.04. | 0,87 |
| 26.04. | 0,85 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München steigt gegenüber gestern von 2 auf heute 3 (+1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 8 gleich (+/-0). Insgesamt somit noch 11 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 3 | +1 | 
| Intensiv, beatmet | 8 | +/-0 | 
| SUMME | 11 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 320 wieder niedriger als gestern (348). Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 35 auf heute 34 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 27 gleich (+/-0). Insgesamt somit 61 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 34 | -1 |
| Intensiv, beatmet | 27 | +/-0 |
| SUMME | 61 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-04-25 mit Werten bis 2023-04-18**

Die Abwasserbelastung sinkt nicht mehr. Das war zu erwarten. Neben Ferienrückkehrer gab es auch noch die Baumesse in München. Die Zahlen gehen jetzt bis 2023-04-18, laufen also um eine Woche hinterher.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230425A.png)