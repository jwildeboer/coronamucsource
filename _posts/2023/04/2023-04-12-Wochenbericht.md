---
layout: default
title: 2023-04-12 Wochenbericht KW14
category: Wochenberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110185462711835256
---

![2023-04-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche aber mit einer Lücke von 4 Tagen. Zweite zeigt den R(t) Wert, der wieder auf 0,87 sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230412.png)

In der letzten Woche gab es offiziell insgesamt nur 297 neue COVID Fälle in München. Das sind 5,7% **weniger** als in der Vorwoche (315). Im April wurden bis jetzt nich keine Todesfälle gemeldet. Die Dunkelziffer schätzen wir zwischen Faktor 8 bis 37.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 04.04. | 20,6 &rarr; 24,4 |
| 05.04. | 24,0 &rarr; 24,2 |
| 06.04. | 21,2 &rarr; 21,8 |
| 07.04. | 19,3 &rarr; 20,0 |
| 08.04. | 19,3 &rarr; 20,0 |
| 09.04. | 19,3 &rarr; 20,0 |
| 10.04. | 13,6 &rarr; 14,3 |

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 315 auf 297. Die um Nachmeldungen korrigierte Inzidenz blieb relativ stabil um 20, Aber wegen der Osterfeiertage und der Abschaffung vieler Maßnahmen sind diese Zahlen kaum realistisch.

Dunkelziffer Faktor 8 bis 36 sollte man im Kopf gleich mitrechnen. 

Getestet werden eigentlich nur noch Krankenhausneuaufnahmen. Daher ergibt sich eine scheinbare Verschiebung der Inzidenz auf ältere Bevölkerungsgruppen. Wie sich die Infektionen in der allgemeinen Bevölkerung verteilen ist schlicht nicht mehr nachvollziehbar.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 02.04. | 0 | 315 | 21,2 |
| 03.04. | 85 | 294 | 19,8 |
| 04.04. | 122 | 363 | 24,4 |
| 05.04. | 58 | 360 | 24,2 |
| 06.04. | 32 | 325 | 21,8 |
| 07.04. | 0 | 297 | 20,0 |
| 08.04. | 0 | 297 | 20,0 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| ITS basiert für München | 41 |
| ITS basiert für Bayern | 36 |

## Reproduktionswert R(t) für München

Unser R(t) stieg über Ostern wegen fehlender Zahlen auf knapp unter 1, geht aber jetzt wieder zurück.

| Datum | R(t) |
|---|---:|
| 03.04. | 0,74 |
| 04.04. | 0,78 |
| 05.04. | 0,82 |
| 06.04. | 0,88 |
| 07.04. | 0,94 |
| 08.04. | 0,95 |
| 09.04. | 0,95 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 04. April. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München jetzt eher sinkende Zahlen.

![Abwasserwerte München](/images/2023/04/20230407A.png)

## Krankenhauszahlen München

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München sank die Zahl der belegten Intensivbetten leicht und ist jetzt unter 30. 
Im Vergleich zum Vorjahr klar weniger - gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 03.04. | 11 | 20 | 31 | +/-0 | 56 |
| 04.04. | 9 | 18 | 27 | -4 | 58 |
| 05.04. | 9 | 17 | 26 | -1 | 40 |
| 06.04. | 9 | 16 | 25 | -1 | 47 |
| 07.04. | 10 | 13 | 23 | -2 | 51 |
| 08.04. | 11 | 15 | 25 | +2 | 49 |
| 09.04. | 11 | 15 | 26 | +1 | 47 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister und die 7 Tage Hospitalisierung.**

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Die (um Nachmeldungen korrigierte) 7 Tage Hospitaliserung sinkt wieder klar unter dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel), es werden aber noch einige Nachmeldungen kommen. Trotzdem &mdash; klar bessere Zahlen als im Vorjahr.

Bei den Intensivbetten sinken die Zahlen zwar auch &mdash; von 153 auf jetzt 127 &mdash; dort aber langsamer.

| Datum | Invasiv beatmet | ICU | Summe | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|
| 03.04. | 50 | 103 | 153 | 814 | 2.056 |
| 04.04. | 45 | 97 | 142 | 780 | 1.998 |
| 05.04. | 46 | 91 | 137 | 734 | 1.891 |
| 06.04. | 44 | 87 | 131 | 714 | 1.789 |
| 07.04. | 47 | 79 | 126 | 622 | 1.722 |
| 08.04. | 42 | 94 | 136 | 615 | 1.732 |
| 09.04. | 42 | 85 | 127 | 609 | 1.698 |

## Impfungen

**Seit Freitag, den 07.04.2023 ist die bisherige Impfregelung ausgelaufen. Damit werden die Impfzahlen vom RKI wohl auch nicht mehr täglich aktualisiert. Damit verharren die Impfzahlen momentan auf Stand 08.04.2023**

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Der Wegfall der Impfregelung scheint zu einem kleinen Run aif die Impfung zu führen, an den Quoten ändert das leider nichts. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 39 | 75,22% (+/-0%) |
| 2. | 79 | 75,14% (+/-0%) |
| 3. | 105 | 59,26% (+/-0%) |
| 4. | 483 | 12,02% (+0,01%) |
| 5. | 347 | 0,85% (+/-0%) |
| 6. | 125 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/04/KW13Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München bleiben auf dem Stand der vorletzten Woche, eine Aktualisierung gab es auch in dieser Woche leider noch nicht. Die Daten gehen somit nur bis zum 20. Februar.

**Stand 2023-03-20**

In München führt XBB.1.5 zwar klar mit 56,61%, so richtig dominant ist das aber auch nicht. BA.2 liegt mit 20,81% auf Platz zwei, die rekombinanten Linien mit 8,91% auf platz drei. Die neue XBB.1.6 Variante ist noch nicht separat ausgewiesen, könnte sich aber in den 6,17% "Andere" befinden.

| KW | 9 | 10 | 11 | 12 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 16,37% | 25,45% | 22,41% | 20,81% |
| Omikron BA.2.75 | 2,86% | 11,16% | 0,00% | 0,09% |
| Omikron BN.1 | 2,41% | 0,00% | 0,65% | 0,00% |
| Omikron XBB.1.5 | 39,56% | 48,53% | 56,44% | 56,61% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 19,74% | 4,12% | 5,97% | 0,57% |
| Omikron BF.7 | 0,00% | 1,60% | 0,00% | 0,00% |
| Omikron BQ.1 | 6,14% | 8,34% | 0,27% | 5,73% |
| Omikron CH.1.1 | 2,63% | 0,00% | 0,77% | 1,11% |
| Rekombinante Linien | 8,19% | 0,00% | 7,26% | 8,91% |
| Andere | 2,10% | 0,80% | 6,23% | 6,17% |

![VoC München](/images/2023/04/KW14VarMuc.png)

## Varianten Bayern

**Aufgrund der sehr geringen Zahl an Sequenzierungen (70) in der KW13 sind die Variantenzahlen ungenau**

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 61,4% nicht wirklich dominant. "Andere" stehen mit 15,7% auf Platz zwei, man darf vermuten, daß da schon viel XBB.1.6 enthalten ist. BA.2 steht mit 14,3% auf Platz drei. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt grad mal 72.

| KW | 10 | 11 | 12 | 13 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 10,5% | 12,7% | 18,7% | 14,3% |
| Omikron BA.2.75 | 0,9% | 0% | 0% | 0% |
| Omikron BN.1 | 0,9% | 1,5% | 1,5% | 0% |
| Omikron XBB.1.5 | 51,5% | 60,8% | 60,1% | 61,4% |
| Omikron BA.4 | 0% | 0% | 0% | 0% |
| Omikron BA.5 | 2,5% | 0,9% | 2,5% | 0% |
| Omikron BF.7 | 0% | 0,3% | 0,5% | 0% |
| Omikron BQ.1 | 13,6% | 9% | 6,4% | 4,3% |
| Omikron CH.1.1 | 14,3% | 9,9% | 7,9% | 2,9% |
| Rekombinante | 3,8% | 3,3% | 2% | 1,4% |
| Andere | 2,00% | 1,60% | 0,40% | 15,70% |
| Sequenzierungen | 356 | 332 | 207 | 72 |

![VoC Bayern](/images/2023/04/KW14Var.png)

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Donnerstag** in %.

| Kreis | 12.04. | 07.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 7,1 | 11,6 | -38,8% |
| LK Ebersberg | 2,8 | 19,4 | -85,6% |
| LK Freising | 7,7 | 13,8 | -44,2% |
| LK Fürstenfeldbruck | 9,2 | 20,1 | -54,2% |
| LK München | 10,6 | 19,7 | -46,2% |
| LK Starnberg | 10,2 | 19,0 | -46,3% |

Sinkende Zahlen. Dunkelziffer Faktor 8&ndash;37.