---
layout: default
title: 2023-04-17 Wochenbericht KW15
category: Wochenberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110185462711835256
---

![2023-04-17 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, weniger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der jetzt auf 0,80 steigt. Dritte zeigt Inzidenz, die zwar stabil weit unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230417.png)

In der letzten Woche gab es offiziell insgesamt nur 222 neue COVID Fälle in München. Das sind 25,2% **weniger** als in der Vorwoche (297) - aber wegen Ostern sind die Zahlen verzerrt. Die Dunkelziffer schätzen wir auf Faktor 9 bis 23. Es wurden leider aber auch 25 Todesfälle in der letzten Woche gemeldet.
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 297 auf 222. Die um Nachmeldungen korrigierte Inzidenz sank auch weiter von um 20 auf jetzt um 15, aber wegen der Osterfeiertage und der Abschaffung vieler Maßnahmen sind diese Zahlen kaum realistisch.

Dunkelziffer Faktor 9 bis 23 sollte man aktuell im Kopf gleich mitrechnen. 

Getestet werden eigentlich nur noch Krankenhausneuaufnahmen. Daher ergibt sich eine scheinbare Verschiebung der Inzidenz auf ältere Bevölkerungsgruppen. Wie sich die Infektionen in der allgemeinen Bevölkerung verteilen ist schlicht nicht mehr nachvollziehbar.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 09.04. | 0 | 297 | 20,0 |
| 10.04. | 0 | 212 | 14,3 |
| 11.04. | 106 | 196 | 13,2 |
| 12.04. | 46 | 184 | 12,4 |
| 13.04. | 40 | 192 | 12,9 |
| 14.04. | 30 | 222 | 14,9 |
| 15.04. | 0 | 222 | 14,9 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 18 |
| ITS basiert für Bayern | 23 |

## Reproduktionswert R(t) für München

Unser R(t) ist in der letzen Woche weiter gesunken, steigt aber grad ein wenig an. Erst morgen erste Signale wo die Tendenz hinzeigen könnte.

| Datum | R(t) |
|---|---:|
| 10.04. | 0,94 |
| 11.04. | 0,87 |
| 12.04. | 0,80 |
| 13.04. | 0,75 |
| 14.04. | 0,74 |
| 15.04. | 0,77 |
| 16.04. | 0,80 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 11. April. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München jetzt eher sinkende Zahlen.

![Abwasserwerte München](/images/2023/04/20230415A.png)

## Krankenhauszahlen München

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München sank die Zahl der belegten Intensivbetten leicht und ist jetzt unter 20. 
Im Vergleich zum Vorjahr klar weniger - gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 10.04. | 11 | 15 | 26 | +/-0 | 45 |
| 11.04. | 11 | 16 | 27 | +1 | 46 |
| 12.04. | 12 | 14 | 26 | -1 | 44 |
| 13.04. | 9 | 11 | 20 | -6 | 41 |
| 14.04. | 10 | 8 | 18 | -2 | 41 |
| 15.04. | 9 | 7 | 16 | -2 | 41 |
| 16.04. | 11 | 6 | 16 | +/-0 | 41 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister und die 7 Tage Hospitalisierung.**

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Die (um Nachmeldungen korrigierte) 7 Tage Hospitaliserung ist wieder klar unter dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel) und sinkt weiter. Nur noch kanpp ein Viertel im Vergleich zum Vorjahr.

Bei den Intensivbetten sinken die Zahlen zwar auch &mdash; von 127 auf jetzt 106 &mdash; dort aber langsamer.

| Datum | Invasiv beatmet | ICU | Summe | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|---:|
| 10.04. | 41 | 85 | 126 | 419 | 1.698 |
| 11.04. | 44 | 76 | 120 | 479 | 1.662 |
| 12.04. | 44 | 66 | 110 | 453 | 1.681 |
| 13.04. | 42 | 54 | 96 | 436 | 1.627 |
| 14.04. | 43 | 50 | 93 | 449 | 1.630 |
| 15.04. | 36 | 52 | 88 | 430 | 1.477 |
| 16.04. | 43 | 63 | 106 | 407 | 1.439 |

## Impfungen

**Seit Freitag, den 07.04.2023 ist die bisherige Impfregelung ausgelaufen. Damit werden die Impfzahlen vom RKI wohl auch nicht mehr täglich aktualisiert. Damit verharren die Impfzahlen momentan auf Stand 08.04.2023**

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Der Wegfall der Impfregelung scheint zu einem kleinen Run aif die Impfung zu führen, an den Quoten ändert das leider nichts. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 39 | 75,22% (+/-0%) |
| 2. | 79 | 75,14% (+/-0%) |
| 3. | 105 | 59,26% (+/-0%) |
| 4. | 483 | 12,02% (+0,01%) |
| 5. | 347 | 0,85% (+/-0%) |
| 6. | 125 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/04/KW13Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München bleiben auf dem Stand der vorletzten Woche, eine Aktualisierung gab es auch in dieser Woche leider noch nicht. Die Daten gehen somit nur bis zum 20. März.

**Stand 2023-03-20**

In München führt XBB.1.5 zwar klar mit 56,61%, so richtig dominant ist das aber auch nicht. BA.2 liegt mit 20,81% auf Platz zwei, die rekombinanten Linien mit 8,91% auf platz drei. Die neue XBB.1.6 Variante ist noch nicht separat ausgewiesen, könnte sich aber in den 6,17% "Andere" befinden.

| KW | 9 | 10 | 11 | 12 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 16,37% | 25,45% | 22,41% | 20,81% |
| Omikron BA.2.75 | 2,86% | 11,16% | 0,00% | 0,09% |
| Omikron BN.1 | 2,41% | 0,00% | 0,65% | 0,00% |
| Omikron XBB.1.5 | 39,56% | 48,53% | 56,44% | 56,61% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 19,74% | 4,12% | 5,97% | 0,57% |
| Omikron BF.7 | 0,00% | 1,60% | 0,00% | 0,00% |
| Omikron BQ.1 | 6,14% | 8,34% | 0,27% | 5,73% |
| Omikron CH.1.1 | 2,63% | 0,00% | 0,77% | 1,11% |
| Rekombinante Linien | 8,19% | 0,00% | 7,26% | 8,91% |
| Andere | 2,10% | 0,80% | 6,23% | 6,17% |

![VoC München](/images/2023/04/KW14VarMuc.png)

## Varianten Bayern

**Aufgrund der sehr geringen Zahl an Sequenzierungen (90) in der KW13 und gradm 6 Sequenzierungen in der KW14 sind die Variantenzahlen ungenau**

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 70,0% nicht wirklich dominant. "Andere" stehen mit 22,2% auf Platz zwei &mdash; man darf vermuten, daß da schon viel XBB.1.6 enthalten ist. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt grad mal 6. Daher haben wir die Zahlen für KW14 nicht in die Tabelle aufgenommen.

| KW% | 10 | 11 | 12 | 13 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,7% | 0,6% | 0,0% | 1,1% |
| Omikron BA.2.75 | 0,9% | 0,0% | 0,5% | 0,0% |
| Omikron BN.1 | 1,3% | 1,4% | 1,4% | 0,0% |
| Omikron XBB.1.5 | 51,5% | 60,8% | 58,4% | 70,0% |
| Omikron BA.4 | 0,0% | 0,0% | 0,0% | 0,0% |
| Omikron BA.5 | 2,5% | 0,9% | 2,4% | 0,0% |
| Omikron BF.7 | 0,0% | 0,3% | 0,5% | 0,0% |
| Omikron BQ.1 | 15,2% | 10,1% | 6,2% | 3,3% |
| Omikron CH 1.1 | 14,3% | 10,1% | 8,6% | 2,2% |
| Rekombinante | 13,6% | 15,3% | 22,0% | 22,2% |
| Andere | 0,0% | 0,5% | 0,0% | 1,2% |
| Sequenzierungen | 356 | 332 | 207 | 90 |

![VoC Bayern](/images/2023/04/KW15Var.png)