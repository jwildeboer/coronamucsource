---
layout: default
title: 2023-04-24 Wochenbericht KW16
category: Wochenberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110185462711835256
---

![2023-04-24 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, weniger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der jetzt auf 0,95 sinkt. Dritte zeigt Inzidenz, die zwar stabil weit unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230424.png)

In der letzten Woche gab es offiziell insgesamt nur 197 neue COVID Fälle in München. Das sind 12,8% **weniger** als in der Vorwoche (226) - aber wegen Ostern sind die Zahlen ein wenig verzerrt. Die Dunkelziffer schätzen wir auf Faktor 10 bis 18. Es wurden leider aber auch 24 Todesfälle in der letzten Woche gemeldet.
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 222 auf 197. Damit zum ersten mal seit Juli 2021 unter 200. Aber wegen der sehr hohen Dunkelziffer ist diese Zahl alles andere als realistisch. Die um Nachmeldungen korrigierte Inzidenz sank auch weiter von 15 auf jetzt um 13, aber wegen der Osterfeiertage und der Abschaffung vieler Maßnahmen sind diese Zahlen kaum realistisch.

Dunkelziffer Faktor 10 bis 18 sollte man aktuell im Kopf gleich mitrechnen. 

Getestet werden eigentlich nur noch Krankenhausneuaufnahmen. Daher ergibt sich eine scheinbare Verschiebung der Inzidenz auf ältere Bevölkerungsgruppen. Wie sich die Infektionen in der allgemeinen Bevölkerung verteilen ist schlicht nicht mehr nachvollziehbar.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 16.04. | 0 | 226 | 15,2 |
| 17.04. | 67 | 293 | 19,7 |
| 18.04. | 39 | 225 | 15,1 |
| 19.04. | 37 | 216 | 14,5 |
| 20.04. | 30 | 206 | 13,8 |
| 21.04. | 24 | 197 | 13,2 |
| 22.04. | 0 | 197 | 13,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 14 |
| ITS basiert für Bayern | 18 |

## Reproduktionswert R(t) für München

Unser R(t) ist in der letzen Woche weiter gesunken. Der Ostereffekt zeigte sich noch kurz mit einem R(t) über 1, aber jetzt geht es klar zurück.

| Datum | R(t) |
|---|---:|
| 17.04. | 0,92 |
| 18.04. | 0,98 |
| 19.04. | 1,04 |
| 20.04. | 1,10 |
| 21.04. | 1,03 |
| 22.04. | 0,99 |
| 23.04. | 0,95 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 18. April. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München jetzt ein wenig aufwärts.

![Abwasserwerte München](/images/2023/04/20230425A.png)

## Krankenhauszahlen München

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München sank die Zahl der belegten Intensivbetten weiter und ist jetzt knapp über 10. 
Im Vergleich zum Vorjahr klar weniger - gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 17.04. | 10 | 5 | 15 | -1 | 42 |
| 18.04. | 9 | 2 | 11 | -4 | 42 |
| 19.04. | 9 | 2 | 11 | +/-0 | 40 |
| 20.04. | 9 | 2 | 11 | +/-0 | 35 |
| 21.04. | 9 | 3 | 12 | +1 | 34 |
| 22.04. | 9 | 3 | 12 | +/-0 | 34 |
| 23.04. | 8 | 4 | 12 | +/-0 | 34 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister und die 7 Tage Hospitalisierung.**

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Die (um Nachmeldungen korrigierte) 7 Tage Hospitaliserung ist wieder klar unter dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel) und sinkt weiter. Nur noch kanpp ein Drittel im Vergleich zum Vorjahr.

Bei den Intensivbetten sinken die Zahlen auch &mdash; von 111 auf jetzt 84 &mdash; damit stbil unter 100 Intensivbetten belegt, das hatten wir zuletzt im August 2021!

| Datum | Invasiv beatmet | ICU | Summe | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|---:|
| 17.04. | 41 | 70 | 111 | 677 | 1.418 |
| 18.04. | 39 | 57 | 96 | 506 | 1.270 |
| 19.04. | 42 | 56 | 98 | 495 | 1.248 |
| 20.04. | 39 | 54 | 93 | 464 | 1.289 |
| 21.04. | 38 | 47 | 85 | 422 | 1.276 |
| 22.04. | 35 | 50 | 85 | 420 | 1.350 |
| 23.04. | 34 | 50 | 84 | 414 | 1.368 |

## Impfungen

**Seit Freitag, den 07.04.2023 ist die bisherige Impfregelung ausgelaufen. Damit werden die Impfzahlen vom RKI wohl auch nicht mehr täglich aktualisiert. Damit verharren die Impfzahlen momentan auf Stand 08.04.2023**

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Der Wegfall der Impfregelung scheint zu einem kleinen Run aif die Impfung zu führen, an den Quoten ändert das leider nichts. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 39 | 75,22% (+/-0%) |
| 2. | 79 | 75,14% (+/-0%) |
| 3. | 105 | 59,26% (+/-0%) |
| 4. | 483 | 12,02% (+0,01%) |
| 5. | 347 | 0,85% (+/-0%) |
| 6. | 125 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/04/KW13Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München bleiben auf dem Stand der vorvorletzten Woche, eine Aktualisierung gab es auch in dieser Woche leider noch nicht. Die Daten gehen somit nur bis zum 20. März.

**Stand 2023-03-20**

In München führt XBB.1.5 zwar klar mit 56,61%, so richtig dominant ist das aber auch nicht. BA.2 liegt mit 20,81% auf Platz zwei, die rekombinanten Linien mit 8,91% auf platz drei. Die neue XBB.1.6 Variante ist noch nicht separat ausgewiesen, könnte sich aber in den 6,17% "Andere" befinden.

| KW | 9 | 10 | 11 | 12 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 16,37% | 25,45% | 22,41% | 20,81% |
| Omikron BA.2.75 | 2,86% | 11,16% | 0,00% | 0,09% |
| Omikron BN.1 | 2,41% | 0,00% | 0,65% | 0,00% |
| Omikron XBB.1.5 | 39,56% | 48,53% | 56,44% | 56,61% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 19,74% | 4,12% | 5,97% | 0,57% |
| Omikron BF.7 | 0,00% | 1,60% | 0,00% | 0,00% |
| Omikron BQ.1 | 6,14% | 8,34% | 0,27% | 5,73% |
| Omikron CH.1.1 | 2,63% | 0,00% | 0,77% | 1,11% |
| Rekombinante Linien | 8,19% | 0,00% | 7,26% | 8,91% |
| Andere | 2,10% | 0,80% | 6,23% | 6,17% |

![VoC München](/images/2023/04/KW14VarMuc.png)

## Varianten Bayern

**Aufgrund der sehr geringen Zahl an Sequenzierungen (von 600/Woche jetzt weniger als 20) können wir diesen Teil des Wochenberichts nicht mehr fortführen.**

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 70,0% nicht wirklich dominant. "Andere" stehen mit 22,2% auf Platz zwei &mdash; man darf vermuten, daß da schon viel XBB.1.6 enthalten ist. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt grad mal 6. Daher haben wir die Zahlen für KW14 nicht in die Tabelle aufgenommen.

| KW% | 10 | 11 | 12 | 13 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,7% | 0,6% | 0,0% | 1,1% |
| Omikron BA.2.75 | 0,9% | 0,0% | 0,5% | 0,0% |
| Omikron BN.1 | 1,3% | 1,4% | 1,4% | 0,0% |
| Omikron XBB.1.5 | 51,5% | 60,8% | 58,4% | 70,0% |
| Omikron BA.4 | 0,0% | 0,0% | 0,0% | 0,0% |
| Omikron BA.5 | 2,5% | 0,9% | 2,4% | 0,0% |
| Omikron BF.7 | 0,0% | 0,3% | 0,5% | 0,0% |
| Omikron BQ.1 | 15,2% | 10,1% | 6,2% | 3,3% |
| Omikron CH 1.1 | 14,3% | 10,1% | 8,6% | 2,2% |
| Rekombinante | 13,6% | 15,3% | 22,0% | 22,2% |
| Andere | 0,0% | 0,5% | 0,0% | 1,2% |
| Sequenzierungen | 356 | 332 | 207 | 90 |

![VoC Bayern](/images/2023/04/KW15Var.png)