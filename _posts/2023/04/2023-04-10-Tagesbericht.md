---
layout: default
title: 2023-04-10 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110174039092338018
---

![2023-04-10 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, insgesamt höher als in der Vorwoche, aber keine neuen Meldungen wegen Feiertag. Zweite zeigt R(t) Wert für München, der jetzt auf 0,94 steht. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230410.png)

Das RKI meldet, wie vorhergesagt, eine Inzidenz von 19,3. Wegen Osterfeiertage bis Mittwoch keine neuen Zahlen vom RKI, da bis Dienstag nicht gemeldet wird.  R(t) steht bei 0,94. Dunkelziffer Faktor 8-27.
<!--more-->

## Inzidenzentwicklung

**Wegen Osterfeiertage bis Mittwoch keine Aktualisierung möglich**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 30.03. | 24,9 &rarr; 25,4 |
| 31.03. | 21,0 &rarr; 21,2 |
| 01.04. | 21,0 &rarr; 21,2 |
| 02.04. | 21,0 &rarr; 21,2 |
| 03.04. | 19,4 &rarr; 19,8 |
| 04.04. | 20,6 &rarr; 24,4 |
| 05.04. | 24,0 &rarr; 24,2 |

Wegen der Osterfeiertage gehen bis Dienstag keine neue Meldungen an das LGL und RKI. Daher können wir die Inzidenzen bis Dienstag schon jetzt berechnen:

| Samstag | 19,3 |
| Sonntag | 19,3 |
| Montag | 19,3 |
| Dienstag | 13,6 |

Die 13,6 für Dienstag wird eine 3 Tage Inzidenz, weil in den 4 Tagen vorher schlicht keine Zahlen gemeldet werden. Somit erst am Mittwoch wieder eine "echte" Inzidenz.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| ITS basiert für München | 27 |
| ITS basiert für Bayern | 27 |

## COVID-positive Todesfälle

**Wegen Osterfeiertage bis Mittwoch keine Aktualisierung möglich**

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt damit auf 7. Damit insgesamt bis jetzt 159 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Wegen Osterfeiertage bis Mittwoch keine Aktualisierung möglich**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 07.04. | 06.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 11,6 | 12,2 | -4,9% |
| LK Ebersberg | 19,4 | 20,1 | -3,5% |
| LK Freising | 13,8 | 14,4 | -4,2% |
| LK Fürstenfeldbruck | 20,1 | 22,9 | -12,2% |
| LK München | 19,7 | 22,9 | -14,0% |
| LK Starnberg | 19,0 | 22,7 | -16,3% |

Sinkende Zahlen. Dunkelziffer Faktor 8&ndash;26.

## Reproduktionswert R(t) für München

Unser R(t) bleibt weiter auf 0,93.

| Datum | R(t) |
|---|---:|
| 03.04. | 0,74 |
| 04.04. | 0,78 |
| 05.04. | 0,83 |
| 06.04. | 0,87 |
| 07.04. | 0,93 |
| 08.04. | 0,93 |
| 09.04. | 0,94 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt gegenüber gestern mit 15  gleich (+/-0). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern mit 10 auf heute 11 (+1). Insgesamt somit 26 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 15 | +/-0 | 
| Intensiv, beatmet | 11 | +1 | 
| SUMME | 26 | +1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung sinkt heute auf 508, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt gegenüber gestern von 94 auf heute 85 (-9). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 42 gleich (+/-0). Insgesamt somit 127 Intensivbetten belegt (-9).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 85 | -9 |
| Intensiv, beatmet | 42 | +/-0 |
| SUMME | 127 | -9 |

## Abwasserbelastung München

Eher stabile Tendenz, Daten gehen bis 2023-04-04

![Abwasserwerte München](/images/2023/04/20230407A.png)