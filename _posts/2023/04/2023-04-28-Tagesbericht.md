---
layout: default
title: 2023-04-28 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-04-28 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell weniger. Zweite zeigt R(t) Wert für München, der weiter sinkt und jetzt bei 0,84 steht. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230428.png)

Das RKI meldet eine Inzidenz von 11,2 für Münchden. 31 Neue, 1 Nachmeldung für Vorgestern. Die gestern genannte Inzidenz von 11,0 erhöht sich dadurch auf 11,1. Dunkelziffer Faktor 10-18. R(t) fällt weiter auf 0,84. Keine neue Todesmeldung. 7 Tage Summe der Todesfälle 4.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 20.04. | 13,8 &rarr; 13,8 |
| 21.04. | 13,2 &rarr; 13,2 |
| 22.04. | 13,2 &rarr; 13,2 |
| 23.04. | 13,2 &rarr; 13,2 |
| 24.04. | 12,1 &rarr; 12,2 |
| 25.04. | 12,0 &rarr; 12,2 |
| 26.04. | 11,0 &rarr; 11,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 14 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht auf 4. Damit insgesamt bis jetzt 210 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 28.04. | 27.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 7,1 | 7,7 | -7,8% |
| LK Ebersberg* | 5,5 | 9,0 | -38,9% |
| LK Freising | 6,6 | 7,2 | -8,3% |
| LK Fürstenfeldbruck | 8,7 | 11,0 | -20,9% |
| LK München | 9,4 | 9,1 | +3,3% |
| LK Starnberg | 15,4 | 16,1 | -4,3% |

Fallend. Dunkelziffer auch hier Faktor 10&ndash;18. * Der LK Ebersberg hat seit 2 Tagen keine Zahlen gemeldet.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt mit 0,85 wieder klar unter 1.

| Datum | R(t) |
|---|---:|
| 21.04. | 1,03 |
| 22.04. | 0,99 |
| 23.04. | 0,95 |
| 24.04. | 0,88 |
| 25.04. | 0,87 |
| 26.04. | 0,85 |
| 27.04. | 0,84 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern von 3 auf heute 2 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 8 auf heute 6 (-2). Insgesamt somit nur noch 8 Intensivbetten belegt (-3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | -1 | 
| Intensiv, beatmet | 6 | -2 | 
| SUMME | 8 | -3 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung ist heute mit 302 wieder niedriger als gestern (320). Aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern von 34 auf heute 35 (+1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 27 auf heute 22 (-5). Insgesamt somit 57 Intensivbetten belegt (-4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 35 | +1 |
| Intensiv, beatmet | 22 | -5 |
| SUMME | 57 | -4 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-04-25 mit Werten bis 2023-04-18**

Die Abwasserbelastung sinkt nicht mehr. Das war zu erwarten. Neben Ferienrückkehrer gab es auch noch die Baumesse in München. Die Zahlen gehen jetzt bis 2023-04-18, laufen also um eine Woche hinterher.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/04/20230425A.png)