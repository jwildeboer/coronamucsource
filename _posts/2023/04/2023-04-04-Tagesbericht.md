---
layout: default
title: 2023-04-04 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110140148953988829 
---

![2023-04-04 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der weiter sinkt und jetzt auf 0,74 steht. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230404.png)

Das RKI meldet eine Inzidenz von 19,4. 79 Neue, 3 Nachmeldungen, davon 1 für letzten Freitag. Die Inzidenz von Freitag bis Sonntag erhöht sich durch Nachmeldungen von 21,0 auf 21,2. Keine neuen Todesfälle, R(t) sinkt auf 0,74.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 27.03. | 31,7 &rarr; 32,3 |
| 28.03. | 27,2 &rarr; 27,3 |
| 29.03. | 25,9 &rarr; 26,7 |
| 30.03. | 24,9 &rarr; 25,3 |
| 31.03. | 21,0 &rarr; 21,2 |
| 01.04. | 21,0 &rarr; 21,2 |
| 02.04. | 21,0 &rarr; 21,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 26 |

## COVID-positive Todesfälle

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt damit auf 7. Damit insgesamt bis jetzt 159 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 04.04. | 01.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 16,7 | 19,9 | -16,1% |
| LK Ebersberg | 14,5 | 20,1 | -27,9% |
| LK Freising | 14,9 | 17,1 | -12,9% |
| LK Fürstenfeldbruck | 18,3 | 22,0 | -16,8% |
| LK München | 15,4 | 18,6 | -17,2% |
| LK Starnberg | 16,1 | 20,5 | -21,5% |

Klar fallende Zahlen. Dunkelziffer Faktor 8&ndash;26.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,74. Sinkt also weiter.

| Datum | R(t) |
|---|---:|
| 28.03. | 0,89 |
| 29.03. | 0,87 |
| 30.03. | 0,84 |
| 31.03. | 0,81 |
| 01.04. | 0,79 |
| 02.04. | 0,76 |
| 03.04. | 0,74 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten in München bleibt gegenüber gestern gleich. Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern auch gleich. Insgesamt somit die gleiche Summe wie gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 20 | +/-0 | 
| Intensiv, beatmet | 11 | +/-0 | 
| SUMME | 31 | +/-0 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung sinkt heute auf 604, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 26. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten in Bayern steigt gegenüber gestern um 4. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 4 zurück. Insgesamt somit die gleiche Summe wie gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 103 | +4 |
| Intensiv, beatmet | 50 | -4 |
| SUMME | 153 | +/-0 |