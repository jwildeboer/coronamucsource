---
layout: default
title: 2023-04-13 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110191126338362939
---

![2023-04-13 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, weniger als in der Vorwoche. Zweite zeigt R(t) Wert für München, der jetzt auf 0,80 sinkt. Dritte zeigt Inzidenz, die zwar stabil weit unter 50 liegt aber wegen Ostern und mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230413.png)

Das RKI meldet eine Inzidenz von 12,0. 40 Neue, 0 Nachmeldungen. Aber 2 Korrekturen. Die gestern genannte Inzidenz von 13,1 sinkt dadurch auf 13,0. R(t) fällt weiter auf 0,80. Dunkelziffer Faktor 8–36. 8 Tote, 7 Tage Summe der Toten somit auch 8.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 05.04. | 24,0 &rarr; 24,2 |
| 06.04. | 21,2 &rarr; 21,8 |
| 07.04. | 19,3 &rarr; 20,0 |
| 08.04. | 19,3 &rarr; 20,0 |
| 09.04. | 19,3 &rarr; 20,0 |
| 10.04. | 13,6 &rarr; 14,3 |
| 11.04. | 13,1 &rarr; 13,0 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| ITS basiert für München | 43 |
| ITS basiert für Bayern | 36 |

## COVID-positive Todesfälle

Es wurden 8 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist damit auch 8. Damit insgesamt bis jetzt 167 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 13.04. | 12.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 6,4 | 7,1 | -9,9% |
| LK Ebersberg | 5,5 | 2,8 | +96,4% |
| LK Freising | 7,7 | 7,7 | +/-0% |
| LK Fürstenfeldbruck | 7,8 | 9,2 | -15,2% |
| LK München | 9,4 | 10,6 | -11,3% |
| LK Starnberg | 9,5 | 10,2 | -6,9% |

Kaum noch realistische Zahlen. Dunkelziffer Faktor 8&ndash;37.

## Reproduktionswert R(t) für München

Unser R(t) fällt weiter auf 0,80.

| Datum | R(t) |
|---|---:|
| 06.04. | 0,88 |
| 07.04. | 0,94 |
| 08.04. | 0,95 |
| 09.04. | 0,95 |
| 10.04. | 0,94 |
| 11.04. | 0,87 |
| 12.04. | 0,80 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern mit 16 auf jetzt 14 (-2). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern mit 11 auf jetzt 12 (+1). Insgesamt somit 26 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 14 | -2 | 
| Intensiv, beatmet | 12 | +1 | 
| SUMME | 26 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung liegt heute bei 370, etwas höher als gestern (353), aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 27. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 76 auf heute 66 (-10). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 44 gleich  (+/-0). Insgesamt somit 110 Intensivbetten belegt (-10).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 66 | -10 |
| Intensiv, beatmet | 44 | +/-0 |
| SUMME | 110 | -10 |

## Abwasserbelastung München

Eher stabile Tendenz, Daten gehen bis 2023-04-04. Auch heute keine Aktualisierung, obwohl man werktägliche Updates anstrebt. Schade.

![Abwasserwerte München](/images/2023/04/20230407A.png)