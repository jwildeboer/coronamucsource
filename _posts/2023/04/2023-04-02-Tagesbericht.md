---
layout: default
title: 2023-04-02 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110129861592666211 
---

![2023-04-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert sinkt wieder und steht jetzt auf 0,79. Neue Zahlen vom RKI aber erst am Dienstag. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl weiter klar sinkt, aber bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/04/20230402.png)

Das RKI meldet, wie vorhergesagt, eine Inzidenz von 21,0. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. Keine IVENA-Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

**Keine Meldungen/Auswertungen am Wochenende, neue Zahlen erst am Dienstag**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 24.03. | 35,6 &rarr; 36,2 |
| 25.03. | 35,6 &rarr; 36,2 |
| 26.03. | 35,6 &rarr; 36,2 |
| 27.03. | 31,7 &rarr; 32,1 |
| 28.03. | 27,2 &rarr; 27,2 |
| 29.03. | 25,9 &rarr; 26,6 |
| 30.03. | 24,9 &rarr; 25,3 |

Bis Montag gehen keine Meldungen vom LGL Bayern an das RKI, daher gibt es erst am Dienstag eine neue Inzidenz. Die Inzidenz für Montag können wir daher schon heute berechnen:

| Montag | 21,0 |

## Dunkelziffer

**Keine Meldungen/Auswertungen am Wochenende, neue Zahlen erst am Dienstag**

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 23 |

## COVID-positive Todesfälle

**Keine Meldungen/Auswertungen am Wochenende, neue Zahlen erst am Dienstag**

Es wurden 7 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle steigt damit auf 7. Damit insgesamt bis jetzt 159 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Keine Meldungen/Auswertungen am Wochenende, neue Zahlen erst am Dienstag**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 01.04. | 31.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 19,9 | 19,9 | +/-0% |
| LK Ebersberg | 20,1 | 18,7 | +7,5% |
| LK Freising | 17,1 | 19,9 | -14,1% |
| LK Fürstenfeldbruck | 22,0 | 23,3 | -5,6% |
| LK München | 18,6 | 19,4 | -4,1% |
| LK Starnberg | 20,5 | 23,4 | -12,4% |

Eher fallende Zahlen. Dunkelziffer Faktor 8&ndash;23.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,84. Sinkt also weiter.

| Datum | R(t) |
|---|---:|
| 26.03. | 0,93 |
| 27.03. | 0,93 |
| 28.03. | 0,89 |
| 29.03. | 0,87 |
| 30.03. | 0,84 |
| 31.03. | 0,81 |
| 01.04. | 0,79 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Keine Änderung gegenüber gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 18 | -1 | 
| Intensiv, beatmet | 11 | +/-0 | 
| SUMME | 29 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung sinkt heute auf 757, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Eventuell kommen wir nächste Woche aber mit den um Nachmeldungen korrigierten Zahlen wieder darunter.

**Stand 2023-03-31 - neue Zahlen erst wieder am Montag**

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute deutlich nach unten. Im Vergleich zu gestern 84 Betten weniger auf Normalstation und IMC, 2 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 6 zurück. Insgesamt somit 86 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.583 | -84 |
| Intensiv | 154 | -2 |
| (davon invasiv beatmet) | 57 | -6 |
| SUMME | 1.737 | -86 |