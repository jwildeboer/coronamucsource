---
layout: default
title: 2023-04-05 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110146108992189512 
---

![2023-04-05 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, tendenziell heute etwas höher als in der Vorwoche. Zweite zeigt R(t) Wert für München, der wieder etwas steigt und jetzt auf 0,76 steht. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist und heute auch wieder ansteigt.](/images/2023/04/20230405.png)

Das RKI meldet eine Inzidenz von 20,6. 66 Neue, 12 Nachmeldungen, davon 6 für vorgestern. Die gestern genannte Inzidenz von 19,4 erhöht sich durch Nachmeldungen auf 19,8. Keine neuen Todesfälle, R(t) steigt wieder auf 0,76. Tag 1 mit Inzidienz über 20.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 28.03. | 27,2 &rarr; 27,3 |
| 29.03. | 25,9 &rarr; 26,7 |
| 30.03. | 24,9 &rarr; 25,3 |
| 31.03. | 21,0 &rarr; 21,2 |
| 01.04. | 21,0 &rarr; 21,2 |
| 02.04. | 21,0 &rarr; 21,2 |
| 03.04. | 19,4 &rarr; 19,8 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte fürr München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| ITS basiert für München | 32 |
| ITS basiert für Bayern | 28 |

## COVID-positive Todesfälle

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt damit auf 7. Damit insgesamt bis jetzt 159 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 05.04. | 04.04. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 12,9 | 16,7 | -22,8% |
| LK Ebersberg | 21,4 | 14,5 | +47,6% |
| LK Freising | 16,6 | 14,9 | +11,4% |
| LK Fürstenfeldbruck | 22,4 | 18,3 | +22,4% |
| LK München | 22,6 | 15,4 | +46,8% |
| LK Starnberg | 22,7 | 16,1 | +41,0% |

Sprung nach oben. Dunkelziffer Faktor 8&ndash;28.

## Reproduktionswert R(t) für München

Unser R(t) steigt wieder auf 0,76.

| Datum | R(t) |
|---|---:|
| 29.03. | 0,87 |
| 30.03. | 0,84 |
| 31.03. | 0,81 |
| 01.04. | 0,79 |
| 02.04. | 0,76 |
| 03.04. | 0,74 |
| 04.04. | 0,76 |

## Krankenhausbelegung München

**Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt gegenüber gestern um 2. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern auch um 2 zurück. Insgesamt somit 4 Intensivbetten weniger belegt im Vergleich zu gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 18 | -2 | 
| Intensiv, beatmet | 9 | -2 | 
| SUMME | 27 | -4 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steigt heute auf 639, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar lagen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Seit dem 26. März liegen wir (endlich) wieder drunter.

**Zum 1. April wurde die Arbeit der landesweiten Koordinierungsstelle Krankenhausbelegung eingestellt. Die IVENA Zahl wird damit leider nicht mehr veröffentlicht. Uns bleiben somit nur die Zahlen vom Intensivregister.**

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt gegenüber gestern um 6. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern wieder um 5 zurück. Insgesamt somit 11 Intensivbetten weniger belegt im Vergleich zu gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 97 | -6 |
| Intensiv, beatmet | 45 | -5 |
| SUMME | 142 | -11 |

## Abwasserbelastung München

Weiter fallende Tendenz, Daten gehen bis 2023-03-28

![Abwasserwerte München](/images/2023/04/20230405A.png)