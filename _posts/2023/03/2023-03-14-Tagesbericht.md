---
layout: default
title: 2023-03-14 Tagesbericht
category: Tagesberichte
---

![2023-03-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 17 Tagen wieder unter 1, sinkt aber nicht mehr. Zweite zeigt Krankenhausbetten in Bayern, die wieder etwas steigen. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230314.png)

Das RKI meldet eine Inzidenz von 43,6. 160 Neue, 27 Nachmeldungen, davon 17 für letzten Freitag. Die Inzidenzen für Freitag bis Sonntag erhöhen sich dadurch  von 46,3 auf 47,7. Der R(t) Wert sinkt nicht mehr.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 06.03. | 73,7 &rarr; 74,7 |
| 07.03. | 61,8 &rarr; 62,9 |
| 08.03. | 49,3 &rarr; 51,4 |
| 09.03. | 47,1 &rarr; 47,5 |
| 10.03. | 46,3 &rarr; 47,7 |
| 11.03. | 46,3 &rarr; 47,7 |
| 12.03. | 46,3 &rarr; 47,7 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 19 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt bis jetzt 133 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 14.03. | 11.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 40,5 | 32,2 | +25,8% |
| LK Ebersberg | 24,2 | 31,1 | -22,2% |
| LK Freising | 37,5 | 37,5 | +/-0% |
| LK Fürstenfeldbruck | 49,4 | 49,0 | +0,8% |
| LK München | 32,9 | 30,3 | +8,6% |
| LK Starnberg | 37,3 | 36,6 | +1,9% |

Leicht steigende Tendenz. Dunkelziffer Faktor 8–19.

## Reproduktionswert R(t) für München

Unser R(t) steht auf 0,74 und sinkt nicht weiter. Könnte Kipppunkt werden.

| Datum | R(t) |
|---|---:|
| 07.03. | 0,82 |
| 08.03. | 0,80 |
| 09.03. | 0,78 |
| 10.03. | 0,75 |
| 11.03. | 0,75 |
| 12.03. | 0,75 |
| 13.03. | 0,74 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht höher.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 22 | +2 | 
| Intensiv, beatmet | 7 | +/-0 | 
| SUMME | 29 | +2 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.252, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sind die Zahlen seit letzem Wochenende wieder etwas gesunken. Bei den belegten Intensivbetten zeichnet sich aber eher eine Tendenz zu mehr belegten Betten ab. Im Vergleich zu gestern 138  Betten weniger auf Normalstation und IMC, aber auch 10 Betten mehr auf Intensiv. 5 Patienten weniger, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.448 | -138 |
| Intensiv | 211 | +10 |
| (davon invasiv beatmet) | 49 | -5 |
| SUMME | 2.659 | -128 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.