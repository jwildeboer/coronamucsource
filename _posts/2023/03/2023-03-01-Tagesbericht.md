---
layout: default
title: 2023-03-01 Tagesbericht
category: Tagesberichte
---

![2023-03-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche. Der R(t) Wert ist seit 8 Tagen wieder knapp unter 1. Zweite zeigt Krankenhausbetten, die weiter gesunken sind. Dritte zeigt Inzidenz, die nach 18 Tagen über 100 heute zum zweiten mal wieder drunter liegt.](/images/2023/03/20230301.png)

Das RKI meldet eine Inzidenz von 95,1. 263 Neue, 25 Nachmeldungen, davon 19 für vorgestern. Die gestern genannte Inzidenz von 91,8 erhöht sich durch Nachmeldungen auf 93,4. Tag 2 unter Inzidenz 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 21.02. | 88,7 &rarr; 102,8 |
| 22.02. | 105,5 &rarr; 108,4 |
| 23.02. | 106,9 &rarr; 107,5 |
| 24.02. | 107,3 &rarr; 108,4 |
| 25.02. | 107,3 &rarr; 108,4 |
| 26.02. | 107,3 &rarr; 108,4 |
| 27.02. | 91,8 &rarr; 93,4 |

## COVID-positive Todesfälle

Es wurden 8 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 23. Damit insgesamt bis jetzt 117 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

Im Feburar 2022 hatten wir 113 Tote zu beklagen, im Februar 2023 "nur" 53. 

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 8 |
| Krankenhaus/Inzidenz | 8 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten Vortag in %.

| Kreis | 01.03 | 28.02. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 95,2 | 97,1 | -2,0% |
| LK Ebersberg | 68,5 | 65,0 | +5,4% |
| LK Freising | 98,8 | 93,3 | +5,9% |
| LK Fürstenfeldbruck | 118,5 | 100,7 | +17,7% |
| LK München | 83,5 | 81,2 | +2,8% |
| LK Starnberg | 73,9 | 64,4 | +14,8% |

Rückschlageffekt wegen Faschingsdienstag in der letzten Woche.

## Reproduktionswert R(t) für München

Unser R(t) steht auf 0,97. Nach 25 Tagen über 1 jetzt also wieder seit 8 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 22.02. | 0,97 |
| 23.02. | 0,96 |
| 24.02. | 0,97 |
| 25.02. | 0,98 |
| 26.02. | 0,99 |
| 27.02. | 0,97 |
| 27.02. | 0,97 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 21 von 351 auf 330. IMC bleibt unverändert auf 7.Intensiv sinkt um 3 von gestern 27 auf heute 24. Damit insgesamt 361 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 330 | -21 | 
| IMC | 7 | +/-0 | 
| Intensiv | 24 | -3 |
| SUMME | 361 | -24 |

Damit sind 39 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.