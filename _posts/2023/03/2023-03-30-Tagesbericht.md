---
layout: default
title: 2023-03-30 Tagesbericht
category: Tagesberichte
---

![2023-03-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert sinkt wieder und steht jetzt auf 0,87. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl in der letzten Woche gesunken ist und auch heute wieder klar sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230330.png)

Das RKI meldet eine Inzidenz von 25,9. 49 Neue, 2 Nachmeldungen, beide für vorgestern. Die gestern genannte Inzidenz von 27,2 erhöht sich durch Nachmeldungen auf 27,3. Tag 8 ohne Todesmeldung. R(t) sinkt weiter auf 0,87.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 22.03. | 35,8 &rarr; 36,5 |
| 23.03. | 34,5 &rarr; 35,2 |
| 24.03. | 35,6 &rarr; 36,2 |
| 25.03. | 35,6 &rarr; 36,2 |
| 26.03. | 35,6 &rarr; 36,2 |
| 27.03. | 31,7 &rarr; 32,2 |
| 28.03. | 27,2 &rarr; 27,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 24 |

## COVID-positive Todesfälle

Seit 8 Tagen wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle sinkt somit auf 0 &mdash; das hatten wir zuletzt am 2. Oktober 2022. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 30.03. | 29.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 23,8 | 24,4 | -2,5% |
| LK Ebersberg | 23,5 | 32,5 | -27,7% |
| LK Freising | 18,8 | 19,3 | -2,6% |
| LK Fürstenfeldbruck | 24,7 | 27,0 | -8,5% |
| LK München | 20,3 | 21,7 | -6,5% |
| LK Starnberg | 21,9 | 25,6 | -14,5% |

Fallende Zahlen. Dunkelziffer Faktor 8&ndash;24.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,89. Sinkt also ein wenig.

| Datum | R(t) |
|---|---:|
| 23.03. | 0,92 |
| 24.03. | 0,92 |
| 25.03. | 0,92 |
| 26.03. | 0,93 |
| 27.03. | 0,93 |
| 28.03. | 0,89 |
| 29.03. | 0,87 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern um 1 gestiegen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 20 | +2 | 
| Intensiv, beatmet | 11 | -1 | 
| SUMME | 31 | +1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steigt heute auf 861, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Eventuell kommen wir diese Woche aber mit den um Nachmeldungen korrigierten Zahlen wieder darunter.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute deutlich nach unten. Im Vergleich zu gestern 95 Betten weniger auf Normalstation und IMC, 9 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 3 zurück. Insgesamt somit 104 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.667 | -95 |
| Intensiv | 156 | -9 |
| (davon invasiv beatmet) | 63 | -3 |
| SUMME | 1.823 | -104 |