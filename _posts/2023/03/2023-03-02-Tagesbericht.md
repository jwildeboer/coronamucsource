---
layout: default
title: 2023-03-02 Tagesbericht
category: Tagesberichte
---

![2023-03-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche. Der R(t) Wert ist seit 9 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten, die weiter gesunken sind. Dritte zeigt Inzidenz, die nach 18 Tagen über 100 heute zum dritten mal wieder drunter liegt.](/images/2023/03/20230302.png)

Das RKI meldet eine Inzidenz von 86,9. 233 Neue, 45 Nachmeldungen, davon 37 für vorgestern. Die gestern genannte Inzidenz von 95,1 erhöht sich durch Nachmeldungen auf 97,9. Tag 3 unter Inzidenz 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 22.02. | 105,5 &rarr; 108,4 |
| 23.02. | 106,9 &rarr; 107,5 |
| 24.02. | 107,3 &rarr; 108,4 |
| 25.02. | 107,3 &rarr; 108,4 |
| 26.02. | 107,3 &rarr; 108,4 |
| 27.02. | 91,8 &rarr; 93,4 |
| 28.02. | 95,1 &rarr; 97,9 |

## COVID-positive Todesfälle

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 23. Damit insgesamt bis jetzt 117 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 7 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten Vortag in %.

| Kreis | 02.03 | 01.03 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 90,7 | 95,2 | -4,7% |
| LK Ebersberg | 71,2 | 68,5 | +3,9% |
| LK Freising | 98,3 | 98,8 | -0,5% |
| LK Fürstenfeldbruck | 96,5 | 118,5 | -18,6% |
| LK München | 84,9 | 83,5 | +1,7% |
| LK Starnberg | 70,9 | 73,9 | -4,1% |

Rückschlageffekt wegen Faschingsdienstag in der letzten Woche.

## Reproduktionswert R(t) für München

Unser R(t) steht auf 0,94. Nach 25 Tagen über 1 jetzt also wieder seit 9 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 23.02. | 0,96 |
| 24.02. | 0,97 |
| 25.02. | 0,98 |
| 26.02. | 0,99 |
| 27.02. | 0,98 |
| 28.02. | 0,97 |
| 01.03. | 0,94 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 26 von 330 auf 304. IMC steigt um 5 von gestern 7 auf heute 12. Intensiv sinkt um 4 von gestern 24 auf heute 20. Damit insgesamt 336 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 304 | -26 | 
| IMC | 12 | +5 | 
| Intensiv | 20 | -4 |
| SUMME | 336 | -25 |

Damit sind 58 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 9.379 Tests/Tag mit einer Positivrate von 20,99%. Gegenüber den Zahlen von letzter Woche (10.064 Tests/Tag mit 20,14% Positivrate) also wieder weniger Tests bei leicht steigender Positivrate.

9.379 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom möglichen Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 9.379 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 20,99%.](/images/2023/03/20230302Test.png)

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.