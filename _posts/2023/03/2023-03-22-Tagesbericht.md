---
layout: default
title: 2023-03-22 Tagesbericht
category: Tagesberichte
---

![2023-03-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ähnlich wie in der Vorwoche. Der R(t) Wert steigt wieder. Zweite zeigt Krankenhausbetten in Bayern, deren Summe heute deutlich sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230322.png)

Das RKI meldet eine Inzidenz von 38,6. 107 Neue, 19 Nachmeldungen, davon 18 für vorgestern. Die gestern genannte Inzidenz von 38,8 erhöht sich durch Nachmeldungen auf 40,0. R(t) steigt auf 0,92. Dunkelziffer Faktor 8–20.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 14.03. | 43,4 &rarr; 44,1 |
| 15.03. | 43,0 &rarr; 43,7 |
| 16.03. | 42,5 &rarr; 43,2 |
| 17.03. | 39,3 &rarr; 40,3 |
| 18.03. | 39,3 &rarr; 40,3 |
| 19.03. | 39,3 &rarr; 40,3 |
| 20.03. | 38,8 &rarr; 40,0 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 20 |

## COVID-positive Todesfälle

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 11. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 22.03. | 21.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 18,7 | 25,7 | -27,2% |
| LK Ebersberg | 25,6 | 29,1 | -12,0% |
| LK Freising | 21,0 | 23,2 | -9,5% |
| LK Fürstenfeldbruck | 30,2 | 27,0 | +11,9% |
| LK München | 32,0 | 32,6 | -1,8% |
| LK Starnberg | 27,1 | 29,3 | -7,5% |

Sinkende Tendenz. Dunkelziffer Faktor 8–20.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,89. Könnte Kipppunkt sein.

| Datum | R(t) |
|---|---:|
| 15.03. | 0,79 |
| 16.03. | 0,84 |
| 17.03. | 0,89 |
| 18.03. | 0,92 |
| 19.03. | 0,92 |
| 20.03. | 0,92 |
| 20.03. | 0,92 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht gesunken.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 17 | -5 | 
| Intensiv, beatmet | 11 | +3 | 
| SUMME | 28 | -2 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.058, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute ungewohnst deutlich nach unten. Im Vergleich zu gestern 136 Betten weniger auf Normalstation und IMC, 13 Betten weniger auf Intensiv. Auch die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 4 zurück. Insgesamt somit 149 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.159 | -136 |
| Intensiv | 182 | -13 |
| (davon invasiv beatmet) | 55 | -4 |
| SUMME | 2.341 | -149 |