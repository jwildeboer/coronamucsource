---
layout: default
title: 2023-03-10 Tagesbericht
category: Tagesberichte
---

![2023-03-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 16 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten in Bayern, die etwas gesunken sind. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230310.png)

Das RKI meldet eine Inzidenz von 47,1. 108 Neue, 28 Nachmeldungen, davon 26 für vorgestern. Die gestern genannte Inzidenz von 49,3 erhöht sich durch Nachmeldungen auf 51,1. Dunkelziffer (geschätzt) Faktor 8-15.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 02.03. | 84,3 &rarr; 85,4 |
| 03.03. | 77,2 &rarr; 78,0 |
| 04.03. | 77,2 &rarr; 78,0 |
| 05.03. | 77,2 &rarr; 78,0 |
| 06.03. | 73,7 &rarr; 74,7 |
| 07.03. | 61,8 &rarr; 62,7 |
| 08.03. | 49,3 &rarr; 51,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 15 |

## COVID-positive Todesfälle

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 15. Damit insgesamt bis jetzt 133 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Auch hier ist Dunkelziffer Faktor 8-15 anzunehmen.

| Kreis | 10.03. | 09.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 39,2 | 40,5 | -3,2% |
| LK Ebersberg | 27,7 | 24,2 | +14,5% |
| LK Freising | 33,7 | 32,6 | +3,4% |
| LK Fürstenfeldbruck | 42,5 | 47,6 | -10,7% |
| LK München | 31,7 | 34,0 | -6,8% |
| LK Starnberg | 35,1 | 36,6 | -4,1% |

Stabilisiert sich.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,78. Nach 25 Tagen über 1 jetzt also wieder seit 14 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 03.03. | 0,90 |
| 04.03. | 0,86 |
| 05.03. | 0,84 |
| 06.03. | 0,84 |
| 07.03. | 0,82 |
| 08.03. | 0,80 |
| 09.03. | 0,78 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern unverändert.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 22 | +/-0 | 
| Intensiv, beatmet | 10 | -1 | 
| SUMME | 32 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung fällt heute auf 1.312, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sinken die Zahlen seit einer Woche wieder. Bei den belegten Intensivbetten zeichnet sich noch keine klare Tendenz ab. Im Vergleich zu gestern 41  Betten weniger auf Normalstation und IMC, aber auch 6 Betten mehr auf Intensiv. 8 Patienten weniger, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.411 | -41 |
| Intensiv | 204 | +6 |
| (davon invasiv beatmet) | 54 | -8 |
| SUMME | 2.615 | -35 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.