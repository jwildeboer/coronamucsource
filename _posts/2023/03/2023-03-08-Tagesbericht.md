---
layout: default
title: 2023-03-08 Tagesbericht
category: Tagesberichte
---

![2023-03-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 15 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten in Bayern, die weiter gesunken sind. Dritte zeigt Inzidenz, die jetzt stabil unter 100 liegt und weiter sinkt.](/images/2023/03/20230308.png)

Das RKI meldet eine Inzidenz von 61,8. 113 Neue, 15 Nachmeldungen, davon 14 für vorgestern. Die gestern genannte Inzidenzen von 73,7 erhöhen sich durch Nachmeldungen auf 74,6. Tag 9 Mit Inzidenz unter 100, Tag 15 mit R(t) unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 28.02. | 95,1 &rarr; 98,5 |
| 01.03. | 86,9 &rarr; 92,2 |
| 02.03. | 84,3 &rarr; 85,5 |
| 03.03. | 77,2 &rarr; 78,0 |
| 04.03. | 77,2 &rarr; 78,0 |
| 05.03. | 77,2 &rarr; 78,0 |
| 06.03. | 73,7 &rarr; 74,6 |

## COVID-positive Todesfälle

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 10. Damit insgesamt bis jetzt 127 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 12 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 08.03. | 07.03 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 47,6 | 45,7 | +4,2% |
| LK Ebersberg | 42,9 | 54,6 | -21,4% |
| LK Freising | 30,4 | 54,7 | -44,4% |
| LK Fürstenfeldbruck | 54,9 | 61,8 | -11,2% |
| LK München | 40,9 | 51,7 | -20,9% |
| LK Starnberg | 40,2 | 50,5 | -20,4% |

Teilweise erstaunlich wenig Meldungen, Dunkelziffer steigt wohl.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,84. Nach 25 Tagen über 1 jetzt also wieder seit 13 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 01.03. | 0,95 |
| 02.03. | 0,92 |
| 03.03. | 0,90 |
| 04.03. | 0,86 |
| 05.03. | 0,84 |
| 06.03. | 0,84 |
| 07.03. | 0,82 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gester unverändert.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 22 | +/-0 | 
| Intensiv, beatmet | 11 | +/-0 | 
| SUMME | 33 | +/-0 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung fällt heute auf 1.313, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sinken die Zahlen seit einer Woche wieder. Bei den belegten Intensivbetten zeichnet sich noch keine klare Tendenz ab. Im Vergleich zu gestern 106  Betten weniger auf Normalstation und IMC, 3 Betten weniger auf Intensiv.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.452 | -106 |
| Intensiv | 198 | -3 |
| (davon invasiv beatmet) | 62 | --- |
| SUMME | 2.650 | -109 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.