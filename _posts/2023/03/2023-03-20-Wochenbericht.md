---
layout: default
title: 2023-03-20 Wochenbericht KW11
category: Wochenberichte
---

![2023-03-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche, neue Zahlen aber erst am Dienstag. Der R(t) Wert bleibt unter 1, steigt aber seite einer Woche wieder. Zweite zeigt Krankenhausbetten in Bayern, deren Belegung etwas gesunken ist. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/03/20230320.png)

In der letzten Woche gab es offiziell insgesamt nur 585 neue COVID Fälle in München. Das sind 17,7% **weniger** als in der Vorwoche (711). Leider aber auch 16 Tote mit COVID Nachweis in den letzten 7 Tagen, doppelt soviel als in der Vorwoche (8). Die Dunkelsziffer schätzen wir zwischen Faktor 8 bis 22.
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe klar von 711 auf 585. Die um Nachmeldungen korrigierte Inzidenz fiel von 47,8 auf 39,3, Aber wegen der Abschaffung der Maßnahmen sind diese Zahlen kaum realistisch. Dunkelziffer Faktor 8 bis 22 darf man im Kopf gleich mitrechnen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 12.03. | 0 | 711 | 47,8 |
| 13.03. | 168 | 655 | 44,0 |
| 14.03. | 128 | 656 | 44,1 |
| 15.03. | 123 | 650 | 43,7 |
| 16.03. | 104 | 641 | 43,1 |
| 17.03. | 62 | 585 | 39,3 |
| 18.03. | 0 | 585 | 39,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 22 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche stetig von 0,74 auf jetzt 0,91.

| Datum | R(t) |
|---|---:|
| 13.03. | 0,74 |
| 14.03. | 0,75 |
| 15.03. | 0,79 |
| 16.03. | 0,84 |
| 17.03. | 0,89 |
| 18.03. | 0,91 |
| 19.03. | 0,91 |

## Abwasserwerte für München

Es gibt jetzt zwar endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 7. März. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend ein Plateau auf hohem Niveau an. Eine in der Öffentlichkeit kaum wahrgenommene Dauerwelle.

![Abwasserwerte München](/images/2023/03/KW11Abwasser.png)

## Krankenhauszahlen München - eingestellt von der Stadt

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München blieb die Zahl der belegten Intensivbetten eher stabil um oder etwas unter 30. 
Im Vergleich zum Vorjahr weiterhin klar niedrigere Zahlen. 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 13.03. | 7 | 22 | 29 | +2 | 67 |
| 14.03. | 8 | 22 | 30 | +1 | 67 |
| 15.03. | 9 | 18 | 27 | -3 | 63 |
| 16.03. | 9 | 15 | 24 | -3 | 66 |
| 17.03. | 7 | 16 | 23 | -1 | 63 |
| 18.03. | 8 | 17 | 25 | +2 | 64 |
| 19.03. | 9 | 21 | 30 | +5 | 65 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche gab es wenig Veränderung bei den belegten Betten, aber weiterhin ähnlich hohe Zahlen wie im Vorjahr. Weiter über der alten Grenze der gelben Ampel von 1.200.

Bei den Intensivbetten ist es eher ein Plateau bei klar zwischen 190–200 mit unklarer Tendenz.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 13.03. | 211 | 1.534 | 2.287 |
| 14.03. | 198 | 1.477 | 2.359 |
| 15.03. | 201 | 1.406 | 2.339 |
| 16.03. | 194 | 1.332 | 2.377 |
| 17.03. | 191 | 1.187 | 2.373 |
| 18.03. | 182 | 1.155 | 2.368 |
| 19.03. | 188 | 1.107 | 2.379 |

## Impfungen

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche wieder weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 118 | 75,22% (+0,01%) |
| 2. | 95 | 75,14% (+0,01%) |
| 3. | 130 | 59,25% (+/-0%) |
| 4. | 873 | 12,00% (+/-0%) |
| 5. | 498 | 0,84% (+/-0%) |
| 6. | 58 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/03/KW11Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute (endlich) auf dem Stand vom 20. Februar aktualisiert.

In München führt XBB.1.5 zwar klar mit 44,73%, so richtig dominant ist das aber auch nicht. BQ.1 liegt mit 20,16% auf Platz zwei, BA.2 mit 14,88% auf platz drei. CH.1.1 bleibt weiter unter 20%.

| KW  | 4  | 5  | 6  | 7 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 3,01% | 12,47% | 20,13% | 14,88% |
| Omikron BA.2.75 | 11,58% | 10,25% | 10,04% | 0,00% |
| Omikron BN.1 | 3,32% | 2,09% | 1,77% | 4,77% | 
| Omikron XBB.1.5 | 27,92% | 39,84% | 27,18% | 44,73% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 24,88% | 2,33% | 12,22% | 0,00% |
| Omikron BF.7 | 0,13% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 19,32% | 17,70% | 14,16% | 20,16% |
| Omikron CH.1.1 | 6,97% | 7,41% | 3,71% | 14,43% |
| Rekombinante Linien | 1,72% | 0,55% | 10,14% | 0,62% |
| Andere | 1,15% | 7,36% | 0,65% | 0,41% |

![VoC München](/images/2023/03/KW11VarMuc.png)

## Varianten Bayern

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 50,60% nicht wirklich dominant. BQ.1 ist mit 16,50% auf Platz zwei, BA.2 ist zurück auf Platz drei. CH.1.1 verliert Anteile. Die Zeit dominanter Varianten ist wohl vorbei.

| KW | 7 | 8 | 9 | 10 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 7,60% | 9,80% | 13,30% | 11,80% |
| Omikron BA.2.75 | 0,20% | 1,10% | 1,20% | 4,70% |
| Omikron BN.1 | 4,10% | 3,10% | 1,60% | 0,00% |
| Omikron XBB.1.5 | 36,40% | 39,40% | 47,00% | 50,60% |
| Omikron BA.4 | 0,00% | 0,30% | 0,00% | 0,00% |
| Omikron BA.5 | 4,30% | 4,20% | 2,60% | 1,20% |
| Omikron BF.7 | 2,30% | 1,20% | 0,70% | 0,00% |
| Omikron BQ.1 | 22,90% | 18,40% | 15,20% | 16,50% |
| CH 1.1 | 17,70% | 16,40% | 11,90% | 8,20% |
| Rekombinante | 2,20% | 4,20% | 3,70% | 3,50% |
| Andere | 2,30% | 1,90% | 2,80% | 3,50% |
| Sequenzierungen | 847 | 642 | 231 | 85 |

![VoC Bayern](/images/2023/03/KW11Var.png)
