---
layout: default
title: 2023-03-24 Tagesbericht
category: Tagesberichte
---

![2023-03-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche. Der R(t) Wert ist stabil bei 0,92. Zweite zeigt Krankenhausbetten in Bayern, deren Summe heute weiter sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230324.png)

Das RKI meldet eine Inzidenz von 34,5. 78 Neue, 10 Nachmeldungen, davon 8 für vorgestern. Die gestern genannte Inzidenz von 35,8 erhöht sich durch Nachmeldungen auf 36,4. R(t) bleibt auf 0,92. Dunkelziffer Faktor 8–20.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 16.03. | 42,5 &rarr; 43,2 |
| 17.03. | 39,3 &rarr; 40,3 |
| 18.03. | 39,3 &rarr; 40,3 |
| 19.03. | 39,3 &rarr; 40,3 |
| 20.03. | 38,8 &rarr; 40,1 |
| 21.03. | 38,6 &rarr; 40,1 |
| 22.03. | 35,8 &rarr; 36,4 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 20 |

## COVID-positive Todesfälle

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle sinkt auf 3. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 24.03. | 23.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 14,8 | 16,7 | -11,4% |
| LK Ebersberg | 32,5 | 28,4 | +14,4% |
| LK Freising | 24,3 | 21,5 | +13,0% |
| LK Fürstenfeldbruck | 29,7 | 28,4 | +4,6% |
| LK München | 28,9 | 26,6 | +8,6% |
| LK Starnberg | 27,8 | 30,0 | -7,3% |

Eher steigend. Dunkelziffer Faktor 8–20.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,92. Seit einer Woche stabil.

| Datum | R(t) |
|---|---:|
| 17.03. | 0,89 |
| 18.03. | 0,92 |
| 19.03. | 0,92 |
| 20.03. | 0,92 |
| 21.03. | 0,93 |
| 22.03. | 0,93 |
| 23.03. | 0,92 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht gestiegen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 20 | +3 | 
| Intensiv, beatmet | 9 | +1 | 
| SUMME | 29 | +4 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.049, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es auch heute weiter nach unten. Im Vergleich zu gestern 93 Betten weniger auf Normalstation und IMC, aber 21 Betten mehr auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 9 nach oben. Insgesamt somit 72 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.917 | -93 |
| Intensiv | 195 | +21 |
| (davon invasiv beatmet) | 62 | +9 |
| SUMME | 2.112 | -72 |