---
layout: default
title: 2023-03-17 Tagesbericht
category: Tagesberichte
---

![2023-03-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 17 Tagen wieder unter 1, steigt aber wieder. Zweite zeigt Krankenhausbetten in Bayern, Tendenz leicht fallend. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230317.png)

Das RKI meldet eine Inzidenz von 42,5. 95 Neue, 12 Nachmeldungen, davon 11 für vorgestern. Die gestern genannte Inzidenz von 43,0 erhöht sich durch Nachmeldungen auf 43,7. Der R(t) Wert steigt weiter.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 09.03. | 47,1 &rarr; 47,8 |
| 10.03. | 46,3 &rarr; 47,8 |
| 11.03. | 46,3 &rarr; 47,8 |
| 12.03. | 46,3 &rarr; 47,8 |
| 13.03. | 43,6 &rarr; 44,0 |
| 14.03. | 43,4 &rarr; 44,1 |
| 15.03. | 43,0 &rarr; 43,7 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 19 |

## COVID-positive Todesfälle

Es wurden 7 neuer Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt bis jetzt 149 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 17.03. | 16.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 35,4 | 30,2 | +17,2% |
| LK Ebersberg | 32,5 | 27,0 | +20,4% |
| LK Freising | 34,8 | 34,2 | +1,8% |
| LK Fürstenfeldbruck | 40,7 | 45,8 | -11,1% |
| LK München | 31,7 | 31,2 | +1,6% |
| LK Starnberg | 31,4 | 33,6 | -6,5% |

Leicht steigende Tendenz. Dunkelziffer Faktor 8–19.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,83. Könnte Kipppunkt sein.

| Datum | R(t) |
|---|---:|
| 10.03. | 0,75 |
| 11.03. | 0,75 |
| 12.03. | 0,75 |
| 13.03. | 0,74 |
| 14.03. | 0,75 |
| 15.03. | 0,79 |
| 16.03. | 0,83 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht höher.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 15 | -3 | 
| Intensiv, beatmet | 9 | +/-0 | 
| SUMME | 24 | -3 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.208, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sind die Zahlen wieder etwas gesunken. Bei den belegten Intensivbetten schwanken die Zahlen hin und her. Im Vergleich zu gestern 37 Betten weniger auf Normalstation und IMC, auch 7 Betten weniger auf Intensiv. Davon 6 Patienten weniger, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.312 | -37 |
| Intensiv | 194 | -7 |
| (davon invasiv beatmet) | 64 | -6 |
| SUMME | 2.506 | -44 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.