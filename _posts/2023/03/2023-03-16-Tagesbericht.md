---
layout: default
title: 2023-03-16 Tagesbericht
category: Tagesberichte
---

![2023-03-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 17 Tagen wieder unter 1, steigt aber wieder. Zweite zeigt Krankenhausbetten in Bayern, Tendenz nur leicht fallend. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230316.png)

Das RKI meldet eine Inzidenz von 43,0. 112 Neue, 13 Nachmeldungen, davon 10 für vorgestern. Die gestern genannte Inzidenz von 43,4 erhöht sich durch Nachmeldungen auf 44,1. Der R(t) Wert steigt wieder.
<!--more-->

## Testzahlen - Beendet vom LGL Bayern

Das [LGL Bayern](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm#positive) hat die Veröffentlichung der PCR-Testzahlen komplett eingestellt.

Damit können wir auch diesen Teil der Daten nicht mehr weiterführen. Wir bedanken uns beim LGL für die bisher veröffetnlichten Daten und bedauern sehr, daß wir nun keinerlei Information mehr zur Testhäufigkeit und Positivrate haben.

Damit werden wohl auch die Variantenuntersuchungen deutlich unzuverlässiger, wenn sie überhaupt noch weitergeführt werden.

> **HINWEIS**
> Da die Meldepflicht am 28.02.2023 ausgelaufen ist, haben die Labore die Übermittlung der Untersuchungszahlen eingestellt. Die Publikation der Probenzahlen endet daher am 01.03.2023 und wird nicht weiter fortgeführt.

![LGL zur Einstellung der Testzahlenveröffentlichung](/images/2023/03/20230316TestStop.png)

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 08.03. | 49,3 &rarr; 51,6 |
| 09.03. | 47,1 &rarr; 47,9 |
| 10.03. | 46,3 &rarr; 47,9 |
| 11.03. | 46,3 &rarr; 47,9 |
| 12.03. | 46,3 &rarr; 47,9 |
| 13.03. | 43,6 &rarr; 44,2 |
| 14.03. | 43,4 &rarr; 44,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 19 |

## COVID-positive Todesfälle

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 14. Damit insgesamt bis jetzt 142 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 16.03. | 15.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 35,4 | 30,2 | +17,2% |
| LK Ebersberg | 32,5 | 27,0 | +20,4% |
| LK Freising | 34,8 | 34,2 | +1,8% |
| LK Fürstenfeldbruck | 40,7 | 45,8 | -11,1% |
| LK München | 31,7 | 31,2 | +1,6% |
| LK Starnberg | 31,4 | 33,6 | -6,5% |

Leicht steigende Tendenz. Dunkelziffer Faktor 8–18.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,78. Könnte Kipppunkt sein.

| Datum | R(t) |
|---|---:|
| 09.03. | 0,78 |
| 10.03. | 0,75 |
| 11.03. | 0,75 |
| 12.03. | 0,75 |
| 13.03. | 0,74 |
| 14.03. | 0,75 |
| 15.03. | 0,78 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht höher.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 18 | -4 | 
| Intensiv, beatmet | 9 | +1 | 
| SUMME | 27 | -3 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.2548, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sind die Zahlen seit letzem Wochenende wieder etwas gesunken. Bei den belegten Intensivbetten schwanken die Zahlen hin und her. Im Vergleich zu gestern 63 Betten weniger auf Normalstation und IMC, auch 13 Betten weniger auf Intensiv. Dafür 7 Patienten mehr, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.349 | -36 |
| Intensiv | 201 | +3 |
| (davon invasiv beatmet) | 64 | +3 |
| SUMME | 2.550 | -33 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.