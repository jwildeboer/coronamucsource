---
layout: default
title: 2023-03-25 Tagesbericht
category: Tagesberichte
---

![2023-03-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche. Der R(t) Wert ist stabil bei 0,92. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl in der letzten Woche gesunken ist. Bis Montag keine neuen Zahlen. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230325.png)

Das RKI meldet eine Inzidenz von 35,6. 81 Neue, 10 Nachmeldungen, davon 9 für vorgestern. Die gestern genannte Inzidenz von 34,5 erhöht sich durch Nachmeldungen auf 35,2. R(t) bleibt auf 0,92. Dunkelziffer Faktor 8&ndash;21.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 17.03. | 39,3 &rarr; 40,3 |
| 18.03. | 39,3 &rarr; 40,3 |
| 19.03. | 39,3 &rarr; 40,3 |
| 20.03. | 38,8 &rarr; 40,1 |
| 21.03. | 38,6 &rarr; 40,1 |
| 22.03. | 35,8 &rarr; 36,5 |
| 23.03. | 34,5 &rarr; 35,2 |

Da am Wochenende weder Zahlen gemeldet noch verarbeitet werden, können wir die Inzidenz für Sonntag und Montag schon heute berechnen:

| Sonntag | 35,6 |
| Montag | 35,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 21 |

## COVID-positive Todesfälle

Seit 3 Tagen wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt auf 3. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 25.03. | 24.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 15,4 | 14,8 | +4,1% |
| LK Ebersberg | 27,0 | 32,5 | -16,9% |
| LK Freising | 24,3 | 24,3 | +/-0% |
| LK Fürstenfeldbruck | 27,9 | 29,7 | -6,1% |
| LK München | 23,4 | 28,9 | -19,0% |
| LK Starnberg | 23,4 | 27,8 | -15,8% |

Eher fallend. Dunkelziffer Faktor 8&ndash;21.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,92. Seit einer Woche stabil.

| Datum | R(t) |
|---|---:|
| 18.03. | 0,92 |
| 19.03. | 0,92 |
| 20.03. | 0,92 |
| 21.03. | 0,93 |
| 22.03. | 0,93 |
| 23.03. | 0,92 |
| 24.03. | 0,92 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern unverändert.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 20 | +/-0 | 
| Intensiv, beatmet | 9 | +/-0 | 
| SUMME | 29 | +/-0 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.012, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

**Bis Montag keine neue IVENA Zahl vom LGL, daher keine Aktualisierung**

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es auch heute weiter nach unten. Im Vergleich zu gestern 93 Betten weniger auf Normalstation und IMC, aber 21 Betten mehr auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 9 nach oben. Insgesamt somit 72 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.917 | -93 |
| Intensiv | 195 | +21 |
| (davon invasiv beatmet) | 62 | +9 |
| SUMME | 2.112 | -72 |