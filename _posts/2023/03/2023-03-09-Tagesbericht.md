---
layout: default
title: 2023-03-09 Tagesbericht
category: Tagesberichte
---

![2023-03-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 15 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten in Bayern, die etwas gesunken sind. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230309.png)

Das RKI meldet eine Inzidenz von 49,3. 102 Neue, 14 Nachmeldungen, davon 12 für vorgestern. Die gestern genannte Inzidenz von 61,8 erhöht sich durch Nachmeldungen auf 62,6. Dunkelziffer (geschätzt) Faktor 8-15.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 01.03. | 86,9 &rarr; 92,1 |
| 02.03. | 84,3 &rarr; 85,4 |
| 03.03. | 77,2 &rarr; 78,0 |
| 04.03. | 77,2 &rarr; 78,0 |
| 05.03. | 77,2 &rarr; 78,0 |
| 06.03. | 73,7 &rarr; 74,7 |
| 07.03. | 61,8 &rarr; 62,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 15 |

## COVID-positive Todesfälle

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 11. Damit insgesamt bis jetzt 128 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Auch hier ist Dunkelziffer Faktor 8-15 anzunehmen.

| Kreis | 09.03. | 08.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 40,5 | 47,6 | -15,0% |
| LK Ebersberg | 24,2 | 42,9 | -43,6% |
| LK Freising | 32,6 | 30,4 | +7,2% |
| LK Fürstenfeldbruck | 47,6 | 54,9 | -13,3% |
| LK München | 34,0 | 40,9 | -16,9% |
| LK Starnberg | 36,6 | 40,2 | -9,0% |

Hohe Dunkelziffer, kaum noch PCR-Tests.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,84. Nach 25 Tagen über 1 jetzt also wieder seit 13 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 02.03. | 0,92 |
| 03.03. | 0,90 |
| 04.03. | 0,86 |
| 05.03. | 0,84 |
| 06.03. | 0,84 |
| 07.03. | 0,82 |
| 08.03. | 0,80 |

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Und die Zahlen sind shockierend. Fast 80% weniger PCR-Tests seit Abschaffubg fast aller Corona Maßnahmen in Bayern seit dem 1. März.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 1.948(!) Tests/Tag mit einer Positivrate von 19,76%. Gegenüber den Zahlen von letzter Woche (9379 Tests/Tag mit 20,99% Positivrate) also eigentlich kaum noch Tests bei stabiler Positivrate.

1.948 Tests/Tag ist deutlich zu wenig um ein halbwegs klares Bild der Infektionslage zu erhalten. Wenn man das vergleich mit dem was möglich wäre, wird das überdeutlich. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Also wird nur noch 2% der Kapazität genutzt.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Ein Bett weniger im Vergleich zu gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 19 | -3 | 
| Intensiv, beatmet | 12 | +2 | 
| SUMME | 31 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung fällt heute auf 1.305, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sinken die Zahlen seit 9 Tagen wieder. Bei den belegten Intensivbetten zeichnet sich noch keine klare Tendenz ab. Im Vergleich zu gestern 47  Betten weniger auf Normalstation und IMC, aber auch 5 Betten mehr auf Intensiv, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.364 | -47 |
| Intensiv | 204 | +/-0 |
| (davon invasiv beatmet) | 59 | +5 |
| SUMME | 2.573 | -42 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.