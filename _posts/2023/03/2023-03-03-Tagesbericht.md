---
layout: default
title: 2023-03-03 Tagesbericht
category: Tagesberichte
---

![2023-03-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche. Der R(t) Wert ist seit 10 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten, die weiter gesunken sind. Dritte zeigt Inzidenz, die nach 18 Tagen über 100 heute zum 4. mal wieder drunter liegt.](/images/2023/03/20230303.png)

Das RKI meldet eine Inzidenz von 84,3. 156 Neue, 80 Nachmeldungen, davon 71(!) für vorgestern. Die gestern genannte Inzidenz von 86,9 erhöht sich durch Nachmeldungen auf 91,6. Tag 4 unter Inzidenz 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 23.02. | 106,9 &rarr; 107,5 |
| 24.02. | 107,3 &rarr; 108,4 |
| 25.02. | 107,3 &rarr; 108,4 |
| 26.02. | 107,3 &rarr; 108,4 |
| 27.02. | 91,8 &rarr; 93,4 |
| 28.02. | 95,1 &rarr; 97,9 |
| 01.03. | 86,9 &rarr; 91,6 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 17. Damit insgesamt bis jetzt 118 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 7 |
| Krankenhaus/Inzidenz | 6 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 03.03 | 02.03 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 69,5 | 90,7 | -23,4% |
| LK Ebersberg | 56,7 | 71,2 | -20,4% |
| LK Freising | 87,8 | 98,3 | -10,7% |
| LK Fürstenfeldbruck | 86,0 | 96,5 | -10,9% |
| LK München | 77,5 | 84,9 | -8,7% |
| LK Starnberg | 65,8 | 70,9 | -7,2% |

Überall fallende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,92. Nach 25 Tagen über 1 jetzt also wieder seit 9 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 24.02. | 0,97 |
| 25.02. | 0,98 |
| 26.02. | 0,99 |
| 27.02. | 0,98 |
| 28.02. | 0,97 |
| 01.03. | 0,95 |
| 02.03. | 0,92 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter (Freitagseffekt kommt dazu): Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 24 von 304 auf 280. IMC sinkt um 2 von gestern 12 auf heute 10. Intensiv steigt um 1 von gestern 20 auf heute 21. Damit insgesamt 311 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 280 | -24 | 
| IMC | 10 | -2 | 
| Intensiv | 21 | +1 |
| SUMME | 311 | -25 |

Damit sind 71 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.