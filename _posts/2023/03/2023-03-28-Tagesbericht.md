---
layout: default
title: 2023-03-28 Tagesbericht
category: Tagesberichte
---

![2023-03-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist stabil bei jetzt 0,92. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl in der letzten Woche gesunken ist, heute minimal steigt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230328.png)

Das RKI meldet eine Inzidenz von 31,7. 98 Neue, 10 Nachmeldungen, davon 9 für letzten Freitag. Die Inzidenzen von Freitag bis Sonntag erhöhen sich durch Nachmeldungen von 35,6 auf 36,3. Tag 7 ohne Todesmeldung. R(t) bleibt stabil bei 0,92.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 20.03. | 38,8 &rarr; 40,1 |
| 21.03. | 38,6 &rarr; 40,1 |
| 22.03. | 35,8 &rarr; 36,5 |
| 23.03. | 34,5 &rarr; 35,2 |
| 24.03. | 35,6 &rarr; 36,3 |
| 25.03. | 35,6 &rarr; 36,3 |
| 26.03. | 35,6 &rarr; 36,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 24 |

## COVID-positive Todesfälle

Seit 7 Tagen wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle sinkt auf 2. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 28.03. | 25.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 19,9 | 15,4 | +29,2% |
| LK Ebersberg | 29,7 | 27,0 | +10% |
| LK Freising | 23,2 | 24,3 | -4,5% |
| LK Fürstenfeldbruck | 28,4 | 27,9 | +1,8% |
| LK München | 23,2 | 23,4 | -0,9% |
| LK Starnberg | 25,6 | 23,4 | +9,4% |

Eher steigend. Dunkelziffer Faktor 8&ndash;24.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,92. Seit einer Woche stabil.

| Datum | R(t) |
|---|---:|
| 21.03. | 0,93 |
| 22.03. | 0,93 |
| 23.03. | 0,92 |
| 24.03. | 0,92 |
| 25.03. | 0,92 |
| 26.03. | 0,93 |
| 27.03. | 0,92 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern etwas gestiegen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 23 | +-/0 | 
| Intensiv, beatmet | 12 | +3 | 
| SUMME | 35 | +3 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 832, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Eventuell kommen wir diese Woche aber mit den um Nachmeldungen korrigierten Zahlen wieder darunter.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute minimal nach oben. Im Vergleich zu gestern 12 Betten mehr auf Normalstation und IMC, aber 7 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 3 nach unten. Insgesamt somit 5 Betten mehr belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.857 | +12 |
| Intensiv | 167 | -7 |
| (davon invasiv beatmet) | 61 | -3 |
| SUMME | 2.024 | +5 |