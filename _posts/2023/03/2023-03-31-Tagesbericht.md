---
layout: default
title: 2023-03-31 Tagesbericht
category: Tagesberichte
comments:
  host: muenchen.social
  username: CoronaMUC
  id: 110117856767728840 
---

![2023-03-31 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert sinkt wieder und steht jetzt auf 0,84. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl weiter klar sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230331.png)

Das RKI meldet eine Inzidenz von 24,9. 59 Neue, 12 Nachmeldungen, alle für vorgestern. Die gestern genannte Inzidenz von 25,9 erhöht sich durch Nachmeldungen auf 26,8. Tag 9 ohne Todesmeldung. R(t) sinkt weiter auf 0,84.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 23.03. | 34,5 &rarr; 35,2 |
| 24.03. | 35,6 &rarr; 36,2 |
| 25.03. | 35,6 &rarr; 36,2 |
| 26.03. | 35,6 &rarr; 36,2 |
| 27.03. | 31,7 &rarr; 32,3 |
| 28.03. | 27,2 &rarr; 27,3 |
| 29.03. | 25,9 &rarr; 26,8 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 23 |

## COVID-positive Todesfälle

Seit 9 Tagen wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt somit 0 &mdash; das hatten wir zuletzt am 2. Oktober 2022. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 31.03. | 30.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 19,9 | 23,8 | -16,4% |
| LK Ebersberg | 18,7 | 23,5 | -20,4% |
| LK Freising | 19,9 | 18,8 | +5,9% |
| LK Fürstenfeldbruck | 23,3 | 24,7 | -5,7% |
| LK München | 19,4 | 20,3 | -4,4% |
| LK Starnberg | 23,4 | 21,9 | +6,8% |

Eher fallende Zahlen. Dunkelziffer Faktor 8&ndash;23.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,84. Sinkt also ein weiter.

| Datum | R(t) |
|---|---:|
| 24.03. | 0,92 |
| 25.03. | 0,92 |
| 26.03. | 0,93 |
| 27.03. | 0,93 |
| 28.03. | 0,89 |
| 29.03. | 0,87 |
| 30.03. | 0,84 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern nur um 1 gesunken.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 19 | -1 | 
| Intensiv, beatmet | 11 | +/-0 | 
| SUMME | 30 | -1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steigt heute auf 861, aber diese Zahl wird sich durch Nachmeldungen wohl noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Eventuell kommen wir diese Woche aber mit den um Nachmeldungen korrigierten Zahlen wieder darunter.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute deutlich nach unten. Im Vergleich zu gestern 84 Betten weniger auf Normalstation und IMC, 2 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 6 zurück. Insgesamt somit 86 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.583 | -84 |
| Intensiv | 154 | -2 |
| (davon invasiv beatmet) | 57 | -6 |
| SUMME | 1.737 | -86 |