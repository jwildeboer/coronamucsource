---
layout: default
title: 2023-03-23 Tagesbericht
category: Tagesberichte
---

![2023-03-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ähnlich wie in der Vorwoche. Der R(t) Wert ist stabil bei 0,92. Zweite zeigt Krankenhausbetten in Bayern, deren Summe heute wieder deutlich sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230323.png)

Das RKI meldet eine Inzidenz von 35,8. 61 Neue, 29 Nachmeldungen, davon 20 für vorgestern und 9 im November 2020(!). Die gestern genannte Inzidenz von 38,6 erhöht sich durch Nachmeldungen auf 39,9. R(t) bleibt auf 0,92. Dunkelziffer Faktor 8–20.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 15.03. | 43,0 &rarr; 43,7 |
| 16.03. | 42,5 &rarr; 43,2 |
| 17.03. | 39,3 &rarr; 40,3 |
| 18.03. | 39,3 &rarr; 40,3 |
| 19.03. | 39,3 &rarr; 40,3 |
| 20.03. | 38,8 &rarr; 40,0 |
| 21.03. | 38,6 &rarr; 39,9 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 20 |

## COVID-positive Todesfälle

Es wurden keine neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 10. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 23.03. | 22.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 16,7 | 18,7 | -10,7% |
| LK Ebersberg | 28,4 | 25,6 | +10,9% |
| LK Freising | 21,5 | 21,0 | +2,4% |
| LK Fürstenfeldbruck | 28,4 | 30,2 | -6,0% |
| LK München | 26,6 | 32,0 | -16,9% |
| LK Starnberg | 30,0 | 27,1 | +10,7% |

Uneinheitlich. Dunkelziffer Faktor 8–20.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,89. Könnte Kipppunkt sein.

| Datum | R(t) |
|---|---:|
| 16.03. | 0,84 |
| 17.03. | 0,89 |
| 18.03. | 0,92 |
| 19.03. | 0,92 |
| 20.03. | 0,92 |
| 21.03. | 0,93 |
| 22.03. | 0,92 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht gesunken.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 17 | +/-0 | 
| Intensiv, beatmet | 8 | -3 | 
| SUMME | 25 | -3 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.057, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es auch heute ungewohnt deutlich nach unten. Im Vergleich zu gestern 149 Betten weniger auf Normalstation und IMC, 8 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 2 nach oben. Insgesamt somit 157 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.010 | -149 |
| Intensiv | 174 | -8 |
| (davon invasiv beatmet) | 53 | +2 |
| SUMME | 2.341 | -157 |