---
layout: default
title: 2023-03-15 Tagesbericht
category: Tagesberichte
---

![2023-03-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 17 Tagen wieder unter 1, sinkt aber nicht mehr. Zweite zeigt Krankenhausbetten in Bayern, Tendenz leicht fallend. Dritte zeigt Inzidenz, die zwar stabil unter 100 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230315.png)

Das RKI meldet eine Inzidenz von 43,4. 117 Neue, 10 Nachmeldungen, davon 9 für vorgestern. Die gestern genannte Inzidenz von 43,6 erhöht sich durch Nachmeldungen auf 44,2. Der R(t) Wert steigt wieder ein wenig.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 07.03. | 61,8 &rarr; 63,3 |
| 08.03. | 49,3 &rarr; 51,6 |
| 09.03. | 47,1 &rarr; 47,9 |
| 10.03. | 46,3 &rarr; 47,8 |
| 11.03. | 46,3 &rarr; 47,8 |
| 12.03. | 46,3 &rarr; 47,8 |
| 13.03. | 43,6 &rarr; 44,2 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurden 8 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 14. Damit insgesamt bis jetzt 141 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 15.03. | 14.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 30,2 | 40,5 | -25,4% |
| LK Ebersberg | 27,0 | 24,2 | +11,6% |
| LK Freising | 34,2 | 37,5 | -8,8% |
| LK Fürstenfeldbruck | 45,8 | 49,4 | -7,3% |
| LK München | 31,2 | 32,9 | -5,2% |
| LK Starnberg | 33,6 | 37,3 | -9,9% |

Uneinheitlich. Dunkelziffer Faktor 8–18.

## Reproduktionswert R(t) für München

Unser R(t) steht auf 0,75 und sinkt nicht weiter. Könnte Kipppunkt werden.

| Datum | R(t) |
|---|---:|
| 08.03. | 0,80 |
| 09.03. | 0,78 |
| 10.03. | 0,75 |
| 11.03. | 0,75 |
| 12.03. | 0,75 |
| 13.03. | 0,74 |
| 14.03. | 0,75 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht höher.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 22 | +/-0 | 
| Intensiv, beatmet | 8 | +1 | 
| SUMME | 30 | +1 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.2548, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sind die Zahlen seit letzem Wochenende wieder etwas gesunken. Bei den belegten Intensivbetten schwanken die Zahlen hin und her. Im Vergleich zu gestern 63 Betten weniger auf Normalstation und IMC, auch 13 Betten weniger auf Intensiv. Dafür 7 Patienten mehr, die invasiv beatmet werden müssen.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.385 | -63 |
| Intensiv | 198 | -13 |
| (davon invasiv beatmet) | 61 | +7 |
| SUMME | 2.583 | -76 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.