---
layout: default
title: 2023-03-06 Wochenbericht KW09
category: Wochenberichte
---

![2023-03-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche, neue Zahlen aber erst am Dienstag. Der R(t) Wert ist seit 11 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten, die weiter gesunken sind, aber von der Stadt München nicht mehr aktualisiert werden. Dritte zeigt Inzidenz, die jetzt stabil unter 100 liegt und weiter sinkt.](/images/2023/03/20230306.png)

In der letzten Woche gab es insgesamt 1.149 neue COVID Fälle in München. Das sind 29,5% **weniger** als in der Vorwoche (1.622). Leider auch 16 Tote mit COVID Nachweis in den letzten 7 Tagen, etwas weniger als in der Vorwoche (18).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe klar von 1.622 auf 1.149. Die um Nachmeldungen korrigierte Inzidenz fiel von 107,3 auf 77,2, die Welle scheint also gebrochen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 26.02. | 0 | 1.622 | 109,0 |
| 27.02. | 268 | 1.398 | 94,0 |
| 28.02. | 302 | 1.463 | 98,3 |
| 01.03. | 302 | 1.368 | 92,0 |
| 02.03. | 167 | 1.269 | 85,3 |
| 03.03. | 110 | 1.149 | 77,2 |
| 04.03. | 0 | 1.149 | 77,2 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 0,98 auf jetzt 0,86 und ist damit wieder stabil unter 1.

| Datum | R(t) 7 Tage |
|---|---:|
| 27.02. | 0,98 |
| 28.02. | 0,97 |
| 01.03. | 0,95 |
| 02.03. | 0,92 |
| 03.03. | 0,90 |
| 04.03. | 0,86 | 
| 05.03. | 0,84 |

## Abwasserwerte für München

Es gibt jetzt zwar endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen Stand heute nur bis zum 22. Februar. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend an, daß wir die Spitze überschritten haben, aber die Werte fallen nur langsam bis kaum.

![Abwasserwerte München](/images/2023/03/KW09Abwasser.png)

## Krankenhauszahlen München - eingestellt von der Stadt

Bei den Krankenhauszahlen in München sank die Zahl der belegten Normalbetten auf unter 300. IMC tendiert etwas höher um 10, Intensiv sank eher auf um  20. Insgesamt eine klar erkennbare Senkung der Zahlen.

Auch im Vergleich zum Vorjahr niedrigere Zahlen. 

**Seit heute, Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr weiterführen.**

Die Zahlen sind zwar weiterhin im IVENA System enthalten, aber der Zugriff auf die IVENA Zahlen ist in Bayern eingeschränkt. In anderen Budesländern sind diese Zahlen für Jedermann zugänglich, in Bayern aber seit einigen Jahren (schon vor Corona) aber nicht.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 27.02. | 351 | 7 | 27 | 385 | -15 | 545 |
| 28.02. | 330 | 7 | 24 | 361 | -39 | 561 |
| 01.03. | 304 | 12 | 20 | 336 |-58 | 568 |
| 02.03. | 280 | 10 | 21 | 311 | -71 | 518 |
| 03.03. | --- | --- | --- | --- | --- | 507 |
| 04.03. | --- | --- | --- | --- | --- | 511 |
| 05.03. | --- | --- | --- | --- | --- | 517 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche gab es eher weniger belegte Betten, aber weiterhin fast so hohe Zahlen wie im Vorjahr. Weit über der alten Grenze der gelben Ampel von 1.200, eher um und über 2.000. Warum das einfch so hingenommen wird - unbegreiflich.

Bei den Intensivbetten ist es eher ein Plateau bei klar über 200.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 27.02. | 221 | 2.248 | 1.986 |
| 28.02. | 238 | 2.269 | 1.966 |
| 01.03. | 214 | 1.968 | 1.905 |
| 02.03. | 219 | 1.807 | 1.916 |
| 03.03. | 215 | 1.585 | 1.928 |
| 04.03. | 216 | 1.539 | 1.943 |
| 05.03. | 212 | 1.473 | 2.035 |

## Impfungen

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche wieder weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 128 | 75,21% (+/-0%) |
| 2. | 118 | 75,13% (+/-0%) |
| 3. | 309 | 59,25% (+/-0%) |
| 4. | 1.741 | 11,99% (+0,02%) |
| 5. | 693 | 0,84% (+0,01%) |
| 6. | 63 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/03/KW09Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 9.379 Tests/Tag mit einer Positivrate von 20,99%. Gegenüber den Zahlen von letzter Woche (10.064 Tests/Tag mit 20,14% Positivrate) also wieder weniger Tests bei leicht steigender Positivrate.

9.379 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom möglichen Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 9.379 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 20,99%.](/images/2023/03/20230302Test.png)

## Varianten München - nicht aktualisiert

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten mittlerweile einen Monat hinterherhinken.

Die Daten für München sind weiterhin auf dem Stand vom 30. Januar und wurden heute im Vergleich zur Vorwoche nicht aktualisiert.

Die Variantenverteiliung stellt daher einen veralteten Stand dar und hat nur wenig mit der aktuellen Situation zu tun. Schade.

| Variante | KW 01 | KW 02 | KW 03 | KW 04 |
|---|--:|--:|--:|--:|
| BA,2 | 0,48% | 0,00% | 9,00% | 0,00% |
| BA,2,75 | 18,06% | 15,26% | 14,27% | 13,79% |
| BN,1 | 4,35% | 4,54% | 2,76% | 6,63% |
| XBB,1,5 | 9,73% | 13,01% | 16,77% | 20,58% |
| BA,4 | 0,00% | 0,00% | 0,00% | 0,00% |
| BA,5 | 43,92% | 42,03% | 34,41% | 38,67% |
| BF,7 | 2,59% | 0,46% | 1,41% | 0,26% |
| BQ,1 | 19,97% | 17,71% | 17,17% | 15,69% |
| Andere | 0,90% | 6,99% | 4,21% | 4,38% |

![VoC München](/images/2023/02/KW07VarMUC.png)

## Varianten Bayern

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 42,3% nicht wirklich dominant. Die neu aufgenommene Varinte CH.1.1 liegt mit 17,9% auf Platz zwei, BQ.1 ist mit 17,6% auf Platz drei. Die Zeit dominanter Varianten ist wohl vorbei.

| Variante | KW 05 | KW06 | KW07 | KW08 |
|:--|--:|--:|--:|--:|
| BA.2 | 4.4% | 7.8% | 8.1% | 8.3% |
| BA.2.75 | 0.9% | 1.7% | 0.0% | 0.6% |
| BN.1 | 9.1% | 5.4% | 4.0% | 2.5% |
| XBB.1.5 | 22.0% | 27.2% | 38.1% | 42.3% |
| BA.4 | 0.0% | 0.0% | 0.0% | 0.6% |
| BA.5 | 8.1% | 6.4% | 3.6% | 4.9% |
| BF.7 | 4.8% | 1.8% | 2.5% | 1.2% |
| BQ.1 | 31,2% | 30.0% | 22.7% | 17.6% |
| CH.1.1 | 14,8% | 12,7% | 15,7% | 17,9% |
| Rekombinante | 4.2% | 5.0% | 2.2% | 2.8% |
| Andere | 0,5% | 2.0% | 3.1% | 1.3% |
| Sequenzierungen | 773 | 706 | 604 | 324 |

![VoC Bayern](/images/2023/03/KW09Var.png)
