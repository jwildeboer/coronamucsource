---
layout: default
title: 2023-03-04 Tagesbericht
category: Tagesberichte
---

![2023-03-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche. Der R(t) Wert ist seit 11 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten, die weiter gesunken sind, aber bis Montag nicht aktualisiert werden. Dritte zeigt Inzidenz, die jetzt stabil unter 100 liegt und weiter sinkt.](/images/2023/03/20230304.png)

Das RKI meldet eine Inzidenz von 77,2. 110 Neue, 15 Nachmeldungen, davon 11 für vorgestern. Die gestern genannte Inzidenz von 84,3 erhöht sich durch Nachmeldungen auf 85,3. Tag 5 unter Inzidenz 100. Samstag, daher keine neuen Krankenhauszahlen.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 24.02. | 107,3 &rarr; 108,4 |
| 25.02. | 107,3 &rarr; 108,4 |
| 26.02. | 107,3 &rarr; 108,4 |
| 27.02. | 91,8 &rarr; 93,4 |
| 28.02. | 95,1 &rarr; 97,9 |
| 01.03. | 86,9 &rarr; 91,6 |
| 03.03. | 84,3 &rarr; 85,3 |

Und da am Wochenende keine Zahlen gemeldet werden, ist die Inzidenz für Sonntag und Montag schon jetzt klar:

| Sonntag | 77,2 |
| Montag | 77,2 |

Am Dienstag dann wieder neue Zahlen vom RKI.

## COVID-positive Todesfälle

Es wurden 7 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt bis jetzt 125 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 8 |
| Krankenhaus/Inzidenz | 7 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 04.03 | 03.03 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 71,4 | 69,5 | +2,7% |
| LK Ebersberg | 55,3 | 56,7 | -2,5% |
| LK Freising | 77,3 | 87,8 | -12,0% |
| LK Fürstenfeldbruck | 76,9 | 86,0 | -10,6% |
| LK München | 68,9 | 77,5 | -11,1% |
| LK Starnberg | 60,7 | 65,8 | -7,8% |

Weiter fallende Zahlen. Inzidenz überall klar und stabil unter 100.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,90. Nach 25 Tagen über 1 jetzt also wieder seit 10 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 25.02. | 0,98 |
| 26.02. | 0,99 |
| 27.02. | 0,98 |
| 28.02. | 0,97 |
| 01.03. | 0,95 |
| 02.03. | 0,92 |
| 03.03. | 0,90 |

## Krankenhaus

**Stand 2023-03-03, Aktualiserung erst wieder am Montag**

Die Zahl der belegten Krankenhausbetten sinkt weiter (Freitagseffekt kommt dazu): Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 24 von 304 auf 280. IMC sinkt um 2 von gestern 12 auf heute 10. Intensiv steigt um 1 von gestern 20 auf heute 21. Damit insgesamt 311 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 280 | -24 | 
| IMC | 10 | -2 | 
| Intensiv | 21 | +1 |
| SUMME | 311 | -25 |

Damit sind 71 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.