---
layout: default
title: 2023-03-29 Tagesbericht
category: Tagesberichte
---

![2023-03-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert sinkt wieder und steht jetzt auf 0,89. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl in der letzten Woche gesunken ist und auch heute wieder klar sinkt. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230329.png)

Das RKI meldet eine Inzidenz von 27,2. 51 Neue, 11 Nachmeldungen, davon 10 für vorgestern. Die gestern genannte Inzidenz von 31,7 erhöht sich durch Nachmeldungen auf 32,4. Tag 8 ohne Todesmeldung. R(t) sinkt etwas auf 0,89.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 21.03. | 38,6 &rarr; 40,2 |
| 22.03. | 35,8 &rarr; 36,6 |
| 23.03. | 34,5 &rarr; 35,3 |
| 24.03. | 35,6 &rarr; 36,3 |
| 25.03. | 35,6 &rarr; 36,3 |
| 26.03. | 35,6 &rarr; 36,3 |
| 27.03. | 31,7 &rarr; 32,4 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 23 |

## COVID-positive Todesfälle

Seit 8 Tagen wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle sinkt somit auf 0 &mdash; das hatten wir zuletzt am 2. Oktober 2022. Damit insgesamt bis jetzt 152 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 29.03. | 28.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 24,4 | 19,9 | +22,6% |
| LK Ebersberg | 32,5 | 29,7 | +9,4% |
| LK Freising | 19,3 | 23,2 | -16,8% |
| LK Fürstenfeldbruck | 27,0 | 28,4 | -4,9% |
| LK München | 21,7 | 23,2 | -6,5% |
| LK Starnberg | 25,6 | 25,6 | +/-0% |

Eher steigend. Dunkelziffer Faktor 8&ndash;23.

## Reproduktionswert R(t) für München

Unser R(t) steht bei 0,89. Sinkt also ein wenig.

| Datum | R(t) |
|---|---:|
| 22.03. | 0,93 |
| 23.03. | 0,92 |
| 24.03. | 0,92 |
| 25.03. | 0,92 |
| 26.03. | 0,93 |
| 27.03. | 0,93 |
| 27.03. | 0,89 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern etwas gesunken.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 18 | -5 | 
| Intensiv, beatmet | 12 | +3 | 
| SUMME | 30 | -5 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steigt heute auf 875, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200. Eventuell kommen wir diese Woche aber mit den um Nachmeldungen korrigierten Zahlen wieder darunter.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, geht es heute deutlich nach unten. Im Vergleich zu gestern 95 Betten weniger auf Normalstation und IMC, 2 Betten weniger auf Intensiv. Die Zahl der invasiv Beatmeten geht im Vergleich zu gestern um 5 nach oben. Insgesamt somit 97 Betten weniger belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 1.762 | -95 |
| Intensiv | 165 | -2 |
| (davon invasiv beatmet) | 66 | +5 |
| SUMME | 1.927 | -95 |