---
layout: default
title: 2023-03-13 Wochenbericht KW10
category: Wochenberichte
---

![2023-03-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar niedriger als in der Vorwoche, neue Zahlen aber erst am Dienstag. Der R(t) Wert bleibt unter 1. Zweite zeigt Krankenhausbetten in Bayern, deren Belegung etwas gestiegen ist. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/03/20230313.png)

In der letzten Woche gab es insgesamt 1.149 neue COVID Fälle in München. Das sind 29,5% **weniger** als in der Vorwoche (1.622). Leider auch 16 Tote mit COVID Nachweis in den letzten 7 Tagen, etwas weniger als in der Vorwoche (18).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe klar von 1.622 auf 1.149. Die um Nachmeldungen korrigierte Inzidenz fiel von 107,3 auf 77,2, die Welle scheint also gebrochen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 05.03. | 0 | 1.164 | 78,2 |
| 06.03. | 222 | 1.117 | 75,1 |
| 07.03. | 128 | 940 | 63,2 |
| 08.03. | 129 | 766 | 51,5 |
| 09.03. | 113 | 711 | 47,8 |
| 10.03. | 118 | 710 | 47,7 |
| 11.03. | 0 | 710 | 47,7 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche nur noch leicht von 0,84 auf jetzt 0,75.

| Datum | R(t) |
|---|---:|
| 06.03. | 0,84 |
| 07.03. | 0,82 |
| 08.03. | 0,80 |
| 09.03. | 0,78 |
| 10.03. | 0,75 |
| 11.03. | 0,75 |
| 12.03. | 0,75 |

## Abwasserwerte für München

Es gibt jetzt zwar endlich Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen Stand heute nur bis zum 28. Februar. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend an, daß wir die Spitze überschritten haben, aber die Werte fallen nur langsam bis kaum.

![Abwasserwerte München](/images/2023/03/KW10Abwasser.png)

## Krankenhauszahlen München - eingestellt von der Stadt

**Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen.**

Bei den Krankenhauszahlen in München sank die Zahl der belegten Intensivbetten von 33 auf 27 - also minimal. 
Auch im Vergleich zum Vorjahr niedrigere Zahlen. 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 06.03. | 11 | 22 | 33 | -1 | 62 |
| 07.03. | 11 | 22 | 33 | 0 | 62 |
| 08.03. | 10 | 22 | 32 |-1 | 68 |
| 09.03. | 12 | 19 | 31 | -1 | 62 |
| 10.03. | 11 | 23 | 34 | +3 | 57 |
| 11.03. | 10 | 21 | 31 | -3 | 60 |
| 12.03. | 7 | 20 | 27 | -4 | 64 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche gab es wenig Veränderung bei den belegten Betten, aber weiterhin ähnlich hohe Zahlen wie im Vorjahr. Weiter über der alten Grenze der gelben Ampel von 1.200.

Bei den Intensivbetten ist es eher ein Plateau bei klar über 200 mit unklarer Tendenz.

| Datum | Intensiv | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|---:|
| 06.03. | 211 | 1.786 | 1.927 |
| 07.03. | 198 | 1.665 | 1.942 |
| 08.03. | 204 | 1.600 | 2.036 |
| 09.03. | 209 | 1.515 | 2.047 |
| 10.03. | 202 | 1.500 | 2.132 |
| 11.03. | 208 | 1.492 | 2.197 |
| 12.03. | 201 | 1.481 | 2.236 |

## Impfungen

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche wieder weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 101 | 75,21% (+/-0%) |
| 2. | 119 | 75,13% (+/-0%) |
| 3. | 175 | 59,25% (+/-0%) |
| 4. | 1.089 | 12,00% (+0,01%) |
| 5. | 523 | 0,84% (+/-0%) |
| 6. | 70 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/03/KW10Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Und die Zahlen sind shockierend. Fast 80% weniger PCR-Tests seit Abschaffubg fast aller Corona Maßnahmen in Bayern seit dem 1. März.

Laut LGL waren wir in der letzten Woche bayernweit bei nur noch 1.948(!) Tests/Tag mit einer Positivrate von 19,76%. Gegenüber den Zahlen von letzter Woche (9379 Tests/Tag mit 20,99% Positivrate) also eigentlich kaum noch Tests bei stabiler Positivrate.

1.948 Tests/Tag ist deutlich zu wenig um ein halbwegs klares Bild der Infektionslage zu erhalten. Wenn man das vergleich mit dem was möglich wäre, wird das überdeutlich. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Also wird nur noch 2% der Kapazität genutzt.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München - weiter nicht aktualisiert

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten mittlerweile einen Monat hinterherhinken.

Die Daten für München sind weiterhin auf dem Stand vom 30. Januar und wurden heute im Vergleich zur Vorwoche wieder nicht aktualisiert.

Die Variantenverteiliung stellt daher einen veralteten Stand dar und hat nur wenig mit der aktuellen Situation zu tun. Schade.

| Variante | KW 01 | KW 02 | KW 03 | KW 04 |
|---|--:|--:|--:|--:|
| BA,2 | 0,48% | 0,00% | 9,00% | 0,00% |
| BA,2,75 | 18,06% | 15,26% | 14,27% | 13,79% |
| BN,1 | 4,35% | 4,54% | 2,76% | 6,63% |
| XBB,1,5 | 9,73% | 13,01% | 16,77% | 20,58% |
| BA,4 | 0,00% | 0,00% | 0,00% | 0,00% |
| BA,5 | 43,92% | 42,03% | 34,41% | 38,67% |
| BF,7 | 2,59% | 0,46% | 1,41% | 0,26% |
| BQ,1 | 19,97% | 17,71% | 17,17% | 15,69% |
| Andere | 0,90% | 6,99% | 4,21% | 4,38% |

![VoC München](/images/2023/02/KW07VarMUC.png)

## Varianten Bayern

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 46,3% nicht wirklich dominant. BQ.1 ist mit 15,6% auf Platz zwei, die letze Woche neu aufgenommene Variante CH.1.1 liegt mit 13,0% auf Platz drei. Die Zeit dominanter Varianten ist wohl vorbei.

| Variante | KW06 | KW07 | KW08 | KW09 |
|:--|--:|--:|--:|--:|
| BA.2 | 7,3% | 7,7% | 10,3% | 11,3% |
| BA.2.75 | 1,5% | 0,2% | 0,9% | 0,0% |
| BN.1 | 5,4% | 4,0% | 2,8% | 2,2% |
| XBB.1.5 | 28,0% | 37,0% | 38,8% | 46,3% |
| BA.4 | 0,0% | 0,0% | 0,3% | 0,0% |
| BA.5 | 6,0% | 4,1% | 4,8% | 3,5% |
| BF.7 | 1,8% | 2,2% | 1,1% | 1,3% |
| BQ.1 | 30,2% | 22,2% | 19,2% | 15,6% |
| CH.1.1 | 12,3% | 17,9% | 16,2% | 13,0% |
| Rekombinante | 5,6% | 2,2% | 4,0% | 3,9% |
| Andere | 1,9% | 2,5% | 1,6% | 2,9% |
| Sequenzierungen | 706 | 847 | 642 | 231 |

![VoC Bayern](/images/2023/03/KW10Var.png)
