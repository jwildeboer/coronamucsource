---
layout: default
title: 2023-03-21 Tagesbericht
category: Tagesberichte
---

![2023-03-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche. Der R(t) Wert steigt wieder. Zweite zeigt Krankenhausbetten in Bayern, eher Plateau. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230321.png)

Das RKI meldet eine Inzidenz von 38,8. 146 Neue, 15 Nachmeldungen, davon 13 für Freitag. Die Inzidenzen von Freitag, Samstag und Sonntag erhöhen sich durch Nachmeldungen von 39,3 auf 40,3. R(t) bleibt stabil bei 0,91. Dunkelziffer Faktor 8–22.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 13.03. | 43,6 &rarr; 44,0 |
| 14.03. | 43,4 &rarr; 44,1 |
| 15.03. | 43,0 &rarr; 43,7 |
| 16.03. | 42,5 &rarr; 43,2 |
| 17.03. | 39,3 &rarr; 40,3 |
| 18.03. | 39,3 &rarr; 40,3 |
| 19.03. | 39,3 &rarr; 40,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 22 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 17. Damit insgesamt bis jetzt 150 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 21.03. | 18.03. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 25,7 | 30,2 | -14,9% |
| LK Ebersberg | 29,1 | 28,4 | +2,5% |
| LK Freising | 23,2 | 24,8 | -6,5% |
| LK Fürstenfeldbruck | 27,0 | 35,2 | -23,3% |
| LK München | 32,6 | 32,3 | +0,9% |
| LK Starnberg | 29,3 | 35,1 | -16,5% |

Sinkende Tendenz. Dunkelziffer Faktor 8–22.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,89. Könnte Kipppunkt sein.

| Datum | R(t) |
|---|---:|
| 14.03. | 0,75 |
| 15.03. | 0,79 |
| 16.03. | 0,84 |
| 17.03. | 0,89 |
| 18.03. | 0,92 |
| 19.03. | 0,92 |
| 20.03. | 0,91 |

## Krankenhausbelegung München

Da die Stadt München am 06.03.2023 die Veröffentlichung der Krankenhausbelegung eingestellt hat, bleiben uns nur die Zahlen vom RKI zur Intensivbettenbelegung mit COVID-positiv getesteten Patienten. Diese Zahl ist gegenüber gestern leicht gesunken.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 22 | +1 | 
| Intensiv, beatmet | 8 | -1 | 
| SUMME | 30 | +/-0 |

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung steht heute auf 1.063, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, bleibt es in Summe gleich. Im Vergleich zu gestern 7 Betten weniger auf Normalstation und IMC, dafür 7 Betten mehr auf Intensiv. 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.295 | -7 |
| Intensiv | 195 | +7 |
| (davon invasiv beatmet) | 55 | +/-0 |
| SUMME | 2.490 | +/-0 |