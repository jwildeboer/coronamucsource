---
layout: default
title: 2023-03-07 Tagesbericht
category: Tagesberichte
---

![2023-03-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche. Der R(t) Wert ist seit 13 Tagen wieder unter 1. Zweite zeigt Krankenhausbetten in Bayern, die weiter gesunken sind. Dritte zeigt Inzidenz, die jetzt stabil unter 100 liegt und weiter sinkt.](/images/2023/03/20230307.png)

Das RKI meldet eine Inzidenz von 73,7. 204 Neue, 15 Nachmeldungen, davon 10 für letzten Freitag. Die Inzidenzen für Freitag, Samstag und Sonntag von 77,2 erhöhen sich durch Nachmeldungen auf 78,1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 27.02. | 91,8 &rarr; 93,4 |
| 28.02. | 95,1 &rarr; 97,9 |
| 01.03. | 86,9 &rarr; 91,6 |
| 02.03. | 84,3 &rarr; 85,3 |
| 03.03. | 77,2 &rarr; 78,1 |
| 04.03. | 77,2 &rarr; 78,1 |
| 05.03. | 77,2 &rarr; 78,1 |

## COVID-positive Todesfälle

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt bis jetzt 126 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert | 8 |
| Krankenhaus/Inzidenz | 9 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 07.03 | 04.03 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 45,7 | 71,4 | -36,0% |
| LK Ebersberg | 54,6 | 55,3 | -1,3% |
| LK Freising | 54,7 | 77,3 | -29,2% |
| LK Fürstenfeldbruck | 61,8 | 76,9 | -19,6% |
| LK München | 51,7 | 68,9 | -25,0% |
| LK Starnberg | 50,5 | 60,7 | -16,8% |

Dachau hat gar nicht gemeldet, Freising und LK München deutlich zu wenig. Morgen hoffentlich aussagekräftigere Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) sinkt auf 0,84. Nach 25 Tagen über 1 jetzt also wieder seit 13 Tagen unter der exponenziellen Grenze.

| Datum | R(t) |
|---|---:|
| 28.02. | 0,97 |
| 01.03. | 0,95 |
| 02.03. | 0,92 |
| 03.03. | 0,90 |
| 04.03. | 0,86 |
| 05.03. | 0,84 |
| 06.03. | 0,84 |

## Krankenhaus

**Stand 2023-03-03, keine Aktualisierung möglich, da Stadt die Veröffentlichung der Zahlen eingestellt hat**

Die Zahl der belegten Krankenhausbetten sinkt weiter (Freitagseffekt kommt dazu): Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 24 von 304 auf 280. IMC sinkt um 2 von gestern 12 auf heute 10. Intensiv steigt um 1 von gestern 20 auf heute 21. Damit insgesamt 311 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 280 | -24 | 
| IMC | 10 | -2 | 
| Intensiv | 21 | +1 |
| SUMME | 311 | -25 |

Damit sind 71 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Krankenhausbelegung Bayern

Die 7 Tage Hospitalisierung fällt heute auf 1.355, aber diese Zahl wird sich durch Nachmeldungen noch deutlich erhöhen. Seit dem 26. Januar liegen wir kontinuierlich und klar über der "alten" gelben Ampel von 1.200.

Bei der IVENA Gesamtzahl, also der Zahl der insgesamt mit COVID-positiven Patienten belegten Betten, sinken die Zahlen seit einer Woche wieder. Bei den belegten Intensivbetten zeichnet sich noch keine klare Tendenz ab. Im Vergleich zu gestern 35 Betten weniger auf Normalstation und IMC, 1 Bett weniger auf Intensiv.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation+IMC | 2.528 | -35 |
| Intensiv | 211 | -1 |
| Invasiv beatmet | 30 | --- |
| SUMME | 2.769 | -36 |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.