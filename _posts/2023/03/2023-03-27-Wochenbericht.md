---
layout: default
title: 2023-03-27 Wochenbericht KW12
category: Wochenberichte
---

![2023-03-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche. Bis Dienstag keine neuen Zahlen. Der R(t) Wert ist jetzt 0,91. Zweite zeigt Krankenhausbetten in Bayern, deren Zahl in der letzten Woche klar gesunken ist. Dritte zeigt Inzidenz, die zwar stabil unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/03/20230327.png)

In der letzten Woche gab es offiziell insgesamt nur 530 neue COVID Fälle in München. Das sind 11,7% **weniger** als in der Vorwoche (600). Leider aber auch 3 Tote mit COVID Nachweis in den letzten 7 Tagen, immerhin deutlich weniger als in der Vorwoche (16). Die Dunkelsziffer schätzen wir zwischen Faktor 8 bis 23.
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 600 auf 530. Die um Nachmeldungen korrigierte Inzidenz fiel von 40,3 auf 35,6, Aber wegen der Abschaffung der Maßnahmen sind diese Zahlen kaum realistisch. Dunkelziffer Faktor 8 bis 21 sollte man im Kopf gleich mitrechnen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 19.03. | 0 | 600 | 40,3 |
| 20.03. | 165 | 597 | 40,1 |
| 21.03. | 128 | 597 | 40,1 |
| 22.03. | 69 | 543 | 36,5 |
| 23.03. | 87 | 524 | 35,2 |
| 24.03. | 81 | 530 | 35,6 |
| 25.03. | 0 | 530 | 35,6 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir die Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für Bayern abschätzen.

Die Wahrheit wid wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 8 |
| Krankenhaus/Inzidenz basiert für Bayern | 23 |

## Reproduktionswert R(t) für München

Unser R(t) war in der letzten Wochen ziemlich stabil zwischen 0,91 und 0,93.

| Datum | R(t) |
|---|---:|
| 20.03. | 0,92 |
| 21.03. | 0,93 |
| 22.03. | 0,93 |
| 23.03. | 0,92 |
| 24.03. | 0,92 |
| 25.03. | 0,91 |
| 26.03. | 0,92 |

## Abwasserwerte für München

Es gibt jetzt auch Daten von der [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring) für München, aber die Daten laufen ein wenig hinterher und gehen aktuell bis zum 14. März. Besonders deutlich zu sehen: Die Spitze im September/Oktober 2022, die man weiterhin NICHT die **WiesnWelle** nennen darf ;)

In den Abwasserwerten zeigt der Trend für München auf eher sinkende Zahlen.

![Abwasserwerte München](/images/2023/03/KW12Abwasser.png)

## Krankenhauszahlen München - eingestellt von der Stadt

Seit Montag, dem 06. März 2023, hat die Stadt München die Veröffentlichung der Krankenhauszahlen eingestellt. Daher können wir diesen Teil der Tages- und Wochenberichte leider nicht mehr vollständig weiterführen. Es fehlen Zahlen zur Belegung auf den Normalstationen.

Bei den Krankenhauszahlen in München blieb die Zahl der belegten Intensivbetten eher stabil um oder etwas über 30. 
Im Vergleich zum Vorjahr nor die Hälfte - gut! 

| Datum | Invasiv beatmet | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:
| 20.03. | 8 | 22 | 30 | +/-0 | 63 |
| 21.03. | 11 | 17 | 28 | -2 | 69 |
| 22.03. | 8 | 17 | 25 | -3 | 68 |
| 23.03. | 9 | 20 | 29 | +4 | 68 |
| 24.03. | 9 | 20 | 29 | -/-0 | 68 |
| 25.03. | 9 | 23 | 32 | +3 | 66 |
| 26.03. | 12 | 19 | 31 | -1 | 63 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Monaten nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

In der letzten Woche sank die IVENA Zahl der insgesamt belegten Krankenhausbetten mit COVID-positiv getesteten Patienten unerwartet deutlich von 2490 auf heute 2.019.

Die 7 Tage Hospitaliserung blieb noch größtenteils über dem "alten" Grenzwert von 1.200 (Stichwort Gelbe Ampel), sinkt aber jetzt auch stabil.

Bei den Intensivbetten sinken die Zahlen zwar auch &ndash; von 195 auf jetzt 174 &ndash; dort aber langsamer.

| Datum | Intensiv | IVENA<br>Gesamt | 7 Tage<br>Hosp | 7 Tage Hosp 2021<br>z. Vgl |
|---|---:|---:|---:|---:|
| 20.03. | 195 | 2.490 | 1.347 | 2.348 |
| 21.03. | 182 | 2.341 | 1.271 | 2.328 |
| 22.03. | 174 | 2.184 | 1.235 | 2.372 |
| 23.03. | 195 | 2.112 | 1.161 | 2.341 |
| 24.03. | 171 | 2.061 | 1.012 | 2.349 |
| 25.03. | 172 | 2.050 | 987 | 2.325 |
| 26.03. | 174 | 2.019 | 950 | 2.284 |

## Impfungen

Mit der Schließung der Impfzentren Ende 2022 hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche wieder weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 72 | 75,22% (+/-0%) |
| 2. | 72 | 75,14% (+/-0%) |
| 3. | 131 | 59,25% (+/-0%) |
| 4. | 741 | 12,01% (+0,01%) |
| 5. | 460 | 0,85% (+0,01%) |
| 6. | 96 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/03/KW12Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Das LGL Bayern hat die Veröffentlichung der Testzahlen und Positivraten zum 06.3.2023 eingestellt. Daher können wir diesen Teil des Wochenberichts nicht mehr weiterführen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 1.948 ziemlich kollabiert und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate liegt bei 19,76%.](/images/2023/03/20230309Test.png)

## Varianten München &mdash; keine Aktualisierung

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir endlich Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München bleiben auf dem Stand der letzten Woche, eine Aktualisierung gab es diese Woche leider noch nicht. Die Daten gehen somit nur bis zum 20. Februar.

In München führt XBB.1.5 zwar klar mit 44,73%, so richtig dominant ist das aber auch nicht. BQ.1 liegt mit 20,16% auf Platz zwei, BA.2 mit 14,88% auf platz drei. CH.1.1 bleibt weiter unter 20%.

| KW  | 4  | 5  | 6  | 7 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 3,01% | 12,47% | 20,13% | 14,88% |
| Omikron BA.2.75 | 11,58% | 10,25% | 10,04% | 0,00% |
| Omikron BN.1 | 3,32% | 2,09% | 1,77% | 4,77% | 
| Omikron XBB.1.5 | 27,92% | 39,84% | 27,18% | 44,73% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 24,88% | 2,33% | 12,22% | 0,00% |
| Omikron BF.7 | 0,13% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 19,32% | 17,70% | 14,16% | 20,16% |
| Omikron CH.1.1 | 6,97% | 7,41% | 3,71% | 14,43% |
| Rekombinante Linien | 1,72% | 0,55% | 10,14% | 0,62% |
| Andere | 1,15% | 7,36% | 0,65% | 0,41% |

![VoC München](/images/2023/03/KW11VarMuc.png)

## Varianten Bayern

In Bayern: XBB.1.5 führt zwar klar, ist aber mit 57,50% nicht wirklich dominant. CH 1.1 ist mit 15,10% auf Platz zwei, BA.2 ist mit 11,00% zurück auf Platz drei. Wobei immer weniger Proben sequenziert werden - von über 600 vor 4 Wochen auf jetzt unter 100.

| KW | 8 | 9 | 10 | 11 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 9,10% | 12,20% | 12,10% | 11,00% |
| Omikron BA.2.75 | 1,10% | 0,80% | 1,30% | 0,00% |
| Omikron BN.1 | 3,30% | 1,80% | 1,00% | 2,70% |
| Omikron XBB.1.5 | 40,20% | 48,90% | 51,00% | 57,50% |
| Omikron BA.4 | 0,30% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 4,20% | 2,30% | 1,60% | 1,40% |
| Omikron BF.7 | 1,10% | 0,50% | 0,00% | 0,00% |
| Omikron BQ.1 | 18,30% | 13,90% | 14,60% | 5,50% |
| CH 1.1 | 16,00% | 11,70% | 11,10% | 15,10% |
| Rekombinante | 4,30% | 3,70% | 4,50% | 6,80% |
| Andere | 2,10% | 4,20% | 2,80% | 0,00% |
| Sequenzierungen | 642 | 617 | 314 | 73 |

![VoC Bayern](/images/2023/03/KW12Var.png)
