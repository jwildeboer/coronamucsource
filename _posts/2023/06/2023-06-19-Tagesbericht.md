---
layout: default
title: 2023-06-19 Tagesbericht
category: Tagesberichte
id: 2023-06-19
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-06-19 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, unverändert weil Sonntag. Zweite zeigt R(t) Wert für München, der auf 1,05 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/06/20230619.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 3,8 für München. Montag, daher keine neuen Fallzahlen und Nachmeldungen beim RKI. R(t) steigt auf 1,05. Dunkelziffer Faktor 10-35. 7 Tage Summe der Todesfälle 0. Morgen dann wieder eine “echte” Inzidenz.
<!--more-->

Das RKI hat die fehlenden Zahlen für Samstag und Sonntag nachgereicht. Damit konnten wir die Krankenhausdaten aktualisieren und unsere Fallzahlen vom Samstag verifizieren. Unsere Ber3chnungen waren alle korrekt. Ob es sich um ein technisches Problem handelte oder ob in Zukunft am Wochenende keine Aktualisierungen erfolgen ist nocht geklärt.

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 11.06. | 3,3 &rarr; 3,5 |
| 12.06. | 2,7 &rarr; 2,8 |
| 13.06. | 2,6 &rarr; 2,8 |
| 14.06. | 3,4 &rarr; 3,5 |
| 15.06. | 4,2 &rarr; 4,2 |
| 16.06. | 3,8 &rarr; 3,8 |
| 17.06. | 3,8 &rarr; 3,8 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 24 |
| ITS basiert für Bayern | 35 |

## COVID-positive Todesfälle

Es wurden heute keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle sinkt auf 0. Damit insgesamt bis jetzt 226 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt auf 1,05. Somit zwar exponenziell, aber ist nur ein Nachholeffekt vom Feiertag. Bis Mittwoch sollte R(t) wieder unter 1 fallen.

| Datum | R(t) |
|---|---:|
| 12.06. | 0,75 |
| 13.06. | 0,70 |
| 14.06. | 0,69 |
| 15.06. | 0,74 |
| 16.06. | 0,86 |
| 17.06. | 0,98 |
| 18.06. | 1,05 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern von 3 auf heute 2 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 3 gleich (+/-0). Insgesamt somit 5 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 2 | -1 | 
| Intensiv, beatmet | 3 | +/-0 | 
| SUMME | 5 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt von vorgestern 72 auf gestern 71.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 14 auf heute 13 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 14 auf heute 13 (-1). Insgesamt somit 26 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 13 | -1 |
| Intensiv, beatmet | 13 | -1 |
| SUMME | 26 | -2 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-06-16 mit Werten bis 2023-06-13**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/06/20230616A.png)

## Varianten München

**Letzte Aktualisierung: 2023-05-23 mit Werten bis 2023-05-01**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-06-13 mit Werten bis 2023-06-13**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 13.06.2023.

In Bayern wurden im Zeitraum zwischen dem 09. Mai 2023 und dem 13. Juni 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-05-09 | 2023-06-13 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.852 | 9.911.913 | +61 | 75,22% (+/-0%) |
| 2 | 9.901.535 | 9.901.619 | +84 | 75,14% (+/-0%) |
| 3 | 7.808.616 | 7.808.672 | +56 | 59,26% (+/-0%) |
| 4 | 1.584.328 | 1.584.603 | +275 | 12,03% (+0,01%) |
| 5 | 112.947 | 113.151 | +204 | 0,86% (+/-0%) |
| 6 | 5.574 | 5.597 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 703 Impfungen in den letzten 36 Tagen. Kaum der Rede wert. Leider.