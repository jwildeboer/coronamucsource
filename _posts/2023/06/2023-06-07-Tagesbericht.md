---
layout: default
title: 2023-06-07 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-06-07 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, die Zahlen steigen ein wenig, Nachholeffekt vom Feiertag. Zweite zeigt R(t) Wert für München, der auf 1,09 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/06/20230607.png)

Das RKI meldet eine Inzidenz von 5,4 für München. 3 Neue, 4 Nachmeldungen für vorgestern. Die gestern genannte Inzidenz von 6,9 erhöht sich durch Nachmeldungen auf 7,1. Dunkelziffer Faktor 10-18. 7 Tage Summe der Todesfälle 1. R(t) steigt auf 1,09. Nachholeffekt wegen Feiertag.
<!--more-->

Wir sehen bei den Indikatoren Reproduktionswert R(t), der Inzidenz und den Abwasserwerten eine leichte Steigung. Es scheint also eine kleine Welle zu rollen. In den Krankenhauszahlen ist diese kleine Welle aber noch nicht angekommen, das passiert dann wohl in den nächsten 7 Tagen, wenn diese Welle tatsächlich größer werden sollte.

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 30.05. | 4,6 &rarr; 4,6 |
| 31.05. | 3,9 &rarr; 4,4 |
| 01.06. | 4,4 &rarr; 4,6 |
| 02.06. | 4,9 &rarr; 5,2 |
| 03.06. | 4,9 &rarr; 5,2 |
| 04.06. | 4,9 &rarr; 5,2 |
| 05.06. | 6,9 &rarr; 7,1 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 24 |
| ITS basiert für Bayern | 18 |

## COVID-positive Todesfälle

Es wurden heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle steht auf 1. Damit insgesamt bis jetzt 224 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt auf 1,09.

| Datum | R(t) |
|---|---:|
| 31.05. | 0,79 |
| 01.06. | 0,77 |
| 02.06. | 0,83 |
| 03.06. | 0,86 |
| 04.06. | 0,89 |
| 05.06. | 1,04 |
| 06.06. | 1,09 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern von 6 auf heute 5 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Insgesamt somit 9 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 5 | -1 | 
| Intensiv, beatmet | 4 | +/-0 | 
| SUMME | 9 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt von vorgestern 125 auf gestern 96.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 21 auf 18 (-3). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 11 auf heute 10 (-1). Insgesamt somit 28 Intensivbetten belegt (-4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 18 | -3 |
| Intensiv, beatmet | 10 | -1 |
| SUMME | 28 | -4 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-06-02 mit Werten bis 2023-05-30**

Die Abwasserbelastung steigt ein wenig, bleibt aber noch unter den Werten im März/April.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/06/20230602A.png)

## Varianten München

**Letzte Aktualisierung: 2023-05-23 mit Werten bis 2023-05-01**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-01  wieder auf einem halbwegs aktuellen Stand.

Die von vielen Seiten gehypte Variante XBB.1.16 ist mit 4,63% zwar vorhanden, aber momentan noch weit von einem bedeutenden Anteil entfernt. Omikron BA.5 führt weiter mit 39,83% &mdash; weit entferntnt von 90+% der dominanten Varianten der Vergangenheit. Platz zwei ist XBB.1.5 mit 26,81%. Auf Platz drei ist das Sammelsurium der rekombinanten Linien mit 25,96%. 

Wie schon öfter angemerkt &mdash; die Zeiten einer klar dominanten Variante sind vorbei. 

| KW% | 15% | 16% | 17% | 18% |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 2,16% |
| Omikron BA.2.75 | 0,00% | 2,83% | 0,00% | 0,00% |
| Omikron BN.1 | 1,50% | 0,14% | 0,27% | 0,00% |
| Omikron XBB.1.5 | 34,84% | 38,68% | 9,67% | 26,81% |
| Omikron XBB.1.16 | 0,00% | 0,00% | 0,00% | 4,63% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 27,38% | 22,17% | 38,68% | 39,83% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 4,13% | 2,57% | 0,00% | 0,00% |
| Rekombinante Linien | 31,46% | 32,96% | 50,87% | 25,96% |
| Andere | 0,69% | 0,65% | 0,51% | 0,61% |

![VoC München](/images/2023/05/20230523V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-05-11 mit Werten bis 2023-05-09**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html), wie wir erst kürzlich bemerkt haben. Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 09.05.2023, mit reichlich Verzögerung unsererseits.

In Bayern wurden im Zeitraum zwischen dem 07. April 2023 und dem 09. Mai 2023 folgende Impfungen registriert:

| Impfung | 2023-04-07 | 2023-05-09 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.784 | 9.911.852 | +68 | 75,22% (+/-0%) |
| 2 | 9.901.382 | 9.901.535 | +153 | 75,14% (+/-0%) |
| 3 | 7.808.396 | 7.808.616 | +119 | 59,26% (+/-0%) |
| 4 | 1.583.651 | 1.584.328 | +677 | 12,02% (+/-0%) |
| 5 | 112.502 | 112.947 | +445 |  0,86% (+0,1%) |
| 6 | 5.500 | 5.574 | +74 | 0,04% (+/-0%) |

Das waren insgesamt nur 1.536 Impfungen. Kaum der Rede wert. Leider.