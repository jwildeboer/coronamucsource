---
layout: default
title: 2023-06-25 Tagesbericht
category: Tagesberichte
id: 20230624
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-06-25 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, relativ stabil sinkend, bis Dienstag aber keine neuen Zahlen. Zweite zeigt R(t) Wert für München, der auf 0,80 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/06/20230625.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 2,4 für München. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen von der Stadt, dem LGL Bayern und dem RKI. R(t) sinkt auf 0,80. Dunkelziffer Faktor 11-63. 7 Tage Summe der Todesfälle 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 17.06. | 3,8 &rarr; 4,0 |
| 18.06. | 3,8 &rarr; 4,0 |
| 19.06. | 3,2 &rarr; 3,3 |
| 20.06. | 3,3 &rarr; 3,3 |
| 21.06. | 3,0 &rarr; 3,0 |
| 22.06. | 2,7 &rarr; 2,7 |
| 23.06. | 2,4 &rarr; 2,4 |

Da bis Dienstag keine neuen Inzidenzen vom RKI kommen, können wir die Inzidenz für Montag schon jetzt berechnen:

| Montag | 2,4 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 11 |
| ITS basiert für München | 37 |
| ITS basiert für Bayern | 63 |

## COVID-positive Todesfälle

Es wurde heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle bleibt 1. Damit insgesamt bis jetzt 227 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt weiter auf 0,80. Somit nicht mehr exponenziell.

| Datum | R(t) |
|---|---:|
| 18.06. | 1,05 |
| 19.06. | 1,08 |
| 20.06. | 1,08 |
| 21.06. | 1,04 |
| 22.06. | 0,96 |
| 23.06. | 0,88 |
| 24.06. | 0,80 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 3 gleich (+/-0). Insgesamt somit 4 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 1 | +/-0 | 
| Intensiv, beatmet | 3 | +/-0 | 
| SUMME | 4 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung blieb mit 41 gleich.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sank im Vergleich zu gestern von 13 auf heute 12 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 9 gleich (+/-0). Insgesamt somit 21 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 12 | -1 |
| Intensiv, beatmet | 9 | +/-0 |
| SUMME | 21 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-06-23 mit Werten bis 2023-06-21**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Wir erkennen jetzt auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/06/20230623A.png)

## Varianten München

**Letzte Aktualisierung: 2023-06-20 mit Werten bis 2023-05-15**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-15  wieder auf einem leidlich aktuellen Stand.

Es gibt keine dominante Variante. Auf Platz 1 finden sich die Rekombinanten mt 31,45%. Dann kommt Omikron BA.5 mit 24,89% und XBB.1.5 mit 19,15% auf Platz 3. XBB.1.16 (Arcturus) ist nicht mehr nachweisbar im Münchner Abwasser.

| KW | 17 | 18 | 19 | 20 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 2,16% | 0,16% | 0,29% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,27% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 9,67% | 26,81% | 5,98% | 19,15% |
| Omikron XBB.1.16 | 0,00% | 4,63% | 0,00% | 0,00% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 8,84% |
| Omikron BA.5 | 38,68% | 39,83% | 39,73% | 24,89% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Rekombinante Linien | 50,87% | 25,96% | 53,27% | 31,45% |
| Andere | 0,00% | 0,00% | 0,00% | 14,58% |

![VoC München](/images/2023/06/20230621V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-06-13 mit Werten bis 2023-06-13**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 13.06.2023.

In Bayern wurden im Zeitraum zwischen dem 09. Mai 2023 und dem 13. Juni 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-05-09 | 2023-06-13 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.852 | 9.911.913 | +61 | 75,22% (+/-0%) |
| 2 | 9.901.535 | 9.901.619 | +84 | 75,14% (+/-0%) |
| 3 | 7.808.616 | 7.808.672 | +56 | 59,26% (+/-0%) |
| 4 | 1.584.328 | 1.584.603 | +275 | 12,03% (+0,01%) |
| 5 | 112.947 | 113.151 | +204 | 0,86% (+/-0%) |
| 6 | 5.574 | 5.597 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 703 Impfungen in den letzten 36 Tagen. Kaum der Rede wert. Leider.