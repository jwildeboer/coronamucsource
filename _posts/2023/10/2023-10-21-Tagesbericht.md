---
layout: default
title: 2023-10-21 Tagesbericht
category: Tagesberichte
---

![2023-10-21 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, klar fallende Tendenz. Zweite zeigt R(t) Wert für München, der weiter auf 0,84 sinkt. Dritte zeigt Inzidenz, die jetzt wieder deutlich unter 50 liegt und weiter fällt, aber wegen kaum noch gemachten Tests keine wirkliche Aussagekraft hat.](/images/2023/10/20231021.png)

Das RKI meldet eine Inzidenz von 30,0 für München. 51 Neue, 6 Nachmeldungen, 0 Korrekturen. Die gestern genannte Inzidenz von 33,3 erhöht sich auf 33,7. Dunkelziffer Faktor IFR: 10x ITS: 10x Abwasser: 25x. R(t) fällt auf 0,84. 7(!) neue Todesfälle. 7 Tage Summe der Todesfälle steigt auf 11.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 13.10. | 42,1 &rarr; 44,8 |
| 14.10. | 42,1 &rarr; 44,8 |
| 15.10. | 42,1 &rarr; 44,8 |
| 16.10. | 41,5 &rarr; 43,6 |
| 17.10. | 38,4 &rarr; 41,3 |
| 18.10. | 35,1 &rarr; 35,9 |
| 19.10. | 33,3 &rarr; 33,7 |

Da heute und morgen keine neuen Zahlen an das RKI gehen, können wir Inzidenz und R(t) für Sonntag und Montag schon jetzt berechnen:

| | Inzidenz | R(t) |
| :-- | --: | --: | 
| Sonntag | 30,0 | 0,82 |
| Montag | 30,0 | 0,81 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 25x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 9 |
| ITS basiert für München | 4 |
| Abwasserbasiert für München | 25 |
| ITS basiert für Bayern | 10 |

## COVID-positive Todesfälle

Es wurden 7(!) neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle springt auf 11. Damit insgesamt bis jetzt 251 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 0,84.

| Datum | R(t) |
|---|---:|
| 14.10. | 1,15 |
| 15.10. | 1,11 |
| 16.10. | 1,07 |
| 17.10. | 1,02 |
| 18.10. | 0,95 |
| 19.10. | 0,89 |
| 20.10. | 0,84 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern mit 9 auf heute 7 (-2). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 6 gleich (+/-0). Insgesamt somit 13 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 7 | -2 | 
| Intensiv, beatmet | 6 | +/-0 | 
| SUMME | 13 | -2 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung in Bayern steigt von vorgestern 670 auf gestern 673. Wenn man die Nachmeldungen mit einrechnet, sind wir wohl schon seit 15 Tagen über 700. Zum Vergleich: letztes Jahr um diese Zeit, nach Wiesnwelle 1, waren wir bei über 3.700.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern mit 74 auf heute 84 (+10). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 27  auf heute 24 (-3). Insgesamt somit 108 Intensivbetten belegt (+7).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 84 | +10 |
| Intensiv, beatmet | 24 | -3 |
| SUMME | 108 | +7 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-10-20 mit Werten bis 2023-10-17**

Mit dem neuen Wert für letzten Dienstag hat sich der Trend von "leicht steigend" auf "fallend" geändert. Der Abwasserwert ist deutlich gefallen, von 98,66 letzte Woche auf 36,21 jetzt. Die Dynamik ist genau wie im letzten Jahr. Somit ist die Wiesnwelle wohl vorbei.

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sahen im Juli/August ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/10/20231020A.png)

## Varianten München

**Letzte Aktualisierung: 2023-09-18 mit Werten bis 2023-08-21**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 3 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-08-21, "nur" knapp 1 Monat hinterher.

TL;DR: Omikron EG.5 liegt mit 43,48% klar vorne. Omikron XBB.1.16 mit 26,94% auf Platz 2 . Omikron XBB.2.3 liegt auf Platz 3 mit 26,94%, die Omikron BA, BN, BF und BQ Varianten kommen nicht mehr vor.

| KW | 29 | 30 | 32 | 33 |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,93% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,26% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 7,47% | 10,08% | 5,86% | 0,00% |
| Omikron XBB.1.9 | 39,61% | 21,65% | 12,76% | 9,49% |
| Omikron XBB.1.16 | 12,72% | 18,60% | 21,99% | 26,94% |
| Omikron XBB.2.3 | 3,16% | 9,56% | 9,69% | 20,06% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 2,44% | 0,00% | 0,00% |
| Omikron EG.5 | 36,67% | 35,85% | 33,85% | 43,48% |
| Rekombinante Linien | 0,00% | 0,31% | 15,60% | 0,00% |
| Andere | 0,37% | 0,32% | 0,25% | 0,03% |

![VoC München](/images/2023/09/20230918V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-10-10**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 10.10.2023.

In Bayern wurden im Zeitraum zwischen dem 09.09.2023 und dem 10.10.2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-09-12 | 2023-10-10 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.927 | 9.913.291 | +1.364 | 75,23% (+0,01%) |
| 2 | 9.901.650 | 9.901.798 | +148 | 75,14% (+/-0%) |
| 3 | 7.808.720 | 7.810.087 | +1.367 | 59,27% (+0,01%) |
| 4 | 1.584.838 | 1.596.304 | +11.466 | 12,11% (+0,08%) |
| 5 | 113.448 | 147.272 | +33.824 | 1,12% (+0,26%) |
| 6 | 5.658 | 12.815 | +7.157 | 0,10% (+0,06%) |

Das waren insgesamt 55.326 Impfungen in den letzten 28 Tagen in ganz Bayern. Nicht schlecht im Vergleich zu den Vormonaten, aber weiterhin niedrige Zahlen im Vergleich zu den großen Impfkampagnen. Trotzdem eine gute Entwicklung.