---
layout: default
title: 2023-10-07 Tagesbericht
category: Tagesberichte
---

![2023-10-07 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, klare Tendenz nach oben. Zweite zeigt R(t) Wert für München, der auf 1,21 fällt. Dritte zeigt Inzidenz, die zwar noch unter 50 liegt aber wegen mangelnder PCR Tests kaum noch aussagekräftig ist.](/images/2023/10/20231007.png)

Das RKI meldet eine Inzidenz von 34,4 für München. 99 Neue, 28(!) Nachmeldungen, 0 Korrekturen. Die gestern genannte Inzidenz von 33,9 erhöht sich auf 35,7. Dunkelziffer Faktor IFR: 10x ITS: 12x Abwasser: 37x. R(t) sinkt auf 1,21. 7 Tage Summe der Todesfälle bleibt 0. Die zweite WiesnWelle rollt.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 29.09. | 27,4 &rarr; 29,4 |
| 30.09. | 27,4 &rarr; 29,4 |
| 01.10. | 27,4 &rarr; 29,4 |
| 02.10. | 31,4 &rarr; 33,0 |
| 03.10. | 26,6 &rarr; 28,2 |
| 04.10. | 29,6 &rarr; 33,3 |
| 05.10. | 33,9 &rarr; 35,7 |

Da heute und morgen keine neuen Zahlen an das RKI gehen, können wir Inzidenz und R(t) für Sonntag und Montag schon jetzt berechnen:

| | Inzidenz | R(t) |
| :-- | --: | --: | 
| Sonntag | 34,4 | 1,17 |
| Montag | 34,4 | 1,13 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Nebenbei schauen wir auch noch auf die Abwasserwerte, obwohl es schwer ist daraus eine halbwegs vernünftige Dunklelziffer zu berechnen. Aber da kommen wir momentan auf ein Faktor um 42x.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 11 |
| Abwasserbasiert für München | 37 |
| ITS basiert für Bayern | 12 |

## COVID-positive Todesfälle

Es wurde seit 24 Tagen kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 0. Damit insgesamt bis jetzt 236 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 1,21. Tag 34 mit R(t) über 1 und damit exponenziell.

| Datum | R(t) |
|---|---:|
| 30.09. | 1,30 |
| 01.10. | 1,35 |
| 02.10. | 1,41 |
| 03.10. | 1,35 |
| 04.10. | 1,32 |
| 05.10. | 1,29 |
| 06.10. | 1,21 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 14 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 2 gleich (+/-0). Insgesamt somit 16 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 14 | +/-0 | 
| Intensiv, beatmet | 2 | +/-0 | 
| SUMME | 16 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung in Bayern macht wieder einen satten Sprung von vorgestern 444 auf gestern 498. Wenn man die Nachmeldungen mit einrechnet, ist heute Tag 16 über 400 und wohl auch Tag 3 über 500, wenn die Nachmeldungen für heute kommen. 

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern steigt im Vergleich zu gestern mit 50 auf heute 56 (+6). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern mit 21 auf heute 18 (-3). Insgesamt somit 74 Intensivbetten belegt (+3).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 56 | +6 |
| Intensiv, beatmet | 18 | -3 |
| SUMME | 74 | +3 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-10-07 mit Werten bis 2023-10-05**

Die Abwasserzahlen wurden heute mit neuen Werten aktualisiert, somit Zahlen bis zum 05.10.2023.

Spitze war am 28.09. mit 71,49, seitdem starker Rückgang (63,59 am 03.10., 35,04 am 05.10.) Wie von uns erwartet bleiben die Werte unter den Vorjahreswerten. Es gibt eine Wiesnwelle, aber sie bleibt unter den Vorjahreswerten.

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte waren im Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Wir sahen im Juli/August ein stabiles, aber langsames Wachstum das (noch) im einstelligem Bereich bleibt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/10/20231007A.png)

## Varianten München

**Letzte Aktualisierung: 2023-09-18 mit Werten bis 2023-08-21**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden aktualisiert, und 3 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-08-21, "nur" knapp 1 Monat hinterher.

TL;DR: Omikron EG.5 liegt mit 43,48% klar vorne. Omikron XBB.1.16 mit 26,94% auf Platz 2 . Omikron XBB.2.3 liegt auf Platz 3 mit 26,94%, die Omikron BA, BN, BF und BQ Varianten kommen nicht mehr vor.

| KW | 29 | 30 | 32 | 33 |
| Omikron BA.2 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,93% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,26% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 7,47% | 10,08% | 5,86% | 0,00% |
| Omikron XBB.1.9 | 39,61% | 21,65% | 12,76% | 9,49% |
| Omikron XBB.1.16 | 12,72% | 18,60% | 21,99% | 26,94% |
| Omikron XBB.2.3 | 3,16% | 9,56% | 9,69% | 20,06% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BA.5 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 2,44% | 0,00% | 0,00% |
| Omikron EG.5 | 36,67% | 35,85% | 33,85% | 43,48% |
| Rekombinante Linien | 0,00% | 0,31% | 15,60% | 0,00% |
| Andere | 0,37% | 0,32% | 0,25% | 0,03% |

![VoC München](/images/2023/09/20230918V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-09-12**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 08.08.2023.

In Bayern wurden im Zeitraum zwischen dem 12. Juli 2023 und dem 8. August 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-08-08 | 2023-09-12 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.991.925 | 9.991.927 | +2 | 75,22% (+/-0%) |
| 2 | 9.901.645 | 9.901.650 | +5 | 75,14% (+/-0%) |
| 3 | 7.808.710 | 7.808.720 | +10 | 59,26% (+/-0%) |
| 4 | 1.584.799 | 1.584.838 | +39 | 12,03% (+/-0%) |
| 5 | 113.329 | 113.448 | +119 | 0,86% (+/-0%) |
| 6 | 5.635 | 5.658 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 198 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es 178 Impfungen insgesamt. 