---
layout: default
title: 2023-01-16 Wochenbericht KW02
category: Wochenberichte
---

![2023-01-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher als in der Vorwoche, der R(t) Wert ist 1,11. Zweite zeigt Krankenhausbetten, deren Zahl wieder sinkt. Dritte zeigt Inzidenz, die wieder leicht steigt, aber erst morgen gibt es neue Fallzahlen vom RKI.](/images/2023/01/20230116.png)

In der letzten Woche gab es insgesamt 1.387 neue COVID Fälle in München. Das sind 27,8% **mehr** als in der Vorwoche (1.085). Eine Ursache: 1 Feiertag in der letzten Woche und deshalb fehlende Meldungen. Leider auch 26(!) Tote mit COVID Nachweis in den letzten 7 Tagen, sehr viel mehr als in der Vorwoche (3). Da sind wohl viele Nachmeldungen aus Weihnachten/Jahreswechsel enthalten.
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe von 1.079 auf 1.397, wobei der Feiertag wohl auch eine Rolle spielt. Die um Nachmeldungen korrigierte Inzidenz stieg von 70,2 auf 93,2.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 08.01. | 1 | 1.079 | 70,2 |
| 09.01. | 553 | 1.264 | 76,1 |
| 10.01. | 305 | 1.312 | 84,7 |
| 11.01. | 247 | 1.263 | 80,9 |
| 12.01. | 188 | 1.294 | 82,9 |
| 13.01. | 93 | 1.387 | 93,2 |
| 14.01. | 0 | 1.397 | 93,2 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche knapp über 1 und stand gestern bei 1,11.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 28.12.) liegt bei 1,01. Das StaBLab der LMU veröffentlicht offiziell nur noch am Mittwoch die R(t) Zahlen. Oder, wie in den letzten 2 Wochen, auch mal einfach gar nicht. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.01. | 0,77 | --- |
| 10.01. | 0,79 | --- |
| 11.01. | 0,82 | --- |
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |
| 14.01. | 1,05 | --- |
| 15.01. | 1,11 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München sinkt die Zahl der belegten Normalbetten wieder. IMC sinkt auch klar, Intensiv mehr ider weniger stabil um 30. An 4 von 7 Tagen war die Summe klar höher als in der Vorwoche.

Im Vergleich zum Vorjahr trotzdem klar höhere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 09.01. | 312 | 11 | 30 | 353 | -28 | 248 |
| 10.01. | 330 | 10 | 29 | 369 | -10 | 247 |
| 11.01. | 334 | 6 | 31 | 371 | +17 | 254 |
| 12.01. | 332 | 6 | 30 | 368 | +14 | 251 |
| 13.01. | 322 | 5 | 30 | 357 | +4 | 270 |
| 14.01. | 312 | 5 | 31 | 348 | -3 | 288 |
| 15.01. | 301 | 4 | 32 | 337 | -12 | 284 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, langsam etwas besser aus. In der letzten Woche waren wir aber an 3 Tagen mit den korrigierten Zahlen noch über dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit, und an jedem einzelnen Tag klar über 1.000. Und es kommen noch weitere Nachmeldungen.

Bei den Intensivbetten bleibt es relativ stabil zwichen 160-180.

Im Vergleich zum Vorjahr aber weiterhin deutlich höhere Zahlen, teilweise ehr als doppelt so hoch.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 09.01. | 1.345 | 620 |
| 10.01. | 1.306 | 613 |
| 11.01. | 1.244 | 606 |
| 12.01. | 1.121 | 622 |
| 13.01. | 1.149 | 681 |
| 14.01. | 1.109 | 688 |
| 15.01. | 1.067 | 701 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche aber doch ein wenig mehr als über die Feiertage. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 127 | 75,20% |
| 2. | 112 | 75,13% |
| 3. | 686 | 59,22% |
| 4. | 6.428 | 11,81% |
| 5. | 1.899 | 0,78% |
| 6. | 62 | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/01/KW02Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag sollen die Testzahlen vom LGL aktualisiert werden. In der letzten Woche ist die allerdings nicht passiert. Daher wiederholen wir die Zahlen der Woche davor und werden diesen Abschnitt aktualisiern, sollte sich das LGL doch noch bequemen aktuellere Zahlen zu veröffetnlichen.

**Testzahlen Stand KW01/2023**

Laut LGL waren wir in der letzten Woche, mit den ganzen Feiertagen, bayernweit bei nur 1.933 Tests/Tag mit einer Positivrate von 12,31%. Gegenüber den Zahlen von letzter Woche (11.519 Tests/Tag mit 17,73% Positivrate) also kaum noch Tests, daher sind die Zahlen nicht sonderlich aussagekräftig. Hoffen wir, daß das nächste Woche wieder besser wird.

1.933 Tests/Tag ist kaum repräsentativ. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 1.933 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 12,31%.](/images/2023/01/20230105Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt jetzt deutlich auf knapp unter 15%, BQ.1 scheint mit 40,5% das Rennen zu machen. BF.7 steigt auf um 18%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

Es gibt nun auch endlich auch Zahlen zur Variante XBB.1.5, die aber noch unter 10% Anteil hat.

| Variante | KW 51 | KW 52 | KW 01 | KW 02 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 3.6% | 3.9% | 0.0% | 0.9% |
| Omikron BA.2.75 | 5.9% | 16.2% | 8.7% | 9.9% |
| Omikron BN.1 | 9.0% | 6.9% | 3.1% | 5.4% |
| Omikron XBB.1.5 | 1.0% | 6.4% | 6.2% | 9.0% |
| Omikron BA.4 | 1.0% | 0.5% | 0.5% | 0.9% |
| Omikron BA.5 | 25.9% | 18.6% | 14.4% | 14.4% |
| Omikron BF.7 | 15.8% | 8.3% | 17.4% | 18.0% |
| Omikron BQ.1 | 37.8% | 39.2% | 49.2% | 40.5% |
| Andere | 0.7% | 0.0% | 0.5% | 0.9% |
| Sequenzierungen | 579 | 204 | 195 | 111 |

![VoC](/images/2023/01/KW02Var.png)