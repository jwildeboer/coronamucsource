---
layout: default
title: 2023-01-23 Wochenbericht KW03
category: Wochenberichte
---

![2023-01-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die langsamer sinkt und erst morgen neue Zahlen enthält.](/images/2023/01/20230123.png)

In der letzten Woche gab es insgesamt nur noch 859 neue COVID Fälle in München. Das sind 39,3% **weniger** als in der Vorwoche (1.415). Leider auch 26 Tote mit COVID Nachweis in den letzten 7 Tagen, wie in der Vorwoche (26).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 1.414 auf 859, eine so niedrige Summe hatten wir zuletzt am 20.08.2021 (881). Die um Nachmeldungen korrigierte Inzidenz sank von 95,0 auf 57,7.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 15.01. | 0 | 1.414 | 95,0 |
| 16.01. | 286 | 1.146 | 77,0 |
| 17.01. | 159 | 1.000 | 67,2 |
| 18.01. | 185 | 936 | 62,9 |
| 19.01. | 119 | 865 | 58,1 |
| 20.01. | 110 | 859 | 57,7 |
| 21.01. | 0 | 859 | 57,7 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche kontinuierlich von 1,10 auf jetzt 0,76.

Das StaBLab der LMU hat die Berechnung des R(t) Wertes zum Jahresbeginn komplett eingestellt. Daher wird es leider keine neuen Zahlen geben.

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.01. | 1,10 | --- |
| 17.01. | 1,03 | --- |
| 18.01. | 0,95 | --- |
| 19.01. | 0,87 | --- |
| 20.01. | 0,82 | --- |
| 21.01. | 0,79 | --- |
| 22.01. | 0,76 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München sinkt die Zahl der belegten Normalbetten weiter. IMC pendelt nur wenig zwischen 6 und 8, Intensiv auch mehr oder weniger stabil um 30. An 5 von 7 Tagen war die Summe niedriger als in der Vorwoche.

Im Vergleich zum Vorjahr ähnliche Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 16.01. | 320 | 6 | 30 | 356 | +3 | 288 |
| 17.01. | 334 | 6 | 30 | 370 | +1 | 284 |
| 18.01. | 315 | 7 | 32 | 354 | -17 | 295 |
| 19.01. | 299 | 9 | 27 | 335 | -33 | 296 |
| 20.01. | 293 | 8 | 27 | 328 | -29 | 316 |
| 21.01. | 286 | 7 | 28 | 321 | -27 | 332 |
| 22.01. | 280 | 7 | 28 | 315 | -22 | 351 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, langsam besser aus. In der letzten Woche waren wir aber an 5 von 7 Tagen mit den korrigierten Zahlen unter 1.000.

Bei den Intensivbetten geht es langsam runter von 160 auf jetzt 132.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 16.01. | 1.131 | 708 |
| 17.01. | 1.047 | 756 |
| 18.01. | 966 | 796 |
| 19.01. | 965 | 873 |
| 20.01. | 869 | 924 |
| 21.01. | 848 | 1.017 |
| 22.01. | 827 | 1.063 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche aber doch ein wenig mehr als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 194 | 75,21% (+0,01%) |
| 2. | 158 | 75,13% (+0,0%) |
| 3. | 590 | 59,23% (+0,01%) |
| 4. | 6.592 | 11,86% (+0,05%) |
| 5. | 1.997 | 0,79% (+0,01%) |
| 6. | 96 | 0,04% (+0,01%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/01/KW03Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 11.929 Tests/Tag mit einer Positivrate von 10,63%. Gegenüber den Zahlen von letzter Woche (9.917 Tests/Tag mit 13,89% Positivrate) also etwas mehr Tests bei sinkender Positivrate.

11.929 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 11.929 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 10,63%.](/images/2023/01/20230119Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt jetzt deutlich auf knapp unter 15%, BQ.1 scheint mit 41,5% das Rennen zu machen. BF.7 steigt auf um 18%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

Es gibt nun auch Zahlen zur Variante XBB.1.5, die aber noch unter 10% Anteil hat.

| Variante | KW 51 | KW 52 | KW 01 | KW 02 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 3.6% | 3.7% | 0.0% | 0.8% |
| Omikron BA.2.75 | 5.9% | 15,6% | 10,4% | 9.8% |
| Omikron BN.1 | 9.0% | 6.4% | 5.7% | 5.7% |
| Omikron XBB.1.5 | 1.0% | 6.9% | 7.5% | 8.1% |
| Omikron BA.4 | 1.0% | 0.5% | 0.4% | 0.8% |
| Omikron BA.5 | 25.9% | 18.8% | 17.6% | 14.6% |
| Omikron BF.7 | 15.8% | 8.7% | 14.7% | 17.9% |
| Omikron BQ.1 | 37.8% | 39.4% | 43.4% | 41.5% |
| Andere | 0.7% | 0.0% | 0.4% | 0.8% |
| Sequenzierungen | 579 | 218 | 279 | 123 |

![VoC](/images/2023/01/KW03Var.png)