---
layout: default
title: 2023-01-11 Tagesbericht
category: Tagesberichte
---

![2023-01-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher als in der Vorwoche, der R(t) Wert ist 0,79. Zweite zeigt Krankenhausbetten, deren Zahl seit 2 Tagen leicht steigt. Dritte zeigt Inzidenz, die nicht mehr sinkt.](/images/2023/01/20230111.png)

Das RKI meldet eine Inzidenz von 84,7 für München. 65 Neue, 122 Nachmeldungen, davon 117 for vorgestern. Die gestern genannte Inzidenz von 76,1 erhöht sich dadurch auf 84,2. Trotzdem - Tag 8 mit Inzidenz unter 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 03.01. | 99,0 -> 103,3 |
| 04.01. | 93,0 -> 98,5 |
| 05.01. | 84,6 -> 86,8 |
| 06.01. | 71,1 -> 73,3 |
| 07.01. | 70,6 -> 72,9 |
| 08.01. | 70,2 -> 72,5 |
| 09.01. | 76,1 -> 84,2 |

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 6. Damit insgesamt 8 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 11.01. | 10.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 96,5 | 54,7 | +76,4% |
| LK Ebersberg | 64,3 | 65,7 | -2,1% |
| LK Freising | 55,8 | 54,1 | +3,1% |
| LK Fürstenfeldbruck | 93,8 | 87,8 | +6,8% |
| LK München | 84,3 | 78,0 | +8,1% |
| LK Starnberg | 62,2 | 62,9 | -1,1% |

Leicht steigende Tendenz. Dachau hat nachgemeldet, daher der scheinbar große Sprung bei der Inzidenz.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 14 Tagen unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt seit einer Woche verspätet.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 04.01. | 0,93 | --- |
| 05.01. | 0,92 | --- |
| 06.01. | 0,85 | --- |
| 07.01. | 0,83 | --- |
| 08.01. | 0,80 | --- |
| 09.01. | 0,77 | --- |
| 10.01. | 0,79 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 18 von 312 auf 330. IMC sinkt um 1 von gestern 11 auf heute 10. Intensivbetten sinkt um 1 von gestern 30 auf heute 29. Damit insgesamt 369 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 330 | +18 | 
| IMC | 10 | -1 | 
| Intensiv | 29 | -1 |
| SUMME | 369 | +16 |

Das sind 10 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.