---
layout: default
title: 2023-01-06 Tagesbericht
category: Tagesberichte
---

![2023-01-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist 0,92. Zweite zeigt Krankenhausbetten, deren Zahl deutlich sinkt aber wegen Feiertag bis Montag keine neuen Zahlen. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230106.png)

Das RKI meldet eine Inzidenz von 84,6 für München. 133 Neue, 73 Nachmeldungen, davon 65 für vorgestern. Die gestern genannte Inzidenz von 93,0 steigt durch Nachmeldungen auf 97,9. Tag 9 mit R(t) unter 1,0, Tag 2 mit Inzidenz unter 100. Wegen Feiertag bis Monatg keine neuen Krankenhauszahlen von der Stadt München.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 29.12. | 116,8 -> 123,3 |
| 30.12. | 108,1 -> 113,3 |
| 31.12. | 108,1 -> 113,7 |
| 01.01. | 108,1 -> 114,2 |
| 02.01. | 133.0 -> 138,8 |
| 03.01. | 99,0 -> 102,9 |
| 04.01. | 93,0 -> 97,9 |

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 4. Damit insgesamt 3 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 06.01 | 05.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 106,8 | 116,4 | -8,2% |
| LK Ebersberg | 66,4 | 94,8 | -30,0% |
| LK Freising | 67,9 | 77,8 | -12,7% |
| LK Fürstenfeldbruck | 91,0 | 125,8 | -27,7% |
| LK München | 100,9 | 112,3 | -10,2% |
| LK Starnberg | 60,0 | 69,5 | -13,7% |

Überall klar sinkende Inzidenz.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 9 Tagen knapp unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung lässt mal mwieder auf sich warten.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.12. | 0,92 | --- |
| 31.12. | 0,89 | --- |
| 01.01. | 0,86 | --- |
| 02.01. | 0,91 | --- |
| 03.01. | 0,92 | --- |
| 04.01. | 0,93 | --- |
| 05.01. | 0,92 | --- |

## Krankenhaus

Wegen Feiertag bis Montag keine neuen Krankenhauszahlen von der Stadt München.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 319 | --- | 
| IMC | 7 | --- | 
| Intensiv | 28 | --- |
| SUMME | 354 | --- |

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.