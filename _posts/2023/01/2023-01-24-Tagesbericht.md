---
layout: default
title: 2023-01-24 Tagesbericht
category: Tagesberichte
---

![2023-01-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet.](/images/2023/01/20230124.png)

Das RKI meldet eine Inzidenz von 54,9 für München. 213 Neue, 31 Nachmeldungen, davon keine für das Wochenende und 30 für Freitag. Die Inzidenz für Freitag erhöht sich von 56,4 auf 58,2. Die Inzidenzen für Samstag bis Montag von 57,7 auf 59,8. R(t) bleibt klar unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 16.01. | 71,7 -> 77,0 |
| 17.01. | 64,1 -> 67,2 |
| 18.01. | 59,3 -> 62,9 |
| 19.01. | 56,4 -> 58,2 |
| 20.01. | 57,7 -> 59,8 |
| 21.01. | 57,7 -> 59,8 |
| 22.01. | 57,7 -> 59,8 |

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 27. Damit insgesamt jetzt 57 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 24.01 | 21.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 66,9 | 66,9 | +/-0% |
| LK Ebersberg | 71,9 | 71,2 | +1,0% |
| LK Freising | 67,9 | 67,9 | +/-0,0% |
| LK Fürstenfeldbruck | 81,4 | 77,8 | +4,6% |
| LK München | 43,4 | 68,3 | -36,5% |
| LK Starnberg | 63,6 | 60,0 | +6,0% |

Der LK München hat keine neuen Zahlen gemeldet. Weiter eher Plateau als steigender Trend. 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 7 Tagen wieder unter der Grenze von 1 gesunken und liegt heute bei 0,77.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 17.01. | 1,03 | --- |
| 18.01. | 0,95 | --- |
| 19.01. | 0,87 | --- |
| 20.01. | 0,83 | --- |
| 21.01. | 0,80 | --- |
| 22.01. | 0,77 | --- |
| 23.01. | 0,77 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute weiter: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 17 von 280 auf 263. IMC bleibt mit 7 Betten gleich. Intensiv steigt um 1 von gestern 28 auf heute 29. Damit insgesamt 299 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 263 | -17 | 
| IMC | 7 | +/-0 | 
| Intensiv | 29 | +1 |
| SUMME | 299 | -16 |

Damit sind 57 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.