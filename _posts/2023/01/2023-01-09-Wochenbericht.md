---
layout: default
title: 2023-01-09 Wochenbericht KW01
category: Wochenberichte
---

![2023-01-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist 0,79. Zweite zeigt Krankenhausbetten, deren Zahl nur noch langsam sinkt. Dritte zeigt Inzidenz, die weiter sinkt, aber erst morgen neue Fallzahlen vom RKI.](/images/2023/01/20230109.png)

In der letzten Woche gab es insgesamt nur 1.051 neue COVID Fälle in München. Das sind 37,9% **weniger** als in der Vorwoche (1.692). Eine Ursache: 2 Feiertage in der letzten Woche und deshalb fehlende Meldungen. Leider auch 3 Tote mit COVID Nachweis in den letzten 7 Tagen, klar weniger als in der Vorwoche (14).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 1.699 auf 1.051, wobei die 2 Feiertage als wichtiger Faktor zu berücksichtigen sind. Die um Nachmeldungen korrigierte Inzidenz sank von 108,1 auf 70,6.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 01.01. | 7 | 1.699 | 114,2 |
| 02.01. | 366 | 2.065 | 138,8 |
| 03.01. | 254 | 1.531 | 102,9 |
| 04.01. | 291 | 1.456 | 97,9 |
| 05.01. | 133 | 1.258 | 84,6 |
| 06.01. | 0 | 1.058 | 71,1 |
| 07.01. | 0 | 1.051 | 70,6 |

## Reproduktionswert R(t) für München

Unser R(t) blieb in der letzten Woche unter 1 und stand gestern bei 0,79.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 28.12.) liegt bei 1,01. Das StaBLab der LMU veröffentlicht offiziell nur noch am Mittwoch die R(t) Zahlen. Oder, wie in der letzten Woche, auch mal einfach gar nicht. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.01. | 0,91 | --- |
| 03.01. | 0,92 | --- |
| 04.01. | 0,93 | --- |
| 05.01. | 0,92 | --- |
| 06.01. | 0,84 | --- |
| 07.01. | 0,82 | --- |
| 08.01. | 0,79 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über Feiertag und Wochenende weniger belegte Normalbetten. IMC steigt ein wenig, Intensiv auch. An 7 von 7 Tagen war die Summe klar niedriger als in der Vorwoche und sank von fast 400 auf jetzt um 350 Betten insgesamt.

Im Vergleich zum Vorjahr trotzdem höhere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 02.01. | 337 | 12 | 32 | 381 | -28 | 257 |
| 03.01. | 332 | 14 | 32 | 379 | -16 | 256 |
| 04.01. | 319 | 7 | 28 | 354 | -55 | 254 |
| 05.01. | 317 | 8 | 29 | 354 | -89 | 253 |
| 06.01. | 313 | 9 | 31 | 353 | -76 | 251 |
| 07.01. | 309 | 10 | 32 | 351 | -64 | 251 |
| 08.01. | 305 | 10 | 34 | 349 | -54 | 250 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, langsam etwas besser aus. In der letzten Woche waren wir an 3 Tagen mit den korrigierten Zahlen noch über dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit, jetzt aber klar unter 1.000.

Bei den Intensivbetten bleint es relativ stabil zwichen 190-200.

Im Vergleich zum Vorjahr aber weiterhin deutlich höhere Zahlen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 02.01. | 1.728 | 680 |
| 03.01. | 1.420 | 678 |
| 04.01. | 1.270 | 668 |
| 05.01. | 1.087 | 643 |
| 06.01. | 898 | 569 |
| 07.01. | 863 | 592 |
| 08.01. | 826 | 619 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 22 | 75,20% |
| 2. | 49 | 75,13% |
| 3. | 275 | 59,22% |
| 4. | 2.315 | 11,76% |
| 5. | 545 | 0,76% |
| 6. | 79 | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/01/KW01Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert.Laut LGL waren wir in der letzten Woche, mit den ganzen Feiertagen, bayernweit bei nur 1.933 Tests/Tag mit einer Positivrate von 12,31%. Gegenüber den Zahlen von letzter Woche (11.519 Tests/Tag mit 17,73% Positivrate) also kaum noch Tests, daher sind die Zahlen nicht sonderlich aussagekräftig. Hoffen wir, daß das nächste Woche wieder besser wird.

1.933 Tests/Tag ist kaum repräsentativ. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 1.933 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 12,31%.](/images/2023/01/20230105Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil bleibt stabil um knapp unter 30%, BQ.1 scheint mit 31.9% das Rennen zu machen. BF.7 liegt nur noch unter 10%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

Zur Verbreitung der neuen XB* Varianten in Bayern gibt es leider immer noch keine Zahlen.

| Variante | KW49 | KW50 | KW 51 | KW 52 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 0.5% | 1.1% | 1.7% | 3.2% |
| Omikron BA.2.75 | 3.1% | 3.7% | 6.5% | 18.1% |
| Omikron BN.1 | 5.3% | 7.3% | 8.4% | 6.4% |
| Omikron BA.4 | 0.0% | 0.5% | 1.4% | 0.0% |
| Omikron BA.5 | 29.1% | 28.2% | 26.3% | 29.8% |
| Omikron BF.7 | 20.5% | 18.4% | 15.8% | 7.4% |
| Omikron BQ.1 | 36.5% | 37.1% | 36.4% | 31.9% |
| Andere | 4.9% | 3.7% | 3.6% | 3.2% |
| Sequenzierungen | 761 | 645 | 418 | 94 | 

![VoC](/images/2023/01/KW01Var.png)