---
layout: default
title: 2023-01-12 Tagesbericht
category: Tagesberichte
---

![2023-01-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher als in der Vorwoche, der R(t) Wert ist 0,81. Zweite zeigt Krankenhausbetten, deren Zahl seit 3 Tagen wieder steigt. Dritte zeigt Inzidenz, die nicht mehr sinkt.](/images/2023/01/20230112.png)

Das RKI meldet eine Inzidenz von 80,9 für München. 196 Neue, 48 Nachmeldungen, davon 34 for vorgestern. Die gestern genannte Inzidenz von 84,7 erhöht sich dadurch auf 87,6. Trotzdem - Tag 9 mit Inzidenz unter 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 04.01. | 93,0 -> 98,6 |
| 05.01. | 84,6 -> 86,9 |
| 06.01. | 71,1 -> 73,5 |
| 07.01. | 70,6 -> 73,0 |
| 08.01. | 70,2 -> 72,6 |
| 09.01. | 76,1 -> 84,9 |
| 10.01. | 84,7 -> 87,6 |

Es wurden 15(!) neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 21. Damit insgesamt 23 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 12.01. | 11.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 95,9 | 96,5 | -0,6% |
| LK Ebersberg | 71,2 | 64,3 | +10,7% |
| LK Freising | 60,7 |  55,8 | +8,8% |
| LK Fürstenfeldbruck | 92,4 | 93,8 | -1,5% |
| LK München | 80,3 | 84,3 | -4,7% |
| LK Starnberg | 63,6 | 62,2 | +2,3% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 14 Tagen unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt seit über einer Woche verspätet.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.01. | 0,93 | --- |
| 06.01. | 0,85 | --- |
| 07.01. | 0,83 | --- |
| 08.01. | 0,80 | --- |
| 09.01. | 0,77 | --- |
| 10.01. | 0,79 | --- |
| 11.01. | 0,81 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 4 von 330 auf 334. IMC sinkt um 4 von gestern 10 auf heute 6. Intensivbetten steigt um 2 von gestern 29 auf heute 31. Damit insgesamt 371 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 334 | +4 | 
| IMC | 6 | -4 | 
| Intensiv | 31 | +2 |
| SUMME | 371 | +2 |

Damit sind 17 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.