---
layout: default
title: 2023-01-18 Tagesbericht
category: Tagesberichte
---

![2023-01-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt nur noch knapp über 1. Zweite zeigt Krankenhausbetten, deren Zahl wieder steigt. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230118.png)

Das RKI meldet eine Inzidenz von 64,1 für München. 118 neue, 77 Nachmeldungen, davon 74 für vorgestern. Die gestern genannte Inzidenz von 71,7 steigt dadurch auf 76,7. R(t) bleibt noch knapp über 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 10.01. | 84,7 -> 88,3 |
| 11.01. | 80,9 -> 85,0 |
| 12.01. | 82,9 -> 87,2 |
| 13.01. | 93,2 -> 95,0 |
| 14.01. | 93,2 -> 95,0 |
| 15.01. | 93,2 -> 94,9 |
| 16.01. | 71,7 -> 76,7 |

Es wurden 13(!) neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 35. Damit insgesamt schon 43 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum vortag in %.

| Kreis | 18.01. | 17.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 70,8 | 74,6 | -5,1% |
| LK Ebersberg | 70,6 | 49,1 | +43,8% |
| LK Freising | 57,4 | 58,0 | -1,0% |
| LK Fürstenfeldbruck | 75,0 | 54,0 | +38,9% |
| LK München | 71,7 | 70,9 | +1,1% |
| LK Starnberg | 76,1 | 68,0 | +11,9% |

Teilweise drastisch höhere Zahlen, weil Ebersberg, Fürstenfeldbruck nachgemeldet haben.

## Reproduktionswert R(t) für München

Unser R(t) ist nach 14 Tagen unter 1 wieder seit 4 Tagen auf einen Wert über 1 gestiegen. Bis ende der WOche wird R(t) wohl wieder unter 1 sein.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt schon zum zweiten Mal nicht erfolgt.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 11.01. | 0,82 | --- |
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |
| 14.01. | 1,05 | --- |
| 15.01. | 1,12 | --- |
| 16.01. | 1,10 | --- |
| 17.01. | 1,02 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt wieder: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 14 von 320 auf 334. IMC und Intensiv bleiben unverändert bei 6 bzw. 30. Damit insgesamt 370 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 334 | +14 | 
| IMC | 6 | +/-0 | 
| Intensiv | 30 | +/-0 |
| SUMME | 370 | +14 |

Damit ist 1 Bett **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.