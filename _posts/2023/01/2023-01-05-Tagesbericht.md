---
layout: default
title: 2023-01-05 Tagesbericht
category: Tagesberichte
---

![2023-01-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist 0,92. Zweite zeigt Krankenhausbetten, deren Zahl deutlich sinkt. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230105.png)

Das RKI meldet eine Inzidenz von 93,0 für München. 226 Neue, 52 Nachmeldungen, davon 40 für vorgestern. Die gestern genannte Inzidenz von 99,0 steigt durch Nachmeldungen auf 102,4. Tag 8 mit R(t) unter 1,0.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 28.12. | 121,7 -> 126,0 |
| 29.12. | 116,8 -> 123,3 |
| 30.12. | 108,1 -> 113,2 |
| 31.12. | 108,1 -> 113,7 |
| 01.01. | 108,1 -> 114,1 |
| 02.01. | 133.0 -> 138,7 |
| 03.01. | 99,0 -> 102,4 |

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 2 COVID-Tote bis jetzt in 2023, in 2020 waren es 579, 2021 sogar 1.050, 2022 waren es 790.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 05.01. | 04.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 116,4 | 128,7 | -9,6% |
| LK Ebersberg | 94,8 | 117,6 | -19,4% |
| LK Freising | 77,8 | 80,6 | -3,5% |
| LK Fürstenfeldbruck | 125,8 | 127,6 | -1,4% |
| LK München | 112,3 | 132,1 | -15,0% |
| LK Starnberg | 69,5 | 84,1 | -17,4% |

Überall sinkende Inzidenz.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 8 Tagen knapp unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.12. | 0,94 | --- |
| 30.12. | 0,92 | --- |
| 31.12. | 0,89 | --- |
| 01.01. | 0,86 | --- |
| 02.01. | 0,91 | --- |
| 03.01. | 0,92 | --- |
| 04.01. | 0,92 | --- |

## Krankenhaus

Weiter Entspannung in den Münchner Krankenhäusern. Auf den Normalstation 13 Betten weniger belegt - von gestern 332 auf heute 319. IMC halbiert sich über nacht, gestern 14, heute 7. Intensiv 5 Betten weniger, von gestern 33 auf heute 28. Insgesamt 354 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 319 | -13 | 
| IMC | 7 | -7 | 
| Intensiv | 28 | -5 |
| SUMME | 354 | -25 |

Das sind insgesamt 55 Betten **weniger** als in der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL waren wir in der letzten Woche, mit den ganzen Feiertagen, bayernweit bei nur 1.933 Tests/Tag mit einer Positivrate von 12,31%. Gegenüber den Zahlen von letzter Woche (11.519 Tests/Tag mit 17,73% Positivrate) also kaum noch Tests, daher sind die Zahlen nicht sonderlich aussagekräftig. Hoffen wir, daß das nächste Woche wieder besser wird.

1.933 Tests/Tag ist kaum repräsentativ. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 1.933 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 12,31%.](/images/2023/01/20230105Test.png)