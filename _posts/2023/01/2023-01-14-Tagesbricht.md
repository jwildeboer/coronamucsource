---
layout: default
title: 2023-01-14 Tagesbericht
category: Tagesberichte
---

![2023-01-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher als in der Vorwoche, der R(t) Wert steigt auf 0,99. Zweite zeigt Krankenhausbetten, deren Zahl stabil bleibt, aber bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die nicht mehr sinkt.](/images/2023/01/20230114.png)

Das RKI meldet eine Inzidenz von 93,2 für München. 93 Neue, 61 Nachmeldungen, davon 58 for vorgestern. Die gestern genannte Inzidenz von 82,9 erhöht sich dadurch auf 87,0. Trotzdem - Tag 10 mit Inzidenz unter 100, aber nur noch knapp.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 06.01. | 71,1 -> 73,4 |
| 07.01. | 70,6 -> 72,9 |
| 08.01. | 70,2 -> 72,5 |
| 09.01. | 76,1 -> 85,0 |
| 10.01. | 84,7 -> 88,2 |
| 11.01. | 80,9 -> 84,9 |
| 12.01. | 82,9 -> 87,0 |

Da bis Montag keine Fallzahlen an das RKI gemeldet werden, können wir die Inzidenz für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 93,2 |
| Montag | 93,2 |

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 26. Damit insgesamt schon 29 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 14.01. | 13.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 98,4 | 86,2 | +14,2% |
| LK Ebersberg | 82,3 | 74,7 | +10,2% |
| LK Freising | 62,9 | 57,4 | +9,6% |
| LK Fürstenfeldbruck | 102,9 | 98,8 | +4,1% |
| LK München | 86,3 | 81,2 | +6,3% |
| LK Starnberg | 84,1 | 70,9 | +18,6% |

Überall steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 14 Tagen unter 1 und damit nicht mehr exponenziell. Sie wird in den nächsten Tagen aber wieder über 1 gehen. Am Montag wird sie 1,11 betragen.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt schon zum zweiten Mal nicht erfolgt.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 07.01. | 0,83 | --- |
| 08.01. | 0,80 | --- |
| 09.01. | 0,77 | --- |
| 10.01. | 0,79 | --- |
| 11.01. | 0,82 | --- |
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |

## Krankenhaus

**Stand 2023-01-13, bis Montag keine Aktualisierung**

Die Zahl der belegten Krankenhausbetten sinkt minimal: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 334 auf 332. IMC bleibt unverändert auf 6. Intensivbetten sinkt um 1 von gestern 31 auf heute 30. Damit insgesamt 368 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 332 | -2 | 
| IMC | 6 | +/-0 | 
| Intensiv | 30 | -1 |
| SUMME | 368 | -3 |

Damit sind 14 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.