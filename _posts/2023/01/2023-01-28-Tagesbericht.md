---
layout: default
title: 2023-01-28 Tagesbericht
category: Tagesberichte
---

![2023-01-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/01/20230128.png)

Das RKI meldet eine Inzidenz von 62,8 für München. 118 Neue, 27 Nachmeldungen, davon 26 für vorgestern. Die gestern genannte Inzidenz von 62,6 erhöht sich auf 64,4. R(t) steht auf 1,00. Weiter leichte Steigung. Samstag und Sonntag keine Krankenhauszahlen.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 20.01. | 57,7 -> 60,0 |
| 21.01. | 57,7 -> 60,0 |
| 22.01. | 57,7 -> 60,0 |
| 23.01. | 54,9 -> 57,1 |
| 24.01. | 58,0 -> 59,7 |
| 25.01. | 59,2 -> 59,8 |
| 26.01. | 62,6 -> 64,4 |

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 5. Damit insgesamt jetzt 60 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

Da bis Dienstag keine neuen Zahlen für die Inzidenzberechnng gemeldet werden, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 62,8 |
| Montag | 62,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 28.01. | 27.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 83,0 | 76,6 | +8,4% |
| LK Ebersberg | 45,0 | 39,4 | +14,2% |
| LK Freising | 72,9 | 78,4 | -7,0% |
| LK Fürstenfeldbruck | 89,2 | 90,6 | -1,5% |
| LK München | 74,0 | 70,3 | +5,3% |
| LK Starnberg | 60,7 | 59,2 | +2,5% |

Ebersberg hat gestern nachgemeldet, heute aber auch wieder nicht gemeldet. Uneinheitlich, aber eher Plateau.

## Reproduktionswert R(t) für München

Unser R(t) ist nach 11 Tagen unter 1 heute wieder auf genau 1,00 gestiegen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.01. | 0,80 | --- |
| 22.01. | 0,77 | --- |
| 23.01. | 0,77 | --- |
| 24.01. | 0,81 | --- |
| 25.01. | 0,86 | --- |
| 26.01. | 0,95 | --- |
| 27.01. | 1,00 | --- |

## Krankenhaus

**Stand 27.01.2023, Aktualisierung erst wieder am Montag**

Die Zahl der belegten Krankenhausbetten sinkt heute minimal: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 7 von 263 auf 256. IMC steigt um 1 von gestern 7 auf heute 8. Intensiv steigt um 2 von gestern 23 auf heute 25. Damit insgesamt 289 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 256 | -7 | 
| IMC | 8 | +1 | 
| Intensiv | 25 | +2 |
| SUMME | 289 | -4 |

Damit sind 46 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.