---
layout: default
title: 2023-01-27 Tagesbericht
category: Tagesberichte
---

![2023-01-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt, bleibt aber noch knapp unter 1. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/01/20230127.png)

Das RKI meldet eine Inzidenz von 62,6 für München. 162 Neue, 14 Nachmeldungen, davon 9 für vorgestern. Die gestern genannte Inzidnez von 59,2 erhöht sich auf 59,8. R(t) steigt wieder leicht, bleibt aber noch unter 1. Weiter leichte Steigung.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 19.01. | 56,4 -> 58,3 |
| 20.01. | 57,7 -> 60,0 |
| 21.01. | 57,7 -> 60,0 |
| 22.01. | 57,7 -> 60,0 |
| 23.01. | 54,9 -> 57,1 |
| 24.01. | 58,0 -> 59,7 |
| 25.01. | 59,2 -> 59,8 |

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 3. Damit insgesamt jetzt 59 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 27.01. | 26.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 76,6 | 77,8 | -1,5% |
| LK Ebersberg | 39,4 | 55,3 | -28,8% |
| LK Freising | 78,4 | 81,7 | -4,0% |
| LK Fürstenfeldbruck | 90,6 | 84,2 | +7,6% |
| LK München | 70,3 | 62,9 | +11,8% |
| LK Starnberg | 59,2 | 61,4 | -3,6% |

Ebersberg hat nicht gemeldet. Uneinheitlich, eher Plateau. 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 10 Tagen wieder unter der Grenze von 1 gesunken aber liegt heute bei 0,94 und steigt seit 3 Tagen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 20.01. | 0,83 | --- |
| 21.01. | 0,80 | --- |
| 22.01. | 0,77 | --- |
| 23.01. | 0,77 | --- |
| 24.01. | 0,81 | --- |
| 25.01. | 0,86 | --- |
| 26.01. | 0,94 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute minimal: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 7 von 263 auf 256. IMC steigt um 1 von gestern 7 auf heute 8. Intensiv steigt um 2 von gestern 23 auf heute 25. Damit insgesamt 289 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 256 | -7 | 
| IMC | 8 | +1 | 
| Intensiv | 25 | +2 |
| SUMME | 289 | -4 |

Damit sind 46 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.