---
layout: default
title: 2023-01-02 Wochenbericht KW52
category: Wochenberichte
---

![2023-01-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, der R(t) Wert bleibt unter 1. Zweite zeigt Krankenbetten, die nach den Feiertagen etwas sinken. Dritte zeigt Inzidenz, die wegen fehlender Meldungen deutlich zu niedrig ist.](/images/2023/01/20230102.png)

In der letzten Woche gab es insgesamt 1.608 neue COVID Fälle in München. Das sind 28,2% **weniger** als in der Vorwoche (2.239). Leider auch 14 Tote mit COVID Nachweis in den letzten 7 Tagen, etws höher als in der Vorwoche (13).
<!--more-->

## Fallzahlen

In der letzten Woche sank die 7 Tages Summe von 2.239 auf 1.608, wobei die Feiertage als wichtiger Faktor zu berücksichtigen sind. Die um Nachmeldungen korrigierte Inzidenz sank von 150,5 auf 108,1.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 25.12. | 0 | 2.239 | 150,5 |
| 26.12. | 0 | 1.625 | 109,2 |
| 27.12. | 786 | 1.947 | 130,9 |
| 28.12. | 361 | 1.865 | 125,4 |
| 29.12. | 323 | 1.820 | 122,3 |
| 30.12. | 138 | 1.608 | 108,1 |
| 31.12. | 0 | 1.608 | 108,1 |

## Reproduktionswert R(t) für München

Unser R(t) fiel in der letzten Woche unter 1 und stand gestern bei 0,84.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 28.12.) liegt bei 1,01. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.12. | 1,06 | 0,98 |
| 27.12. | 1,03 | 0,99 |
| 28.12. | 0,99 | 1,01 |
| 29.12. | 0,94 | --- |
| 30.12. | 0,92 | --- |
| 31.12. | 0,87 | --- |
| 01.01. | 0,84 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über die Feiertage klar weniger belegte Normalbetten. IMC sinkt auch ein wenig, Intensiv bleibt konstant etwas über 30. An 7 von 7 Tagen war die Summe etwas niedriger als in der Vorwoche aber immer über 400 Betten insgesamt.

Im Vergleich zum Vorjahr trozzdem deutlich höhere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 26.12. | 354 | 23 | 32 | 409 | -3 | 281 |
| 27.12. | 346 | 19 | 30 | 395 | -14 | 275 |
| 28.12. | 356 | 19 | 34 | 409 | -28 | 267 |
| 29.12. | 394 | 16 | 33 | 443 | -12 | 262 |
| 30.12. | 382 | 14 | 33 | 429 | -15 | 261 |
| 31.12. | 370 | 12 | 33 | 415 | -18 | 259 |
| 01.01. | 358 | 11 | 34 | 403 | -19 | 258 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, langsam etwas besser aus. In der letzten Woche waren wir an 4 Tagen mit den korrigierten Zahlen noch über dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten allerdings steigt die Summe von letzte Woche 144 auf jetzt 194.

Im Vergleich zum Vorjahr weiterhin deutlich höhere Zahlen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 26.12. | 1.255 | 736 |
| 27.12. | 1.361 | 736 |
| 28.12. | 1.359 | 711 |
| 29.12. | 1.307 | 708 |
| 30.12. | 1.173 | 694 |
| 31.12. | 1.138 | 685 |
| 01.01. | 1.109 | 682 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen. Änderungen gegenüber der Vorwoche nur im Bereich der Nachkommastellen.

Mit der Schließung der Impfzentren hat die Stadt München wohl auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit wird in Zukunft kein Vergleich mit den Landeszahlen mehr möglich sein.

Das RKI gibt jetzt auch Zahlen für die 5te und 6te Impfung an, die Stadt München fasst die 4., 5. und 6. Impfung zusammen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 73,01% | 75,20% |
| 2. | 71,48% | 75,13% |
| 3. | 48,38% | 59,22% |
| 4. | 12,12% | 11,74% |
| 5. | --- | 0,76% |
| 6. | --- | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/12/KW52Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Laut LGL sind wir jetzt bayernweit bei nur 11.519 Tests/Tag mit einer Positivrate von 17,73%. Gegenüber den Zahlen von letzter Woche (11.629 Tests/Tag mit 13,80% Positivrate) also weiter weniger Tests, gepaart mit steigender Positivrate.

11.519 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Seit 4 Wochen schaffen wir aber nich nichtmal 100.000 Test pro **Woche**.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 11.519 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 17,73%.](/images/2022/12/20221229Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter auf jetzt 24.6%, BQ.1 scheint mit 36.5% das Rennen zu machen. Aber BF.7 liegt auch um 18.6%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

| Variante | KW48 | KW49 | KW50 | KW 51 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 0.9% | 0.5% | 0.3% | 0.0% |
| Omikron BA.2.75 | 2.5% | 3.1% | 3.5% | 6.0% |
| Omikron BN.1 | 5.1% | 5.3% | 7.9% | 9.3% |
| Omikron BA.4 | 0.7% | 0.0% | 0.5% | 2.0% |
| Omikron BA.5 | 35.6% | 29.1% | 27.4% | 24.6% |
| Omikron BF.7 | 24.4% | 20.5% | 18.5% | 18.6% |
| Omikron BQ.1 | 28,2% | 36.5% | 37.9% | 36.5% |
| Andere | 2.6% | 4.9% | 4.0% | 3.0% |
| Sequenzierungen | 761 | 736 | 573 | 301 | 

![VoC](/images/2023/01/KW52Var.png)