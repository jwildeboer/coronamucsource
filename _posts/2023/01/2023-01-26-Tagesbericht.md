---
layout: default
title: 2023-01-26 Tagesbericht
category: Tagesberichte
---

![2023-01-26 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt, bleibt aber klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl nicht mehr sinkt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet.](/images/2023/01/20230126.png)

Das RKI meldet eine Inzidenz von 59,2 für München. 179 Neue, 27 Nachmeldungen, davon239 für vorgestern. Die gestern genannte Inzidnez von 58,0 erhöht sich auf 59,7. R(t) steigt wieder leicht, bleibt aber klar unter 1. Leichte Steigung.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 18.01. | 59,3 -> 63,0 |
| 19.01. | 56,4 -> 58,3 |
| 20.01. | 57,7 -> 60,0 |
| 21.01. | 57,7 -> 60,0 |
| 22.01. | 57,7 -> 60,0 |
| 23.01. | 54,9 -> 57,1 |
| 24.01. | 58,0 -> 59,7 |

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 12. Damit insgesamt jetzt 57 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 26.01. | 25.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 77,8 | 79,1 | -1,6% |
| LK Ebersberg | 55,3 | 61,6 | -10,2% |
| LK Freising | 81,7 | 75,1 | +8,8% |
| LK Fürstenfeldbruck | 84,2 | 89,2 | -5,6% |
| LK München | 62,9 | 67,2 | -6,4% |
| LK Starnberg | 61,4 | 57,8 | +6,2% |

Uneinheitlich, eher Plateau. 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 9 Tagen wieder unter der Grenze von 1 gesunken und liegt heute bei 0,86.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.01. | 0,87 | --- |
| 20.01. | 0,83 | --- |
| 21.01. | 0,80 | --- |
| 22.01. | 0,77 | --- |
| 23.01. | 0,77 | --- |
| 24.01. | 0,81 | --- |
| 25.01. | 0,86 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute minimal: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 6 von 257 auf 263. IMC bleibt mit 7 Betten gleich. Intensiv sinkt um 4 von gestern 27 auf heute 23. Damit insgesamt 293 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 263 | +6 | 
| IMC | 7 | +/-0 | 
| Intensiv | 23 | -4 |
| SUMME | 293 | +2 |

Damit sind 61 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.

## Testzahlen

Heute gab es auch wieder das wöchentliche Update der Testzahlen. Laut LGL waren wir in der letzten Woche bayernweit bei 11.173 Tests/Tag mit einer Positivrate von 9,93%. Gegenüber den Zahlen von letzter Woche (12.311 Tests/Tag mit 10,50% Positivrate) also etwas weniger Tests bei sinkender Positivrate.

11.173 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 11.173 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 9,93%.](/images/2023/01/20230126Test.png)