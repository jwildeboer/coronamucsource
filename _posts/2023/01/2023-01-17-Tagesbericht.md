---
layout: default
title: 2023-01-17 Tagesbericht
category: Tagesberichte
---

![2023-01-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt über 1. Zweite zeigt Krankenhausbetten, deren Zahl wieder steigt. Dritte zeigt Inzidenz, die heute zwar kräftig sinkt, aber das sagt noch nicht viel aus.](/images/2023/01/20230117.png)

Das RKI meldet eine Inzidenz von 93,2 für München. 209 neue, 26 Nachmeldungen, davon 23 für Freitag. Die Inzidenzen von Freitag, Samstag und Sonntag steigen dadurch von 93,2 auf 94,8. R(t) bleibt über 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 09.01. | 76,1 -> 85,0 |
| 10.01. | 84,7 -> 88,2 |
| 11.01. | 80,9 -> 84,9 |
| 12.01. | 82,9 -> 87,0 |
| 13.01. | 93,2 -> 94,8 |
| 14.01. | 93,2 -> 94,8 |
| 15.01. | 93,2 -> 94,8 |

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 27. Damit insgesamt schon 30 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 17.01. | 14.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 74,6 | 98,4 | -24,2% |
| LK Ebersberg | 49,1 | 82,3 | -40,3% * |
| LK Freising | 58,0 | 62,9 | -7,8% |
| LK Fürstenfeldbruck | 54,0 | 102,9 | -47,5% * |
| LK München | 70,9 | 86,3 | -17,8% |
| LK Starnberg | 68,0 | 84,1 | -19,1% |

Teilweise drastisch niedrigere Zahlen, weil Meldungen aus Ebersberg, Fürstenfeldbruck fehlen.

## Reproduktionswert R(t) für München

Unser R(t) ist nach 14 Tagen unter 1 wieder seit 3 Tagen auf einen Wert über 1 gestiegen. .

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt schon zum zweiten Mal nicht erfolgt.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.01. | 0,79 | --- |
| 11.01. | 0,82 | --- |
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |
| 14.01. | 1,05 | --- |
| 15.01. | 1,12 | --- |
| 16.01. | 1,09 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt wieder: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 19 von 301 auf 320. IMC steigt von gestern 4 auf heute 6. Intensivbetten sinkt um 2 von gestern 32 auf heute 30. Damit insgesamt 356 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 320 | +19 | 
| IMC | 6 | +2 | 
| Intensiv | 30 | -2 |
| SUMME | 356 | +19 |

Damit sind 3 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.