---
layout: default
title: 2023-01-20 Tagesbericht
category: Tagesberichte
---

![2023-01-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl jetzt sinkt. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230120.png)

Das RKI meldet eine Inzidenz von 56,4 für München. 195 neue, 60 Nachmeldungen, davon 49 für vorgestern. Die gestern genannte Inzidenz von 59,3 steigt dadurch auf 62,7. R(t) fällt wieder klar unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 12.01. | 82,9 -> 87,2 |
| 13.01. | 93,2 -> 95,0 |
| 14.01. | 93,2 -> 95,0 |
| 15.01. | 93,2 -> 94,9 |
| 16.01. | 71,7 -> 76,8 |
| 17.01. | 64,1 -> 67,0 |
| 18.01. | 59,3 -> 62,7 |

Es wurden 11(!) neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 32. Damit insgesamt schon 56 COVID-Tote bis jetzt, nach gerade mal 20 Tagen in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum vortag in %.

| Kreis | 20.01. | 19.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 66,9 | 63,0 | +6,2% |
| LK Ebersberg | 74,7 | 68,5 | +9,1% |
| LK Freising | 65,7 | 58,0 | +13,3% |
| LK Fürstenfeldbruck | 68,6 | 75,9 | -9,6% |
| LK München | 67,5 | 68,0 | -0,7% |
| LK Starnberg | 65,1 | 71,7 | -9,2% |

Eher Plateau als irgendein Trend.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 2 Tagen wieder unter der Grenze von 1 gesunken und liegt heute bei 0,87.

Das R(t) vom StaBLab der LMU wird wohl nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |
| 14.01. | 1,05 | --- |
| 15.01. | 1,12 | --- |
| 16.01. | 1,10 | --- |
| 17.01. | 1,03 | --- |
| 18.01. | 0,95 | --- |
| 19.01. | 0,87 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute (wie häufig an einem Freitag) wieder: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 16 von 315 auf 299. IMC steigt um 2 von gestern 7 auf heute 9. Intensiv sinkt um 5 von gestern 32 auf heute 27. Damit insgesamt 335 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 299 | -16 | 
| IMC | 9 | +2 | 
| Intensiv | 27 | -5 |
| SUMME | 335 | -19 |

Damit sind 33 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.