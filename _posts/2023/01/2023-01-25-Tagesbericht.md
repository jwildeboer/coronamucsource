---
layout: default
title: 2023-01-25 Tagesbericht
category: Tagesberichte
---

![2023-01-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ähnlich wie in der Vorwoche, der R(t) Wert steigt, bleibt aber klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet.](/images/2023/01/20230125.png)

Das RKI meldet eine Inzidenz von 58,0 für München. 175 Neue, 31 Nachmeldungen, davon 29 für vorgestern. Die gestern genannte Inzidnez von 54,9 erhöht sich auf 56,9. R(t) steigt wieder leicht, bleibt aber klar unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 17.01. | 64,1 -> 67,2 |
| 18.01. | 59,3 -> 62,9 |
| 19.01. | 56,4 -> 58,2 |
| 20.01. | 57,7 -> 59,9 |
| 21.01. | 57,7 -> 59,9 |
| 22.01. | 57,7 -> 59,9 |
| 23.01. | 54,9 -> 56,9 |

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 14. Damit insgesamt jetzt 57 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 25.01. | 24.01 | Änderung |
|:---|---:|---:|---:|
| LK Dachau |79,1 | 66,9 | +18,2% |
| LK Ebersberg | 61,6 | 71,9 | -14,3% |
| LK Freising | 75,1 | 67,9 | +10,6% |
| LK Fürstenfeldbruck | 89,2 | 81,4 | +9,6% |
| LK München | 67,2 | 43,4 | +54,8% |
| LK Starnberg | 57,8 | 63,6 | -9,1% |

Der LK München hat nachgemeldet. Leicht steigende Tendenz. 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 8 Tagen wieder unter der Grenze von 1 gesunken und liegt heute bei 0,81.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.01. | 0,95 | --- |
| 19.01. | 0,87 | --- |
| 20.01. | 0,83 | --- |
| 21.01. | 0,80 | --- |
| 22.01. | 0,77 | --- |
| 23.01. | 0,77 | --- |
| 24.01. | 0,81 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute weiter, aber nur leicht: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 6 von 263 auf 257. IMC bleibt mit 7 Betten gleich. Intensiv sinkt um 2 von gestern 29 auf heute 27. Damit insgesamt 291 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 257 | -6 | 
| IMC | 7 | +/-0 | 
| Intensiv | 27 | -2 |
| SUMME | 291 | -8 |

Damit sind 79 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.