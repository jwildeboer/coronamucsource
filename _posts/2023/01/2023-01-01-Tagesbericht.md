---
layout: default
title: 2023-01-01 Tagesbericht
category: Tagesberichte
---

![2023-01-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche, der R(t) Wert sinkt auf 0,87. Zweite zeigt Krankenhausbetten, die heute nicht aktualisiert werden, aber auf hohem Niveau bleiben. Dritte zeigt Inzidenz, die zwar sinkt, aber wegen Feiertage verzerrt ist.](/images/2023/01/20230101.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 108,1 für München. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert, Stand 31.12, Aktualisierung erst am Dienstag):

| 23.12. | 145,5 -> 150,5 |
| 24.12. | 145,5 -> 150,5 |
| 25.12. | 145,5 -> 150,5 |
| 26.12. | 104,3 -> 109,2 |
| 27.12. | 121,4 -> 130,9 |
| 28.12. | 121,7 -> 125,4 |
| 29.12. | 116,8 -> 122,3 |

 Erst in der 2. Januarwoche werden die Zahlen wieder wirklich zuverlässig - sofern es dann noch aktuelle Zahlen geben wird.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten Vortag in %. Stand 31.12., Aktualisierung erst wieder am Dienstag.

| Kreis | 31.12. | 30.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 94,6 | 119,0 | -20,5% |
| LK Ebersberg | 110,0 | 100,3 | +9,7% |
| LK Freising | 76,7 | 81,7 | -6,1% |
| LK Fürstenfeldbruck | 128,6 | 142,3 | -9,6% |
| LK München | 116,9 | 123,2 | -5,1% |
| LK Starnberg | 88,5 | 95,8 | -7,6% |

In Ebersberg ungewöhlich hohe Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 4 Tagen knapp unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.12. | 1,10 | 0,99 |
| 26.12. | 1,06 | 0,98 |
| 27.12. | 1,03 | 0,99 |
| 28.12. | 0,90 | 1,01 |
| 29.12. | 0,94 | --- |
| 30.12. | 0,92 | --- |
| 30.12. | 0,87 | --- |

## Krankenhaus

Am Sonntag gibt es keine neuen Krankenhauszahlen von der Stadt München. Stand vorgestern sind insgesamt 443 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 394 | --- | 
| IMC | 16 | --- | 
| Intensiv | 33 | --- |
| SUMME | 443 | --- |

Das sind insgesamt 12 Betten **weniger** als in der Vorwoche.

## Impfungen

Am Sonntag gibt es keine Impfzahlen von der Stadt München und für das Land Bayern.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,01% | --- | 75,20% |
| 2. | --- | 71,48% | --- | 75,13% |
| 3. | --- | 48,38% | --- | 59,22% |
| 4. | ---0 | 12,12% | --- | 11,73% |
| 5. | --- | --- | --- | 0,75% |
| 6. | --- | --- | --- | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.