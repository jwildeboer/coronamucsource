---
layout: default
title: 2023-01-13 Tagesbericht
category: Tagesberichte
---

![2023-01-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, höher als in der Vorwoche, der R(t) Wert ist 0,81. Zweite zeigt Krankenhausbetten, deren Zahl stabil bleibt. Dritte zeigt Inzidenz, die nicht mehr sinkt.](/images/2023/01/20230113.png)

Das RKI meldet eine Inzidenz von 82,9 für München. 130 Neue, 57 Nachmeldungen, davon 50 for vorgestern. Die gestern genannte Inzidenz von 80,9 erhöht sich dadurch auf 84,7. Trotzdem - Tag 10 mit Inzidenz unter 100.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 05.01. | 84,6 -> 86,8 |
| 06.01. | 71,1 -> 73,4 |
| 07.01. | 70,6 -> 72,9 |
| 08.01. | 70,2 -> 72,5 |
| 09.01. | 76,1 -> 84,9 |
| 10.01. | 84,7 -> 88,1 |
| 11.01. | 80,9 -> 84,7 |

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 21. Damit insgesamt 24 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 13.01. | 12.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 86,2 | 95,9 | -10,1% |
| LK Ebersberg | 74,7 | 71,2 | +4,9% |
| LK Freising | 57,4 | 60,7 | -5,4% |
| LK Fürstenfeldbruck | 98,8 | 92,4 | +6,9% |
| LK München | 81,2 | 80,3 | +1,1% |
| LK Starnberg | 70,9 | 63,6 | +11,5% |

Eher steigende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 14 Tagen unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung ist jetzt seit über einer Woche verspätet.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 06.01. | 0,85 | --- |
| 07.01. | 0,83 | --- |
| 08.01. | 0,80 | --- |
| 09.01. | 0,77 | --- |
| 10.01. | 0,79 | --- |
| 11.01. | 0,82 | --- |
| 12.01. | 0,88 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt minimal: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 334 auf 332. IMC bleibt unverändert auf 6. Intensivbetten sinkt um 1 von gestern 31 auf heute 30. Damit insgesamt 368 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 332 | -2 | 
| IMC | 6 | +/-0 | 
| Intensiv | 30 | -1 |
| SUMME | 368 | -3 |

Damit sind 14 Betten **mehr** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.