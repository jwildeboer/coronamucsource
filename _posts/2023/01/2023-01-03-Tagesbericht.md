---
layout: default
title: 2023-01-03 Tagesbericht
category: Tagesberichte
---

![2023-01-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche, der R(t) Wert steigt auf 0,91 - Nachholeffekt wegen Feiertage. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt.. Dritte zeigt Inzidenz, die steigt, aber wegen Feiertage noch verzerrt ist.](/images/2023/01/20230103.png)

Das RKI meldet eine Inzidenz von 133,0 für München. 291 Neue, 82 Nachmeldungen, davon 2 für Sonntag, 7 für Samstag und 57 für Freitag. Die Inzidenz für Sonntag geht von 108,1 auf 113,4. Samstag von 108,1 auf 113,3. Freitag von 108,1 auf 112,8.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 26.12. | 104,3 -> 109,3 |
| 27.12. | 121,4 -> 131,0 |
| 28.12. | 121,7 -> 125,8 |
| 29.12. | 116,8 -> 123,2 |
| 30.12. | 108,1 -> 112,8 |
| 31.12. | 108,1 -> 113,3 |
| 01.01. | 108,1 -> 113,4 |

 Erst Ende der woche werden die Zahlen wieder wirklich zuverlässig.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 03.01 | 31.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 144,7 | 94,6 | +53,0% |
| LK Ebersberg | 134,2 | 110,0 | +22% |
| LK Freising | 78,4 | 76,7 | +2,2% |
| LK Fürstenfeldbruck | 167,4 | 128,6 | +30,2% |
| LK München | 121,5 | 116,9 | +3,9% |
| LK Starnberg | 104,6 | 88,5 | +18,2% |

Große Sprünge wegen Feiertage, morgen wieder realistischere Vergleiche.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 6 Tagen knapp unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 27.12. | 1,03 | 0,99 |
| 28.12. | 0,99 | 1,01 |
| 29.12. | 0,94 | --- |
| 30.12. | 0,92 | --- |
| 31.12. | 0,89 | --- |
| 01.01. | 0,86 | --- |
| 02.01. | 0,91 | --- |

## Krankenhaus

Weiter Entspannung in den Münchner Krankenhäusern. Auf den Normalstation 21 betten weniger belegt - von gestern 358 auf heute 337. IMC ein Bett mehr belegt, gestern 11, heute 12. Intensiv 2 Betten weniger, von gestern 34 auf heute 32. Insgesamt 381 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 337 | -21 | 
| IMC | 12 | +1 | 
| Intensiv | 32 | -2 |
| SUMME | 381 | -22 |

Das sind insgesamt 28 Betten **weniger** als in der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht wird dann in Zukunft nur noch die landesweite Impfquote genannt.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,01% | 4 | 75,20% |
| 2. | --- | 71,48% | 11 | 75,13% |
| 3. | --- | 48,38% | 30 | 59,22% |
| 4. | --- | 12,12% | 358 | 11,75% |
| 5. | --- | --- | 66 | 0,76% |
| 6. | --- | --- | 0 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.