---
layout: default
title: 2023-01-30 Wochenbericht KW04
category: Wochenberichte
---

![2023-01-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die langsamer sinkt und erst morgen neue Zahlen enthält.](/images/2023/01/20230128.png)

In der letzten Woche gab es insgesamt 978 neue COVID Fälle in München. Das sind 9,4% **mehr** als in der Vorwoche (894). Leider auch 5 Tote mit COVID Nachweis in den letzten 7 Tagen, deutlich weniger als in der Vorwoche (26).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe von 894 auf 978. Die um Nachmeldungen korrigierte Inzidenz stieg von 60,1 auf 65,7.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 22.01. | 0 | 894 | 60,1 |
| 23.01. | 246 | 854 | 57,4 |
| 24.01. | 200 | 895 | 60,2 |
| 25.01. | 187 | 896 | 60,2 |
| 26.01. | 189 | 965 | 64,9 |
| 27.01. | 156 | 978 | 65,7 |
| 28.01. | 0 | 978 | 65,7 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche kontinuierlich von 0,77 auf jetzt 1,06 und ist damit wieder im exponenziellem Bereich.

| Datum | R(t) 7 Tage |
|---|---:|
| 23.01. | 0,77 |
| 24.01. | 0,81 |
| 25.01. | 0,86 |
| 26.01. | 0,95 |
| 27.01. | 1,00 |
| 28.01. | 1,04 | 
| 29.01. | 1,06 |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München bleibt die Zahl der belegten Normalbetten relativ konstant. IMC pendelt nur wenig zwischen 6 und 8, Intensiv wird weniger. An 7 von 7 Tagen war die Summe niedriger als in der Vorwoche.

Im Vergleich zum Vorjahr niedrigere Zahlen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 23.01. | 263 | 7 | 29 | 299 | -57 | 351 |
| 24.01. | 257 | 7 | 27 | 291 | -79 | 369 |
| 25.01. | 263 | 7 | 23 | 293 | -61 | 404 |
| 26.01. | 256 | 8 | 25 | 289 | -46 | 414 |
| 27.01. | 268 | 8 | 24 | 300 | -28 | 407 |
| 28.01. | 280 | 7 | 23 | 310 | -11 | 452 |
| 29.01. | 264 | 6 | 22 | 292 | -23 | 456 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt, zumindest auf den Normalstationen, ähnlich wie im Vorjahr aus. Klare Steigung der Zahlen. Aber unter den Vorjahreszahlen. Sieht lkangsam nach einer saisonalen Entwicklung aus.

Bei den Intensivbetten bleibt es eher stabil um 130.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 23.01. | 1.080 | 1.123 |
| 24.01. | 1.152 | 1.202 |
| 25.01. | 1.170 | 1.316 |
| 26.01. | 1.195 | 1.409 |
| 27.01. | 1.269 | 1.477 |
| 28.01. | 1.274 | 1.557 |
| 29.01. | 1.283 | 1.609 |

## Impfungen

Mit der Schließung der Impfzentren hat die Stadt München auch die arbeitstägliche Veröffentlichung der Impfzahlen eingestellt. Damit ist kein Vergleich mit den Landeszahlen mehr möglich.

Insgesamt wird eh kaum noch geimpft, in der letzten Woche aber weniger als in der Vorwoche. Wir haben in der Tabelle die Summe der jeweiligen Impfungen der letzten 7 Tage in ganz Bayern angegeben. 

| Impfung | 7 Tage Summe | Quote Bayern |
|---|--:|--:|
| 1. | 356 | 75,21% (+/-0%) |
| 2. | 115 | 75,13% (+/-0%) |
| 3. | 529 | 59,24% (+0,01%) |
| 4. | 4.929 | 11,90% (+0,04%) |
| 5. | 1.502 | 0,81% (+0,02%) |
| 6. | 114 | 0,04% (+/-0%) |

![Balken- und Kurvendiagramm der Impfzahlen in Bayern das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen und in 2023 kaum noch geimpft wird.](/images/2023/01/KW03Impf.png)

Seit dem 1. Oktober 2022 gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag aktualisiert das LGL die PCR-Testzahlen für Bayern.

Laut LGL waren wir in der letzten Woche bayernweit bei 11.262 Tests/Tag mit einer Positivrate von 9,91%. Gegenüber den Zahlen von letzter Woche (12.311 Tests/Tag mit 10,50% Positivrate) also etwas weniger Tests bei sinkender Positivrate.

11.262 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen und weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 11.929 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 10,63%.](/images/2023/01/20230126Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter auf um 15%, BQ.1 scheint mit 36.1% an der Spitze, verliert aber auch. BF.7 fällt auf unter 10%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

Es gibt nun auch Zahlen zur Variante XBB.1.5, die aber nur um 10% Anteil hat.

Die Zeiten dominanter Varianten ist wohl vorbei.


| Variante | KW 52 | KW 01 | KW 02 | KW 03 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 3.7% | 0.0% | 2.5% | 3.0% |
| Omikron BA.2.75 | 15.6% | 10.4% | 13.7% | 16.4% |
| Omikron BN.1 | 6.4% | 5.7% | 5.7% | 6.1% |
| Omikron XBB.1.5 | 6.9% | 7.5% | 7.1% | 10.2% |
| Omikron BA.4 | 0.5% | 0.4% | 0.2% | 0.0% |
| Omikron BA.5 | 18.8% | 17.6% | 15.1% | 15.5% |
| Omikron BF.7 | 8.7% | 14.7% | 11.8% | 9.3% |
| Omikron BQ.1 | 39.4% | 43.4% | 42.2% | 36.1% |
| Andere | 0.0% | 0.4% | 1.8% | 3.4% |
| Sequenzierungen | 218 | 279 | 510 | 440 |

![VoC](/images/2023/01/KW03Var.png)