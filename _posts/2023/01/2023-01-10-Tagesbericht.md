---
layout: default
title: 2023-01-10 Tagesbericht
category: Tagesberichte
---

![2023-01-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist 0,76. Zweite zeigt Krankenhausbetten, deren Zahl leicht steigt und seit 6 Tagen um 350 schwankt. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230110.png)

Das RKI meldet eine Inzidenz von 70,6 für München. 425 Neue, 31 Nachmeldungen, davon 1 am Sonntag, 23 für letzte Woche Donnerstag. Die Inzidenzen Donnersteg gehen von 84,6 auf 86,5, Freitag von 71,1 auf 73,1, Samstag von 70,6 auf 72,6, Sonntag von 70,2 auf 72,2. Seit 4 Tagen kein neuer Todesfall.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 02.01. | 133.0 -> 138,9 |
| 03.01. | 99,0 -> 103,2 |
| 04.01. | 93,0 -> 98,3 |
| 05.01. | 84,6 -> 86,5 |
| 06.01. | 71,1 -> 73,1 |
| 07.01. | 70,6 -> 72,6 |
| 08.01. | 70,2 -> 72,2 |

Es wurde kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 3. Damit insgesamt 3 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu letzter Woche Freitag in %.

| Kreis | 10.01. | 06.01 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 54,7 | 106,8 | -48,8% |
| LK Ebersberg | 65,7 | 66,4 | -1,1% |
| LK Freising | 54,1 | 67,9 | -20,3% |
| LK Fürstenfeldbruck | 87,8 | 91,0 | -3,5% |
| LK München | 78,0 | 100,9 | -22,7% |
| LK Starnberg | 62,9 | 60,0 | +4,8% |

Fast überall sinkende Inzidenz. Dachau hat noch keine neuen Zahlen gemeldet, daher der scheinbar große Rückgang bei der Inzidenz.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 13 Tagen unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München. Die versprochene Neuberechnung lässt mal wieder auf sich warten.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Oder auch gar nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.01. | 0,92 | --- |
| 04.01. | 0,93 | --- |
| 05.01. | 0,92 | --- |
| 06.01. | 0,85 | --- |
| 07.01. | 0,82 | --- |
| 08.01. | 0,80 | --- |
| 09.01. | 0,76 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute ein wenig: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 7 von 305 auf 312. IMC steigt um 1 von gestern 10 auf heute 11. Intensivbetten hingegen sinkt um 4 von gestern 34 auf heute 30. Damit insgesamt 353 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 312 | +7 | 
| IMC | 11 | +1 | 
| Intensiv | 30 | -4 |
| SUMME | 353 | +4 |

Das sind 28 Betten **weniger** belegt als in der Vorwoche.
## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.