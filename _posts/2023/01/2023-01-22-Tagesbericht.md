---
layout: default
title: 2023-01-22 Tagesbericht
category: Tagesberichte
---

![2023-01-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt klar unter 1. Zweite zeigt Krankenhausbetten, deren Zahl jetzt sinkt aber bis Moontag nicht aktualisiert wird. Dritte zeigt Inzidenz, die langsamer sinkt.](/images/2023/01/20230122.png)

Das RKI meldet eine Inzidenz von 57,7 für München. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

## Inzidenzentwicklung

**Stand 21.01., neue Aktualisierung am Dienstag**

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 13.01. | 93,2 -> 95,1 |
| 14.01. | 93,2 -> 95,1 |
| 15.01. | 93,2 -> 95,0 |
| 16.01. | 71,7 -> 77,0 |
| 17.01. | 64,1 -> 67,2 |
| 18.01. | 59,3 -> 62,9 |
| 19.01. | 56,4 -> 58,1 |

Da bis Montag auch keine Fallmeldungen an das RKI gehen, können wir die Inzidenz für Montag schon heute berechnen:

| Montag | 57,7 |

Es wurde um 1 Todesfall nach unten korrigiert. Die 7 Tage Summe der Todesfälle ist 26. Damit insgesamt jetzt 55 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

**Stand 21.01., neue Aktualisierung am Dienstag**

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 21.01. | 20.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 66,9 | 66,9 | +/-0% |
| LK Ebersberg | 71,2 | 74,7 | -4,7% |
| LK Freising | 67,9 | 65,7 | +3,3% |
| LK Fürstenfeldbruck | 77,8 | 68,6 | +13,4% |
| LK München | 68,3 | 67,5 | +1,2% |
| LK Starnberg | 60,0 | 65,1 | -7,8% |

Noch eher Plateau als steigender Trend.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 4 Tagen wieder unter der Grenze von 1 gesunken und liegt heute bei 0,82.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.01. | 1,12 | --- |
| 16.01. | 1,10 | --- |
| 17.01. | 1,03 | --- |
| 18.01. | 0,95 | --- |
| 19.01. | 0,87 | --- |
| 20.01. | 0,82 | --- |
| 21.01. | 0,79 | --- |

## Krankenhaus

**Stand 20.01., neue Aktualisierung am Montag**

Die Zahl der belegten Krankenhausbetten sinkt heute (wie häufig an einem Freitag) wieder: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 16 von 315 auf 299. IMC steigt um 2 von gestern 7 auf heute 9. Intensiv sinkt um 5 von gestern 32 auf heute 27. Damit insgesamt 335 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 299 | -16 | 
| IMC | 9 | +2 | 
| Intensiv | 27 | -5 |
| SUMME | 335 | -19 |

Damit sind 33 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.