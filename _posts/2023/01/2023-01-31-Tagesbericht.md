---
layout: default
title: 2023-01-31 Tagesbericht
category: Tagesberichte
---

![2023-01-31 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas höher wie in der Vorwoche, der R(t) Wert steigt auf genau 1,00. Zweite zeigt Krankenhausbetten, deren Zahl minimal sinkt, und bis Montag nicht aktualisiert wird. Dritte zeigt Inzidenz, die sich auf einem Plateau befindet mit zunehmender Tendenz nach oben.](/images/2023/01/20230131.png)

Das RKI meldet eine Inzidenz von 71,7 für München. 338 neue und 37 Nachmeldungen für die letzten 7 Tage, bis auf 1 alles für Freitag. 3 neu gemeldete Todesfälle, 7-Tage-Summe 6.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 23.01. | 54,9 -> 57,1 |
| 24.01. | 58,0 -> 59,7 |
| 25.01. | 59,2 -> 59,8 |
| 26.01. | 62,6 -> 64,4 |
| 27.01. | 62,8 -> 65,3 |
| 28.01. | 62,8 -> 65,3 |
| 29.01. | 62,8 -> 65,3 |

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 6. Damit insgesamt jetzt 63 COVID-Tote in 2023, in 2022 waren es insgesmt 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 31.01. | 30.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 70,1 | 83,0 | -15,5% |
| LK Ebersberg | 59,5 | 45,0 | +32,2% |
| LK Freising | 69,6 | 59,6 | +16,8% |
| LK Fürstenfeldbruck | 91,0 | 90,6 | +0,4% |
| LK München | 74,6 | 66,9 | +11,5% |
| LK Starnberg | 60,0 | 60,7 | -1,2% |

## Reproduktionswert R(t) für München

Unser R(t) ist heute weiter auf 1,08 gestiegen.

Das R(t) vom StaBLab der LMU wird nicht mehr aktualisiert. Die bisherige [Seite vom StaBLab](https://corona.stat.uni-muenchen.de) ist jetzt leer, alle Daten, auch die historischen, sind verschwunden. Wir haben die R(t) Werte der LMU aber weiterhin in unserer [Großen Tabelle]( https://icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) gesichert.

| Datum | R(t) | LMU |
|---|---:|---:|
| 24.01. | 0,81 | --- |
| 25.01. | 0,86 | --- |
| 26.01. | 0,95 | --- |
| 27.01. | 1,00 | --- |
| 28.01. | 1,04 | --- |
| 29.01. | 1,06 | --- |
| 30.01. | 1,08 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 19 von 264 auf 245. IMC steigt um 4 von gestern 6 auf heute 10. Intensiv steigt um 7 von gestern 22 auf heute 29. Damit insgesamt 284 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 245 | -19 | 
| IMC | 10 | +4 | 
| Intensiv | 29 | +7 |
| SUMME | 284 | -8 |

Damit sind 15 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, haben wir die tägliche Angabe der Impfzahlen eingestellt. Im Wochenbericht, jeweils am Montag gibt es dann Angaben zu Änderungen bayernweit gegenüber der Vorwoche.