---
layout: default
title: 2023-01-04 Tagesbericht
category: Tagesberichte
---

![2023-01-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert bleibt auf 0,91. Zweite zeigt Krankenhausbetten, deren Zahl weiter sinkt. Dritte zeigt Inzidenz, die wieder sinkt, aber wegen Feiertage noch verzerrt ist.](/images/2023/01/20230104.png)

Das RKI meldet eine Inzidenz von 99,0 für München. 08 Neue, 76 Nachmeldungen, davon 68 für vorgestern. Die Die gestern genannte Inzidenz von 133,0 steigt durch Nachmeldungen auf 138,0. Tag 7 mit R(t) unter 1,0.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 27.12. | 121,4 -> 131,0 |
| 28.12. | 121,7 -> 125,9 |
| 29.12. | 116,8 -> 123,3 |
| 30.12. | 108,1 -> 113,1 |
| 31.12. | 108,1 -> 113,5 |
| 01.01. | 108,1 -> 113,9 |
| 02.01. | 133.0 -> 138,0 |

 Erst Ende der woche werden die Zahlen wieder wirklich zuverlässig.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis |04.01. |  03.01 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,7 | 144,7 | -11,1% |
| LK Ebersberg | 117,6 | 134,2 | -12,4% |
| LK Freising | 80,6 | 78,4 | +2,8% |
| LK Fürstenfeldbruck | 127,6 | 167,4 | -23,8% |
| LK München | 132,1 | 121,5 | +8,7% |
| LK Starnberg | 84,1 | 104,6 | -19,6% |

Große Sprünge wegen Feiertage, morgen wieder realistischere Vergleiche.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 7 Tagen knapp unter 1 und damit nicht mehr exponenziell.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,01 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.12. | 0,99 | 1,01 |
| 29.12. | 0,94 | --- |
| 30.12. | 0,92 | --- |
| 31.12. | 0,89 | --- |
| 01.01. | 0,86 | --- |
| 02.01. | 0,91 | --- |
| 03.01. | 0,91 | --- |

## Krankenhaus

Weiter Entspannung in den Münchner Krankenhäusern. Auf den Normalstation 5 Betten weniger belegt - von gestern 337 auf heute 332. IMC 2 Betten mehr belegt, gestern 12, heute 14. Intensiv 1 Bett mehr, von gestern 32 auf heute 33. Insgesamt 379 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 332 | -5 | 
| IMC | 14 | +2 | 
| Intensiv | 33 | +1 |
| SUMME | 379 | -2 |

Das sind insgesamt 16 Betten **weniger** als in der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.