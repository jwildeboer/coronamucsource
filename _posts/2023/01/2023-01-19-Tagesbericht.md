---
layout: default
title: 2023-01-19 Tagesbericht
category: Tagesberichte
---

![2023-01-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, niedriger als in der Vorwoche, der R(t) Wert ist wieder unter 1. Zweite zeigt Krankenhausbetten, deren Zahl jetzt sinkt. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2023/01/20230119.png)

Das RKI meldet eine Inzidenz von 59,3 für München. 135 neue, 42 Nachmeldungen, davon 39 für vorgestern. Die gestern genannte Inzidenz von 64,1 steigt dadurch auf 66.9. R(t) fällt wieder unter 1.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 11.01. | 80,9 -> 85,0 |
| 12.01. | 82,9 -> 87,2 |
| 13.01. | 93,2 -> 95,0 |
| 14.01. | 93,2 -> 95,0 |
| 15.01. | 93,2 -> 94,9 |
| 16.01. | 71,7 -> 76,8 |
| 17.01. | 64,1 -> 66,9 |

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 22. Damit insgesamt schon 45 COVID-Tote bis jetzt in 2023, in 2022 waren es 790, 2021 sogar 1.050, 2020 waren es 579.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum vortag in %.

| Kreis | 19.01. | 18.01. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 63,0 | 70,8 | -11,0% |
| LK Ebersberg | 68,5 | 70,6 | -3,0% |
| LK Freising | 58,0 | 57,4 | +1,0% |
| LK Fürstenfeldbruck | 75,9 | 75,0 | +1,2% |
| LK München | 68,0 | 71,7 | -5,2% |
| LK Starnberg | 71,7 | 76,1 | -5,8% |

Fast überall sinkende Inzidenz.

## Reproduktionswert R(t) für München

Unser R(t) ist nach 4 Tagen über 1 heute wieder unter der Grenze von 1 gesunken und liegt bei 0,95.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.12. aktualisiert, danach erfolgte keine Neuberechnung nehr. Ob es nochmal neue Werte geben wird? Wir sind da eher skeptisch. Eine Erklärung vom StaBLab gab es bisher nicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.01. | 0,87 | --- |
| 13.01. | 0,99 | --- |
| 14.01. | 1,05 | --- |
| 15.01. | 1,12 | --- |
| 16.01. | 1,10 | --- |
| 17.01. | 1,03 | --- |
| 18.01. | 0,95 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute wieder: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 19 von 334 auf 315. IMC steigt um 1 von gestern 6 auf heute 7. Intensiv steigt um 2 von gestern 30 auf heute 32. Damit insgesamt 354 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 315 | -19 | 
| IMC | 7 | +1 | 
| Intensiv | 32 | +2 |
| SUMME | 354 | -16 |

Damit sind 17 Betten **weniger** belegt als im Vergleich mit der Vorwoche.

## Impfungen

Da die Stadt München keine Impfzahlen mehr veröffentlicht und auch bayernweit seit Monaten die Impfquoten nahezu gleich bleiben, werden wir die tägliche Angabe der Impfzahlen einstellen. Im Wochenbericht am Montag gibt es dann Angaben zu Änderungen gegenüber der Vorwoche.

## Testzahlen

Heute gab es endlich auch wieder das wöchentliche Update der Testzahlen. Laut LGL waren wir in der letzten Woche bayernweit bei 11.929 Tests/Tag mit einer Positivrate von 10,63%. Gegenüber den Zahlen von letzter Woche (9.917 Tests/Tag mit 13,89% Positivrate) also etwas mehr Tests bei sinkender Positivrate.

11.929 Tests/Tag ist vergleichbar mit den letzten Dezemberwochen udn weit entfernt vom Maximum. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe wohl genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. 

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit 11.929 ziemlich niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 10,63%.](/images/2023/01/20230119Test.png)