---
layout: default
title: 2023-07-27 Tagesbericht
category: Tagesberichte
---

![2023-07-27 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, heute wurden Nachmeldungen für 5 Tage geliefert, insgesamt 42. Zweite zeigt R(t) Wert für München, der auf 1,30 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/07/20230727.png)

Das RKI meldet eine Inzidenz von 3,2 für München. 4 Neue, 43 Nachmeldungen, 1 Korrektur. Die gestern genannte Inzidenz von 0,8 springt auf 3,7(!). Dunkelziffer Faktor 11-26. R(t) springt auf 1,30. 1 neuer Todesfall, 7 Tage Summe der Todesfälle somit 1. Meldeverzögerungen bei der Stadt.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 19.07. | 1,3 &rarr; 2,3 |
| 20.07. | 2,2 &rarr; 3,4 |
| 21.07. | 1,8 &rarr; 3,7 |
| 22.07. | 1,8 &rarr; 3,7 |
| 23.07. | 1,8 &rarr; 3,7 |
| 24.07. | 1,1 &rarr; 3,6 |
| 25.07. | 0,8 &rarr; 3,7 |

Durch die große Zahl an Nachmeldungen, verschieben sich viele Zahlen. Es gab eine kleine Welle, mit R(t) Werten fast von 1,5, die man erst jetzt im Nachhinein erkennen kann. Da muss es bei der Stadt und dem GSR (Gesundheitsreferat) also wirklich kräftige Probleme gegeben haben.

Ähnliche "glückliche Problemchen" mit Meldeverzögerungen hatten wir in den Vorjahren ein paar mal, häufig, ganz zufällig, vor und während großer Veranstaltung wie IAA oder der Wiesn. Mal schauen, ob es eine Erklärung seitens der Stadt gibt. Wir hatten da vorgestern schon nachgefragt.

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 11 |
| ITS basiert für München | 11 |
| ITS basiert für Bayern | 26 |

## COVID-positive Todesfälle

Es wurde ein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle geht somit auf 1. Damit insgesamt bis jetzt 230 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 1,30 nachdem die Nachmeldungen eine Spitze von 1,47 am 23.07. offenbaren.

| Datum | R(t) |
|---|---:|
| 20.07. | 1,08 |
| 21.07. | 1,21 |
| 22.07. | 1,35 |
| 23.07. | 1,47 |
| 24.07. | 1,41 |
| 25.07. | 1,37 |
| 26.07. | 1,30 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München sinkt im Vergleich zu gestern mit 2 auf heute 1 (-1). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Insgesamt somit 2 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 1 | -1 | 
| Intensiv, beatmet | 1 | +/-0 | 
| SUMME | 2 | -1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung steigt von vorgestern 34 auf gestern 42.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern mit 9 auf heute 7 (-2). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern mit 7 auf heute 8 (+1). Insgesamt somit 15 Intensivbetten belegt (-1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 7 | -2 |
| Intensiv, beatmet | 8 | +1 |
| SUMME | 15 | -1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-07-20 mit Werten bis 2023-07-19**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte sind seit dem 07. Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Auch wenn aktuell eine steigende Tendenz gemeldet wird, relativ betrachtet bleiben die Werte trotzdem sehr niedrig und sind weit von vorherigen Spitzen entfernt.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/07/20230721A.png)

## Varianten München

**Letzte Aktualisierung: 2023-07-11 mit Werten bis 2023-06-19**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 4 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-06-19  wieder auf einem leidlich aktuellen Stand.

Es gibt kaum noch Varianten. Auf Platz 1 finden sich die Rekombinanten mit jetzt 66,62%. Dann kommt nur noch Omikron XBB.1.5 mit 32,94% und das war es dann auch schon. Andere Varianten wie Arcturus (XBB.1.16) tauchen gar nicht mehr auf.

| KW | 21 | 22 | 23 | 24 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,14% | 0,24% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 22,90% | 8,90% | 54,80% | 32,94% |
| Omikron XBB.1.16 | 5,06% | 10,43% | 5,97% | 0,00% |
| Omikron BA.4 | 0,00% | 5,89% | 0,00% | 0,00% |
| Omikron BA.5 | 20,63% | 34,49% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Rekombinante Linien | 50,54% | 39,37% | 38,86% | 66,62% |
| Andere | 0,73% | 0,68% | 0,37% | 0,44% |

![VoC München](/images/2023/07/20230711V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-07-11**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 11.07.2023.

In Bayern wurden im Zeitraum zwischen dem 14. Juni 2023 und dem 11. Juli 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-06-13 | 2023-07-11 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.913 | 9.991.920 | +7 | 75,22% (+/-0%) |
| 2 | 9.901.619 | 9.901.631 | +12 | 75,14% (+/-0%) |
| 3 | 7.808.672 | 7.808.698 | +26 | 59,26% (+/-0%) |
| 4 | 1.584.603 | 1.584.729 | +126 | 12,03% (+/-0%) |
| 5 | 113.151 | 113.267 | +116 | 0,86% (+/-0%) |
| 6 | 5.597 | 5.626 | +29 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 316 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es noch 703 Impfungen insgesamt. 