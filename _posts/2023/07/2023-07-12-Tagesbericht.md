---
layout: default
title: 2023-07-12 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-07-12 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, wieder etwas fallende Zahlen. Zweite zeigt R(t) Wert für München, der auf 1,02 sinkt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/07/20230712.png)

Das RKI meldet eine Inzidenz von 2,0 für München. 3 Neue, keine Nachmeldungen oder Korrekturen. Die gestern genannte Inzidenz von 2,3 bleibt daher unverändert. Dunkelziffer Faktor 10-61. R(t) sinkt wieder auf 1,02. 7 Tage Summe der Todesfälle 0.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 04.07. | 2,6 &rarr; 2,7 |
| 05.07. | 2,3 &rarr; 2,3 |
| 06.07. | 2,6 &rarr; 2,6 |
| 07.07. | 2,6 &rarr; 2,8 |
| 08.07. | 2,6 &rarr; 2,7 |
| 09.07. | 2,6 &rarr; 2,7 |
| 10.07. | 2,3 &rarr; 2,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 29 |
| ITS basiert für Bayern | 61 |

## COVID-positive Todesfälle

Es wurde auch heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle bleibt auf 0. Damit insgesamt bis jetzt 227 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579. Tag 21 ohne Todesfall.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) sinkt auf 1,02. Somit nur noch knapp exponenziell.

| Datum | R(t) |
|---|---:|
| 05.07. | 1,01 |
| 06.07. | 1,04 |
| 07.07. | 1,10 |
| 08.07. | 1,09 |
| 09.07. | 1,13 |
| 10.07. | 1,10 |
| 11.07. | 1,02 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Insgesamt somit 2 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 1 | +/-0 | 
| Intensiv, beatmet | 1 | +/-0 | 
| SUMME | 2 | +/-0 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung sinkt im vergleich zu vorgestern von 35 auf gestern 30.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern sinkt im Vergleich zu gestern von 7 auf heute 6 (-1). Die Zahl der invasiv Beatmeten sinkt im Vergleich zu gestern von 9 auf heute 8 (-1). Insgesamt somit 14 Intensivbetten belegt (-2).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 6 | -1 |
| Intensiv, beatmet | 8 | -1 |
| SUMME | 14 | -2 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-07-07 mit Werten bis 2023-07-05**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte sind seit dem 07. Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Komischwerweise steigt der R(t) Wert aber seit einigen Tagen beständig. Wir vermuten, daß die Sommerferien hier eine Rolle spielen.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/07/20230707A.png)

## Varianten München

**Letzte Aktualisierung: 2023-07-11 mit Werten bis 2023-06-19**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden heute aktualisiert, und 4 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-06-19  wieder auf einem leidlich aktuellen Stand.

Es gibt kaum noch Varianten. Auf Platz 1 finden sich die Rekombinanten mit jetzt 66,62%. Dann kommt nur noch Omikron XBB.1.5 mit 32,94% und das war es dann auch schon. Andere Varianten wie Arcturus (XBB.1.16) tauchen gar nicht mehr auf.

| KW | 21 | 22 | 23 | 24 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,14% | 0,24% | 0,00% | 0,00% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 22,90% | 8,90% | 54,80% | 32,94% |
| Omikron XBB.1.16 | 5,06% | 10,43% | 5,97% | 0,00% |
| Omikron BA.4 | 0,00% | 5,89% | 0,00% | 0,00% |
| Omikron BA.5 | 20,63% | 34,49% | 0,00% | 0,00% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Rekombinante Linien | 50,54% | 39,37% | 38,86% | 66,62% |
| Andere | 0,73% | 0,68% | 0,37% | 0,44% |

![VoC München](/images/2023/07/20230711V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-07-11**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 11.07.2023.

In Bayern wurden im Zeitraum zwischen dem 14. Juni 2023 und dem 11. Juli 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-06-13 | 2023-07-11 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.913 | 9.991.920 | +7 | 75,22% (+/-0%) |
| 2 | 9.901.619 | 9.901.631 | +12 | 75,14% (+/-0%) |
| 3 | 7.808.672 | 7.808.698 | +26 | 59,26% (+/-0%) |
| 4 | 1.584.603 | 1.584.729 | +126 | 12,03% (+/-0%) |
| 5 | 113.151 | 113.267 | +116 | 0,86% (+/-0%) |
| 6 | 5.597 | 5.626 | +29 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 316 Impfungen in den letzten 28 Tagen in ganz Bayern. Kaum der Rede wert. Leider. Im Vormonat waren es noch 703 Impfungen insgesamt. 