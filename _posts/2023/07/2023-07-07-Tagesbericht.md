---
layout: default
title: 2023-07-07 Tagesbericht
category: Tagesberichte
# comments:
#   host: muenchen.social
#   username: CoronaMUC
#   id: 110221238757839670
---

![2023-07-07 Dashboard mit den Zahlen für München. Drei Graphen. Erste zeigt Neu- und Nachmeldungen, in Summe eher steigend. Zweite zeigt R(t) Wert für München, der auf 1,04 steigt. Dritte zeigt Inzidenz, die zwar weiter weit unter 50 liegt aber wegen mangelnder PCR Tests wenig aussagekräftig ist.](/images/2023/07/20230707.png)

Das RKI meldet eine Inzidenz von 2,6 für München. 8 Neue, keine Nachmeldungen. Dadurch bleibt die gestern genannte Inzidenz von 2,3 gleich. Dunkelziffer Faktor 10-56. R(t) steigt weiter auf 1,04. 7 Tage Summe der Todesfälle bleibt 0. Leicht steigende Tendenz.
<!--more-->

## Inzidenzentwicklung

Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet &rarr; aktualisiert):

| Datum | Änderung |
|---:| :---: |
| 29.06. | 2,2 &rarr; 2,3 |
| 30.06. | 2,0 &rarr; 2,1 |
| 01.07. | 2,0 &rarr; 2,2 |
| 02.07. | 2,0 &rarr; 2,2 |
| 03.07. | 2,3 &rarr; 2,4 |
| 04.07. | 2,6 &rarr; 2,7 |
| 05.07. | 2,3 &rarr; 2,3 |

## Dunkelziffer

Aus einer angenommenen IFR (Infection Fatality Rate) von 0,14% bei einer zu 80% geimpften/genesenen Bevölkerung, können wir eine Dunkelziffer für München als Faktor grob abschätzen.

Basierend auf der ITS Belegung in den Krankenhäusern und dem Vergleich mit der jeweiligen Inzidenz von vor einem Jahr können wir eine zweite Dunkelziffer, diesmal für München und Bayern, als Faktor abschätzen. Aufgrund der kleinen Werte für München schwankt diese ITS-Dunkelziffer für München aber ziemlich heftig.

Die Wahrheit wird wohl irgendwo dazwischen oder sogar darüber liegen. Dies sind sehr grobe Abschätzungen, keine Modellrechnungen. Wir finden es trotzdem wichtig, diese Faktoren anzugeben.

| Berechnungsart | Geschätzter Faktor |
| --- | :---: |
| IFR basiert für München | 10 |
| ITS basiert für München | 82 |
| ITS basiert für Bayern | 56 |

## COVID-positive Todesfälle

Es wurde auch heute kein neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle bleibt auf 0. Damit insgesamt bis jetzt 227 COVID-Tote in 2023, in 2022 waren es insgesamt 790, 2021 sogar 1.050, 2020 waren es 579. Tag 17 ohne Todesfall.

## Reproduktionswert R(t) für München

Unser 7 Tage R(t) steigt weiter auf 1,00. Somit knapp exponenziell.

| Datum | R(t) |
|---|---:|
| 30.06. | 0,86 |
| 01.07. | 0,91 |
| 02.07. | 0,94 |
| 03.07. | 0,95 |
| 04.07. | 1,00 |
| 05.07. | 1,01 |
| 06.07. | 1,04 |

## Krankenhausbelegung München &mdash; Intensiv

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in München bleibt im Vergleich zu gestern mit 4 gleich (+/-0). Die Zahl der invasiv Beatmeten bleibt im Vergleich zu gestern mit 1 gleich (+/-0). Insgesamt somit 5 Intensivbetten belegt (+/-0).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 4 | +1 | 
| Intensiv, beatmet | 1 | +/-0 | 
| SUMME | 5 | +1 |

## Krankenhausbelegung Bayern &mdash; Intensiv

Die 7 Tage Hospitalisierung bleibt mit 37 gleich.

Die Zahl der belegten Intensivbetten (ohne invasiv beatmet) in Bayern bleibt im Vergleich zu gestern mit 9 gleich (+/-0). Die Zahl der invasiv Beatmeten steigt im Vergleich zu gestern von 4 auf heute 5 (+1). Insgesamt somit 14 Intensivbetten belegt (+1).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Intensiv | 9 | +2 |
| Intensiv, beatmet | 5 | +1 |
| SUMME | 14 | +1 |

## Abwasserbelastung München

**Letzte Aktualisierung: 2023-07-07 mit Werten bis 2023-07-05**

Die Abwasserbelastung hatte eine kräftige Spitze am 04.06., fiel danach aber schnell wieder ab. Könnte mit der Outdoor ISPO vom 3.-5. Juni und Pfingstferienbeginn zusammenhängen. Überraschenderweise wurden die Zahlen am 16.06. dann nach der Veröffentlichung nochmal korrigiert und der auffällig hohe Wert vom 04.06 wurde in der Tabelle schlicht gelöscht, ohne Begründung oder Kommentar. Wir lassen den Wert in unseren Daten stehen bis es eine Erklärung für diesen Wert gibt.

Die Werte sind seit dem 07. Juni stabil niedrig um 5. Das scheint dann wohl die endemische Grundbelastung zu sein, mit der wir momentan leben müssen. Komischwerweise steigt der R(t) Wert aber seit einigen Tagen beständig. Wir vermuten, daß die Sommerferien hier eine Rolle spielen.

Wir erkennen auch ein Muster in den Daten. Es wird wohl zweimal pro Woche berechnet, jeweils für Sonntag und für Dienstag. Und dann dauert es noch einmal ca. 4 Tage bis die Daten veröffentlicht werden. Manchmal aber auch länger.

Und weil die Frage häufiger kam &mdash; die Niederschlagsmenge wird bei der Auswertung berücksichtigt, beeinflusst die Werte also nicht nennenswert. Es wird eine relative Belastung berechnet und veröffentlicht, nicht die absolute Menge der identifizierten Genfragmente.

![Abwasserwerte München](/images/2023/07/202300707A.png)

## Varianten München

**Letzte Aktualisierung: 2023-06-20 mit Werten bis 2023-05-15**

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Dazu gehört seit Kurzem auch die [Abwasseruntersuchung](https://www.bay-voc.lmu.de/abwassermonitoring). Damit haben wir Daten zur Varientenverteilung in München, auch wenn die Daten ein wenig hinterherhinken.

Die Daten für München wurden am 20. Juni zuletzt aktualisiert, und 2 Wochen wurden nachgeliefert. Somit sind wir, mit Daten bis 2023-05-15  wieder auf einem leidlich aktuellen Stand.

Es gibt keine dominante Variante. Auf Platz 1 finden sich die Rekombinanten mt 31,45%. Dann kommt Omikron BA.5 mit 24,89% und XBB.1.5 mit 19,15% auf Platz 3. XBB.1.16 (Arcturus) ist nicht mehr nachweisbar im Münchner Abwasser.

| KW | 17 | 18 | 19 | 20 |
| :--- | ---: | ---: | ---: | ---: |
| Omikron BA.2 | 0,00% | 2,16% | 0,16% | 0,29% |
| Omikron BA.2.75 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BN.1 | 0,27% | 0,00% | 0,00% | 0,00% |
| Omikron XBB.1.5 | 9,67% | 26,81% | 5,98% | 19,15% |
| Omikron XBB.1.16 | 0,00% | 4,63% | 0,00% | 0,00% |
| Omikron BA.4 | 0,00% | 0,00% | 0,00% | 8,84% |
| Omikron BA.5 | 38,68% | 39,83% | 39,73% | 24,89% |
| Omikron BF.7 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron BQ.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Omikron CH.1.1 | 0,00% | 0,00% | 0,00% | 0,00% |
| Rekombinante Linien | 50,87% | 25,96% | 53,27% | 31,45% |
| Andere | 0,00% | 0,00% | 0,00% | 14,58% |

![VoC München](/images/2023/06/20230621V.png)

## Impfungen Bayern

**Letzte Aktualisierung: 2023-06-13 mit Werten bis 2023-06-13**

Jeden zweiten Dienstag im Monat veröffentlicht das RKI die [aktualisierten Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html). Wir werden die Aktualisierungen dann auch jeweils im Tagesbericht nennen. Hier also die Zahlen vom 13.06.2023.

In Bayern wurden im Zeitraum zwischen dem 09. Mai 2023 und dem 13. Juni 2023 folgende SarsCOV2 Impfungen registriert:

| Impfung | 2023-05-09 | 2023-06-13 | Differenz | Impfquote |
| ---: | ---: | ---: | ---: | ---: | 
| 1 | 9.911.852 | 9.911.913 | +61 | 75,22% (+/-0%) |
| 2 | 9.901.535 | 9.901.619 | +84 | 75,14% (+/-0%) |
| 3 | 7.808.616 | 7.808.672 | +56 | 59,26% (+/-0%) |
| 4 | 1.584.328 | 1.584.603 | +275 | 12,03% (+0,01%) |
| 5 | 112.947 | 113.151 | +204 | 0,86% (+/-0%) |
| 6 | 5.574 | 5.597 | +23 | 0,04% (+/-0%) |

Das waren insgesamt gerade mal 703 Impfungen in den letzten 36 Tagen. Kaum der Rede wert. Leider.