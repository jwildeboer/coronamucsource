---
layout: default
title: 2022-11-07 Wochenbericht KW44
category: Wochenberichte
---

![2022-11-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen noch, aber es könnte kippen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221107.png)

In der letzten Woche gab es insgesamt nur 1.515 neue COVID Fälle in München, der Feiertag am Diesntag spielt da aber rein. Das sind satte 50,2% weniger als in der Vorwoche (3.041). Leider aber auch 19 Tote mit COVID Nachweis in den letzten 7 Tagen, wie in der Vorwoche.
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 3.041 stetig zurück auf 1.515, mit dem Somderfaktor Feiertag am Dienstag. Die Inzidenz halbierte sich wieder nahezu, wie schon in der letzten Woche. 102,6 Punkte weniger. Eine erstaunliche Leistung. München gehört jetzt bundesweit zu den Top 5 der Kreise mit der **niedrigesten** Inzidenz. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 30.10. | 0 | 3.041 | 204,4 |
| 31.10. | 347 | 2.670 | 179,5 |
| 01.11. | 0 | 1.965 | 132,1 |
| 02.11. | 576 | 2.009 | 135,0 |
| 03.11. | 330 | 1.866 | 125,4 |
| 04.11. | 262 | 1.824 | 122,6 |
| 05.11. | 0 | 1.515 | 101,8 |

## Reproduktionswert R(t) für München

Unser R(t) war in der letzten Woche konstant um 0,70, bleibt somit klar unter 1, durch die (noch fehlenden) Wochenendzahlen ist das aber wohl noch ein wenig nach unten verzerrt.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 02.11.) liegt bei 0,58.

| Datum | R(t) | LMU |
|---|---:|---:|
| 31.10. | 0,72 | 0,56 |
| 01.11. | 0,70 | 0,56 |
| 02.11. | 0,70 | 0,58 |
| 03.11. | 0,70 | --- |
| 04.11. | 0,70 | --- |
| 05.11. | 0,70 | --- |
| 06.11. | 0,69 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder weniger Normal- und IMC-Betten aber Intensiv bleibt unverändert. Dennoch - klarer Rückgang bei den Krankenhauszahlen. An jedem Tag deitlich weniger als in der Vorwoche.

Im Vergleich zum Vorjahrimmer immer noch mehr.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 31.10. | 360 | 13 | 36 | 409 | -72 | 174 |
| 01.11. | 351 | 15 | 37 | 403 | -83 | 199 |
| 02.11. | 328 | 12 | 34 | 374 | -78 | 210 |
| 03.11. | 312 | 13 | 29 | 354 | -86 | 218 |
| 04.11. | 306 | 11 | 29 | 346 | -86 | 231 |
| 05.11. | 300 | 10 | 29 | 339 | -84 | 261 |
| 06.11. | 294 | 9 | 29 | 332 | -83 | 274 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es langsam besser aus. Seit dem 22.09. waren wir klar über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche, jetzt seit 2 Tagen etwas drunter, auch wenn noch die Nachmeldungen fehlen.

Bei den Intensivbetten sinkt die Sume von letzte Woche 255 auf jetzt 195.

Im Vergleich zum Vorjahr bessere Zahlen. Im letzten Jahr lief um diese Zeit die große Herbstwelle, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 31.10. | 1.580 | 1.306 |
| 01.11. | 1.248 | 1.284 |
| 02.11. | 1.297 | 1.313 |
| 03.11. | 1.202 | 1.444 |
| 04.11. | 1.050 | 1.538 |
| 05.11. | 995 | 1.661 |
| 06.11. | 950 | 1.691 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,98% | 75,18% |
| 2. | 71,45% | 75,10% |
| 3. | 48,24% | 59,08% |
| 4. | 9,82% | 9,76% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/11/KW43Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 17.796 Tests/Tag mit einer Positivrate von 25,27%. Gegenüber den Zahlen von letzter Woche (22.897 Tests/Tag mit 33,78% Positivrate) also klar weniger Tests, gepaart mit sinkdender Positivrate.

Aber 17.796 Tests/Tag sind deutlich weniger als 20% der vorhandenen Testkapazität. Das beobachtete Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für (PCR-Pool-)Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 21.416 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 32,42%.](/images/2022/11/20221103Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. BA.5 herrscht zwar noch vor, hat aber kräftig Anteil verloren. Weniger an BQ.1, deutlich mehr an BF.7. Wobei in der KW44 bisher nur 5 Proben ausgewertet wurden.

| Variante | KW41 | KW42 | KW 43 | KW 44 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 2.1% | 2.6% | 3,2% | 0,0%|
| Omikron BA.2.75 | 1,2% | 0.7% | 0,3% | 0,0% |
| Omikron BA.4 | 1.6% | 2.4% | 1,1 | 0,0% |
| Omikron BA.5 | 73.1% | 68,7% | 65,1% | 60,0% |
| Omikron BF.7 | 15.2% | 17,3% | 18,8% | 40,0% |
| Omikron BQ.1 | 6,3% | 6,9% | 10,5% | 0,0% |
| Andere | 0.5% | 1.5% | 1,1% | 0,0% |
| Sequenzierungen | 1.291 | 549 | 372 | 5 | 

![VoC](/images/2022/11/KW44Var.png)