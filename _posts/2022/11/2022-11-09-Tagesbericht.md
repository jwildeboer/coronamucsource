---
layout: default
title: 2022-11-09 Tagesbericht
category: Tagesberichte
---

![2022-11-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, eine Trendwende ist aber noch nicht zu erkennen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.Heute Sondereffekt dürch Feiertag letzter Woche.](/images/2022/11/20221109.png)

Das RKI meldet eine Inzidenz von 144,5 für München. 282 Neue, 135 Nachmeldungen, davon 130  für vorgestern. Die Inzidenz für gestern erhöht sich von 116,6 auf 125,5. Tag 8 unter Inzidenz 150, aber nur noch knapp.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 31. Damit insgesamt 705 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 22.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 01.11. | 125,9 -> 132,2 |
| 02.11. | 128,0 -> 135,4 |
| 03.11. | 118,7 -> 126,3 |
| 04.11. | 122,6 -> 129,3 |
| 05.11. | 101,8 -> 108,6 |
| 06.11. | 101,8 -> 108,6 |
| 07.11. | 116,6 -> 125,5 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 09.11. | 08.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 209,7 | 167,3 | +25,3% |
| LK Ebersberg | 199,9 |  172,2 | +16,1% |
| LK Freising | 120,9 | 119,2 | +1,4% |
| LK Fürstenfeldbruck | 200,8 | 167,4 | +20,0% |
| LK München | 153,2 | 124,3 | +23,3% |
| LK Starnberg | 160,9 | 125,0 | +28,7% |

Der Rückschlagseffekt vom Feiertag. 

## Reproduktionswert R(t) für München

Unser R(t) steigt wieder auf 0,79 - das ist ein Nachzüglereffekt vom Feiertag in der letzten Woche.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 08.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,66 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.11. | 0,70 | 0,67 |
| 03.11. | 0,70 | 0,70 |
| 04.11. | 0,71 | 0,69 |
| 05.11. | 0,71 | 0,67 |
| 06.11. | 0,70 | 0,65 |
| 07.11. | 0,72 | 0,64 |
| 08.11. | 0,79 | 0,66 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und ist heute zum ersten mal seit dem 27.09. wieder unter 300 insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 9 Betten auf jetzt 264. IMC steigt von 9 auf 12. Intensivbetten sinkt von gestern 28 auf jetzt 23. Damit insgesamt 299 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 264 | -9 | 
| IMC | 12 | +3 | 
| Intensiv | 23 | -5 |
| SUMME | 299 | -11 |

Damit 104 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +16
2. +22
3. +145
4. +3.225

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,26%, Bayern 59,09%.
