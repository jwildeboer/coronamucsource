---
layout: default
title: 2022-11-29 Tagesbericht
category: Tagesberichte
---

![2022-11-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht weiter über 1. Zweite zeigt Krankenbetten, die heute etwas steigen. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert.](/images/2022/11/20221129.png)

Das RKI meldet eine Inzidenz von 97,9 für München. 427 Neue, 40 Nachmeldungen, davon 18 für Samstag und 17 für Freitag. Durch Nachmeldungen erhöhen sich die Inzidenzen von Sonntag mit 99,8, Samstag mit 99,8 und Freitag mit 101,0 auf identische 102,4.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 736 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 32.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 21.11. | 86,4 -> 96,3 |
| 22.11. | 94,1 -> 101,7 |
| 23.11. | 92,7 -> 103,4 |
| 24.11. | 98,9 -> 106,0 |
| 25.11. | 101,0 -> 102,4 |
| 26.11. | 99,8 -> 102,4 |
| 27.11. | 99,8 -> 102,4 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu **Samstag** in %.

| Kreis | 29.11. | 26.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 124,8 | 120,9 | +3,2% |
| LK Ebersberg | 92,7 | 103,1 | -10,1% |
| LK Freising | 71,2 | 77,3 | -7,9% |
| LK Fürstenfeldbruck | 103,4 | 98,8 | +4,7% |
| LK München | 82,0 | 97,8 | -16,2% |
| LK Starnberg | 88,5 | 92,9 | -4,7% |

Leicht fallende Tendenz insgesamt.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 5 Tagen über 1 und damit über der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,95 | 0,92 |
| 24.11. | 1,01 | --- |
| 25.11. | 1,04 | --- |
| 26.11. | 1,06 | --- |
| 27.11. | 1,06 | --- |
| 28.11. | 1,04 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 12 von 182 auf 194. IMC sinkt von 10 auf 8. Intensivbetten steigt um 1 von 16 auf 17. Damit insgesamt 219 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 194 | +12 | 
| IMC | 8 | -2 | 
| Intensiv | 17 | +1 |
| SUMME | 219 | +11 |

Damit nur noch 3 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +5 (72,99%)
2. +6 (71,47%)
3. +15 (48,31%)
4. +95 (11,07%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,31%, Bayern 59,16%.

Das RKI gibt seit gestern auch Zahlen zur 5ten Impfung (also 3. Boosterimpfung) auf Landesebene an. Aktuell 78.454 5. Impfungen in Bayern, das entpricht einer Impfquote von 0,60%. Ob und wann die Stadt München diese Zahl nennt ist uns nicht bekannt.
