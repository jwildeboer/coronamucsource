---
layout: default
title: 2022-11-03 Tagesbericht
category: Tagesberichte
---

![2022-11-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die auch kontinuierlich weiter sinkt.](/images/2022/11/20221103.png)

Das RKI meldet eine Inzidenz von 128,0 für München. 478 Neue, 103 Nachmeldungen, davon 0 für gestern und 80 für Montag. Die gestern genannte Inzidenz von 125,9 erhöht sich durch Nachmeldungen auf 131,6. Tag 3 unter Inzidenz 200.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 15. Damit insgesamt 674 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle auch 18.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 26.10. | 242,9 -> 253,5 |
| 27.10. | 226,1 -> 236,9 |
| 28.10. | 208,4 -> 212,9 |
| 29.10. | 179,3 -> 204,2 |
| 30.10. | 179,3 -> 204,2 |
| 31.10. | 173,2 -> 178,9 |
| 01.11. | 125,9 -> 131,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Dienstag** in %.

| Kreis | 03.11 | 01.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 136,4 | 224,5 | -39,2% |
| LK Ebersberg | 170,2 | 215,1 | -20,9% |
| LK Freising | 123,7 | 180,5 | -31,5% |
| LK Fürstenfeldbruck | 74,1 | 190,3 | -61,1% |
| LK München | 138,4 | 175,5 | -21,1% |
| LK Starnberg | 135,3 | 185,7 | -27,1% |

Wobei Fürstenfeldbruck in den letzten 5 Tagen nur 2 Fälle gemeldet hat. Da ist wohl was kaputt.

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 5 Tagen wieder leicht auf jetzt 0,71 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 27.10. | 0,67 | 0,78 |
| 28.10. | 0,69 | 0,75 |
| 29.10. | 0,71 | 0,69 |
| 30.10. | 0,71 | 0,65 |
| 31.10. | 0,71 | 0,64 |
| 01.11. | 0,70 | 0,65 |
| 02.11. | 0,69 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter, jetzt zum ersten Mal seit dem 29.09. wieder unter 400 insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 23 Betten auf jetzt 328. IMC sinkt von gestern 15 auf jetzt 12. Intensivbetten sinkt von gestern 37 auf jetzt 34. Damit insgesamt 374 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 328 | -23 | 
| IMC | 12 | -3 | 
| Intensiv | 34 | -3 |
| SUMME | 374 | -29 |

Damit 78 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +3
2. +3
3. +14
4. +151

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,24%, Bayern 59,06%.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur noch 17.798 Tests/Tag mit einer Positivrate von 25,27%. Gegenüber den Zahlen von letzter Woche (22.897 Tests/Tag mit 33,78% Positivrate) also wieder weniger Tests, gepaart mit sinkender Positivrate.

17.798 Tests/Tag ist klar weniger als 20% der vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 17.798 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 25,27%.](/images/2022/11/20221103Test.png)
