---
layout: default
title: 2022-11-01 Tagesbericht
category: Tagesberichte
---

![2022-11-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken, wegen Feiertag aber heute keine Aktualisierung. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und weiter sinkt.](/images/2022/11/20221101.png)

Das RKI meldet eine Inzidenz von 173,2 für München. 261 Neue, 369 Nachmeldungen, davon 307 für Samstag und 41 für Freitag. Die Inzidenzen korrigieren sich für Montag und Sonntag von 179,3 auf 203,7, Samstag von 208,4 auf 212,1. Tag 1 unter Inzidenz 200.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 18. Damit insgesamt 674 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle auch 18.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 24.10. | 302,6 -> 319,8 |
| 25.10. | 269,0 -> 281,6 |
| 26.10. | 242,9 -> 252,7 |
| 27.10. | 226,1 -> 236,1 |
| 28.10. | 208,4 -> 212,1 |
| 29.10. | 179,3 -> 203,7 |
| 30.10. | 179,3 -> 203,7 |

Da heute Feiertag ist und somit keine neuen Fälle an das RKI (nach-)gemeldet werden, können wir schon jetzt die Inzidenz für Mittwoch berechnen:

| Mittwoch | 125,9 |

Das diese Inzidenz an der Realität vorbei geht braucht man hoffentlich nicht extra betonen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 01.11. | 29.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 224,5 | 265,0 | -15,3% |
| LK Ebersberg | 215,1 | 242,8 | -11,4% |
| LK Freising | 180,5 | 235,5 | -23,4% |
| LK Fürstenfeldbruck | 190,3 | 299,7 | -36,5% |
| LK München | 175,5 | 217,8 | -19,4% |
| LK Starnberg | 185,7 | 231,1 | -19,6% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 5 Tagen wieder leicht auf jetzt 0,71 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.10. | 0,61 | 0,53 |
| 26.10. | 0,65 | 0,55 |
| 27.10. | 0,67 | 0,58 |
| 28.10. | 0,69 | 0,59 |
| 29.10. | 0,71 | --- |
| 30.10. | 0,71 | --- |
| 31.10. | 0,71 | --- |

## Krankenhaus

Am Feiertag gibt es keine aktualiserten Krankenhauszahlen der Stadt München.

## Impfungen

Am Feiertag gibt es keine aktualiserten Impfzahlen der Stadt München.
