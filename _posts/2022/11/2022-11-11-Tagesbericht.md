---
layout: default
title: 2022-11-11 Tagesbericht
category: Tagesberichte
---

![2022-11-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, eine Trendwende deutet sich an. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221111.png)

Das RKI meldet eine Inzidenz von 123,1 für München. 166 Neue, 138 Nachmeldungen, davon 127  für vorgestern. Die Inzidenz für gestern erhöht sich von 125,8 auf 134,. Somit wieder Tag 1 unter Inzidenz 150.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 34. Damit insgesamt 710 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 03.11. | 118,7 -> 126,6 |
| 04.11. | 122,6 -> 129,7 |
| 05.11. | 101,8 -> 108,9 |
| 06.11. | 101,8 -> 109,0 |
| 07.11. | 116,6 -> 126,4 |
| 08.11. | 144,5 -> 152,2 |
| 09.11. | 125,8 -> 134,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 11.11 | 10.11 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 153,7 | 185,3 | -17,1% |
| LK Ebersberg | 151,5 | 171,6 | -11,7% |
| LK Freising | 114,3 | 127,0 | -10,0% |
| LK Fürstenfeldbruck | 188,0 | 170,6 | +10,2% |
| LK München | 136,1 | 143,5 | -5,2% |
| LK Starnberg | 122,9 | 132,4 | -7,2% |

Weiterhin uneinheitlich.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,96 und steht damit wieder kurz vor der exponentielle Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 08.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,75 und steigt damit auch wieder an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 04.11. | 0,71 | 0,70 |
| 05.11. | 0,71 | 0,68 |
| 06.11. | 0,71 | 0,66 |
| 07.11. | 0,73 | 0,66 |
| 08.11. | 0,80 | 0,70 |
| 09.11. | 0,87 | 0,75 |
| 10.11. | 0,96 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und bleibt klar unter 300 Betten insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 21 auf jetzt 226. IMC sinkt von 9 auf 7. Intensivbetten steigt von gestern 21 auf jetzt 24. Damit insgesamt 257 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 226 | -21 | 
| IMC | 7 | -2 | 
| Intensiv | 24 | +3 |
| SUMME | 257 | -20 |

Damit 97 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Bei den Viertimpfungen heute ein überraschender Sprung.

1. +6
2. +5
3. +18
4. +2.405

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,26%, Bayern 59,10%.
