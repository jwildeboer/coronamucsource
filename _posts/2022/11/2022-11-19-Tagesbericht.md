---
layout: default
title: 2022-11-19 Tagesbericht
category: Tagesberichte
---

![2022-11-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, und auch der R(t) Wert sinkt wieder. Zweite zeigt Krankenbetten, die weiter klar sinken, aber am Wochenende nicht aktualisiert werden. Dritte zeigt Inzidenz, die langsam sinkt.](/images/2022/11/20221119.png)

Das RKI meldet eine Inzidenz von 89,4 für München. 130 Neue, nur 59 Nachmeldungen, davon 52 für vorgestern. Die gestern genannte Inzidenz von 90,5 erhöht sich auf 94,1. Mit der um Nachmeldungen korrigierten Inzidenz somit Tag 3 unter Inzidenz 100.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 13. Damit insgesamt 728 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 24.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 11.11. | 115,3 -> 120,3 |
| 12.11. | 115,3 -> 122,7 |
| 13.11. | 115,2 -> 122,7 |
| 14.11. | 103,0 -> 111,1 |
| 15.11. | 97,8 -> 101,4 |
| 16.11. | 91,9 -> 95,6 |
| 17.11. | 90,5 -> 94,1 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 19.11. | 18.11 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 113,2 | 124,8 | -9,3% |
| LK Ebersberg | 80,9 | 80,9 | --- |
| LK Freising | 75,1 | 77,8 | -3,5% |
| LK Fürstenfeldbruck | 118,5 | 113,5 | +4,4% |
| LK München | 88,3 | 90,9 | -2,9% |
| LK Starnberg | 85,6 | 84,1 | +1,8% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steht auf 0,82 und damit klar unter der Grenze zum exponenziellem Wachstum von 1,0.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 16.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,91 und gleicht sich unserem R(t) an.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Wir berechnen unser R(t) natürlich weiterhin täglich neu.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.11. | 1,01 | 0,95 |
| 13.11. | 1,02 | 0,93 |
| 14.11. | 1,00 | 0,90 |
| 15.11. | 0,95 | 0,90 |
| 16.11. | 0,89 | 0,91 |
| 17.11. | 0,84 | --- |
| 18.11. | 0,82 | --- |

## Krankenhaus

Am Samstag keine Aktualisierung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Samstag keine Aktualisierung der Impfzahlen durch die Stadt München.
