---
layout: default
title: 2022-11-16 Tagesbericht
category: Tagesberichte
---

![2022-11-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, aber eine Trendwende ist möglich, weil R(t) um 1 pendelt. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die nur noch langsam sinkt.](/images/2022/11/20221116.png)

Das RKI meldet eine Inzidenz von 97,8 für München. 311 Neue, 120 Nachmeldungen, davon 114 für vorgestern. Die gestern genannte Inzidenz von 103,0 erhöht sich auf 110,8. Durch Nachmeldungen ist es sehr wahrscheinlich, daß die heutige Inzidenz sich morgen wieder auf einen Wert über 100 korrigiert.
<!--more-->

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt 721 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 28.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 08.11. | 144,5 -> 152,9 |
| 09.11. | 125,8 -> 136,0 |
| 10.11. | 123,1 -> 130,4 |
| 11.11. | 115,3 -> 120,2 |
| 12.11. | 115,3 -> 122,7 |
| 13.11. | 115,2 -> 122,6 |
| 14.11. | 103,0 -> 110,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 16.11. | 15.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 104,9 | 145,4 | -27,9% |
| LK Ebersberg | 100,3 | 85,1 | +17,9% |
| LK Freising | 83,9 | 90,5 | -7,3% |
| LK Fürstenfeldbruck | 144,1 | 153,3 | -6,0% |
| LK München | 96,3 | 110,1 | -12,5% |
| LK Starnberg | 87,0 | 105,3 | -17,4% |

Ebersberg hat wieder gemeldet.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 1,01 und steht damit knapp über der exponentiellen Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 09.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,75 und steigt damit auch wieder an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.11. | 0,87 | 0,75 |
| 10.11. | 0,97 | 0,81 |
| 11.11. | 1,02 | --- |
| 12.11. | 1,01 | --- |
| 13.11. | 1,02 | --- |
| 14.11. | 1,00 | --- |
| 15.11. | 0,95 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und ist jetzt klar unter 250 Betten insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 12 auf jetzt 203. IMC sinkt von 9 auf 8. Intensivbetten sinkt von gestern 20 auf jetzt 19. Damit insgesamt 230 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 203 | -12 | 
| IMC | 8 | -1 | 
| Intensiv | 19 | -1 |
| SUMME | 230 | -14 |

Damit 69 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +9
2. +16
3. +219
4. +5.176

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,28%, Bayern 59,12%.
