---
layout: default
title: 2022-11-28 Wochenbericht KW47
category: Wochenberichte
---

![2022-11-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche steigen die Zahlen. Zweite zeigt Krankenbetten, die insgesamt stabil bleiben. Dritte zeigt Inzidenz, die langsam steigt, aber ohne Wochenendzahlen nichts eindeutiges.](/images/2022/11/20221128.png)

In der letzten Woche gab es insgesamt nur 1.292 neue COVID Fälle in München. Das sind 29,2% weniger als in der Vorwoche (1.826). Leider aber auch 13 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (23).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe von 1.377 auf 1.485. Die Inzidenz stieg um 7,8%. Seit Dienstag letzter Woche ist die korrigierte Inzidenz über 100.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 20.11. | 0 | 1.377 | 92,6 |
| 21.11. | 494 | 1.432 | 96,3 |
| 22.11. | 325 | 1.512 | 101,6 |
| 23.11. | 271 | 1.535 | 103,2 |
| 24.11. | 272 | 1.572 | 105,7 |
| 25.11. | 123 | 1.503 | 101,0 |
| 26.11. | 0 | 1.485 | 99,8 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche von 0,87 auf jetzt 1,05 und ist damit seit 4 Tagen stabil über 1.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 23.11.) liegt bei 0,92. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen netürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.11. | 0,87 | 0,85 |
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,95 | 0,92 |
| 24.11. | 1,00 | --- |
| 25.11. | 1,04 | --- |
| 26.11. | 1,05 | --- |
| 27.11. | 1,05 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende etwas weniger Normal- und Intensiv-Betten aber IMC einen deutlichen Anstieg. Zum ersten mal seit dem 17.10. auch wieder ansteigende Zahlen im Vergleich zur Vorwoche.

Im Vorjahr schlug um diese Zeit die Herbstwelle zu - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 21.11. | 198 | 3 | 21 | 222 | -22 | 424 |
| 22.11. | 188 | 3 | 21 | 212 | -18 | 435 |
| 23.11. | 184 | 2 | 21 | 207 | -22 | 447 |
| 24.11. | 187 | 7 | 16 | 210 | -6 | 451 |
| 25.11. | 185 | 8 | 16 | 209 | -2 | 458 |
| 26.11. | 183 | 9 | 16 | 208 | +1 | 466 |
| 27.11. | 182 | 10 | 16 | 208 | +4 | 474 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt etwas besser aus. Seit dem 08.11. sind wir auch mit korrigierten Zahlen wieder unter dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten sinkt die Summe von letzte Woche 133 auf jetzt 120.

Im Vergleich zum Vorjahr deutlich bessere Zahlen. Im letzten Jahr lief um diese Zeit die große Herbstwelle, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 21.11. | 990 | 2.753 |
| 22.11. | 989 | 2.776 |
| 23.11. | 994 | 2.743 |
| 24.11. | 1.014 | 2.693 |
| 25.11. | 947 | 2.640 |
| 26.11. | 921 | 2.568 |
| 27.11. | 887 | 2.611 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Das RKI gibt jetzt auch Zahlen für die 5te Impfung an, die Stadt München nch nicht.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,99% | 75,19% |
| 2. | 71,47% | 75,11% |
| 3. | 48,31% | 59,16% |
| 4. | 11,05% | 10,95% |
| 5. | --- | 0,59% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/11/KW47Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 13.445 Tests/Tag mit einer Positivrate von 11,26%. Gegenüber den Zahlen von letzter Woche (15.482 Tests/Tag mit 15,11% Positivrate) also wieder weniger Tests, gepaart mit weiter sinkender Positivrate.

13.445 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.445 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 11,26%.](/images/2022/11/20221124Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. BA.5 herrscht zwar noch vor, hat aber kräftig Anteil verloren. Weniger an BQ.1, eher an BF.7. Wobei in der KW46 bisher nur 51 Proben ausgewertet wurden.

| Variante | KW 43 | KW 44 | KW 45 | KW 46 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 4.1% | 4.1% | 4.8% | 7.8% |
| Omikron BA.2.75 | 0.7% | 0.0% | 0.0% | 2.0% |
| Omikron BA.4 | 1.1% | 4.1% | 1.6% | 7.8% |
| Omikron BA.5 | 65.9% | 55.2% | 48.6% | 45.1% |
| Omikron BF.7 | 17.7% | 20.8% | 23.4% | 17.6% |
| Omikron BQ.1 | 9.8% | 14.9% | 18.2% | 13.7%
| Andere | 0.7% | 0.9% | 3.4% | 5.9% |
| Sequenzierungen | 825 | 221 | 440 | 51 | 

![VoC](/images/2022/11/KW47Var.png)