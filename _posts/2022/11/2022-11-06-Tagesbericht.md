---
layout: default
title: 2022-11-06 Tagesbericht
category: Tagesberichte
---

![2022-11-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen noch, aber es könnte kippen. Zweite zeigt Krankenbetten, die weiter klar sinken. Sonntag keine neuen Zahlen von der Stadt München. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221106.png)

Sonntag, daher keinen neue Meldungen beim RKI. Die gemeldete Inzidenz für München von 101,8 ist daher klar zu niedrig. Tag 5 unter Inzidenz 150.
<!--more-->

Leider hat das RKI heute die Krankenhauszahlen nicht aktualisiert, daher ist die einzig neue Zahl die der mit COVID-positiv getesteten belegten Intensivbetten, die heute 201 beträgt.

Und weil auch heute keine Meldungen an das RKI gehen, können wor die Inzidenzen für Montag schonmal berechnen:

| Montag | 101,8 |

## Umland

Keine neuen Zahlen vorhanden, erst am Dienstag wieder ein Vergleich möglich.

## Reproduktionswert R(t) für München

Unser R(t) stabilisiert sich um 0,70.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 02.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,58 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.10. | 0,71 | 0,57 |
| 31.10. | 0,72 | 0,56 |
| 01.11. | 0,70 | 0,56 |
| 02.11. | 0,70 | 0,58 |
| 03.11. | 0,70 | --- |
| 04.11. | 0,70 | --- |
| 05.11. | 0,70 | --- |

## Krankenhaus

Am Sonntag keine neuen Krankenhauszahlen von der Stadt München.

## Impfungen

Am Sonntag keine neuen Impfzahlen von der Stadt München.
