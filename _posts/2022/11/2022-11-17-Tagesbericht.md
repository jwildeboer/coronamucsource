---
layout: default
title: 2022-11-17 Tagesbericht
category: Tagesberichte
---

![2022-11-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, und auch der R(t) Wert sinkt wieder. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die langsam sinkt.](/images/2022/11/20221117.png)

Das RKI meldet eine Inzidenz von 91,9 für München. 194 Neue, nur 56 Nachmeldungen, davon 48 für vorgestern. Die gestern genannte Inzidenz von 97,8 erhöht sich auf 101,1. Die LMU wird ab sofort nur noch wöchentlich am Mittwoch die R(t)-Wert Berechung veröffentlichen.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt 722 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 28.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 09.11. | 125,8 -> 136,0 |
| 10.11. | 123,1 -> 130,4 |
| 11.11. | 115,3 -> 120,2 |
| 12.11. | 115,3 -> 122,7 |
| 13.11. | 115,2 -> 122,6 |
| 14.11. | 103,0 -> 111,0 |
| 15.11. | 97,8 -> 101,1 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 17.11. | 16.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 115,8 | 104,9 | +10,4% |
| LK Ebersberg | 87,9 | 100,3 | -12,4% |
| LK Freising | 82,3 | 83,9 | -1,9% |
| LK Fürstenfeldbruck | 131,8 | 144,1 | -8,5% |
| LK München | 92,3 | 96,3 | -4,2% |
| LK Starnberg | 73,9 | 87,0 | -15,1% |

Bis auf Dachau überall sinkende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 1,01 und steht damit knapp über der exponentiellen Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 16.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,91 und gleicht sich unserem R(t) an.

Ab heute wird der R(t) Wert der LMU nur noch einmal wöchentlich am Mittwoch aktualisiert. Wir berechnen unser R(t) natürlich weiterhin täglich neu.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.11. | 0,97 | 0,85 |
| 11.11. | 1,02 | 0,92 |
| 12.11. | 1,01 | 0,95 |
| 13.11. | 1,02 | 0,93 |
| 14.11. | 1,00 | 0,90 |
| 15.11. | 0,95 | 0,90 |
| 16.11. | 0,89 | 0,91 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten stabilisiert sich: Gegenüber gestern bleibt die Zahl der belegten Normalbetten gleich mit 203. IMC sinkt von 8 auf 6. Intensivbetten steigt von gestern 19 auf jetzt 20. Damit insgesamt 229 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 203 | +0 | 
| IMC | 6 | -2 | 
| Intensiv | 20 | +1 |
| SUMME | 229 | -1 |

Damit 48 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +4
2. +6
3. +29
4. +296

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,28%, Bayern 59,13%.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 15.482 Tests/Tag mit einer Positivrate von 15,11. Gegenüber den Zahlen von letzter Woche (14.196 Tests/Tag mit 19,83% Positivrate) also wieder etwas mehr Tests, gepaart mit weiter sinkender Positivrate.

15.482 Tests/Tag ist weniger als 20% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.482 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 15,11%.](/images/2022/11/20221117Test.png)
