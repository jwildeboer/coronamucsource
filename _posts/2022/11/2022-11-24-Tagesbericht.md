---
layout: default
title: 2022-11-24 Tagesbericht
category: Tagesberichte
---

![2022-11-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen tendeziell höher, und auch der R(t) Wert steigt wieder. Zweite zeigt Krankenbetten, die heute etwas sinken. Dritte zeigt Inzidenz, die langsam steigt.](/images/2022/11/20221124.png)

Das RKI meldet eine Inzidenz von 92,7 für München. 222 Neue, 108 Nachmeldungen, davon 105 für vorgestern. Die gestern genannte Inzidenz von 94,1 steigt auf 101,2. Somit wieder zurück zu Tag 1 unter Inzidenz 100.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 9. Damit insgesamt 731 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 16.11. | 91,9 -> 95,9 |
| 17.11. | 90,5 -> 94,4 |
| 18.11. | 89,4 -> 93,8 |
| 19.11. | 86,8 -> 92,5 |
| 20.11. | 86,8 -> 92,5 |
| 21.11. | 86,4 -> 96,1 |
| 22.11. | 94,1 -> 101,2 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 24.11. | 23.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,7 | 122,2 | +5,3% |
| LK Ebersberg | 92,7 | 80,9 | +14,6% |
| LK Freising | 69,6 | 59,6 | +16,8% |
| LK Fürstenfeldbruck | 88,3 | 89,7 | -1,6% |
| LK München | 86,9 | 83,2 | +4,4% |
| LK Starnberg | 93,6 | 92,9 | +0,8% |

Fast überall steigende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 4 Tagen und steht heute auf 0,93 und damit weiter knapp unter der Grenze zum exponenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 17.11. | 0,84 | 0,95 |
| 18.11. | 0,83 | 0,91 |
| 19.11. | 0,84 | 0,85 |
| 20.11. | 0,85 | 0,83 |
| 21.11. | 0,87 | 0,85 |
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,93 | 0,92 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 4 von 188 auf 184. IMC geht von 3 auf 2. Intensivbetten bleibt gleich mit 21. Damit insgesamt 207 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 184 | -4 | 
| IMC | 2 | -1 | 
| Intensiv | 21 | +/-0 |
| SUMME | 207 | -5 |

Damit 22 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Heute wieder Nachmeldungen aus den Arztpraxen, insebesondere Viertimpfungen.

1. +15 (72,99%)
2. +4 (71,47%)
3. +13 (48,30%)
4. +182 (11,02%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,30%, Bayern 59,15%.

Das RKI gibt seit heute auch Zahlen zur 5ten Impfung (also 3. Boosterimpfung) auf Landesebene an. Aktuell 74.403 5. Impfungen in Bayern, das entpricht einer Impfquote von 0,56%. Ob und wann die Stadt München diese Zahl nennt ist uns nicht bekannt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 13.445 Tests/Tag mit einer Positivrate von 11,26%. Gegenüber den Zahlen von letzter Woche (15.482 Tests/Tag mit 15,11% Positivrate) also wieder weniger Tests, gepaart mit weiter sinkender Positivrate.

13.445 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.445 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 11,26%.](/images/2022/11/20221124Test.png)
