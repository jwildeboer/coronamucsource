---
layout: default
title: 2022-11-21 Wochenbericht KW46
category: Wochenberichte
---

![2022-11-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221121.png)

In der letzten Woche gab es insgesamt nur 1.292 neue COVID Fälle in München. Das sind 29,2% weniger als in der Vorwoche (1.826). Leider aber auch 13 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (23).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 1.825 stetig zurück auf 1.292. Die Inzidenz verlor weitere 30%. Seit Donnerstag letzter Woche ist die korrigierte Inzidenz unter 100. So niedrig wie nie zuvor in 2022.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 13.11. | 0 | 1.825 | 122,7 |
| 14.11. | 439 | 1.653 | 111,1 |
| 15.11. | 244 | 1.508 | 101,4 |
| 16.11. | 245 | 1.422 | 95,6 |
| 17.11. | 234 | 1.400 | 94,1 |
| 18.11. | 130 | 1.330 | 89,4 |
| 19.11. | 0 | 1.292 | 86,8 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 1,00 auf jetzt 0,83 und bleibt somit klar unter 1.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 16.11.) liegt bei 0,91. Diese Woche hat das StaBLab der LMU verkündet, daß man ab sofort nur noch Mittwochs ein Upde der Zahlen machen wird. Wir berechnen netürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 14.11. | 1,00 | 0,90 |
| 15.11. | 0,95 | 0,90 |
| 16.11. | 0,89 | 0,91 |
| 17.11. | 0,84 | --- |
| 18.11. | 0,82 | --- |
| 19.11. | 0,83 | --- |
| 20.11. | 0,83 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder weniger Normal- und IMC-Betten aber Intensiv bleibt unverändert. Dennoch - klarer Rückgang bei den Krankenhauszahlen. An jedem Tag deutlich weniger als in der Vorwoche.

Im Vorjahr schlug um diese Zeit die Herbstwelle zu - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 14.11. | 215 | 9 | 20 | 244 | -66 | 330 |
| 15.11. | 203 | 8 | 19 | 230 | -69 | 352 |
| 16.11. | 203 | 6 | 20 | 229 | -48 | 377 |
| 17.11. | 191 | 6 | 19 | 216 | -41 | 397 |
| 18.11. | 187 | 5 | 19 | 211 | -44 | 414 |
| 19.11. | 184 | 4 | 19 | 207 | -46 | 414 |
| 20.11. | 181 | 4 | 19 | 204 | -49 | 414 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es immer besser aus. Seit dem 08.11. sind wir auch mit korrigierten Zahlen wieder unter dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten sinkt die Sume von letzte Woche 148 auf jetzt 131. Am Sonntag alleine wurden allerdings 21 Intensivbetten mehr belegt.

Im Vergleich zum Vorjahr deutlich bessere Zahlen. Im letzten Jahr lief um diese Zeit die große Herbstwelle, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 14.11. | 1.026 | 2.367 |
| 15.11. | 997 | 2.433 |
| 16.11. | 916 | 2.582 |
| 17.11. | 832 | 2.707 |
| 18.11. | 836 | 2.772 |
| 19.11. | 805 | 2.753 |
| 20.11. | 775 | 2.776 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,99% | 75,19% |
| 2. | 71,46% | 75,11% |
| 3. | 48,28% | 59,14% |
| 4. | 10,68% | 10,63% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/11/KW46Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 15.482 Tests/Tag mit einer Positivrate von 15,11. Gegenüber den Zahlen von letzter Woche (14.196 Tests/Tag mit 19,83% Positivrate) also wieder etwas mehr Tests, gepaart mit weiter sinkender Positivrate.

15.482 Tests/Tag ist weniger als 20% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.482 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 15,11%.](/images/2022/11/20221117Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. BA.5 herrscht zwar noch vor, hat aber kräftig Anteil verloren. Weniger an BQ.1, deutlich mehr an BF.7. Wobei in der KW44 bisher nur 5 Proben ausgewertet wurden.

| Variante | KW42 | KW 43 | KW 44 | KW 45 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 2,1% | 4,1% | 4,5% | 3,6%|
| Omikron BA.2.75 | 1,2% | 0,7% | 0,0% | 0,0% |
| Omikron BA.4 | 1,6% | 1,1% | 6,0% | 1,5% |
| Omikron BA.5 | 73,1% | 65,9% | 44,8% | 51,8% |
| Omikron BF.7 | 15.2% | 17,7% | 22,6% | 22,6% |
| Omikron BQ.1 | 6,3% | 9,8% | 20,3% | 16,7% |
| Andere | 0,5% | 0,7% | 1,5% | 3,9% |
| Sequenzierungen | 1.291 | 825 | 133 | 336 | 

![VoC](/images/2022/11/KW46Var.png)