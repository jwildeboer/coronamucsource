---
layout: default
title: 2022-11-15 Tagesbericht
category: Tagesberichte
---

![2022-11-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, aber eine Trendwende deutet sich an mir R(t) um 1. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221115.png)

Das RKI meldet eine Inzidenz von 103,0 für München. 320 Neue, 109 Nachmeldungen, davon 37 für Samstag und 55 für Freitag., Die Inzidenz von Sonntag geht von 115,2 auf 122,4, Samstag von 115,3 auf 122,5, Freitag von 115,3 auf 120,1. Tag 6 unter Inizidenz 150
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 15. Damit insgesamt 716 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 24.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 07.11. | 116,6 -> 126,7 |
| 08.11. | 144,5 -> 152,9 |
| 09.11. | 125,8 -> 136,0 |
| 10.11. | 123,1 -> 130,3 |
| 11.11. | 115,3 -> 120,1 |
| 12.11. | 115,3 -> 122,5 |
| 13.11. | 115,2 -> 122,4 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu **Samstag** in %.

| Kreis | 15.11. | 12.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 145,4 | 159,5 | -8,8% |
| LK Ebersberg | 85,1 | 132,8 | -35,9% |
| LK Freising | 90,5 | 106,5 | -15,0% |
| LK Fürstenfeldbruck | 153,3 | 159,7 | -4,0% |
| LK München | 110,1 | 136,6 | -19,4% |
| LK Starnberg | 105,3 | 116,3 | -9,5 % |

Ebersberg hat nicht gemeldet.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 1,01 und steht damit knapp über der exponentiellen Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 09.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,75 und steigt damit auch wieder an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 08.11. | 0,80 | 0,69 |
| 09.11. | 0,87 | 0,75 |
| 10.11. | 0,97 | 0,81 |
| 11.11. | 1,02 | --- |
| 12.11. | 1,01 | --- |
| 13.11. | 1,02 | --- |
| 14.11. | 0,99 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und bleibt klar unter 300 Betten insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 9 auf jetzt 215. IMC steigt von 7 auf 9. Intensivbetten sinkt von gestern 22 auf jetzt 20. Damit insgesamt 244 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 215 | -9 | 
| IMC | 9 | +2 | 
| Intensiv | 20 | -2 |
| SUMME | 244 | -9 |

Damit 66 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +11
2. +13
3. +15
4. +151

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,27%, Bayern 59,12%.
