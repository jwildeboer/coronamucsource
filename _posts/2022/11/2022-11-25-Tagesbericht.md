---
layout: default
title: 2022-11-25 Tagesbericht
category: Tagesberichte
---

![2022-11-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, und auch der R(t) Wert steigt wieder. Zweite zeigt Krankenbetten, die heute etwas steigen. Dritte zeigt Inzidenz, die langsam und stabil auf über 100 steigt.](/images/2022/11/20221125.png)

Das RKI meldet eine Inzidenz von 98,9 für München. 176 Neue, 157 Nachmeldungen, davon 146(!) für vorgestern. Die gestern genannte Inzidenz von 92,7 steigt auf 102,9. Ab morgen dann wohl Inzidenz über 100. Seit 3 Tagen steigt die korrigierte Inzidenz wieder an.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 734 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 17.11. | 90,5 -> 94,6 |
| 18.11. | 89,4 -> 93,9 |
| 19.11. | 86,8 -> 92,6 |
| 20.11. | 86,8 -> 92,6 |
| 21.11. | 86,4 -> 96,3 |
| 22.11. | 94,1 -> 101,6 |
| 23.11. | 92,7 -> 102,9 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 25.11. | 24.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 121,6 | 128,7 | -5,5% |
| LK Ebersberg | 94,8 | 92,7 | +2,3% |
| LK Freising | 80,6 | 69,6 | +15,8% |
| LK Fürstenfeldbruck | 99,3 | 88,3 | +12,5% |
| LK München | 88,6 | 86,9 | +2,0% |
| LK Starnberg | 92,9 | 93,6 | -0,7% |

Fast überall steigende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 7 Tagen und steht heute auf 0,99 und damit nur noch ganz knapp unter der Grenze zum exponenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.11. | 0,83 | 0,91 |
| 19.11. | 0,84 | 0,85 |
| 20.11. | 0,85 | 0,83 |
| 21.11. | 0,87 | 0,85 |
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,95 | 0,92 |
| 24.11. | 0,99 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute ein wenig: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 3 von 184 auf 187. IMC springt von 2 auf 7. Intensivbetten sinkt um 5 von 21 auf 16. Damit insgesamt 210 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 187 | +3 | 
| IMC | 7 | +5 | 
| Intensiv | 21 | -5 |
| SUMME | 210 | +3 |

Damit nur noch 6 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +9 (72,99%)
2. +5 (71,47%)
3. +18 (48,30%)
4. +174 (11,04%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,30%, Bayern 59,16%.

Das RKI gibt seit gestern auch Zahlen zur 5ten Impfung (also 3. Boosterimpfung) auf Landesebene an. Aktuell 76.127 5. Impfungen in Bayern, das entpricht einer Impfquote von 0,58%. Ob und wann die Stadt München diese Zahl nennt ist uns nicht bekannt.
