---
layout: default
title: 2022-11-22 Tagesbericht
category: Tagesberichte
---

![2022-11-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, und auch der R(t) Wert sinkt wieder. Zweite zeigt Krankenbetten, die heute wieder steigen. Dritte zeigt Inzidenz, die nur noch langsam sinkt.](/images/2022/11/20221122.png)

Das RKI meldet eine Inzidenz von 86,4 für München. 351 Neue, 88 Nachmeldungen, davon 18 für Samstag und 63 für Freitag. Die Inzidenz für Sonntag steigt von 86,8 auf 92,8, Samstag von 86,8 auf 92,4 und Freitag von 89,4 auf 93,7. Tag 6 unter Inzidenz 100.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 12. Damit insgesamt 728 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 31.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 14.11. | 103,0 -> 111,2 |
| 15.11. | 97,8 -> 101,4 |
| 16.11. | 91,9 -> 95,8 |
| 17.11. | 90,5 -> 94,3 |
| 18.11. | 89,4 -> 93,7 |
| 19.11. | 86,8 -> 92,4 |
| 20.11. | 86,8 -> 92,4 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu **Samstag** in %.

| Kreis | 22.11. | 19.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 111,9 | 113,2 | -1,1% |
| LK Ebersberg | 82,3 | 80,9 | +1,7% |
| LK Freising | 69,0 | 75,1 | -8,1% |
| LK Fürstenfeldbruck | 98,4 | 118,5 | -17,0% |
| LK München | 80,6 | 88,3 | -8,7% |
| LK Starnberg | 87,0 | 85,6 | +1,6% |

Bis auf Dachau überall Inzidenz unter 100.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 5 Tagen relatov stabil und steht heute auf 0,85 und damit unter der Grenze zum expnenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 16.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,91 und gleicht sich unserem R(t) an.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Wir berechnen unser R(t) natürlich weiterhin täglich neu.

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.11. | 0,96 | 0,90 |
| 16.11. | 0,90 | 0,91 |
| 17.11. | 0,84 | --- |
| 18.11. | 0,83 | --- |
| 19.11. | 0,84 | --- |
| 20.11. | 0,85 | --- |
| 21.11. | 0,85 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 17 von 181 auf 198. IMC geht von 4 auf 3. Intensivbetten steigt von gestern 19 auf jetzt 21. Damit insgesamt 222 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 198 | +17 | 
| IMC | 3 | -1 | 
| Intensiv | 21 | +2 |
| SUMME | 222 | +18 |

Damit 22 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +9 (72,99%)
2. +4 (71,46%)
3. +17 (48,29%)
4. +109 (10,71%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,29%, Bayern 59,14%.
