---
layout: default
title: 2022-11-10 Tagesbericht
category: Tagesberichte
---

![2022-11-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, eine Trendwende ist aber noch nicht zu erkennen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221110.png)

Das RKI meldet eine Inzidenz von 125,8 für München. 198 Neue, 107 Nachmeldungen, davon 95  für vorgestern. Die Inzidenz für gestern erhöht sich von 144,5 auf 151,5. Somit wieder Tag 1 unter Inzidenz 150.
<!--more-->

Es wurde 1 neuer Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 31. Damit insgesamt 706 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 02.11. | 128,0 -> 135,5 |
| 03.11. | 118,7 -> 126,5 |
| 04.11. | 122,6 -> 129,5 |
| 05.11. | 101,8 -> 108,8 |
| 06.11. | 101,8 -> 108,8 |
| 07.11. | 116,6 -> 126,2 |
| 08.11. | 144,5 -> 151,5 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 10.11 | 09.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 185,3 | 209,7 | -11,6% |
| LK Ebersberg | 171,6 | 199,9 | -14,2% |
| LK Freising | 127,0 | 120,9 | +5,0% |
| LK Fürstenfeldbruck | 170,6 | 200,8 | -15,0% |
| LK München | 143,5 | 153,2 | -6,3% |
| LK Starnberg | 132,4 | 160,9 | -17,7% |

Immer noch Rückschlageffekt vom Feiertag in der letzten Woche.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 0,86. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 08.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,75 und steigt damit auch wieder an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.11. | 0,70 | 0,69 |
| 04.11. | 0,71 | 0,70 |
| 05.11. | 0,71 | 0,68 |
| 06.11. | 0,71 | 0,66 |
| 07.11. | 0,73 | 0,66 |
| 08.11. | 0,80 | 0,70 |
| 09.11. | 0,86 | 0,75 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und bleibt klar unter 300 Betten insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 17 auf jetzt 247. IMC sinkt von 12 weider auf 9. Intensivbetten sinkt von gestern 23 auf jetzt 21. Damit insgesamt 277 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 247 | -17 | 
| IMC | 9 | -3 | 
| Intensiv | 21 | -2 |
| SUMME | 277 | -22 |

Damit 97 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +5
2. +11
3. +26
4. +250

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,26%, Bayern 59,10%.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur noch 14.186 Tests/Tag mit einer Positivrate von 19,83. Gegenüber den Zahlen von letzter Woche (18.433 Tests/Tag mit 24,80% Positivrate) also wieder weniger Tests, gepaart mit sinkender Positivrate.

14.186 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 14.186 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 19,83%.](/images/2022/11/20221110Test.png)
