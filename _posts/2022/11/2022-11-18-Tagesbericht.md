---
layout: default
title: 2022-11-18 Tagesbericht
category: Tagesberichte
---

![2022-11-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, und auch der R(t) Wert sinkt wieder. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die langsam sinkt.](/images/2022/11/20221118.png)

Das RKI meldet eine Inzidenz von 90,5 für München. 85 Neue, nur 55 Nachmeldungen, davon 48 für vorgestern. Die gestern genannte Inzidenz von 91,9 erhöht sich auf 95,3. Mit der um Nachmeldungen korrigierten Inzidenz somit Tag 2 unter Inzidenz 100.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 16. Damit insgesamt 726 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 24.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 10.11. | 123,1 -> 130,5 |
| 11.11. | 115,3 -> 120,3 |
| 12.11. | 115,3 -> 122,7 |
| 13.11. | 115,2 -> 122,7 |
| 14.11. | 103,0 -> 111,1 |
| 15.11. | 97,8 -> 101,3 |
| 16.11. | 91,9 -> 95.3 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 18.11 | 17.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 124,8 | 115,8 | +7,8% |
| LK Ebersberg | 80,9 | 87,9 | -8,0% |
| LK Freising | 77,8 | 82,3 | -5,5% |
| LK Fürstenfeldbruck | 113,5 | 131,8 | -13,9% |
| LK München | 90,9 | 92,3 | -1,5% |
| LK Starnberg | 84,1 | 73,9 | +13,8% |

Dachau und Starnberg mit steigenden Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 1,01 und steht damit knapp über der exponentiellen Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 16.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,91 und gleicht sich unserem R(t) an.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Wir berechnen unser R(t) natürlich weiterhin täglich neu.

| Datum | R(t) | LMU |
|---|---:|---:|
| 11.11. | 1,02 | 0,92 |
| 12.11. | 1,01 | 0,95 |
| 13.11. | 1,02 | 0,93 |
| 14.11. | 1,00 | 0,90 |
| 15.11. | 0,95 | 0,90 |
| 16.11. | 0,89 | 0,91 |
| 17.11. | 0,84 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter, wie häufig am Freitag auf der Normalstation: Gegenüber gestern sinkt die Zahl der belegten Normalbetten von 103 auf 191. IMC bleibt auf 6. Intensivbetten sinkt von gestern 20 auf jetzt 19. Damit insgesamt 216 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 191 | -12 | 
| IMC | 6 | +/-0 | 
| Intensiv | 19 | -1 |
| SUMME | 216 | -13 |

Damit 41 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden.

1. +5
2. +5
3. +20
4. +180

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,28%, Bayern 59,13%.
