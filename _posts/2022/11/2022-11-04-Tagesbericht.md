---
layout: default
title: 2022-11-04 Tagesbericht
category: Tagesberichte
---

![2022-11-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen noch, aber es könnte kippen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221104.png)

Das RKI meldet eine Inzidenz von 118,7 für München. 241 Neue, 100 Nachmeldungen, davon 91 für gestern. Die gestern genannte Inzidenz von 128,0 erhöht sich durch Nachmeldungen auf 134,3. Tag 4 unter Inzidenz 200.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 10. Damit insgesamt 676 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 27.10. | 226,1 -> 237,2 |
| 28.10. | 208,4 -> 213,2 |
| 29.10. | 179,3 -> 204,5 |
| 30.10. | 179,3 -> 204,5 |
| 31.10. | 173,2 -> 179,3 |
| 01.11. | 125,9 -> 131,9 |
| 02.11. | 128,0 -> 134,3 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 04.11. | 03.11 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 155,7 | 136,4 | +14,1% |
| LK Ebersberg | 181,9 | 170,2 | +6,9% |
| LK Freising | 148,5 | 123,7 | +20,0% |
| LK Fürstenfeldbruck | 175,7 | 74,1 | +137,1% |
| LK München | 148,4 | 138,4 | +7,2% |
| LK Starnberg | 133,1 | 135,3 | -1,6% |

Fast überall steigende Inzidenzen, FFB war klar, in den anderen Kreisen muss man schauen ob das noch Feiertagseffekt ist oder doch Trendwende.

## Reproduktionswert R(t) für München

Unser R(t) stabilisiert sich um 0,70.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 02.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,58 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.10. | 0,69 | 0,61 |
| 29.10. | 0,71 | 0,59 |
| 30.10. | 0,71 | 0,57 |
| 31.10. | 0,72 | 0,56 |
| 01.11. | 0,70 | 0,56 |
| 02.11. | 0,70 | 0,58 |
| 03.11. | 0,69 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und ist seit zwei Tagen wieder unter 400 insgesamt: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 16 Betten auf jetzt 312. IMC steigt von gestern 12 auf jetzt 13. Intensivbetten sinkt von gestern 34 auf jetzt 29. Damit insgesamt 354 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 312 | -16 | 
| IMC | 13 | +1 | 
| Intensiv | 29 | -5 |
| SUMME | 354 | -20 |

Damit 86 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +15
2. +5
3. +35
4. +268

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,24%, Bayern 59,07%.
