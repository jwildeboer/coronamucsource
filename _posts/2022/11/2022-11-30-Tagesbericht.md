---
layout: default
title: 2022-11-30 Tagesbericht
category: Tagesberichte
---

![2022-11-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht weiter über 1. Zweite zeigt Krankenbetten, die heute weiter steigen. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert.](/images/2022/11/20221130.png)

Das RKI meldet eine Inzidenz von 96,1 für München. 210 Neue, 91 Nachmeldungen, davon 88 für vorgestern. Durch Nachmeldungen erhöht sich die gestern genannte Inzidenz von 97,9 auf 103,8. Wir bleiben also mit einem Tag Verzögerung seit 7 Tagen über Inzidenz 100.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt 738 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 32.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 22.11. | 94,1 -> 101,8 |
| 23.11. | 92,7 -> 103,4 |
| 24.11. | 98,9 -> 106,1 |
| 25.11. | 101,0 -> 102,5 |
| 26.11. | 99,8 -> 102,5 |
| 27.11. | 99,8 -> 102,5 |
| 28.11. | 97,9 -> 103,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 30.11. | 29.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,0 | 124,8 | +2,6% |
| LK Ebersberg | 98,2 | 92,7 | +5,9% |
| LK Freising | 72,9 | 71,2 | +2,4% |
| LK Fürstenfeldbruck | 115,3 | 103,4 | +11,5% |
| LK München | 92,0 | 82,0 | +12,2% |
| LK Starnberg | 88,5 | 88,5 | +/-0,0% |

Leicht steigende Tendenz insgesamt.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 5 Tagen über 1 und damit über der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 23.11. | 0,95 | 0,92 |
| 24.11. | 1,01 | --- |
| 25.11. | 1,04 | --- |
| 26.11. | 1,06 | --- |
| 27.11. | 1,06 | --- |
| 28.11. | 1,05 | --- |
| 29.11. | 1,03 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 9 von 194 auf 203. IMC steigt von 8 auf 10. Intensivbetten sinktt um 1 von 17 auf 16. Damit insgesamt 229 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 203 | +9 | 
| IMC | 10 | +2 | 
| Intensiv | 16 | -1 |
| SUMME | 229 | +10 |

Damit insgesamt 17 Betten **mehr** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Heute wurden die Zahlen der letzten Woche aus den Arztpraxen nachgemeldet, daher die höheren Zahlen.

1. +22 (73,00%)
2. +30 (71,47%)
3. +268 (48,33%)
4. +4.869 (11,40%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,33%, Bayern 59,17%.

Das RKI gibt auch Zahlen zur fünften und sechsten Impfung (also 3. und 4. Boosterimpfung) auf Landesebene an. Aktuell 78.454 5. Impfungen in Bayern, das entpricht einer Impfquote von 0,60%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.