---
layout: default
title: 2022-11-27 Tagesbericht
category: Tagesberichte
---

![2022-11-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, und auch der R(t) Wert steigt weiter. Zweite zeigt Krankenbetten, die in der vergangenen Woche etwas gestiegen sind. Am Wochenende werde die Zahlen aber nicht aktualisiert. Dritte zeigt Inzidenz, die langsam und stabil seit 4 Tagen auf über 100 steigt.](/images/2022/11/20221127.png)

Sonntag, daher keine neuen Fallzahlen beim RKI. Das RKI meldet eine Inzidenz von 99,8 für München. Erst am Dienstag gibt es wieder neue Zahlen und damit auch eine neue "echte" Inzidenz.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert) bleibt auf dem Stand von gestern, da es keine neuen Meldungen gibt:

| 18.11. | 89,4 -> 93,9 |
| 19.11. | 86,8 -> 92,6 |
| 20.11. | 86,8 -> 92,6 |
| 21.11. | 86,4 -> 96,3 |
| 22.11. | 94,1 -> 101,6 |
| 23.11. | 92,7 -> 103,2 |
| 24.11. | 98,9 -> 105,7 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Montag schon jetzt berechnen:

| 28.11. | 99,8 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Auch hier bleibt alles bis Dienstag unverändert, weil keine neuen Zahlen vorliegen.

| Kreis | 26.11. | 25.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 120,9 | 121,6 | -0,6% |
| LK Ebersberg | 103,1 | 94,8 | +8,8% |
| LK Freising | 77,3 | 80,6 | -4,1% |
| LK Fürstenfeldbruck | 98,8 | 99,3 | -0,5% |
| LK München | 97,8 | 88,6 | +10,4% |
| LK Starnberg | 92,9 | 92,9 | --- |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 8 Tagen und steht heute auf 1,05 und damit zum dritten Tag in Folge knapp über der Grenze zum exponenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 20.11. | 0,85 | 0,83 |
| 21.11. | 0,87 | 0,85 |
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,95 | 0,92 |
| 24.11. | 1,00 | --- |
| 25.11. | 1,04 | --- |
| 26.11. | 1,05 | --- |

## Krankenhaus

Am Sonntag meldet die Stadt München keine neuen Krankenhauszahlen.

## Impfungen

Am Sonntag meldet die Stadt München keine neuen Impfzahlen.
