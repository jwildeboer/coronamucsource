---
layout: default
title: 2022-11-23 Tagesbericht
category: Tagesberichte
---

![2022-11-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen tendeziell minimal höher, und auch der R(t) Wert steigt wieder. Zweite zeigt Krankenbetten, die heute wieder steigen. Dritte zeigt Inzidenz, die sich weiterhin auf einem Plateau befindet.](/images/2022/11/20221123.png)

Das RKI meldet eine Inzidenz von 94,1 für München. 216 Neue, 148 Nachmeldungen, davon 141 für vorgestern. Die gestern genannte Inzidenz 86,4 setigt auf 96,1. Tag 7 unter Inzidenz 100, aber die Zahlen ziehen wieder leicht an.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 10. Damit insgesamt 731 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 15.11. | 97,8 -> 101,5 |
| 16.11. | 91,9 -> 95,9 |
| 17.11. | 90,5 -> 94,4 |
| 18.11. | 89,4 -> 93,8 |
| 19.11. | 86,8 -> 92,5 |
| 20.11. | 86,8 -> 92,5 |
| 21.11. | 86,4 -> 96,1 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 23.11. | 22.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 122,2 | 111,9 | +9,2% |
| LK Ebersberg | 80,9 | 82,3 | -1,7% |
| LK Freising | 59,6 | 69,0 | -13,6% |
| LK Fürstenfeldbruck | 89,7 | 98,4 | -8,8% |
| LK München | 83,2 | 80,6 | +3,2% |
| LK Starnberg | 92,9 | 87,0 | +6,8% |

Bis auf Dachau überall Inzidenz unter 100.

## Reproduktionswert R(t) für München

Unser R(t) ist seit 5 Tagen relatov stabil und steht heute auf 0,85 und damit unter der Grenze zum expnenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 16.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,91 und gleicht sich unserem R(t) an.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch. Wir berechnen unser R(t) natürlich weiterhin täglich neu.

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.11. | 0,90 | 0,91 |
| 17.11. | 0,84 | --- |
| 18.11. | 0,83 | --- |
| 19.11. | 0,84 | --- |
| 20.11. | 0,85 | --- |
| 21.11. | 0,87 | --- |
| 22.11. | 0,89 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 10 von 198 auf 188. IMC bleibt gleich mit 3. Intensivbetten bleibt gleicht mit 21. Damit insgesamt 212 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 188 | -10 | 
| IMC | 3 | +/-0 | 
| Intensiv | 21 | +/-0 |
| SUMME | 212 | +-10 |

Damit 18 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Heute wieder Nachmeldungen aus den Arztpraxen, insebesondere Viertimpfungen.

1. +13 (72,99%)
2. +12 (71,46%)
3. +207 (48,30%)
4. +4.552 (11,01%)

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,30%, Bayern 59,15%.
