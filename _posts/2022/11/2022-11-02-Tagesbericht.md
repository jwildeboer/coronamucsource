---
layout: default
title: 2022-11-02 Tagesbericht
category: Tagesberichte
---

![2022-11-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Wegen Feiertag aber keine neuen Zahlen heute. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und weiter sinkt.](/images/2022/11/20221102.png)

Wegen Feiertag gestern keine Neu-/Nachmeldungen beim RKI. Die gemeldete Inzidenz von 125,9 für München ist daher deutlich zu niedrig. Morgen dann Neu-/Nachmeldungen und eher realistische Zahlen.
<!--more-->

Wegen Feiertag gestern heute auch keine neuen Zahlen zu Todesfällen.

## Umland

Auch für das Umland heute keine neuen Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 5 Tagen wieder leicht auf jetzt 0,71 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 28.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.10. | 0,65 | 0,55 |
| 27.10. | 0,67 | 0,58 |
| 28.10. | 0,69 | 0,59 |
| 29.10. | 0,71 | --- |
| 30.10. | 0,71 | --- |
| 31.10. | 0,71 | --- |
| 01.11. | 0,70 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Gegenüber vorgestern sinkt die Zahl der belegten Normalbetten um 18 Betten auf jetzt 351. IMC steigt von vorgestern 12 auf jetzt 15. Intensivbetten steigt von vorgestern 34 auf jetzt 37. Damit insgesamt 403 Betten belegt.

In der Tabelle haben wir die Zahlen auf Änderung pro Tag umgerechnet.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 351 | -9 | 
| IMC | 15 | +2 | 
| Intensiv | 37 | +1 |
| SUMME | 403 | -6 |

Damit 83 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +16
2. +19
3. +112
4. +1818

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,24%, Bayern 59,06%.