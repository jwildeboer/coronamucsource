---
layout: default
title: 2022-11-05 Tagesbericht
category: Tagesberichte
---

![2022-11-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen noch, aber es könnte kippen. Zweite zeigt Krankenbetten, die weiter klar sinken. Samstag keine neuen Zahlen von der Stadt Müchen. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221105.png)

Das RKI meldet eine Inzidenz von 122,6 für München. 262 Neue, 100 Nachmeldungen, davon 89 für gestern. Die gestern genannte Inzidenz von 118,7 erhöht sich durch Nachmeldungen auf 125,4. Tag 5 unter Inzidenz 200.
<!--more-->

Es wurden 16(!) neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 19. Damit insgesamt 692 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 28.10. | 208,4 -> 213,1 |
| 29.10. | 179,3 -> 204,4 |
| 30.10. | 179,3 -> 204,4 |
| 31.10. | 173,2 -> 179,5 |
| 01.11. | 125,9 -> 132,1 |
| 02.11. | 128,0 -> 135,0 |
| 03.11. | 118,7 -> 125,4 |

Und weil am Wochenende keine Meldungen an das RKI gehen, können wor schon heute die Inzidenzen für Sonntag und Montag berechnen:

| Sonntag | 101,8 |
| Montag | 101,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 05.11. | 04.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 158,9 | 155,7 | +2,0% |
| LK Ebersberg | 184,0 | 181,9 | +1,1% |
| LK Freising | 125,9 | 148,5 | +15,2% |
| LK Fürstenfeldbruck | 176,1 | 175,7 | +0,2% |
| LK München | 143,5 | 148,4 | -3,3% |
| LK Starnberg | 124,3 | 133,1 | -6,6% |

Uneinheitliche Entwicklung. 

## Reproduktionswert R(t) für München

Unser R(t) stabilisiert sich um 0,70.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 02.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,58 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.10. | 0,71 | 0,59 |
| 30.10. | 0,71 | 0,57 |
| 31.10. | 0,72 | 0,56 |
| 01.11. | 0,70 | 0,56 |
| 02.11. | 0,70 | 0,58 |
| 03.11. | 0,70 | --- |
| 04.11. | 0,70 | --- |

## Krankenhaus

Am Samstag keine neuen Krankenhauszahlen von der Stadt München.

## Impfungen

Am Samstag keine neuen Impfzahlen von der Stadt München.
