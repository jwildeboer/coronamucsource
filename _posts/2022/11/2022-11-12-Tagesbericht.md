---
layout: default
title: 2022-11-12 Tagesbericht
category: Tagesberichte
---

![2022-11-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, eine Trendwende deutet sich an. Zweite zeigt Krankenbetten, die weiter klar sinken aber am Wochenende nicht aktualisert werden. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221112.png)

Das RKI meldet eine Inzidenz von 115,3 für München. 166 Neue, 138 Nachmeldungen, davon 127  für vorgestern. Die Inzidenz für gestern erhöht sich von 125,8 auf 134,. Somit wieder Tag 1 unter Inzidenz 150.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 34. Damit insgesamt 710 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 04.11. | 122,6 -> 129,7 |
| 05.11. | 101,8 -> 109,1 |
| 06.11. | 101,8 -> 109,2 |
| 07.11. | 116,6 -> 126,6 |
| 08.11. | 144,5 -> 152,4 |
| 09.11. | 125,8 -> 135,3 |
| 10.11. | 123,1 -> 129,3 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 12.11. | 11.11 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 159,5 | 153,7 | +3,8% |
| LK Ebersberg | 132,8 | 151,5 | -12,3% |
| LK Freising | 106,5 | 114,3 | -6,8% |
| LK Fürstenfeldbruck | 159,7 | 188,0 | -15,1% |
| LK München | 136,6 | 136,1 | +0,4% |
| LK Starnberg | 116,3 | 122,9 | -5,4% |

Weiterhin uneinheitlich.

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf 1,01 und steht damit knapp über der exponentiellen Grenze. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 09.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,75 und steigt damit auch wieder an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.11. | 0,71 | 0,68 |
| 06.11. | 0,71 | 0,66 |
| 07.11. | 0,73 | 0,66 |
| 08.11. | 0,80 | 0,70 |
| 09.11. | 0,87 | 0,75 |
| 10.11. | 0,97 | --- |
| 11.11. | 1,01 | --- |

## Krankenhaus

Am Samstag und Sonntag keine neuen Krankenhauszahlen von der Stadt München.

## Impfungen

Am Samstag und Sonntag keine neuen Impfzahlen von der Stadt München.
