---
layout: default
title: 2022-11-26 Tagesbericht
category: Tagesberichte
---

![2022-11-26 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, und auch der R(t) Wert steigt weiter. Zweite zeigt Krankenbetten, die in der vergangenen Woche etwas gestiegen sind. Am Wochenende werde die Zahlen aber nicht aktualisiert. Dritte zeigt Inzidenz, die langsam und stabil seit 4 Tagen auf über 100 steigt.](/images/2022/11/20221126.png)

Das RKI meldet eine Inzidenz von 101,0 für München. 123 Neue, 102 Nachmeldungen, davon 96 für vorgestern. Die gestern genannte Inzidenz von 98,9 steigt auf 105,7. Somit ist die korrigierte Inzidenz jetzt seit 4 Tagen wieder über 100.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 736 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 18.11. | 89,4 -> 93,9 |
| 19.11. | 86,8 -> 92,6 |
| 20.11. | 86,8 -> 92,6 |
| 21.11. | 86,4 -> 96,3 |
| 22.11. | 94,1 -> 101,6 |
| 23.11. | 92,7 -> 103,2 |
| 24.11. | 98,9 -> 105,7 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| 27.11. | 99,8 |
| 28.11. | 99,8 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 26.11. | 25.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 120,9 | 121,6 | -0,6% |
| LK Ebersberg | 103,1 | 94,8 | +8,8% |
| LK Freising | 77,3 | 80,6 | -4,1% |
| LK Fürstenfeldbruck | 98,8 | 99,3 | -0,5% |
| LK München | 97,8 | 88,6 | +10,4% |
| LK Starnberg | 92,9 | 92,9 | --- |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 7 Tagen und steht heute auf 1,04 und damit zum zweiten Tag in Folge knapp über der Grenze zum exponenziellem Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.11. | 0,84 | 0,85 |
| 20.11. | 0,85 | 0,83 |
| 21.11. | 0,87 | 0,85 |
| 22.11. | 0,90 | 0,88 |
| 23.11. | 0,95 | 0,92 |
| 24.11. | 1,00 | --- |
| 25.11. | 1,04 | --- |

## Krankenhaus

Am Samstag meldet die Stadt München keine neuen Krankenhauszahlen.

## Impfungen

Am Samstag meldet die Stadt München keine neuen Impfzahlen.
