---
layout: default
title: 2022-11-08 Tagesbericht
category: Tagesberichte
---

![2022-11-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen noch, aber es könnte kippen. Zweite zeigt Krankenbetten, die weiter klar sinken. Dritte zeigt Inzidenz, die sich jetzt eher auf einem Plateau befindet.](/images/2022/11/20221108.png)

Das RKI meldet eine Inzidenz von 116,6 für München. 470 Neue, 99 Nachmeldungen, davon 1 für Sonntag, 1 für Samstag und 85 für Freitag. Die Inzidenz für Sonntag erhöht sich von 101,8 auf 108,4, Samstag von 101,8 auf 108,3 und Freitag von 122,6 auf 129,1. Tag 7 unter Inzidenz 150.
<!--more-->

Es wurden 9 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 27. Damit insgesamt 701 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 19.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 31.10. | 173,2 -> 179,5 |
| 01.11. | 125,9 -> 132,2 |
| 02.11. | 128,0 -> 135,3 |
| 03.11. | 118,7 -> 126,2 |
| 04.11. | 122,6 -> 129,1 |
| 05.11. | 101,8 -> 108,3 |
| 06.11. | 101,8 -> 108,4 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu **Samstag** in %.

| Kreis | 08.11. | 05.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 167,3 | 158,9 | +5,3% |
| LK Ebersberg |172,2 | 184,0 | -6,4% |
| LK Freising | 119,2 | 125,9 | -5,3% |
| LK Fürstenfeldbruck | 167,4 | 176,1 | -4,9% |
| LK München | 124,3 | 143,5 | -13,4% |
| LK Starnberg | 125,0 | 124,3 | +0,6% |

Weiter uneinheitliche Entwicklung. 

## Reproduktionswert R(t) für München

Unser R(t) stabilisiert sich um 0,71.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 04.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,65 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 01.11. | 0,70 | 0,61 |
| 02.11. | 0,70 | 0,64 |
| 03.11. | 0,70 | 0,67 |
| 04.11. | 0,71 | 0,65 |
| 05.11. | 0,71 | --- |
| 06.11. | 0,70 | --- |
| 07.11. | 0,72 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter und seit zwei Tagen ist die Zahl der belegten Normalbetten unter 300: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 21 Betten auf jetzt 273. IMC bleibt auf 9. Intensivbetten sinkt minimal von gestern 29 auf jetzt 28. Damit insgesamt 310 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 273 | -21 | 
| IMC | 9 | +0 | 
| Intensiv | 28 | -1 |
| SUMME | 310 | -22 |

Damit 99 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +15
2. +5
3. +35
4. +268

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,24%, Bayern 59,07%.
