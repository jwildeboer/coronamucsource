---
layout: default
title: 2022-09-05 Wochenbericht KW35
category: Wochenberichte
---

![2022-09-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche jetzt etwas höhere Tendenz erkennbar. Zweite zeigt Krankenbetten, leicht steigende Zahlen, dritte zeigt Inzidenz welches seit 2 Wochen um 200 pendelt.](/images/2022/09/20220905.png)

In der letzten Woche gab es insgesamt 2.720 neue COVID Fälle in München, das sind nur noch 3,2% weniger als in der Vorwoche (2.810). Leider aber auch 15 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (18).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 2.809 zurück auf 2.720. Die Inzidenz verlor nur noch 6 Punkte in 7 Tagen. Das Abklingen der Welle hat sich also deutlich verlangsamt. Die ersten Ferienrückkehrer werden wohl auch noch "Souvenire" mitbringen und dann ist ja auch bald schon Oktoberfest. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 28.08. | 0 | 2.809 | 188,8 |
| 29.08. | 917 | 2.978 | 200,1 |
| 30.08. | 622 | 2.951 | 198,3 |
| 31.08. | 499 | 2.955 | 198,6 |
| 01.09. | 386 | 2.805 | 188,5 |
| 02.09. | 296 | 2.721 | 182,8 |
| 03.09. | 0 | 2.720 | 182,8 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das war in der letzten Woche knapp unter 1, mit leicht steigender Tendenz.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf vom 01.09.) liegt bei 0,93.

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.08. | 0,93 | 0,88 |
| 30.08. | 0,92 | 0,88 |
| 31.08. | 0,93 | 0,93 |
| 01.09. | 0,94 | --- |
| 02.09. | 0,97 | --- |
| 03.09. | 0,98 | --- |
| 04.09. | 0,98 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München stabilisert sich die Lage, bei IMC und Intensivbetten etwas zögerlicher. Im Schnitt jetzt um 235 Betten insgesamt belegt. Immer noch mehr als das doppelte im Vergleich zum Vorjahr.

Im Vergleich zu letzter Woche immerhin noch 12 Betten weniger belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 29.08. | 228 | 11 | 17 | 256 | -17 | 99 |
| 30.08. | 209 | 9 | 19 | 237 | -30 | 99 |
| 31.08. | 204 | 10 | 16 | 230 | -20 | 116 |
| 01.09. | 201 | 8 | 17 | 226 | -25 | 111 |
| 02.09. | 201 | 10 | 18 | 229 | -20 | 111 |
| 03.09. | 201 | 11 | 19 | 231 | -17 | 111 |
| 04.09. | 202 | 12 | 20 | 234 | -12 | 111 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiterhin relativ gut aus. Seit dem 10.08. sind wir wieder unter dem Grenzwert der alten gelben Ampel und die Zahlen sinken weiter kontinuierlich. Aber wir sind jetzt auf demn gleichen Level wie im Vorjahr.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 29.08. | 848 | 506 |
| 30.08. | 788 | 506 |
| 31.08. | 728 | 510 |
| 01.09. | 674 | 527 |
| 02.09. | 624 | 546 |
| 03.09. | 599 | 588 |
| 04.09. | 581 | 588 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,90% | 75,34% |
| 2. | 71,37% | 75,23% |
| 3. | 47,9% | 58,86% |
| 4. | 6,15% | 6,04% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/09/KW35Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

# Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca 15.405 Tests/Tag mit einer Positivrate von 20,87%. Gegenüber den Zahlen von letzter Woche (15.217 Tests/Tag mit 21,76% Positivrate) also wieder minimal mehr Tests, gepaart mit leicht sinkender Positivrate.

15.405 Tests/Tag ist sehr wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.405 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 20,87%.](/images/2022/09/20220901-Test.png)
