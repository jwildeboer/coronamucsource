---
layout: default
title: 2022-09-07 Tagesbericht
category: Tagesberichte
---

![2022-09-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche etwas niedriger. Zweite zeigt Krankenbetten, insgesamt 12 Betten weniger belegt als gestern, dritte zeigt Inzidenz, die jetzt seit 2 Tagen unter 190 ist.](/images/2022/09/20220907.png)

Das RKI meldet eine Inzidenz von 174,1 für München, 471 neue und 84 Nachmeldungen für die letzten 7 Tage, davon 74 für vorgestern. Die gestern genannte Inzidenz von 179,2 korrigiert sich durch die Nachmeldungen auf 184,9. Sonntag auf 190,2. Also wieder Tag 2 unter 190.
<!--more-->

Seit 4 Tagen keine  neuen Todesfälle - die 7-Tage-Summe der Todesfälle sinkt auf 4. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 2.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 30.08. | 187,8 -> 199,1 |
| 31.08. | 189,1 -> 200,1 |
| 01.09. | 183,8 -> 190,8 |
| 02.09. | 182,8 -> 189,1 |
| 03.09. | 182,8 -> 189,6 |
| 04.09. | 182,8 -> 190,2 |
| 05.09. | 179,2 -> 184,9 |

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,97.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 06.09. liefert ein R(t) von 0,95.

| Datum | R(t) | LMU |
|---|---:|---:|
| 31.08. | 0,93 | 0,97 |
| 01.09. | 0,94 | 1,03 |
| 02.09. | 0,98 | 1,03 |
| 03.09. | 1,00 | 0,99 |
| 04.09. | 1,00 | 0,96 |
| 05.09. | 0,99 | 0,94 |
| 06.09. | 0,97 | 0,95 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt klar: Normalbetten sinken um 11 von gestern 199 auf jetzt 188, IMC bleibt auf 6 und Intensivbetten sinkt von 20 auf 19. Damit insgesamt 213 Betten belegt, 12 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 188 | -11 | 
| IMC | 6 | +0 | 
| Intensiv | 19 | -1 |
| SUMME | 213 | -12 |

Im Wochenvergleich somit 24 Betten weniger belegt. 

## Impfungen

Die Impfzahlen bewegen sich jetzt doch mal ein wenig, zumindest bei Booster- und 4te Impfung. Nachmeldungen von den Arztpraxen oder holen sich die ersten den Omikronbooster?

1. +16
2. +27
3. +211
4. +1338

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.