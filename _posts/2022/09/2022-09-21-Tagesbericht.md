---
layout: default
title: 2022-09-21 Tagesbericht
category: Tagesberichte
---

![2022-09-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche klar höher. Zweite zeigt Krankenbetten, insgesamt 10 Betten weniger belegt als gestern. Dritte zeigt Inzidenz, die jetzt schnell steigt und seit 2 Tagen über 240 zeigt.](/images/2022/09/20220921.png)

Das RKI meldet eine Inzidenz von 240,4 für München, 685 Neue und 244 Nachmeldungen, davon 215(!) für gestern. Die gestern genannte Inzidenz von 224,7 erhöht sich durch Nachmeldungen auf 240,2. Tag 2 über 240.
<!--more-->

Leider auch 2 neue Todesfälle - die 7-Tage-Summe der Todesfälle geht damit auf 19. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle auch 19.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 13.09. | 183,6 -> 190,9 |
| 14.09. | 187,7 -> 198,4 |
| 15.09. | 199,4 -> 211,7 |
| 16.09. | 204,9 -> 218,0 |
| 17.09. | 204,5 -> 217,7 |
| 18.09. | 203,5 -> 217,6 |
| 19.09. | 224,7 -> 240,2 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,12 und steigt seit 12 Tagen, Tag 9 über 1.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert, der letzte Nowcast Lauf vom 20.09. liefert ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 14.09. | 1,01 | 1,09 |
| 15.09. | 1,04 | 1,11 |
| 16.09. | 1,06 | 1,10 |
| 17.09. | 1,08 | 1,05 |
| 18.09. | 1,09 | 1,04 |
| 19.09. | 1,10 | 1,05 |
| 20.09. | 1,12 | 1,09 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute: Normalbetten sinkt um 7 von gestern 197 auf jetzt 190, IMC sinkt von 12 auf 10 und Intensivbetten von 20 auf 19. Damit insgesamt 219 Betten belegt, 10 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 190 | -7 | 
| IMC | 10 | -2 | 
| Intensiv | 19 | -1 |
| SUMME | 219 | -10 |

Im Wochenvergleich trotzdem 3 Betten mehr.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Eine doch ordentliche Zunahme im Vergleich zur Vorwoche bei den Viertimpfungen, wo heute die 100.000er Grenze geknacht wurde.  Da gibt es wohl Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen.

1. +24
2. +51
3. +255
4. +2.930

Wir bleiben in München trotzdem unter dem Landesdurchschnitt, besonders klar bei den Drittimpfugen: München 47,98%, Bayern 58,95%.

## Wiesnbarometer

Tag 5 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 25,9% auf heute 240,4. Die Zahl der mit COVID-positiven Patienten belegten Betten steigt im Vergleich zur Vorwoche leicht um 1,39% auf jetzt 219.

![Wiesnbarometer 2022-09-21](/images/2022/09/20220921WB.png)