---
layout: default
title: 2022-09-06 Tagesbericht
category: Tagesberichte
---

![2022-09-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche etwas niedrigerer Wochenstart. Zweite zeigt Krankenbetten, insgesamt 9 Betten weniger belegt als gestern, dritte zeigt Inzidenz, die jetzt seit 4 Tagen unter 190 ist.](/images/2022/09/20220906.png)

Das RKI meldet eine Inzidenz von 179,2 für München, 769 neue und 105 Nachmeldungen, davon 8 für Sonntag, 9 für Samstag und 56 für Freitag. Die Inzidenzen vom Wochenende gehen von 182,8 auf 188,8 am Samstag, 189,3 am Sonntag. Die gestrige Inzidenz von 182,8 erhöht sich durch Nachmeldungen auf 189,3. Tag 4 unter Inzidenz 180.
<!--more-->

Keine neuen Todesfälle - die 7-Tage-Summe der Todesfälle sinkt auf 8. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):


| 29.08. | 188,7 -> 200,2 |
| 30.08. | 187,8 -> 198,9 |
| 31.08. | 189,1 -> 199,8 |
| 01.09. | 183,8 -> 190,2 |
| 02.09. | 182,8 -> 188,2 |
| 03.09. | 182,8 -> 188,8 |
| 04.09. | 182,8 -> 189,3 |

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,98.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 06.09. liefert ein R(t) von 0,86.

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.08. | 0,92 | 0,89 |
| 31.08. | 0,93 | 0,95 |
| 01.09. | 0,94 | 0,99 |
| 02.09. | 0,98 | 0,98 |
| 03.09. | 0,99 | 0,92 |
| 04.09. | 1,00 | 0,88 |
| 05.09. | 0,98 | 0,86 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt wieder etwas mehr: Normalbetten sinken um 3 von gestern 202 auf jetzt 199, IMC sinkt deutlich von 12 auf 6 und Intensivbetten unverändert 20. Damit insgesamt 225 Betten belegt, 9 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 199 | -3 | 
| IMC | 6 | -6 | 
| Intensiv | 20 | +0 |
| SUMME | 225 | -9 |

Im Wochenvergleich somit 31 Betten weniger belegt. 

## Impfungen

Die Impfzahlen bewegen sich weiterhin kaum, ob sich das durch die neuen Omikron-Booster ändern wird, zeigt sich hoffentlich in den nächsten Tagen bei den Dritt- und Viertimpfungen.

1. +10
2. +12
3. +27
4. +69

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.