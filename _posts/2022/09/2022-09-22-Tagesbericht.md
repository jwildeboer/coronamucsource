---
layout: default
title: 2022-09-22 Tagesbericht
category: Tagesberichte
---

![2022-09-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche klar höher. Zweite zeigt Krankenbetten, insgesamt 5 Betten mehr belegt als gestern. Dritte zeigt Inzidenz, die jetzt schnell steigt und seit 2 Tagen über 250 liegt.](/images/2022/09/20220922.png)

Das RKI meldet eine Inzidenz von 255,6 für München, 628 Neue und 191 Nachmeldungen, davon 168 für vorgestern. Die gestern genannte Inzidenz von 240,4 erhöht sich durch Nachmeldungen auf 253,0. Tag 2 über 250.
<!--more-->

Leider auch 1 neuer Todesfall - die 7-Tage-Summe der Todesfälle bleibt damit 19. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle auch 19.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 14.09. | 187,7 -> 198,4 |
| 15.09. | 199,4 -> 211,7 |
| 16.09. | 204,9 -> 218,2 |
| 17.09. | 204,5 -> 217,9 |
| 18.09. | 203,5 -> 217,8 |
| 19.09. | 224,7 -> 241,6 |
| 20.09. | 240,4 -> 253,0 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,14 und steigt seit 13 Tagen, Tag 10 über 1.

Das R(t) vom StaBLab der LMU wurde vorgestern aktualisiert, der letzte Nowcast Lauf vom 20.09. liefert ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.09. | 1,04 | 1,11 |
| 16.09. | 1,06 | 1,10 |
| 17.09. | 1,08 | 1,05 |
| 18.09. | 1,09 | 1,04 |
| 19.09. | 1,11 | 1,05 |
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,14 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt: Normalbetten steigt um 7 von gestern 190 auf jetzt 197, IMC stabil bei 10 und Intensivbetten sinkt 19 auf 17. Damit insgesamt 224 Betten belegt, 5 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 197 | +7 | 
| IMC | 10 | +0 | 
| Intensiv | 17 | -2 |
| SUMME | 224 | +5 |

Im Wochenvergleich sind das 18 Betten mehr.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Bei den Viertimpfungen tut sich ein wenig mehr, Betonung auf wenig.  Da gibt es wohl doch langsam Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen.

1. +8
2. +6
3. +55
4. +652

Wir bleiben in München trotzdem unter dem Landesdurchschnitt, besonders klar bei den Drittimpfugen: München 47,99%, Bayern 58,96%.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca. 16.424 Tests/Tag mit einer Positivrate von 25,74%. Gegenüber den Zahlen von letzter Woche (15.293 Tests/Tag mit 21,13% Positivrate) also wieder etwas mehr Tests, gepaart mit steigender Positivrate.

16.424 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 16.424 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 25,74%.](/images/2022/09/20220922-Test.png)

## Wiesnbarometer

Tag 6 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um satte 28,8% auf heute 255,6. Die Zahl der mit COVID-positiven Patienten belegten Betten in den müchner Krankenhäusern steigt im Vergleich zur Vorwoche klar um 8,74% auf jetzt 224.

![Wiesnbarometer 2022-09-22](/images/2022/09/20220922WB.png)