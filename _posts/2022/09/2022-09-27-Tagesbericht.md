---
layout: default
title: 2022-09-27 Tagesbericht
category: Tagesberichte
---

![2022-09-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche schon jetzt drastisch höher. Zweite zeigt Krankenbetten, die weiter deutlich steigen, Dritte zeigt Inzidenz, die jetzt sehr schnell steigt und auch jetzt über 400 liegt.](/images/2022/09/20220927.png)

Das RKI meldet eine Inzidenz von 424,9(!) für München. 1672 Neue, 1237 Nachmeldungen, davon 28 für Sonntag, 962(!) für Samstag, 204 für Freitag. Die Inzidenz von Sonntag korrigiert sich von 310,3 auf 393,2(!), Samstag von 311,1 auf 392,1(!) und Freitag von 311,2 auf 327,6.
<!--more-->

Noch kein neuer Todesfall - die 7-Tage-Summe der Todesfälle ist 5. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 19.09. | 224,7 -> 243,4 |
| 20.09. | 240,4 -> 257,0 |
| 21.09. | 255,6 -> 271,2 |
| 22.09. | 273,8 -> 289,8 |
| 23.09. | 311,2 -> 327,6 |
| 24.09. | 311,1 -> 392,1 |
| 25.09. | 310,3 -> 393,2 |

Bisher wurden die DEMIS Meldungung von Samstag und Sonntag immer einfach am Montag mit gemeldet. Dieses mal jeduch wurden 962 Fälle für Samstag nachgemeldet. Ob das eine Änderung (wir würden eher Korrektur sagen) der Meldepolitik ist wurde offiziell nicht bestätigt. Wir werden das nächsten Mittwoch (weil Feiertag am 3.) erst prüfen können.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,37 und steigt seit 20(!) Tagen, Tag 15 über 1, also exponentielles Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt am 20.09. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,16 | --- |
| 22.09. | 1,19 | --- |
| 23.09. | 1,22 | --- |
| 24.09. | 1,28 | --- |
| 25.09. | 1,33 | --- |
| 26.09. | 1,37 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt jetzt stark an: Normalbetten steigt um 25(!) von gestern 235 auf jetzt 260, IMC steigt von gestern 10 auf jetzt 13 und Intensivbetten steigt von gestern 23 auf jetzt 24. Damit insgesamt 297 Betten belegt, 29 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 260 | +25 | 
| IMC | 10 | +3 | 
| Intensiv | 24 | +1 |
| SUMME | 297 | +29 |

Im Wochenvergleich sind das 68(!) Betten mehr.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Bei den Viertimpfungen tut sich auch kaum noch was. Dabei sollte es doch gerade jetzt Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen geben.

1. +9
2. +9
3. +25
4. +162

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfugen: München 48,02%, Bayern 58,81%.

## Wiesnbarometer

Tag 11 der Wiesn. Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 74,6%(!) auf heute 424,9. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 29,69% auf jetzt 297.

![Wiesnbarometer 2022-09-27](/images/2022/09/20220927WB.png)