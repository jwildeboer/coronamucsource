---
layout: default
title: 2022-09-12 Wochenbericht KW36
category: Wochenberichte
---

![2022-09-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche ähnlich, vielleicht etws niedriger.  Zweite zeigt Krankenbetten, wieder steigende Tendenz, dritte zeigt Inzidenz, jetzt stabil unter 180.](/images/2022/09/20220912.png)

In der letzten Woche gab es insgesamt 2.467 neue COVID Fälle in München, das sind 13% weniger als in der Vorwoche (2.832). Leider auch 4 Tote mit COVID Nachweis in den letzten 7 Tagen, aber deutlich weniger als in der Vorwoche (15).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 2.832 zurück auf 2.467. Die Inzidenz verlor 25,1 Punkte in 7 Tagen. Das Abklingen der Welle setzt sich also noch fort. Die ersten Ferienrückkehrer werden wohl auch noch "Souvenire" mitbringen und dann ist ja auch bald schon Oktoberfest. Somit ist es keine gewagte Prognose zu sagen, daß in dieser Woche der Tiefpunkt wohl erreicht wird. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 04.09. | 8 | 2.832 | 190,3 |
| 05.09. | 852 | 2.764 | 185,7 |
| 06.09. | 624 | 2.758 | 185,3 |
| 07.09. | 466 | 2.716 | 182,5 |
| 08.09. | 353 | 2.667 | 179,2 |
| 09.09. | 164 | 2.476 | 166,4 |
| 10.09. | 0 | 2.467 | 165,8 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das war in der letzten Woche knapp unter 1, mit leicht fallender Tendenz.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war am 09.09.) liegt bei 1,00.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.09. | 0,99 | 0,95 |
| 06.09. | 0,98 | 0,96 |
| 07.09. | 0,97 | 0,99 |
| 08.09. | 0,96 | 1,00 |
| 09.09. | 0,95 | --- |
| 10.09. | 0,94 | --- |
| 11.09. | 0,94 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder ein leichte Steigung bei den Zahlen (+13), insbesondere bei IMC (+4) und Intensivbetten (+5). Im Schnitt jetzt wieder über 200 Betten insgesamt belegt. Im Vergleich zur Vorwoche 22 Betten weniger, aber immer noch mehr als im Vergleich zum Vorjahr.

Im Vergleich zu letzter Woche immerhin noch 0 Betten weniger belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 05.09. | 199 | 6 | 20 | 225 | -31 | 117 |
| 06.09. | 188 | 6 | 19 | 213 | -24 | 141 |
| 07.09. | 182 | 6 | 17 | 205 | -25 | 139 |
| 08.09. | 180 | 3 | 16 | 199 | -27 | 142 |
| 09.09. | 181 | 4 | 17 | 202 | -27 | 152 |
| 10.09. | 183 | 6 | 19 | 208 | -23 | 152 |
| 11.09. | 184 | 7 | 21 | 212 | -22 | 152 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiterhin relativ gut aus. Seit dem 10.08. sind wir wieder unter dem Grenzwert der alten gelben Ampel und die Zahlen sinken weiter kontinuierlich.

In der letzten woche sank die 7 Tages Summe der Hospitaliserungen von 707 auf heute 541. Wir nähern uns damit den Vorjahreszahlen an, die zu dieser Zeit wieder zu steigen begannen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 05.09. | 707 | 591 |
| 06.09. | 684 | 602 |
| 07.09. | 676 | 624 |
| 08.09. | 649 | 637 |
| 09.09. | 591 | 634 |
| 10.09. | 566 | 626 |
| 11.09. | 541 | 642 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Auf Landesebene gab es einen kräftigen Sprung bei den Impfzahlen. Wir haben beim RKI nachgefragt und die [Antwort](https://github.com/robert-koch-institut/COVID-19-Impfungen_in_Deutschland/issues/16) bekommen, daß es zwischen dem 25.08. und 7.9. Probleme mit dem Meldeweg in Bayern gab, die Zahlen aber jetzt wieder stimmen. Somit eine etwas höhere Impfquote bayernweit jetzt, insbesondere bei den Viertimpfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,90% | 75,35% |
| 2. | 71,38% | 75,25% |
| 3. | 47,93% | 58,91% |
| 4. | 6,29% | 6,22% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/09/KW36Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca 15.460 Tests/Tag mit einer Positivrate von 20,98%. Gegenüber den Zahlen von letzter Woche (15.626 Tests/Tag mit 20,70% Positivrate) also wieder minimal weniger Tests, gepaart mit minimal steigender Positivrate.

15.460 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.460 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt minimal auf jetzt 20,98%.](/images/2022/09/20220908-Test.png)

## Straubing

In der vergangenen Woche haben wir uns die Zahlen in Straubing genauer angeschaut. Dort war vom 12.-22.08 das Gäubodenvolksfest. Von vielen als eine Art Generalprobe für die Wiesn betrachtet, aufgrund der Größe (ca 1 Million Besucher) und auch der zeitlichen Nähe zur Wiesn.

Wir haben uns deshalb täglich die Zahlen der Neuinfektionen in Straubing und Landkreis angeschaut. Hier die Summen als Graph vom 01.05.2022 bis zum 10. September.

![Straubing](/images/2022/09/Straubing01.png)

Wie (leider) zu erwarten war, sieht man einen schnellen Anstieg der Infekzionszahlen schon 6 Tage nach der Festeröffnung, am 18.08. Das R(t) ging in der Spitze auf 2,6. Das war doppelt so viel wie in der Juliwelle davor. Nach 15 Tagen war dann die Welle überwunden und am 02.09. wurde die Wachstumsrate wieder negativ.

In dem Zeitraum vom 18.08. bis 02.09. wurden insgesamt 2.128 Infektionen gemeldet. In der Woche danach, also vom 03.09. bis 09.09. "nur" noch 415. Leider konnten wir keine Zahlen zu belegten Normalbetten in den 3 Krankenhäusern von Straubing und Landkreis finden, und die Zahl der belegten Intensivbetten ist noch niedrig. Wir werden das weiter beobachten.

Wohlgemerkt - bei all den genannten Zahlen geht es um die Bevölkerung vor Ort. Wieviele von den auswärtigen Besuchern sich infiziert haben - oder schon infiziert waren - kann man aus den Zahlen nicht ableiten.

## München und die Wiesn

Was bedeutet das für München und die Wiesn? Dort kann man von der 6-fachen Menge an Besucher ausgehen. Ein weiterer Faktor ist, daß zur Wiesn die Sommerferien vorbei sind. Ein R(t) von 2,6 in München würde sehr schnell zu einer Welle mit tausenden von Neuinfektionen pro Tag führen, vielleicht sogar fünfstellig - aber da kaum getestet wird, passiert das dann wohl zum größten Teil als Dunkelziffer - auch weil die vielen Touristen, selbst wenn sie hier getestet werden, nicht in die RKI Statistik für München eingehen - Wohnortprinzip.

Die Politik und die Medien haben sich sehr zurückgehalten mit Bewertungen und Einordnungen. Heute, am 12. September, werden die Betriebsgenehmigungehn für die Wiesnzelte erteilt und es sind weiterhin keinerlei Schutzmaßnahmen geplant.

Noch sieht die Kurve für München halbwegs friedlich aus. Spätestens 6 Tage nach der Eröffnung der Wiesn wird das Bild wohl ganz anders ausschauen.

![München](/images/2022/09/Muenchen01.png)