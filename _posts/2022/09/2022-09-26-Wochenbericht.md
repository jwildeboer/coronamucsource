---
layout: default
title: 2022-09-26 Wochenbericht KW38
category: Wochenberichte
---

![2022-09-26 Dashboard mit den Zahlen für München. Da Montag, keine neuen Zahlen von Stadt oder RKI. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche deutlich höher. Zweite zeigt Krankenbetten, die auch deutlich steigen, Dritte zeigt Inzidenz, die jetzt schnell steigt und auch heute über 300 liegt.](/images/2022/09/20220926.png)

In der letzten Woche gab es insgesamt 4.629 neue COVID Fälle in München, das sind satte 42,3% mehr als in der Vorwoche (3.252). Leider auch 11 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (20).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 3.250 hoch auf 4.629. Die Inzidenz stieg um satte 92,6 Punkte in 7 Tagen, 6 mal mehr als in der letzten Woche. Die neue Welle ist also da und wird in der laufenden Woche wohl ihre volle Wucht entfalten. Wenn sich die Zahlen in München an den Zahlen in anderen Orten mit Volksfesten halten, könnte sich die Inzidenz bis zum näcshten Wochenbericht locker verdoppeln.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 18.09. | 12 | 3.250 | 218,5 |
| 19.09. | 1.194 | 3.613 | 242,9 |
| 20.09. | 875 | 3.806 | 255,8 |
| 21.09. | 795 | 4.012 | 269,7 |
| 22.09. | 877 | 4.272 | 287,2 |
| 23.09. | 876 | 4.630 | 311,2 |
| 24.09. | 0 | 4.629 | 311,1 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich.

Daher berechnen wir parallel "unser" R(t) weiter. Das ist in der letzten Woche stabil weiter gewachsen und liegt jetzt bei 1,22 und damit klar im exponenziellem Bereich.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war am 21.09.) liegt bei 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.09. | 1,11 | 1,05 |
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,16 | --- |
| 22.09. | 1,18 | --- |
| 23.09. | 1,20 | --- |
| 24.09. | 1,21 | --- |
| 25.09. | 1,22 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder ein klare Steigung bei den Zahlen (+42), insbesondere bei den Normalbetten (+33). Seit 2 Tagen wieder über 250 Betten insgesamt belegt. Im Vergleich zur Vorwoche 48 Betten mehr, im Vergleich zum Vorjahr sogar 122 Betten mehr belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 19.09. | 197 | 12 | 20 | 229 | +14 | 150 |
| 20.09. | 190 | 10 | 19 | 219 | +3 | 155 |
| 21.09. | 197 | 10 | 17 | 224 | +18 | 159 |
| 22.09. | 202 | 7 | 17 | 226 | +24 | 154 |
| 23.09. | 213 | 8 | 18 | 239 | +31 | 146 |
| 24.09. | 224 | 9 | 21 | 254 | +41 | 146 |
| 25.09. | 235 | 10 | 23 | 268 | +48 | 146 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

Und wir gehen davon aus, daß bei einigen Patienten von der Wiesn auch eher zufällig ein positiver COVID Test auftauchte.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es durchwachsen aus. Seit dem 10.08. sind wir zwar wieder unter dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche, die Zahlen steigen jetzt aber wieder langsam. Der Hospitalisierungsnowcast des RKI sieht die 1200er Grenze aber schon bald durch Nachmeldungen erreicht.

In der letzten woche sank die 7 Tages Summe der Hospitaliserungen leicht von 972 auf jetzt 869. Und sind damit trotzdem klar um ca 35% über den Vorjahreszahlen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 19.09. | 972 | 634 |
| 20.09. | 953 | 637 |
| 21.09. | 972 | 634 |
| 22.09. | 980 | 626 |
| 23.09. | 933 | 642 |
| 24.09. | 901 | 655 |
| 25.09. | 869 | 647 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Landesweit gab es einen weiteren Schub bei den Viertimpfungen. Es scheint, daß die neuen Omikron-angepassten Bosster ganz gut angenommen werden. Im Vergleich zur Vorwoche sind die Impfquoten in Bayern etwas gesunken, in München sehr geringfügig höher. Das ist ein Resultat der Bevölkerungszahl, die vom RKI aktualisiert wurde. Damit hat Bayern mehr, München etwas weniger Einwohner im Vergleich zum Vorjahr.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,94% | 75,14% |
| 2. | 71,42% | 75,05% |
| 3. | 48,01% | 58,81% |
| 4. | 6,84% | 6,64% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/09/KW38Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca. 16.424 Tests/Tag mit einer Positivrate von 25,74%. Gegenüber den Zahlen von letzter Woche (15.293 Tests/Tag mit 21,13% Positivrate) also wieder etwas mehr Tests, gepaart mit steigender Positivrate.

16.424 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 16.424 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 25,74%.](/images/2022/09/20220922-Test.png)

## Varianten

Die LMU betreibt jetzt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Aktuell eigentlich ganz einfach: Fast alles BA.5. Vereinzelt BA.2.75, noch kein BJ.1, BQ.1, B2.75.2.

| Variante |	KW 35 |	KW 36 | KW 37 | KW38 |
|:--|--:|--:|--:|--:|
| Andere | 0.0 % | 1.0 % | 0.0 % | 0.0% |
| Omikron BA.2 | 1.0 % | 1.0 % | 0.0 % | 1.0% |
| Omikron BA.4 | 3.0 % | 3.0 % | 3.0 % | 1.0% |
| Omikron BA.5 | 95.0 % | 96.0 % | 97.0 % | 97.0% |

![VoC](/images/2022/09/KW38Var.png)

## Wiesnbarometer

Tag 10 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 42,1%(!) auf heute 310,3., allerdinges ohne Meldungen am Sonntag und Montag. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche deutlich um 21,82% auf jetzt 268.

Bis Dienstag keine neuen Fallzahlen und daher auch keine aktualisierten Inzidenzen wegen Meldepause am Wochenende.

![Wiesnbarometer 2022-09-26](/images/2022/09/20220926WB.png)