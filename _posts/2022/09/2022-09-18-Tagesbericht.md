---
layout: default
title: 2022-09-18 Tagesbericht
category: Tagesberichte
---

![2022-09-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche leichte Aufwärtstendenz. Zweite zeigt Krankenbetten, am Sonntag keine neuen Zahlen. Dritte zeigt Inzidenz, die jetzt wieder stabil über 200 ist.](/images/2022/09/20220918.png)

Das RKI meldet eine Inzidenz von 204,5 für München, da Sonntag allerdings ohne Neu- oder Nachmeldungen. 
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 09.09. | 166,4 -> 188,5 |
| 10.09. | 165,8 -> 188,3 |
| 11.09. | 165,2 -> 188,8 |
| 12.09. | 179,2 -> 186,3 |
| 13.09. | 183,6 -> 189,8 |
| 14.09. | 187,7 -> 196,7 |
| 15.09. | 199,4 -> 209,0 |

Da am Wochenende keine Zahlen an das LGL/RKI gemeldet werden, gibt es erst am Dienstag wieder neue Fallzahlen. Immerhin können wir schon mal die Inzidenz für Montag berechnen:

| Mo, 19.09. | 203,5 |

Am Montag ist somit schon jetzt sicher Tag 4 mit Inzidenz über 200.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,05 und steigt damit seit 10 Tagen, Tag 7 über 1.

Das R(t) vom StaBLab der LMU wurde in dieser Woche gar nicht aktualisiert, der letzte Nowcast Lauf vom 10.09. liefert ein R(t) von 0,96 für den 09.09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 11.09. | 1,00 | --- |
| 12.09. | 1,00 | --- |
| 13.09. | 1,01 | --- |
| 14.09. | 1,03 | --- |
| 15.09. | 1,04 | --- |
| 16.09. | 1,05 | --- |
| 17.09. | 1,05 | --- |

## Krankenhaus

Am Sonntag gibt es von der Stadt keinen neuen Krankenhauszahlen.

## Impfungen

Am Sonntag gibt es von der Stadt keinen neuen Impfzahlen.

## Oktoberfest

Am Samstag wurde das 187. Oktoberfest eröffnet und auch wenn laut Polizei der Andrang etwas geringer war als beim letzten Fest in 2019, so waren doch sehr viele Besucher da um den ersten Tag zu erleben.

Da es keinerlei Schutzmaßnahmen gibt, darf man bei einer Inzidenz um 200 davon ausgehen, daß in den größeren Zelten jederzeit, statistisch gesehen, um 20 Menschen infiziert sind und Viren verbreiten.

Gestern war das Wetter nicht gerade sonnig, damit war die Verweildauer in den Zelten höher. Bei einer Inkubationszeit von 3-4 Tagen können wir also damit rechnen, ab Mittwoch/Donnerstag spätestens den Anfang der Wiesnwelle in den RKI Zahlen zu sehen.

Basierend auf den Erfahrungen aus Straubing und anderen Orten müssen wir einen schnellen Anstieg mit einem R(t) von 2-3 einkalkulieren. Bei den Zahlen ist aber auch zu berücksichtigen, daß immer weniger zum PCR Test gehen (obwohl der weiterhin kostenlos ist nach positivem Selbsttest) und viele Infizierte auch nicht aus München kommen und daher in den Zahlen nicht erfasst werden.