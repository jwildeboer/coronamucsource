---
layout: default
title: 2022-09-08 Tagesbericht
category: Tagesberichte
---

![2022-09-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche kaum Unterschied. Zweite zeigt Krankenbetten, insgesamt 8 Betten weniger belegt als gestern, dritte zeigt Inzidenz, die jetzt seit 3 Tagen unter 190 ist.](/images/2022/09/20220908.png)

Das RKI meldet eine Inzidenz von 176,3 für München, 389 Neue, 154 Nachmeldungen, davon 144 für vorgestern. Die gestern genannte Inzidenz von 174,1 erhöht sich auf 184,3. Tag 3 unter 190.
<!--more-->

leider auch 2 neue Todesfälle - die 7-Tage-Summe der Todesfälle ist damit 5. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 2.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 31.08. | 189,1 -> 200,2 |
| 01.09. | 183,8 -> 191,0 |
| 02.09. | 182,8 -> 189,3 |
| 03.09. | 182,8 -> 189,8 |
| 04.09. | 182,8 -> 190,4 |
| 05.09. | 179,2 -> 185,4 |
| 06.09. | 174,1 -> 184,3 |

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,96.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 07.09. liefert ein R(t) von 0,99.

| Datum | R(t) | LMU |
|---|---:|---:|
| 01.09. | 0,94 | 1,00 |
| 02.09. | 0,98 | 1,00 |
| 03.09. | 1,00 | 0,95 |
| 04.09. | 1,00 | 0,93 |
| 05.09. | 0,99 | 0,93 |
| 06.09. | 0,98 | 0,95 |
| 07.09. | 0,96 | 0,99 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Normalbetten sinken um 6 von gestern 188 auf jetzt 182, IMC bleibt auf 6 und Intensivbetten sinkt von 19 auf 17. Damit insgesamt 205 Betten belegt, 8 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 182 | -6 | 
| IMC | 6 | +0 | 
| Intensiv | 17 | -2 |
| SUMME | 205 | -8 |

Im Wochenvergleich somit 25 Betten weniger belegt. 

## Impfungen

Die Impfzahlen dümpeln weiter vor sich hin. Sich nochmal vor der Wiesn zu boostern scheint keine hohe Priorität zu haben. Auch wenn die neuen Omikron Booster jetzt im Impfzentrum vorhanden sind. 

1. +25
2. +26
3. +78
4. +214

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 15.460 Tests/Tag mit einer Positivrate von 20,98%. Gegenüber den Zahlen von letzter Woche (15.626 Tests/Tag mit 20,70% Positivrate) also wieder minimal weniger Tests, gepaart mit minimal steigender Positivrate.

15.460 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.460 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt minimal auf jetzt 20,98%.](/images/2022/09/20220908-Test.png)