---
layout: default
title: 2022-09-20 Tagesbericht
category: Tagesberichte
---

![2022-09-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche klar höher. Zweite zeigt Krankenbetten, insgesamt 4 Betten weniger belegt als gestern. Dritte zeigt Inzidenz, die jetzt schnell steigt und auf die 230 zugeht.](/images/2022/09/20220920.png)

Das RKI meldet eine Inzidenz von 224,7 für München, 951 Neue und 185 Nachmeldungen, davon 12 für Sonntag, 1 für Samstag, 154 für Freitag. Die gestern genannte Inzidenz von 203,5 erhöht sich durch Nachmeldungen auf 224,7. Sonntag von 204,5 auf 215,8, Samstag von 204,9 auf 216,2, Freitag von 199,4 auf 210,0. Tag 5 über 200.
<!--more-->

Leider auch 6 neue Todesfälle - die 7-Tage-Summe der Todesfälle geht damit auf 18. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 12.09. | 175,7 -> 186,3 |
| 13.09. | 183,6 -> 190,0 |
| 14.09. | 187,7 -> 197,1 |
| 15.09. | 199,4 -> 210,0 |
| 16.09. | 204,9 -> 216,2 |
| 17.09. | 204,5 -> 215,8 |
| 18.09. | 203,5 -> 215,7 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,09 und steigt seit 11 Tagen, Tag 8 über 1.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert, der letzte Nowcast Lauf vom 19.09. liefert ein R(t) von 0,96.

| Datum | R(t) | LMU |
|---|---:|---:|
| 13.09. | 1,00 | 1,02 |
| 14.09. | 1,01 | 1,07 |
| 15.09. | 1,03 | 1,09 |
| 16.09. | 1,05 | 1,07 |
| 17.09. | 1,07 | 1,01 |
| 18.09. | 1,08 | 0,97 |
| 19.09. | 1,09 | 0,96 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter: Normalbetten steigt um 6 von gestern 191 auf jetzt 197, IMC steigt von 11 auf 12 und Intensivbetten von 18 auf 20. Damit insgesamt 229 Betten belegt, 9 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 197 | +6 | 
| IMC | 12 | +1 | 
| Intensiv | 20 | +2 |
| SUMME | 229 | +9 |

Im Wochenvergleich jetzt 14 Betten mehr.

## Impfungen

Die Impfungen laufen weiter auf Sparflamme. Eine leichte Zunahme im Vergleich zur Vorwoche bei den Viertimpfungen. Da gibt es wohl Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen.

1. +11
2. +8
3. +38
4. +399

Wir bleiben in München trotzdem unter dem Landesdurchschnitt, besonders klar bei den Drittimpfugen: München 47,97%, Bayern 58,95%.

![Neue Impfstoffe](/images/2022/09/20220914NewVacc.png)

Die Stadt München hat ab sofort die neuen Omikron-spezifischen Booster von BioNTech und Moderna und einen neuen Totimpfstoff im Angebot. [Mehr Informationen](https://ru.muenchen.de/2022/176/Drei-neue-Impfstoffe-in-den-Impfstellen-der-Stadt-verfuegbar-103143)