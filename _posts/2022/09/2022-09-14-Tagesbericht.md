---
layout: default
title: 2022-09-14 Tagesbericht
category: Tagesberichte
---

![2022-09-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche kaum Unterschied. Zweite zeigt Krankenbetten, insgesamt 1 Bett mehr belegt als gestern, deutlich mehr Intensiv- und IMC Betten. Dritte zeigt Inzidenz, die jetzt seit 9 Tagen knapp zwischen 180 und 190 pendelt.](/images/2022/09/20220914.png)

Das RKI meldet eine Inzidenz von 183,6 für München, 593 Neue und 151 Nachmeldungen, davon 138 für vorgestern. Die gestern genannte Inzidenz von 175,7 erhöht sich damit auf 185,8.
<!--more-->

Leider auch 1 neuer Todesfall - die 7-Tage-Summe der Todesfälle steigt damit auf 13. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 06.09. | 174,1 -> 185,9 |
| 07.09. | 176,3 -> 183,8 |
| 08.09. | 178,5 -> 184,8 |
| 09.09. | 166,4 -> 188,8 |
| 10.09. | 165,8 -> 188,5 |
| 11.09. | 165,2 -> 189,0 |
| 12.09. | 179,2 -> 185,8 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,00 und steigt leicht seit 5 Tagen.

Das R(t) vom StaBLab der LMU wurde nicht aktualisiert, der letzte Nowcast Lauf vom 10.09. liefert ein R(t) von 0,96 für den 09.09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 07.09. | 0,97 | 0,97 |
| 08.09. | 0,96 | 0,99 |
| 09.09. | 0,97 | 0,96 |
| 10.09. | 0,98 | --- |
| 11.09. | 0,99 | --- |
| 12.09. | 1,00 | --- |
| 13.09. | 1,00 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt minimal, aber deutliche Verschiebungen Richtung Intensiv/IMC: Normalbetten sinken um 6 von gestern 182 auf jetzt 176, IMC steigt von 10 auf 13 und Intensivbetten steigt von 23 auf 27. Damit insgesamt 216 Betten belegt, 1 Bett mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 176 | -6 | 
| IMC | 13 | +3 | 
| Intensiv | 27 | +4 |
| SUMME | 216 | +1 |

Im Wochenvergleich damit zum ersten mal seit dem 31.07. 3 Betten **mehr** belegt als in der Vorwoche. 

## Impfungen

Die Impfungen laufen weiter auf Sparflamme. Was doch ein wenig erstaunlich ist: die Quote bei den Dritt-/Boosterimpfungen liegt in München bei nur 47,95%. Landesweit sind wir bei 58,92%. Das ist ein deutlicher Unterschied. München liegt bei den Drittimpfungen sogar unter Sachsen mit 49,9%.

1. +10
2. +24
3. +120
4. +620

Wir bleiben in München also weiter unter dem Landesdurchschnitt.

![Neue Impfstoffe](/images/2022/09/20220914NewVacc.png)

Die Stadt München hat ab sofort die neuen Omikron-spezifischen Booster von BioNTech und Moderna im Angebot. [Mehr Informationen](https://ru.muenchen.de/2022/176/Drei-neue-Impfstoffe-in-den-Impfstellen-der-Stadt-verfuegbar-103143)