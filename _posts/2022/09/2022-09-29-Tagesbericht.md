---
layout: default
title: 2022-09-29 Tagesbericht
category: Tagesberichte
---

![2022-09-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher. Zweite zeigt Krankenbetten, die weiter deutlich steigen, Dritte zeigt Inzidenz, die jetzt klar exponentiell steigt und heute bei fast 700 liegt.](/images/2022/09/20220929.png)

Das RKI meldet eine Inzidenz von 695,8(!) für München. 2442 Neue, 585 Nachmeldungen, davon 521 für vorgestern. Die gestern genannte Inzidenz von 547,0 erhöht sich auf 585,6. 
<!--more-->

Keine neuen Todesfälle gemeldet - die 7-Tage-Summe der Todesfälle ist 9. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 21.09. | 255,6 -> 271,8 |
| 22.09. | 273,8 -> 290,4 |
| 23.09. | 311,2 -> 328,6 |
| 24.09. | 311,1 -> 393,8 |
| 25.09. | 310,3 -> 394,8 |
| 26.09. | 424,9 -> 446,2 |
| 27.09. | 547,0 -> 585,6 |

So schaut exponentielles Wachstum aus.

## Umland

Inzidenz im Umland von München.

| Kreis | 29.09. |
|:---|---:|
| LK Dachau | 684,5 |
| LK Ebersberg | 817,0 |
| LK Fürstenfeldbruck | 725,6 |
| LK München | 693,5 |
| LK Starnberg | 719,6 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,56 und steigt seit 22(!) Tagen, Tag 17 über 1, also exponentielles Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt am 20.09. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 22.09. | 1,19 | --- |
| 23.09. | 1,22 | --- |
| 24.09. | 1,28 | --- |
| 25.09. | 1,33 | --- |
| 26.09. | 1,39 | --- |
| 27.09. | 1,48 | --- |
| 28.09. | 1,56 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter stark an: Normalbetten steigt wieder um 22 von gestern 285 auf jetzt 307, IMC sinkt von gestern 15 auf jetzt 13. Intensivbetten steigt von gestern 23 auf jetzt 24. Damit insgesamt 343 Betten belegt, 20 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 307 | +22 | 
| IMC | 12 | -3 | 
| Intensiv | 24 | +1 |
| SUMME | 343 | +20 |

Im Wochenvergleich sind das 119(!) Betten mehr. Einen dreistelligen Anstieg hatten wir zuletzt mit 111 am 29.01.2022.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Bei den Viertimpfungen tut sich auch kaum noch was. Dabei sollte es doch gerade jetzt Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen geben.

1. +18
2. +9
3. +72
4. +369

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,05%, Bayern 58,84%.

## Änderung des Status "vollständig geimpft"

Ab dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca. 17.930 Tests/Tag mit einer Positivrate von 28,34%. Gegenüber den Zahlen von letzter Woche (16.994 Tests/Tag mit 25,24% Positivrate) also wieder etwas mehr Tests, gepaart mit steigender Positivrate.

17.930 Tests/Tag während der Wiesn ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter dann auch in Anspruch nehmen sollten.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 17.930 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 28,34%.](/images/2022/09/20220929-Test.png)

## Wiesnbarometer

Tag 13 der Wiesn. Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 156,0%(!) auf heute 695,8. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 53,13% auf jetzt 343.

![Wiesnbarometer 2022-09-29](/images/2022/09/20220929WB.png)