---
layout: default
title: 2022-09-30 Tagesbericht
category: Tagesberichte
---

![2022-09-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher. Zweite zeigt Krankenbetten, die weiter deutlich steigen, Dritte zeigt Inzidenz, die jetzt klar exponentiell steigt und heute bei fast 800 liegt.](/images/2022/09/20220930.png)

Das RKI meldet eine Inzidenz von 792,8(!) für München. 1672 Neue, 677 Nachmeldungen, davon 616 für vorgestern. Die gestern genannte Inzidenz von 695,8 erhöht sich auf 740,9. Tag 2 über Inzidenz 700. 
<!--more-->

Keine neuen Todesfälle gemeldet - die 7-Tage-Summe der Todesfälle ist 8. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle auch 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 22.09. | 273,8 -> 290,8 | 
| 23.09. | 311,2 -> 329,0 |
| 24.09. | 311,1 -> 394,6 |
| 25.09. | 310,3 -> 395,6 |
| 26.09. | 424,9 -> 447,7 |
| 27.09. | 547,0 -> 589,5 |
| 28.09. | 695,8 -> 740,9 |

So schaut exponentielles Wachstum aus.

Es könnte sein, daß die Contact Tracer der Stadt an die Kapazitätsgrenze stoßen und daher jetzt wieder vermehrt Meldeverzögerungen kommen werden. Die Zahlen von heute lassen das vermuten.

Dazu dann das verlängerte Wochenende wo es wohl trotz der dramatischen Lage mit einem R(t) von fast 1,7 bis Mittwoch keine neuen Fallzahlen und Inzidenz beim RKI geben wird - das ist schon eine gefährliche Mischung.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 30.09. | 29.09. | Änderung |
|:---|---:|
| LK Dachau | 777,7 | 684,5 | +13,6% |
| LK Ebersberg | 913,8 | 817,0 | +11,8% |
| LK Fürstenfeldbruck | 909,5 | 725,6 | +25,3% |
| LK München | 839,0 | 693,5 | +21,0% |
| LK Starnberg | 861,4 | 719,6 | +19,7% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,69 und steigt seit 23(!) Tagen, Tag 3 über 1,5, also klar exponentielles Wachstum.

Das R(t) vom StaBLab der LMU wird voraussichtlich erst nach der Wiesn wieder aktualisiert - Grund sind "technische Probleme".

| Datum | R(t) | LMU |
|---|---:|---:|
| 23.09. | 1,22 | --- |
| 24.09. | 1,28 | --- |
| 25.09. | 1,33 | --- |
| 26.09. | 1,39 | --- |
| 27.09. | 1,49 | --- |
| 28.09. | 1,59 | --- |
| 29.09. | 1,69 | --- |

## Krankenhaus

Die Zahl der mit COVID-positiven Patienten belegten Krankenhausbetten insgesamt steigt weiter stark an: Normalbetten steigt wieder um 37 von gestern 307 auf jetzt 344, IMC sinkt von gestern 12 auf jetzt 9. Intensivbetten sinkt von gestern 24 auf jetzt 23. Damit insgesamt 376 Betten belegt, 33 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 344 | +37 | 
| IMC | 9 | -3 | 
| Intensiv | 23 | -1 |
| SUMME | 343 | +33 |

Im Wochenvergleich sind das 150(!) Betten mehr.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Bei den Viertimpfungen tut sich auch kaum noch was. Dabei sollte es doch gerade jetzt Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen geben.

1. +10
2. +14
3. +37
4. +292

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,05%, Bayern 58,85%.

## Änderung des Status "vollständig geimpft"

Ab dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 14 der Wiesn. Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 172,7%(!) auf heute 792,8. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 66,37% auf jetzt 376.

![Wiesnbarometer 2022-09-30](/images/2022/09/20220930WB.png)