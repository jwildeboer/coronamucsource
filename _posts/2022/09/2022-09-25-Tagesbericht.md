---
layout: default
title: 2022-09-25 Tagesbericht
category: Tagesberichte
---

![2022-09-25 Dashboard mit den Zahlen für München. Da Sonntag, keine neuen Zahlen von Stadt oder RKI. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche deutlich höher. Zweite zeigt Krankenbetten, die auch steigen, Sonntag ohnne neune Zahlen. Dritte zeigt Inzidenz, die jetzt schnell steigt und auch heute über 300 liegt.](/images/2022/09/20220925.png)

Das RKI meldet eine Inzidenz von 311,1 für München, da Sonntag allerdings ohne Neu- oder Nachmeldungen. 
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 16.09. | 204,9 -> 218,9 |
| 17.09. | 204,5 -> 218,6 |
| 18.09. | 203,5 -> 218,5 |
| 19.09. | 224,7 -> 242,9 |
| 20.09. | 240,4 -> 255,8 |
| 21.09. | 255,6 -> 269,7 |
| 22.09. | 273,8 -> 287,2 |

Da am Wochenende keine Zahlen an das LGL/RKI gemeldet werden, gibt es erst am Dienstag wieder neue Fallzahlen. Immerhin können wir schon mal die Inzidenz für Montag berechnen:

| Mo, 26.09. | 310,3 |

Am Montag ist somit schon jetzt sicher Tag 3 mit Inzidenz über 300.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,20 und steigt seit 15 Tagen, Tag 12 über 1.

Das R(t) vom StaBLab der LMU wurde zuletzt am Mittwoch aktualisiert, der letzte Nowcast Lauf vom 20.09. liefert ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.09. | 1,09 | 1,04 |
| 19.09. | 1,11 | 1,05 |
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,16 | --- |
| 22.09. | 1,18 | --- |
| 23.09. | 1,20 | --- |
| 24.09. | 1,21 | --- |

## Krankenhaus

Am Sonntag leider keinen neuen Zahlen zur Krankenhausbelegung der Stadt München.

## Impfungen

Am Sonntag leider keine neuen Impfzahlen von der Stadt München.

## Wiesnbarometer

Tag 9 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 42,3%(!) auf heute 311,1. Die Zahl der mit COVID-positiven Patienten belegten Betten in den münchner Krankenhäusern (Stand 2022-09-23) steigt im Vergleich zur Vorwoche klar um 11,88% auf jetzt 226.

Bis Montag keine neuen Krankenhauszahlen. Bis Dienstag keine neuen Fallzahlen und daher auch keine aktualisierten Inzidenzen wegen Meldepause am Wochenende.

![Wiesnbarometer 2022-09-24](/images/2022/09/20220925WB.png)