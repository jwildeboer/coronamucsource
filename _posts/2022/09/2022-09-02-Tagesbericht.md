---
layout: default
title: 2022-09-02 Tagesbericht
category: Tagesberichte
---

![2022-09-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche jetzt etwas höhere Tendenz erkennbar. Zweite zeigt Krankenbetten, insgesamt 4 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches seit 2 Wochen um 200 pendelt.](/images/2022/09/20220902.png)

Das RKI meldet eine Inzidenz von 183,8 für München, 324 neue und 133 Nachmeldungen, davon 113 für vorgestern. Die gestern gemeldete Inzidenz steigt damit von 189,1 auf 198,0.
<!--more-->

Keine neuen Todesfälle - die 7-Tage-Summe der Todesfälle bleibt bei 19. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 25.08. | 186,7 -> 196,3 | 
| 26.08. | 183,9 -> 188,8 | 
| 27.08. | 183,8 -> 188,8 | 
| 28.08. | 183,8 -> 188,8 | 
| 29.08. | 188,7 -> 200,0 | 
| 30.08. | 187,8 -> 198,1 | 
| 31.08. | 189,1 -> 198,0 | 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 2 Tagen stabil bei 0,93.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der letzte NowCast Lauf vom 01.09. liefert ein R(t) von 0,93.

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.08. | 1,00 | 1,10 |
| 27.08. | 0,96 | 1,02 |
| 28.08. | 0,95 | 0,93 |
| 29.08. | 0,92 | 0,88 |
| 30.08. | 0,92 | 0,88 |
| 31.08. | 0,93 | 0,93 |
| 01.09. | 0,93 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt wieder ein wenig: Normalbetten sinken um 3 von gestern 204 auf jetzt 201, IMC sinkt von 10 auf 8 und Intensivbetten steigt von 16 auf 17. Damit insgesamt 226 Betten belegt, 4 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 201 | -3 | 
| IMC | 8 | -2 | 
| Intensiv | 17 | +1 |
| SUMME | 226 | -4 |

Im Wochenvergleich somit 25 Betten weniger belegt. 

## Impfungen

Die Impfzahlen bewegen sich weiterhin kaum, bis auf die 4. Impfungen, wo sich zumindest ein bisschen was tut und München mit 6,14% sogar über dem Landesdurchschnitt von 6,03% liegt. 

1. +19
2. +6
3. +62
4. +235

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.