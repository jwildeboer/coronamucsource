---
layout: default
title: 2022-09-01 Tagesbericht
category: Tagesberichte
---

![2022-09-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche vielleicht etwas niedriger. Zweite zeigt Krankenbetten, insgesamt 7 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches seit 2 Wochen um 200 pendelt.](/images/2022/09/20220901.png)

Das RKI meldet eine Inzidenz von 189,1 für München. 381 Neue, 135 Nachmeldungen, davon 112 für vorgestern. Die Inzidenz von gestern steigt durch die Nachmeldungen von 187,8 auf 196,7.
<!--more-->

Leider wieder 1 neuer Todesfall, die 7-Tages-Summe der Todesfälle ist damit 19. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 24.08. | 192,3 -> 202,2 | 
| 25.08. | 186,7 -> 196,3 | 
| 26.08. | 183,9 -> 188,6 | 
| 27.08. | 183,8 -> 188,6 | 
| 28.08. | 183,8 -> 188,5 | 
| 29.08. | 188,7 -> 199,4 | 
| 30.08. | 187,8 -> 196,7 | 

## Reproduktionswert R(t) für München

Unser R(t) ist seit 3 Tagen stabil bei 0,92.

Das R(t) vom StaBLab der LMU wurde gestern nicht aktualisiert. Der letzte NowCast Lauf war am 30.08 und liefert ein R(t) von 0,87.

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.08. | 1,07 | 1,07 |
| 26.08. | 1,00 | 1,00 |
| 27.08. | 0,96 | 0,91 |
| 28.08. | 0,95 | 0,86 |
| 29.08. | 0,92 | 0,87 |
| 30.08. | 0,92 | --- |
| 31.08. | 0,92 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt wieder: Normalbetten sinken um 5 von gestern 209 auf jetzt 204, IMC steigt von 9 auf 10 und Intensivbetten sinkt von 19 auf 16. Damit insgesamt 230 Betten belegt, 7 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 204 | -5 | 
| IMC | 10 | +1 | 
| Intensiv | 16 | -3 |
| SUMME | 230 | -7 |

Im Wochenvergleich somit 20 Betten weniger. 

## Impfungen

Die Impfzahlen bewegen sich weiterhin kaum, bis auf die 4. Impfungen, wo sich zumindest ein bisschen was tut und München mit 6,13% sogar über dem Landesdurchschnitt von 5.99% liegt. Bei den Erstimpfungen haben wir nach 89 Tagen den Sprung von 72,80% (04.06.2022) auf 72,9% geschafft. 

1. +23
2. +18
3. +74
4. +325

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 15.405 Tests/Tag mit einer Positivrate von 20,87%. Gegenüber den Zahlen von letzter Woche (15.217 Tests/Tag mit 21,76% Positivrate) also wieder minimal mehr Tests, gepaart mit leicht sinkender Positivrate.

15.405 Tests/Tag ist sehr wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.405 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 20,87%.](/images/2022/09/20220901-Test.png)