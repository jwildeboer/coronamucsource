---
layout: default
title: 2022-09-16 Tagesbericht
category: Tagesberichte
---

![2022-09-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche leichte Aufwärtstendenz. Zweite zeigt Krankenbetten, insgesamt 4 Betten weniger belegt als gestern. Dritte zeigt Inzidenz, die jetzt wieder auf die 200 zugeht.](/images/2022/09/20220916.png)

Das RKI meldet eine Inzidenz von 199,4 für München, 470 Neue und 125 Nachmeldungen, davon 119 für vorgestern. Die gestern genannte Inzidenz von 187,7 erhöht sich damit auf 195,9. Tag 2 über 190.
<!--more-->

Leider auch 7 neue Todesfälle - die 7-Tage-Summe der Todesfälle geht damit auf 19. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 08.09. | 178,5 -> 184,5 |
| 09.09. | 166,4 -> 188,4 |
| 10.09. | 165,8 -> 188,2 |
| 11.09. | 165,2 -> 188,6 |
| 12.09. | 179,2 -> 186,2 |
| 13.09. | 183,6 -> 189,8 |
| 14.09. | 187,7 -> 195,9 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,02 und steigt leicht seit 6 Tagen, Tag 4 über 1.

Das R(t) vom StaBLab der LMU wurde nicht aktualisiert, der letzte Nowcast Lauf vom 10.09. liefert ein R(t) von 0,96 für den 09.09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.09. | 0,97 | 0,96 |
| 10.09. | 0,98 | --- |
| 11.09. | 0,99 | --- |
| 12.09. | 1,00 | --- |
| 13.09. | 1,00 | --- |
| 14.09. | 1,01 | --- |
| 15.09. | 1,02 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute leicht: Normalbetten sinkt um 3 von gestern 177 auf jetzt 174, IMC unverändert bei 9 und Intensivbetten sinkt von 20 auf 19. Damit insgesamt 202 Betten belegt, 4 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 174 | -3 | 
| IMC | 9 | +0 | 
| Intensiv | 19 | -1 |
| SUMME | 202 | -4 |

Im Wochenvergleich aber trotzdem 3 Betten mehr.

## Impfungen

Die Impfungen laufen weiter auf Sparflamme. Was doch ein wenig erstaunlich ist: die Quote bei den Dritt-/Boosterimpfungen liegt in München bei nur 47,95%. Landesweit sind wir bei 58,92%. Das ist ein deutlicher Unterschied. München liegt bei den Drittimpfungen sogar unter Sachsen mit 49,9%.

1. +12
2. +16
3. +78
4. +393

Wir bleiben in München also weiter unter dem Landesdurchschnitt.

![Neue Impfstoffe](/images/2022/09/20220914NewVacc.png)

Die Stadt München hat ab sofort die neuen Omikron-spezifischen Booster von BioNTech und Moderna und einen neuen Totimpfstoff im Angebot. [Mehr Informationen](https://ru.muenchen.de/2022/176/Drei-neue-Impfstoffe-in-den-Impfstellen-der-Stadt-verfuegbar-103143)