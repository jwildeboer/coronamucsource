---
layout: default
title: 2022-09-19 Wochenbericht KW37
category: Wochenberichte
---

![2022-09-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche leichte Aufwärtstendenz. Zweite zeigt Krankenbetten, klare Steigung, insbesondere bei den Normalbetten. Dritte zeigt Inzidenz, die jetzt wieder stabil über 200 ist.](/images/2022/09/20220919.png)

In der letzten Woche gab es insgesamt 3.043 neue COVID Fälle in München, das sind 8,6% mehr als in der Vorwoche (2.803). Leider auch 20 Tote mit COVID Nachweis in den letzten 7 Tagen, deutlich mehr als in der Vorwoche (4).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 2.809 hoch auf 3.043. Die Inzidenz stieg um 15,7 Punkte in 7 Tagen. Die neue Welle ist also da und die Wiesn ist in den Zahlen der letzten Woche noch gar nicht erfasst. Wenn sich die Zahlen in München an den Zahlen in anderen Orten mit Volksfesten halten, könnte sich die Inzidenz bis zum näcshten Wochenbericht locker verdoppeln.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 11.09. | 14 | 2.809 | 188,8 |
| 12.09. | 817 | 2.772 | 186,3 |
| 13.09. | 677 | 2.825 | 189,8 |
| 14.09. | 581 | 2.928 | 196,7 |
| 15.09. | 600 | 3.110 | 209,0 |
| 16.09. | 354 | 3.049 | 204,9 |
| 17.09. | 0 | 3.043 | 204,5 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das ist jetzt stabil über 1 mit steigender Tendenz.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war am 10.09.) liegt bei 0,96.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.09. | 1,00 | --- |
| 13.09. | 1,01 | --- |
| 14.09. | 1,03 | --- |
| 15.09. | 1,04 | --- |
| 16.09. | 1,05 | --- |
| 17.09. | 1,05 | --- |
| 18.09. | 1,05 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder ein klare Steigung bei den Zahlen (+14), insbesondere bei den Normalbetten (+17). Im Schnitt jetzt wieder klar über 200 Betten insgesamt belegt. Im Vergleich zur Vorwoche 8 Betten mehr, im Vergleich zum Vorjahr sogar 74.

Im Vergleich zu letzter Woche immerhin noch 0 Betten weniger belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 12.09. | 182 | 10 | 23 | 215 | -10 | 162 |
| 13.09. | 176 | 13 | 27 | 216 | +3 | 156 |
| 14.09. | 177 | 9 | 20 | 206 | +1 | 154 |
| 15.09. | 174 | 9 | 19 | 202 | +3 | 145 |
| 16.09. | 179 | 10 | 19 | 208 | +6 | 146 |
| 17.09. | 185 | 10 | 18 | 213 | +5 | 146 |
| 18.09. | 191 | 11 | 18 | 220 | +8 | 146 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es durchwachsen aus. Seit dem 10.08. sind wir zwar wieder unter dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche, die Zahlen sind aber volatil.

In der letzten woche sank die 7 Tages Summe der Hospitaliserungen von 707 auf heute 664. Und damit klar über den Vorjahreszahlen und im Vergleich zur Vorwoche eine Steigerung von 22,74%.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 12.09. | 707 | 655 |
| 13.09. | 738 | 647 |
| 14.09. | 748 | 625 |
| 15.09. | 742 | 617 |
| 16.09. | 711 | 615 |
| 17.09. | 684 | 579 |
| 18.09. | 664 | 559 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Auf Landesebene gab es einen kräftigen Sprung bei den Impfzahlen. Wir haben beim RKI nachgefragt und die [Antwort](https://github.com/robert-koch-institut/COVID-19-Impfungen_in_Deutschland/issues/16) bekommen, daß es zwischen dem 25.08. und 7.9. Probleme mit dem Meldeweg in Bayern gab, die Zahlen aber jetzt wieder stimmen. Somit eine etwas höhere Impfquote bayernweit jetzt, insbesondere bei den Viertimpfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,91% | 75,35% |
| 2. | 71,39% | 75,25% |
| 3. | 47,96% | 58,94% |
| 4. | 6,47% | 6,44% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/09/KW37Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei 14.552 Tests/Tag mit einer Positivrate von 21,95%. Gegenüber den Zahlen von letzter Woche (15.464 Tests/Tag mit 20,97% Positivrate) also wieder minimal weniger Tests, gepaart mit steigender Positivrate.

14.552 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 14.552 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 21,95%.](/images/2022/09/20220915-Test.png)

## Varianten

Die LMU betreibt jetzt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Aktuell eigentlich ganz einfach: Fast alles BA.5. Vereinzelt BA.2.75, noch kein BJ.1.

| Variante | KW 34 |	KW 35 |	KW 36 | KW 37 |
|:--|--:|--:|--:|--:|
| Andere | 0.0 % | 0.0 % | 1.0 % | 0.0 % | 
| Omikron BA.2 | 2.0 % | 1.0 % | 1.0 % | 0.0 % | 
| Omikron BA.4 | 3.0 % | 3.0 % | 3.0 % | 3.0 % | 
| Omikron BA.5 | 95.0 % | 95.0 % | 96.0 % | 97.0 % |

![VoC](/images/2022/09/KW37Var.png)


## Oktoberfest

Am Samstag wurde die 187. Wiesn mit viel Regen und bis zu 700.000 Besuchern am ersten Tag eröffnet. Schutzmaßnahmen oder ein Testangebot auf der Wiesn? Fehlanzeige.

Radio Gong 96,3 hat eine Stichprobe mit 100 PCR Tests gemacht. 2 Tests waren positiv. Das wäre dann umgerechnet eine Wiesnizidenz von 2.000. Wir schauen da lieber auf die RKI Zahlen im Laiufe der Woche um zu sehen, wohin uns die Reise führt. Auch wenn wir ein eher mulmiges Gefühl haben.

Ein R(t) von 2,6, wie in Straubing nach dem Gäubodenvolksfest würde in München sehr schnell zu einer Welle mit tausenden von Neuinfektionen pro Tag führen, vielleicht sogar fünfstellig - aber da kaum getestet wird, passiert das dann wohl zum größten Teil als Dunkelziffer - auch weil die vielen Touristen, selbst wenn sie hier getestet werden, nicht in die RKI Statistik für München eingehen - Wohnortprinzip.

Noch sieht die Kurve für München halbwegs friedlich aus. Spätestens 6 Tage nach der Eröffnung der Wiesn wird das Bild wohl ganz anders ausschauen.

![München](/images/2022/09/KW37Muenchen.png)