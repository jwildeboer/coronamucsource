---
layout: default
title: 2022-09-13 Tagesbericht
category: Tagesberichte
---

![2022-09-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche kaum Unterschied. Zweite zeigt Krankenbetten, insgesamt 3 Betten mehr belegt als gestern, dritte zeigt Inzidenz, die jetzt seit 8 Tagen knapp unter 190 bleibt.](/images/2022/09/20220913.png)

Das RKI meldet eine Inzidenz von 175,7 für München, 672 Neue und 348(!) Nachmeldungen für die letzten 7 Tage, davon 14 für Sonntag, 6 für Samstag und 247(!) für Freitag. Die für das WE gemeldeten Inzidenzen steigen für Sonntag von 165,2 auf 188,1, Samstag von 165,8 auf 187,7.
<!--more-->

Leider auch 8 neue Todesfälle - die 7-Tage-Summe der Todesfälle steigt auf 12. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 9.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 05.09. | 179,2 -> 186,1 |
| 06.09. | 174,1 -> 185,9 |
| 07.09. | 176,3 -> 183,3 |
| 08.09. | 178,5 -> 184,2 |
| 09.09. | 166,4 -> 187,9 |
| 10.09. | 165,8 -> 187,7 |
| 11.09. | 165,2 -> 188,1 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,99 und steigt leicht seit 4 Tagen.

Das R(t) vom StaBLab der LMU wurde zwar aktualisiert, aber nur bis zum 09.09 und liefert ein R(t) von 0,96.

| Datum | R(t) | LMU |
|---|---:|---:|
| 06.09. | 0,98 | 0,95 |
| 07.09. | 0,97 | 0,97 |
| 08.09. | 0,96 | 0,99 |
| 09.09. | 0,97 | 0,96 |
| 10.09. | 0,98 | --- |
| 11.09. | 0,99 | --- |
| 12.09. | 0,99 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt minimal: Normalbetten sinken um 2 von gestern 184 auf jetzt 182, IMC steigt von 7 auf 10 und Intensivbetten steigt von 21 auf 23. Damit insgesamt 215 Betten belegt, 3 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 182 | -2 | 
| IMC | 10 | +3 | 
| Intensiv | 23 | +2 |
| SUMME | 215 | +3 |

Im Wochenvergleich somit immerhin noch 10 Betten weniger belegt. 

## Impfungen

Die Impfungen laufen weiter auf Sparflamme. Was doch ein wenig erstaunlich ist: die Quote bei den Dritt-/Boosterimpfungen liegt in München bei nur 47,93%. Landesweit sind wir bei 58,91%. Das ist ein deutlicher Unterschied. München liegt bei den Drittimpfungen sogar unter Sachsen mit 49,9%.

1. +41
2. +24
3. +94
4. +276

Wir bleiben weiter unter dem Landesdurchschnitt.