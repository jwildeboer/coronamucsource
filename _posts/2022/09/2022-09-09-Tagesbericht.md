---
layout: default
title: 2022-09-09 Tagesbericht
category: Tagesberichte
---

![2022-09-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche kaum Unterschied. Zweite zeigt Krankenbetten, insgesamt 6 Betten weniger belegt als gestern, dritte zeigt Inzidenz, die jetzt seit 4 Tagen unter 190 ist.](/images/2022/09/20220909.png)

Das RKI meldet eine Inzidenz von 176,3 für München, 347 Neue, 97 Nachmeldungen, davon 74 für vorgestern. Die gestern genannte Inzidenz von 176,3 erhöht sich auf 182,2. Tag 4 unter Inzidenz 190.
<!--more-->

Keine  neuen Todesfälle - die 7-Tage-Summe der Todesfälle bleibt auf 5. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 2.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 01.09. | 183,8 -> 190,9 |
| 02.09. | 182,8 -> 189,2 |
| 03.09. | 182,8 -> 189,8 |
| 04.09. | 182,8 -> 190,3 |
| 05.09. | 179,2 -> 185,7 |
| 06.09. | 174,1 -> 185,3 |
| 07.09. | 176,3 -> 182,2 |

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,96.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 08.09. liefert ein R(t) von 1,00.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.09. | 0,98 | 1,01 |
| 03.09. | 1,00 | 0,98 |
| 04.09. | 1,00 | 0,95 |
| 05.09. | 0,99 | 0,95 |
| 06.09. | 0,98 | 0,96 |
| 07.09. | 0,97 | 0,99 |
| 08.09. | 0,96 | 1,00 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Normalbetten sinken um 2 von gestern 182 auf jetzt 180, IMC sinkt von 6 auf 3 und Intensivbetten sinkt von 17 auf 16. Damit insgesamt 199 Betten belegt, 6 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 180 | -2 | 
| IMC | 3 | -3 | 
| Intensiv | 16 | -1 |
| SUMME | 199 | -6 |

Im Wochenvergleich somit 27 Betten weniger belegt. 

## Impfungen

Die Stadt München hat heute die Impfzahlen (bis 13:45) nicht aktualisiert.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.