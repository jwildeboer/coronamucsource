---
layout: default
title: 2022-09-28 Tagesbericht
category: Tagesberichte
---

![2022-09-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher. Zweite zeigt Krankenbetten, die weiter deutlich steigen, Dritte zeigt Inzidenz, die jetzt klar exponentiell steigt und heute über 500 liegt.](/images/2022/09/20220928.png)

Das RKI meldet eine Inzidenz von 547,0(!) für München. 2441 Neue, 271 Nachmeldungen, davon 252 für vorgestern. Die gestern genannte Inzidenz von 424,9 erhöht sich auf 442,4. 
<!--more-->

Leider auch 7 neue Todesfälle - die 7-Tage-Summe der Todesfälle ist 10. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle auch 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 20.09. | 240,4 -> 257,2 |
| 21.09. | 255,6 -> 271,5 |
| 22.09. | 273,8 -> 290,0 |
| 23.09. | 311,2 -> 327,9 |
| 24.09. | 311,1 -> 392,8 |
| 25.09. | 310,3 -> 393,8 |
| 26.09. | 424,9 -> 442,4 |

So schaut exponentielles Wachstum aus.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,46 und steigt seit 21(!) Tagen, Tag 16 über 1, also exponentielles Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt am 20.09. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.09. | 1,16 | --- |
| 22.09. | 1,19 | --- |
| 23.09. | 1,22 | --- |
| 24.09. | 1,28 | --- |
| 25.09. | 1,33 | --- |
| 26.09. | 1,38 | --- |
| 27.09. | 1,46 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter stark an: Normalbetten steigt wieder um 25(!) von gestern 260 auf jetzt 285, IMC steigt von gestern 13 auf jetzt 15. Intensivbetten sinkt von gestern 24 auf jetzt 23. Damit insgesamt 323 Betten belegt, 26 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 280 | +25 | 
| IMC | 15 | +2 | 
| Intensiv | 23 | -1 |
| SUMME | 323 | +26 |

Im Wochenvergleich sind das 104(!) Betten mehr. Einen dreistelligen Anstieg hatten wir zuletzt mit 111 am 29.01.2022.

## Impfungen

Heute wieder etwas größere Zahlen bei den Impfungen weil die gesammelten Nachmeldungen der letzten Woche aus den Arztpraxen etc. eingearbeitet wurden. Trotzdem kaum Erst- und Zweitimpfungen.

1. +29
2. +29
3. +360
4. +3614

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,04%, Bayern 58,81%.

## Änderung des Status "vollständig geimpft"

Ab dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)


## Wiesnbarometer

Tag 12 der Wiesn. Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 112,7%(!) auf heute 547,0. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 47,49% auf jetzt 323.

![Wiesnbarometer 2022-09-28](/images/2022/09/20220928WB.png)