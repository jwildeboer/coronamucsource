---
layout: default
title: 2022-09-04 Tagesbericht
category: Tagesberichte
---

![2022-09-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche jetzt etwas höhere Tendenz erkennbar. Zweite zeigt Krankenbetten, Sonntags keine Aktualisierung aber fallende Tendenz, dritte zeigt Inzidenz welches seit 2 Wochen um 200 pendelt.](/images/2022/09/20220904.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 115 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 119, letzte Woche 121.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 599, gestern waren es 624, letzte Woche 648. 

## Reproduktionswert R(t) für München

Unser R(t) steht auf jetzt 0,98.

Das R(t) vom StaBLab der LMU wurde nicht aktualisiert. Der letzte NowCast Lauf vom 01.09. liefert ein R(t) von 0,93.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.08. | 0,95 | 0,93 |
| 29.08. | 0,93 | 0,88 |
| 30.08. | 0,92 | 0,88 |
| 31.08. | 0,93 | 0,93 |
| 01.09. | 0,94 | --- |
| 02.09. | 0,97 | --- |
| 03.09. | 0,98 | --- |

## Krankenhaus

Am Sonntag keine Aktualiserung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen durch die Stadt München.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.