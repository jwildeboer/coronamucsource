---
layout: default
title: 2022-09-03 Tagesbericht
category: Tagesberichte
---

![2022-09-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche jetzt etwas höhere Tendenz erkennbar. Zweite zeigt Krankenbetten, Samstags keine Aktualisierung aber fallende Tendenz, dritte zeigt Inzidenz welches seit 2 Wochen um 200 pendelt.](/images/2022/09/20220903.png)

Das RKI meldet eine Inzidenz von 182,8 für München, 296 neue und 78 Nachmeldungen, davon 69 für vorgestern. Die gestern gemeldete Inzidenz steigt damit von 183,8 auf 188,5.
<!--more-->

Leider auch wieder 3 neue Todesfälle - die 7-Tage-Summe der Todesfälle ist jetzt 15. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 26.08. | 183,9 -> 188,8 | 
| 27.08. | 183,8 -> 188,8 | 
| 28.08. | 183,8 -> 188,8 | 
| 29.08. | 188,7 -> 200,1 | 
| 30.08. | 187,8 -> 198,3 | 
| 31.08. | 189,1 -> 198,6 | 
| 01.09. | 183,8 -> 188,5 | 

## Reproduktionswert R(t) für München

Unser R(t) steigt wieder auf jetzt 0,97.

Das R(t) vom StaBLab der LMU wurde gestern nicht aktualisiert. Der letzte NowCast Lauf vom 01.09. liefert ein R(t) von 0,93.

| Datum | R(t) | LMU |
|---|---:|---:|
| 27.08. | 0,96 | 1,02 |
| 28.08. | 0,95 | 0,93 |
| 29.08. | 0,93 | 0,88 |
| 30.08. | 0,92 | 0,88 |
| 31.08. | 0,93 | 0,93 |
| 01.09. | 0,94 | --- |
| 02.09. | 0,97 | --- |

## Krankenhaus

Am Samstag keine Aktualiserung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Samstag keine Aktualisierung der Impfzahlen durch die Stadt München.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.