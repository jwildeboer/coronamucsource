---
layout: default
title: 2022-09-15 Tagesbericht
category: Tagesberichte
---

![2022-09-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche kaum Unterschied. Zweite zeigt Krankenbetten, insgesamt 10 Betten weniger belegt als gestern. Dritte zeigt Inzidenz, die jetzt seit 10 Tagen knapp zwischen um 190 pendelt.](/images/2022/09/20220915.png)

Das RKI meldet eine Inzidenz von 187,7 für München, 451 Neue und 94 Nachmeldungen, davon 82 für vorgestern. Die gestern genannte Inzidenz von 183,6 erhöht sich damit auf 189,6.
<!--more-->

Leider auch 1 neuer Todesfall - die 7-Tage-Summe der Todesfälle geht damit auf 12. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 07.09. | 176,3 -> 183,6 |
| 08.09. | 178,5 -> 184,6 |
| 09.09. | 166,4 -> 188,5 |
| 10.09. | 165,8 -> 188,3 |
| 11.09. | 165,2 -> 188,8 |
| 12.09. | 179,2 -> 186,2 |
| 13.09. | 183,6 -> 189,6 |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,00 und steigt leicht seit 5 Tagen.

Das R(t) vom StaBLab der LMU wurde nicht aktualisiert, der letzte Nowcast Lauf vom 10.09. liefert ein R(t) von 0,96 für den 09.09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 08.09. | 0,96 | 0,99 |
| 09.09. | 0,97 | 0,96 |
| 10.09. | 0,98 | --- |
| 11.09. | 0,99 | --- |
| 12.09. | 1,00 | --- |
| 13.09. | 1,00 | --- |
| 14.09. | 1,00 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute wieder: Normalbetten steigt um 1 von gestern 176 auf jetzt 177, IMC sinkt von 13 auf 9 und Intensivbetten sinkt deutlicht von 27 auf 20. Damit insgesamt 206 Betten belegt, 10 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 177 | +1 | 
| IMC | 9 | -4 | 
| Intensiv | 20 | -7 |
| SUMME | 206 | -10 |

Im Wochenvergleich aber trotzdem 1 Bett mehr.

## Impfungen

Die Impfungen laufen weiter auf Sparflamme. Was doch ein wenig erstaunlich ist: die Quote bei den Dritt-/Boosterimpfungen liegt in München bei nur 47,95%. Landesweit sind wir bei 58,92%. Das ist ein deutlicher Unterschied. München liegt bei den Drittimpfungen sogar unter Sachsen mit 49,9%.

1. +2
2. +12
3. +26
4. +214

Wir bleiben in München also weiter unter dem Landesdurchschnitt.

![Neue Impfstoffe](/images/2022/09/20220914NewVacc.png)

Die Stadt München hat ab sofort die neuen Omikron-spezifischen Booster von BioNTech und Moderna und einen neuen Totimpfstoff im Angebot. [Mehr Informationen](https://ru.muenchen.de/2022/176/Drei-neue-Impfstoffe-in-den-Impfstellen-der-Stadt-verfuegbar-103143)

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur ca 14.552 Tests/Tag mit einer Positivrate von 21,95%. Gegenüber den Zahlen von letzter Woche (15.464 Tests/Tag mit 20,97% Positivrate) also wieder weniger Tests, gepaart mit steigender Positivrate.

14.552 Tests/Tag ist wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 14.552 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 21,95%.](/images/2022/09/20220915-Test.png)