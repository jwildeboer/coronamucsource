---
layout: default
title: 2022-09-23 Tagesbericht
category: Tagesberichte
---

![2022-09-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche klar höher. Zweite zeigt Krankenbetten, insgesamt 2 Betten mehr belegt als gestern. Dritte zeigt Inzidenz, die jetzt schnell steigt und seit 3 Tagen über 250 liegt.](/images/2022/09/20220923.png)

Das RKI meldet eine Inzidenz von 273,8 für München, 694 Neue und 199 Nachmeldungen, davon 160 für vorgestern. Die gestern genannte Inzidenz von 255,6 erhöht sich durch Nachmeldungen auf 268,5. Tag 3 über 250.
<!--more-->

Leider auch 1 neuer Todesfall - die 7-Tage-Summe der Todesfälle ist 13. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 15.09. | 199,4 -> 211,9 |
| 16.09. | 204,9 -> 218,7 |
| 17.09. | 204,5 -> 218,4 |
| 18.09. | 203,5 -> 218,2 |
| 19.09. | 224,7 -> 242,4 |
| 20.09. | 240,4 -> 255,1 |
| 21.09. | 255,6 -> 268,5 |

Das RKI hat heute die Bevölkerungszahlen, die für die Inzidenzberechnung wichtig ist, aktualisiert. Für München gilt jetzt die Einwoherzahl von 1.487.708 (vorher 1.488.202, 494 weniger) und für Bayern 13.176.989 (vorher 13.140.183, 36860 mehr). Dadurch ändern sich die Inzidenzen und Impfquoten heute ein wenig. In München steigen die Inzidenzen und Quoten, in Bayern sinken sie dafür, umgekehrt zur Bevölkerungsentwicklung.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,17 und steigt seit 14 Tagen, Tag 11 über 1.

Das R(t) vom StaBLab der LMU wurde zuletzt am Mittwoch aktualisiert, der letzte Nowcast Lauf vom 20.09. liefert ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.09. | 1,06 | 1,10 |
| 17.09. | 1,08 | 1,05 |
| 18.09. | 1,09 | 1,04 |
| 19.09. | 1,11 | 1,05 |
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,15 | --- |
| 22.09. | 1,17 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt heute minimal: Normalbetten steigt um 5 von gestern 197 auf jetzt 202, IMC sinkt um 3 auf jetzt 10 und Intensivbetten bleibt auf 17. Damit insgesamt 226 Betten belegt, 2 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 202 | +5 | 
| IMC | 7 | -3 | 
| Intensiv | 17 | +0 |
| SUMME | 224 | +2 |

Im Wochenvergleich sind das 24 Betten mehr.

## Impfungen

Die Erst- und Zweitimpfungen laufen weiter auf Sparflamme. Bei den Viertimpfungen tut sich ein wenig mehr, Betonung auf wenig.  Da gibt es wohl doch langsam Bedarf für einen "Wiesnbooster" mit den neuen Omikron-spezifischen Impfstoffen.

1. +21
2. +19
3. +69
4. +346

Wir bleiben in München trotzdem unter dem Landesdurchschnitt, besonders klar bei den Drittimpfugen: München 47,99%, Bayern 58,8%.

## Wiesnbarometer

Tag 7 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 29,2% auf heute 273,8. Die Zahl der mit COVID-positiven Patienten belegten Betten in den münchner Krankenhäusern steigt im Vergleich zur Vorwoche klar um 11,88% auf jetzt 226.

![Wiesnbarometer 2022-09-23](/images/2022/09/20220923WB.png)