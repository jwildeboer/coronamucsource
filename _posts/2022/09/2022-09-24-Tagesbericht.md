---
layout: default
title: 2022-09-24 Tagesbericht
category: Tagesberichte
---

![2022-09-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche deutlich höher. Zweite zeigt Krankenbetten, die auch steigen. Dritte zeigt Inzidenz, die jetzt schnell steigt und heute über 300 liegt.](/images/2022/09/20220924.png)

Das RKI meldet eine Inzidenz von 311,2 für München, 876 Neue und 202 Nachmeldungen, davon 183 für vorgestern. Die gestern genannte Inzidenz von 273,8 erhöht sich durch Nachmeldungen auf 287,2. Tag 1 über 300.
<!--more-->

Leider auch 1 neuer Todesfall - die 7-Tage-Summe der Todesfälle ist 11. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 14.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 16.09. | 204,9 -> 218,9 |
| 17.09. | 204,5 -> 218,6 |
| 18.09. | 203,5 -> 218,5 |
| 19.09. | 224,7 -> 242,9 |
| 20.09. | 240,4 -> 255,8 |
| 21.09. | 255,6 -> 269,7 |
| 22.09. | 273,8 -> 287,2 |

Das RKI hat gestern die Bevölkerungszahlen, die für die Inzidenzberechnung wichtig ist, aktualisiert. Für München gilt jetzt die Einwoherzahl von 1.487.708 (vorher 1.488.202, 494 weniger) und für Bayern 13.176.989 (vorher 13.140.183, 36860 mehr). Dadurch ändern sich die Inzidenzen und Impfquoten heute ein wenig. In München steigen die Inzidenzen und Quoten, in Bayern sinken sie dafür, umgekehrt zur Bevölkerungsentwicklung.

Da heute Samstag ist, gibt es keine neuen Krankenhaus- oder Impfzahlen aus München. Morgen und übermorgen gehen, wie leider immer am Wochenende, keine Meldungen an das RKI, daher können wir die Inzidenz für Sonntag und Montag schon heute berechnen:

| Sonntag | 311,1 |
| Montag | 310,3 |

Erst am Dienstag wieder neue Fallzahlen und Inzidenzen. Und es steht zu befürchten, daß die Zahlen dann noch deutlicher steigen werden. Am Montag auf jeden Fall Tag 3 über Inzidenz 300.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,20 und steigt seit 15 Tagen, Tag 12 über 1.

Das R(t) vom StaBLab der LMU wurde zuletzt am Mittwoch aktualisiert, der letzte Nowcast Lauf vom 20.09. liefert ein R(t) von 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 17.09. | 1,08 | 1,05 |
| 18.09. | 1,09 | 1,04 |
| 19.09. | 1,11 | 1,05 |
| 20.09. | 1,13 | 1,09 |
| 21.09. | 1,16 | --- |
| 22.09. | 1,18 | --- |
| 23.09. | 1,20 | --- |

## Krankenhaus

Am Samstag leider keinen neuen Zahlen zur Krankenhausbelegung der Stadt München.

## Impfungen

Am Samstag leider keine neuen Impfzahlen von der Stadt München.

## Wiesnbarometer

Tag 8 der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 42,2%(!) auf heute 311,2. Die Zahl der mit COVID-positiven Patienten belegten Betten in den münchner Krankenhäusern (Stand 2022-09-23) steigt im Vergleich zur Vorwoche klar um 11,88% auf jetzt 226.

Bis Montag keine neuen Krankenhauszahlen. Bis Dienstag keine neuen Inzidenzen wegen Meldepause am Wochenende.

![Wiesnbarometer 2022-09-24](/images/2022/09/20220924WB.png)