---
layout: default
title: 2022-09-11 Tagesbericht
category: Tagesberichte
---

![2022-09-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche ähnlich, vielleicht etws niedriger.  Zweite zeigt Krankenbetten, Sonntags keine Aktualisierung aber fallende Tendenz, dritte zeigt Inzidenz, jetzt stabil unter 180.](/images/2022/09/20220911.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 108 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 112, letzte Woche 115.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 566, gestern waren es 591, letzte Woche 599. 

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,96.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 08.09. liefert ein R(t) von 1,00.

| Datum | R(t) | LMU |
|---|---:|---:|
| 04.09. | 1,00 | 0,95 |
| 05.09. | 0,99 | 0,95 |
| 06.09. | 0,98 | 0,96 |
| 07.09. | 0,97 | 0,99 |
| 08.09. | 0,96 | 1,00 |
| 09.09. | 0,95 | --- |
| 10.09. | 0,94 | --- |

## Krankenhaus

Am Sonntag keine Aktualiserung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen durch die Stadt München.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.