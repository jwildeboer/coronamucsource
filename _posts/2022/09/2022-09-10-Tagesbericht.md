---
layout: default
title: 2022-09-10 Tagesbericht
category: Tagesberichte
---

![2022-09-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche weniger. Zweite zeigt Krankenbetten, deutlich sinkend, aber keine neuen Zahlen am Samstag, dritte zeigt Inzidenz, die jetzt seit 5 Tagen unter 190 ist.](/images/2022/09/20220910.png)

Das RKI meldet eine überraschend niedrige Inzidenz von 166,4 für München. Nur 174 Neue und 12 Nachmeldungen für die letzten 7 Tage, davon 8 für vorgestern. Die gestern genannte Inzidenz von 178,5 erhöht sich auf 179,2. Tag 5 unter 190.
<!--more-->

Leider wieder 2 neue Todesfälle - die 7-Tage-Summe der Todesfälle steht auf 4. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 02.09. | 182,8 -> 189,2 |
| 03.09. | 182,8 -> 189,8 |
| 04.09. | 182,8 -> 190,3 |
| 05.09. | 179,2 -> 185,7 |
| 06.09. | 174,1 -> 185,3 |
| 07.09. | 176,3 -> 182,5 |
| 08.09. | 178,5 -> 179,2 |

Und da am Wochenende keine neuen Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 165,8 |
| Montag | 165,2 |

## Reproduktionswert R(t) für München

Unser R(t) hat kurz an der 1 gekratzt, liegt jetzt bei 0,96.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Der NowCast Lauf vom 08.09. liefert ein R(t) von 1,00.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.09. | 1,00 | 0,98 |
| 04.09. | 1,00 | 0,95 |
| 05.09. | 0,99 | 0,95 |
| 06.09. | 0,98 | 0,96 |
| 07.09. | 0,97 | 0,99 |
| 08.09. | 0,96 | 1,00 |
| 09.09. | 0,95 | --- |

## Krankenhaus

Am Samstag meldet die Stadt München keine neuen Krankenhauszahlen, erst Montag wieder neue Zahlen.

## Impfungen

Am Samstag meldet die Stadt München keine neuen Impfzahlen, erst Montag wieder neue Zahlen.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.