---
layout: default
title: 2022-09-17 Tagesbericht
category: Tagesberichte
---

![2022-09-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche leichte Aufwärtstendenz. Zweite zeigt Krankenbetten, am Samstag keine neuen Zahlen. Dritte zeigt Inzidenz, die jetzt wieder über 200 ist.](/images/2022/09/20220917.png)

Das RKI meldet eine Inzidenz von 204,9 für München, 354 Neue und 150 Nachmeldungen, davon 131 für vorgestern. Die gestern genannte Inzidenz von 199,4 erhöht sich damit auf 209,0. Somit Tag 2 über 200.
<!--more-->

Leider auch 3 neue Todesfälle - die 7-Tage-Summe der Todesfälle geht damit auf 20. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 15.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 09.09. | 166,4 -> 188,5 |
| 10.09. | 165,8 -> 188,3 |
| 11.09. | 165,2 -> 188,8 |
| 12.09. | 179,2 -> 186,3 |
| 13.09. | 183,6 -> 189,8 |
| 14.09. | 187,7 -> 196,7 |
| 15.09. | 199,4 -> 209,0 |

Da am Wochenende keine Zahlen an das LGL/RKI gemeldet werden, gibt es erst am Dienstag wieder neue Fallzahlen. Immerhin können wir schon mal die Inzidenz für Sonntag und Montag berechnen:

| So, 18.09. | 204,5 |
| Mo, 19.09. | 203,5 |

Am Montag ist somit schon jetzt sicher Tag 4 mit Inzidenz über 200.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,04 und steigt damit seit 9 Tagen, Tag 5 über 1.

Das R(t) vom StaBLab der LMU wurde in dieser Woche gar nicht aktualisiert, der letzte Nowcast Lauf vom 10.09. liefert ein R(t) von 0,96 für den 09.09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.09. | 0,98 | --- |
| 11.09. | 0,99 | --- |
| 12.09. | 1,00 | --- |
| 13.09. | 1,00 | --- |
| 14.09. | 1,01 | --- |
| 15.09. | 1,03 | --- |
| 16.09. | 1,04 | --- |

## Krankenhaus

Am Samstag gibt es von der Stadt keinen neuen Krankenhauszahlen.

## Impfungen

Am Samstag gibt es von der Stadt keinen neuen Impfzahlen.