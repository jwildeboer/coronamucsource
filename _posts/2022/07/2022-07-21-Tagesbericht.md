---
layout: default
title: 2022-07-21 Tagesbericht
category: Tagesberichte
---

![2022-07-21](/images/2022/07/20220721.png)

Das RKI meldet eine Inzidenz von 864,7 für München. 2.445 neue und 610 Nachmeldungen für die letzten 7 Tage, davon 545 für vorgestern.
Die gestern gemeldete Inzidenz von 823,5 steigt durch die Nachmeldungen auf 864,5.
<!--more-->
Leider auch wieder 4 neue Todesfällle. Damit insgesamt 8 Tote in den letzen 7 Tagen.

Die Inzidenzen der letzten 7 Tage im Vergleich gemeldet -> aktualisiert:

| 13.07 |  798,6 -> 845,7 | 
| 14.07 | 715,6 -> 802,3 | 
| 15.07 | 797,5 -> 839,9 | 
| 16.07 | 795,1 -> 837,7 | 
| 17.07 | 793,4 -> 836,5 | 
| 18.07 | 806,7 -> 852,6 | 
| 19.07 | 823,5 -> 864,5 | 

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bei ca 33900 Tests/Tag mit einer Positivrate von 41,25%. Gegenüber den Zahlen von letzter Woche (40,44% Positivrate) eine kleinere Steigerung.

![Testzahlen KW 28](/images/2022/07/20220721-Test.png)

## Krankenhaus

Die Zahl der belegten KH-Betten steigt weiter (+25) und das auf allen Stationstypen:

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 391 | +18 | 
| IMC | 10 | +1 | 
| Intensiv | 38 | +6 | 
| SUMME | 439 | +26 |

Gegenüber der Vorwoche sind das 94 belegte Betten mehr!
Gegenüber dem gemeldeten Wert von vor 4 Wochen (236 gesamt am 23.6.) sind das 186 Betten mehr.

## Impfungen

Die Impfzahlen dümpeln weiter (Stand 19.7.):

1. +25
2. +17
3. +102
4. +287

Weiterhin nicht mal bei den 1.-Impfungen die 73% erreicht.