---
layout: default
title: 2022-07-29 Tagesbericht
category: Tagesberichte
---

![2022-07-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern leichte Abnahme um 4 Betten, dritte zeigt Inzidenz, die weiter klar sinkt.](/images/2022/07/20220729.png)

Das RKI meldet eine Inzidenz von 712,1 für München. 1.254 neue und 522 Nachmeldungen für die letzten 7 Tage, davon 448 für vorgestern.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 781,3 auf 816,4 und bleibt damit klar unter 900.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 21.07. | 919,1 -> 963,2 |
| 22.07. | 904,4 -> 946,9 |
| 23.07. | 904,0 -> 947,1 |
| 24.07. | 903,6 -> 947,8 |
| 25.07. | 876,7 -> 926,8 |
| 26.07. | 819,5 -> 862,2 |
| 27.07. | 781,3 -> 816,4 |

## Krankenhaus

Die Krankenhaus-Belegung ist heute wieder leicht gesunken, hauptsächlich bei den Intensivbetten (-4). 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 423 | +1 | 
| IMC | 13 | -1 | 
| Intensiv | 35 | -4 |
| SUMME | 471 | -4 |

Im Wochenvergleich entspricht das weiterhin einem Plus von 28 Betten.

## Impfungen

Die Impfzahlen, Stand 27.07., wie gehabt. Kaum Erst- und Zweitimpfungen. 

1. +25
2. +10
3. +60
4. +186

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.