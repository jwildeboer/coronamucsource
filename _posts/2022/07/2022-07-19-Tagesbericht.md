---
layout: default
title: 2022-07-19 Tagesbericht
category: Tagesberichte
---

![2022-07-19](/images/2022/07/20220719.png)

Das RKI meldet heute eine Inzidenz von 806,7 für München. 2.246 neue und 556 Nachmeldungen für die letzten 7 Tage, davon 0 für vorgestern 4 für Samstag und 491 für Freitag.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 793,4 auf 830,6.
<!--more-->
Die Entwicklung der Inzidenzen der letzten 7 Tage (gemeldet -> aktualisiert):

| 11.07 | 794,2 -> 834,1 |
| 12.07 | 794,0 -> 841,1 |
| 13.07 | 798,6 -> 843,4 |
| 14.07 | 715,6 -> 799,3 |
| 15.07 | 797,5 -> 834,5 | 
| 16.07 | 795,1 -> 832,3 |
| 17.07 | 793,4 -> 830,6 |

Seit dem 7.7. war damit (mit Ausnahme des Knicks vom 14.7.) die Inzidenz über 800. Und auch der 14.7. kratzt mit 799,3 an der 800er Grenze.

## Krankenhaus

Die Krankenhaus-Belegung in München steigt insgesamt weiter (+7). 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 372 |  +8 | 
| IMC | 9 | -6 | 
| Intensiv | 32 | +5 | 
| SUMME | 413 | +7 |

Im Wochenvergleich sind das 55 zusätzliche belegte Betten.

## Impfungen

Bei den Impfzahlen sind wir heute bei Stand 16.7., also Zuwachs für zwei Tage.

1. +52 (26/T)
2. +37 (19/T)
3. +162 (81/T)
4. +483 (242/T)

München liegt bei den Erstimpfungen weiterhin unter 73 %. Bayernweit liegt die Erstimpfungsquote bei 75,3%.