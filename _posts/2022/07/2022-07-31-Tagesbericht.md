---
layout: default
title: 2022-07-31 Tagesbericht
category: Tagesberichte
---

![2022-07-31 Dashboard mit den Zahlen für München. Am Sonntag keine neuen Zahlen. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, dritte zeigt Inzidenz, die weiter klar sinkt.](/images/2022/07/20220731.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI):

- Bayernweit gibt es 247 mit COVID positiven Patienten belegte Intensivbetten, 9 mehr als gestern.
- Die 7 Tages Summe der Hospitalisierten, bayernweit. Heute 1313, gestern 1366. Weiterhin klar über den Grenzwert von 1200 für die "alte" gelbe Ampel.

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München