---
layout: default
title: 2022-07-30 Tagesbericht
category: Tagesberichte
---

![2022-07-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, am Samstag keine neuen Zahlen, dritte zeigt Inzidenz, die weiter klar sinkt.](/images/2022/07/20220730.png)

Das RKI meldet eine Inzidenz von 665,2 für München. 1.051 Neue, 353 Nachmeldungen in den letzten 7 Tagen, davon 320 für vorgestern.

Die gestern genannte Inzidenz von 712,1 erhöht sich durch die Nachmeldungen auf 735,9. Tag 4 unter 900, Tag 2 unter 800!
<!--more-->

Die 7 Tages Summe geht damit auf 9.900 - das erste Mal seit dem 30.06. wieder unter 10.000. Leider aber auch 4 Tote mehr, damit 7 Tote in den letzten 7 Tagen.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 22.07. | 904,4 -> 947,3 |
| 23.07. | 904,0 -> 947,5 |
| 24.07. | 903,6 -> 948,2 |
| 25.07. | 876,7 -> 927,4 |
| 26.07. | 819,5 -> 863,3 |
| 27.07. | 781,3 -> 818,8 |
| 28.07. | 712,1 -> 735,9 |

Und, weil am Wochenende weiterhin keine Zahlen an das RKI gehen, hier schon mal die Inzidenzen für Sonntag und Montag:

| Sonntag | 664,7 |
| Montag | 663,5 |

Dienstag gibt es dann erst wieder eine "echte" neue Inzidenz.

## Nowcast und Reproduktionswert R(t)

Die [LMU bzw. das StaBLab](https://corona.stat.uni-muenchen.de/nowcast/) hat gestern leider wieder mal nicht den Nowcast und das R(t) aktualisiert, deswegen nennen sie weiterhin ein R(t) von 0,98. Unser eigenes R(t) geht eher Richtung 0,90 und ist seit 2 Tagen stabil unter 1.

## Krankenhaus

Am Samstag keine Aktualisierung der Krankenhausbelegung in München.

| Art | Gesamt |
|---|---:|
| Normalstation | 423 | 
| IMC | 13 |
| Intensiv | 35 |
| SUMME | 471 |

## Impfungen

Am Samstag keine Aktualisierung der Impfzahlen in München