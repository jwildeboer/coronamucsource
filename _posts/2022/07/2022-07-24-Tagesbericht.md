---
layout: default
title: 2022-07-24 Tagesbericht
category: Tagesberichte
---

![2022-07-24](/images/2022/07/20220724.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. Die einzige neue Zahl: Bayernweit gibt es 242 belegte Intensivbetten, zwei weniger als gestern.
<!--more-->

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München