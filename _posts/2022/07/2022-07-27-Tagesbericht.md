---
layout: default
title: 2022-07-27 Tagesbericht
category: Tagesberichte
---

![2022-07-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern leichte Abnahme um 5 Betten, dritte zeigt Inzidenz, die jetzt wohl sinkt.](/images/2022/07/20220727.png)

Das RKI meldet eine Inzidenz von 819,5, 1.663 neue und 632 Nachmeldungen für die letzten 7 Tage, davon 539 für vorgestern.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 876,7 auf 919,1.
<!--more-->

Die Tendenz zeigt damit eine Wende zum Besseren an und die heutie genannte Inzidenz dürfte auch mit Nachmeldungen morgen unter 900 bleiben.

Mit den aktuellen Meldungen ist die Summe der Infektionen seit Beginn der Pandemie in München über 600.000 gestiegen. 

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 19.07. |  823,5 -> 875,7 | 
| 20.07. |  864,7 -> 920,3 | 
| 21.07. |  919,1 -> 961,7 | 
| 22.07. |  904,4 -> 944,0 | 
| 23.07. |  904,0 -> 944,2 | 
| 24.07. |  903,6 -> 944,9 | 
| 25.07. |  876,7 -> 919,1 | 

## Krankenhaus

Die Krankenhausbelegung sinkt heute zwar um 5 Betten, bleibt aber mit insgeamt 471 weiterhin hoch:

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 419 | -4 | 
| IMC | 12 | -1 | 
| Intensiv | 40 | +0 |
| SUMME | 471 | -5 |

Im Wochenvergleich +58 Betten.

## Impfungen

Die Impfzahlen sind Stand 25.07. - ein Tag, aber dafür inkl. Nachmeldungen:

1. +40
2. +82
3. +391
4. +2813

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.