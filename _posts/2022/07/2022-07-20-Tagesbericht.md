---
layout: default
title: 2022-07-20 Tagesbericht
category: Tagesberichte
---

![2022-07-20](/images/2022/07/20220720.png)

Das RKI meldet eine Inzidenz von 823,5 für München, 2.525 neue und 612 Nachmeldungen für die letzten 7 Tage, davon 563 für vorgestern.
Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 806,7 auf 834,8.
<!--more-->
Leider auch 3 neu gemeldete Todesfällle.

Mit den Nachmeldungen ist nun auch der 14.7. über 800 angekommen. Die Entwicklung der Inzidenzen (gemeldet -> aktualisiert):

| 12.07 |  794,0 -> 842,0 | 
| 13.07 |  798,6 -> 844,9 | 
| 14.07 |  715,6 -> 801,4 | 
| 15.07 |  797,5 -> 838,2 | 
| 16.07 |  795,1 -> 836,0 | 
| 17.07 |  793,4 -> 834,8 | 
| 18.07 |   806,7 -> 847,6 | 

Damit liegt die Inzidenz nun seit 13 Tagen über 800 und das Plateau, das sich zwischendurch andeutete ist noch nicht in Sicht.

## Krankenhaus

Die Krankenhaus-Belegung ist unverändert gegenüber gestern - auch auf die Stationen gesehen. Im Wochenvergleich trotzdem ein Plus von 59 belegten Betten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 372 |  +0 | 
| IMC | 9 | +0 | 
| Intensiv | 32 | +0 | 
| SUMME | 413 | +0 |


## Impfungen

Die Impfzahlen sind Stand 18.7., somit 2 Tage auf einmal und zudem Nachmeldungen.

1. +35 (18/T)
2. +47 (24/T)
3. +317 (159/T)
4. +2206 (1103/T)