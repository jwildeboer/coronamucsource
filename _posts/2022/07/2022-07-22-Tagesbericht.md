---
layout: default
title: 2022-07-22 Tagesbericht
category: Tagesberichte
---

![2022-07-21](/images/2022/07/20220722.png)

Das RKI meldet eine Inzidenz von 919,1 für München, 2.295 neue und 688 Nachmeldungen für die letzten 7 Tage, davon 618 für vorgestern.

Die gestern gemeldete Inzidenz springt mit den Nachmeldungen von 864,7 auf 910,6.
Damit, mit Nachmeldungen, der zweite Tag mit Inzidenz über 900.
<!--more-->
Die Entwicklung der Inzidenzen im Vergleich (gemeldet --> aktualisiert):

| 14.07. |  715,6  ->  802,8 | 
| 15.07. |  797,5  ->  840,7 | 
| 16.07. |  795,1  ->  838,7 | 
| 17.07. |  793,4  ->  837,4 | 
| 18.07. |  806,7  ->  855,1 | 
| 19.07. |  823,5  ->  869,4 | 
| 20.07. |  864,7  ->  910,6 | 

## Krankenhaus

Nur noch leichte Steigerung bei den Krankenhauszahlen gegenüber gestern (+4).

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 397 |+6 | 
| IMC | 10 | +0 | 
| Intensiv | 36 |-2 | 
| SUMME | 443 | +4 |
 
Aber im Vergleich zur Vorwoche 91 Betten mehr belegt.

## Impfungen

Bei den Impfungen tut sich auch wenig.

1. +15
2. +20
3. +49
4. +181

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.