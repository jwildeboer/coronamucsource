---
layout: default
title: 2022-07-23 Tagesbericht
category: Tagesberichte
---

![2022-07-23](/images/2022/07/20220723.png)

Das RKI meldet eine Inzidenz von 904,4 für München. 1.612 neue und 512 Nachmeldungen für die letzten 7 Tage, davon 440 für vorgestern,

Damit Tag 3 über 900. Und leider auch 5 Tote mehr.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 919,1 auf 953,3.
<!--more-->

Und da morgen und am Montag keine Zahlen an das RKI gemeldet werden, können wir schon jetzt die Inzidenz im Voraus berechnen:

| Sonntag: | 904,0 |
| Montag: | 903,6 |

Die Entwicklung der Inzidenzen im Vergleich (gemeldet --> aktualisiert):

| 15.07. | 797,5 -> 841,4 | 
| 16.07. | 795,1 -> 839,3 | 
| 17.07. | 793,4 -> 838,0 | 
| 18.07. | 806,7 -> 857,1 | 
| 19.07. | 823,5 -> 872,3 | 
| 20.07. | 864,7 -> 915,5 | 
| 21.07. | 919,1 -> 953,3 | 

## Krankenhaus

Am Samstag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Samstag keine Aktualisierung der Impfzahlen in München