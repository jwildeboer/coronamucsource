---
layout: default
title: 2022-07-26 Tagesbericht
category: Tagesberichte
---

![Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weniger als letzte Woche. Zweite zeigt Krankenbetten, deutlicher Anstieg um 26 Betten, dritte zeigt Inzidenzen, die langsam zu sinken scheint.](/images/2022/07/20220726.png)

Das RKI meldet heute eine Inzidenz von 876,7 für München. 1.982 neue und 512 Nachmeldungen für die letzten 7 Tage, davon 11 für vorgestern, 6 für Samstag und 421 für Freitag.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 903,6 auf 938,4.
<!--more-->
Die Entwicklung der Inzidenzen im Vergleich (gemeldet --> aktualisiert):

| 18.07. | 806,7 -> 858,2 |
| 19.07. | 823,5 -> 874,7 |
| 20.07. | 864,7 -> 918,8 |
| 21.07. | 919,1 -> 959,0 |
| 22.07. | 904,4 -> 938,0 |
| 23.07. | 904,0 -> 938,1 |
| 24.07. | 903,6 -> 938,4 |

## Krankenhaus

Die Krankenhausbelegung springt um +26 und das über alle Stationsarten hinweg:

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 423 | + 19 | 
| IMC | 13 |+2 | 
| Intensiv | 40 | +5 | 
| SUMME | 476 | +26 |

Im Wochenvergleich +63 (!), verglichen mit dem Wert vor 4 Wochen +187.

## Impfungen

Die heute gemeldeten Impfzahlen sind Stand 24.7. und damit mal wieder Zuwachs von 3 Tagen auf einmal:

1. +41 (14/T)
2. +40 (13/T)
3. +146 (49/T)
4. +315 (105/T)

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.