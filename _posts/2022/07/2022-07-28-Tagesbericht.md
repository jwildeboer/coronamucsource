---
layout: default
title: 2022-07-28 Tagesbericht
category: Tagesberichte
---

![2022-07-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, klar weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern leichte Zunahme um 5 Betten, dritte zeigt Inzidenz, die jetzt klar sinkt.](/images/2022/07/20220728.png)

Das RKI meldet eine Inzidenz von 781,3 für München. 1.982 neue und 563 Nachmeldungen für die letzten 7 Tage, davon 481 für vorgestern.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 819,5 auf 857,3 und bleibt damit unter 900.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 20.07. | 864,7 -> 920,8 | 
| 21.07. | 919,1 -> 962,8 | 
| 22.07. | 904,4 -> 946,0 | 
| 23.07. | 904,0 -> 946,1 | 
| 24.07. | 903,6 -> 946,8 | 
| 25.07. | 876,7 -> 924,7 | 
| 26.07. | 819,5 -> 857,3 |

## Krankenhaus

Die Krankenhaus-Belegung hat heute das Minus von gestern fast wieder aufgefüllt (+4). 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 422 | +3| 
| IMC | 14 | +2 | 
| Intensiv | 39 | -1|
| SUMME | 475 | +4 |

Im Wochenvergleich entspricht das weiterhin einem Plus von 36.

## Impfungen

Die Impfzahlen, Stand 26.07., bleiben weiter niedrig. 

1. +27
2. +34
3. +101
4. +259

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 30.900 Tests/Tag mit einer Positivrate von 43,75%. Gegenüber den Zahlen von letzter Woche (35.000 Tests/tag mit 40,47% Positivrate) eine klare Steigerung bei weniger durchgeführten Tests.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Man erkennt: Seit Juni 2022 steigt die Positivrate der Tests von 20% auf jetzt 43,75%. Die Zahl der Tests pro Tag ist mit ca. 30.900 weit vom Maximum von 100.000 Tests/Tag entfernt.](/images/2022/07/20220728-Test.png)