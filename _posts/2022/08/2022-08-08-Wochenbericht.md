---
layout: default
title: 2022-08-08 Wochenbericht KW31
category: Wochenberichte
---

![2022-08-08 Dashboard mit den Zahlen für München. Am Montag keine neuen Fallzahlen. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, stabil sinkende Tendenz, dritte zeigt Inzidenz, die weiter klar sinkt.](/images/2022/08/20220808.png)

In der letzten Woche gab es insgesamt 5.495 neue COVID Fälle in München, das sind 45,5% weniger als in der Vorwoche (10.088). Leider aber auch insgesamt 22 Tote mit COVID Nachweis in den letzten 7 Tagen, deutlich mehr als in der Vorwoche (7).
<!--more-->

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berehchnen wir parallel "unser" R(t) weiter. Das war in der letzten Woche stabil unter 1. Das ist keine komplette Entwarnung (dazu wäre ein, über mehrere Tage stabiles R(t) um 0,6 erforderlich aber 0,8 und darunter ist ein guter Wert.).

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 10.088 kontinuierlich zurück auf 5.502. Die Inzidenz verlor wieder fast 300 Punkte in 7 Tagen. Die Welle klingt also weiter klar ab. Wegen der Sommerferien wird es aber wohl noch weniger Tests geben, die Dunkelziffer bleibt daher wohl hoch.

Wenn morgen die ersten neuen Zahlen der Woche kommen, könnte die Inzidenz schon um 350 liegen. Wir werden es sehen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 31.07. | 7 | 10.088 | 677,9 |
| 01.08. | 7 | 9.407 | 632,1 |
| 02.08. | 1924 | 8.539 | 573,8 | 
| 03.08. | 1342 | 7.066 | 474,8 |
| 04.08. | 1000 | 6.189 | 415,9 |
| 05.08. | 732 | 5.509 | 370,2 |
| 06.08. | 497 | 5.502 | 369,7 |

## Krankenhaus

Bei den Krankenhauszahlen in München schaut es langsam auch besser aus. Am Montag wurden insgesamt 381 Betten gemeldet, 336 Normal, 13 IMC und 32 Intensiv. Damit erfreulich 83 Betten weniger belegt als eine Woche zuvor.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 31.07. | 409 | 18 | 37 | 464 | +14  | 17 |
| 01.08. | 408 | 18 | 37 | 463 | -13 | 25 |
| 02.08. | 374 | 13 | 34 | 421 | -50 | 26 |
| 03.08. | 371 | 11 | 28 | 410 | -65 | 24 |
| 04.08. | 353 | 14 | 37 | 404 | -67 | 24 |
| 05.08. | 348 | 14 | 35 | 397 | -70 | 21 |
| 06.08. | 342 | 13 | 33 | 388 | -78 | 21 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine Zunahme bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,87% | 75,32% |
| 2. | 71,34% | 75,21% |
| 3. | 47,73% | 58,75% |
| 4. | 5,51% | 5,62% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/08/KW31Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsminiteriums](/images/2022/08/KW30FAQImpf.png)

## Tests (bayernweit)

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Wir testen mit ca 30% der maximalen Kapazität und einer leicht sinkenden Positivrate von jetzt um 37% (Vorwoche: 40%). Da es keine Zahlen zu Bürgertests gibt, kann man nur vermuten, daß deutlich zu wenig getestet wird und die Dunkelziffer daher hoch sein muss.

Auch hier wird sich in der nächsten Woche wohl der Einfluß der Sommerferien bemerkbar machen - weniger Tests. Ob die Positivrate dann auch weiter niedriger wird, das zeigt sich am Donnerstag, wenn die neuen Zahlen vom LGL kommen.

![Bayernweite Tests - Anzahl Tests steigt leicht auf 320000 pro Tag, Positivrate steigt auf über 40%](/images/2022/08/20220804-Test.png)