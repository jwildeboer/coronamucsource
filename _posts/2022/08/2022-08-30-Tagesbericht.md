---
layout: default
title: 2022-08-30 Tagesbericht
category: Tagesberichte
---

![2022-08-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche ähnlich. Zweite zeigt Krankenbetten, insgesamt 10 Betten mehr belegt als gestern, dritte zeigt Inzidenz, welches um sich um 190 bewegt, mit unklarer Tendenz.](/images/2022/08/20220830.png)

Das RKI meldet eine Inzidenz von 188,7 für München. 756 Neue, 67 Nachmeldungen, davon 1 für Samstag und 44 für Freitag. Die Inzidenzen von Freitag bis Sonntag gehen damit auf 188,1
<!--more-->

Leider wieder 7 neue Todesfälle, die 7-Tages-Summe der Todesfälle steigt damit auf 21. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 0.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 22.08. | 246,3 -> 257,2 | 
| 23.08. | 212,1 -> 222,8 | 
| 24.08. | 192,3 -> 201,9 | 
| 25.08. | 186,7 -> 196,1 | 
| 26.08. | 183,9 -> 188,1 | 
| 27.08. | 183,8 -> 188,1 | 
| 28.08. | 183,8 -> 188,1 | 

## Reproduktionswert R(t) für München

Unser R(t) sinkt leicht auf jetzt 0,92.

Das R(t) vom StaBLab der LMU wurde auch heute nicht aktualsiert, bleibt daher auf dem  Stand vom 25.08. bei 1,01.

| Datum | R(t) | LMU |
|---|---:|---:|
| 23.08. | 1,02 | 0,95 |
| 24.08. | 1,05 | 1,01 |
| 25.08. | 1,07 | --- |
| 26.08. | 0,99 | --- |
| 27.08. | 0,96 | --- |
| 28.08. | 0,95 | --- |
| 29.08. | 0,92 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt wieder: Normalbetten steigen um 9 von gestern 219 auf jetzt 228, IMC steigt von 8 auf 11 und Intensivbetten gehen von 19 zurück auf 17. Damit insgesamt 256 Betten belegt, 10 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 228 | +9 | 
| IMC | 11 | +3 | 
| Intensiv | 17 | -2 |
| SUMME | 256 | +10 |

Im Wochenvergleich nur noch 17 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Und die etwas komische Korrektur bei den Zweitimpfungen bleibt weiterhin unerklärt. Am 19.08. laut Stadt 1.062.042 Zweitimpfungen, am 20.08. dann aber nur 1.062.013 Zweitimpfungen - also 29 weniger? 

1. +11
2. +8
3. +35
4. +190

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.