---
layout: default
title: 2022-08-24 Tagesbericht
category: Tagesberichte
---

![2022-08-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, insgesamt 6 Normalbetten weniger belegt als gestern, dritte zeigt Inzidenz, welches um sich um 200 bewegt, mit leicht steigender Tendenz.](/images/2022/08/20220824.png)

Das RKI meldet eine Inzidenz von 212,1 für München. 527 Neue, 128  Nachmeldungen, davon 117 für vorgestern. Die gestern gemeldete Inzidenz von 246,3 erhöht sich auf 254,5. Damit ist seit 7 Tagen Inzidenz über 200, mit leicht steigender Tendenz.
<!--more-->

Leider auch wieder 4 neue Todesfälle, die 7-Tages-Summe der Todesfälle steigt auf 18. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 16.08. | 179,7 -> 192,4 |
| 17.08. | 195,8 -> 206,4 |
| 18.08. | 199,4 -> 210,0 | 
| 19.08. | 195,1 -> 206,6 | 
| 20.08. | 195,0 -> 206,6 | 
| 21.08. | 195,0 -> 206,6 | 
| 22.08. | 246,3 -> 254,5 |

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf jetzt 1,01. Damit zum ersten mal seit dem 26.07. wieder über 1.

Das R(t) vom StaBLab der LMU wurde auch heute noch nicht aktualsiert (letzter Nowcast Lauf war am 19.08.) und liegt deshalb weiter bei 0,79.

| Datum | R(t) | LMU |
|---|---:|---:|
| 17.08. | 0,75 | 0,77 |
| 18.08. | 0,78 | 0,79 |
| 19.08. | 0,83 | --- |
| 20.08. | 0,86 | --- |
| 21.08. | 0,87 | --- |
| 22.08. | 0,96 | --- |
| 23.08. | 1,01 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt insgesamt wieder ein wenig: Normalbetten sinken von gestern 247 auf jetzt 241, IMC und Intensivbetten bleiben unverändert. Damit insgesamt 267 Betten belegt, das sind 6 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 241 | -6 | 
| IMC | 6 | +0 | 
| Intensiv | 20 | +0 |
| SUMME | 267 | -6 |

Im Wochenvergleich sind das 83 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Und eine etwas komische Korrektur bei den Zweitimpfungen. Am 19.08. laut Stadt 1.062.042 Zweitimpfungen, am 20.08. dann aber nur 1.062.013 Zweitimpfungen - also 29 weniger? 

1. +7
2. +15
3. +39
4. +454

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.