---
layout: default
title: 2022-08-18 Tagesbericht
category: Tagesberichte
---

![2022-08-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, wieter sinkende Tendenz. Zweite zeigt Krankenbetten, wieder weniger als gestern, dritte zeigt Inzidenz, welches sich gerade auf einem Plateau einpendelt wegen Meldeverzug ducrh Wochenende/Feiertag.](/images/2022/08/20220818.png)

Das RKI meldet eine Inzidenz von 195,8 für München. 673 Neue, 174 Nachmeldungen. Die für gestern gemeldete Inzidenz von 179,7 erhöht sich auf 190,6. Da sind wohl weiterhin noch einige Nachmeldungen vom Wochenende und Feiertag enthalten.
<!--more-->

Leider auch wieder 5 neue Todesfälle gemeldet, die 7-Tages-Summe der Todesfälle sinkt aber auf 15. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle nur 2.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 10.08. | 276,7 -> 285,7 |
| 11.08. | 265,2 -> 272,2 |
| 12.08. | 245,6 -> 254,3 |
| 13.08. | 244,8 -> 253,5 |
| 14.08. | 244,5 -> 253,2 |
| 15.08. | 158,2 -> 166,8 |
| 16.08. | 179,7 -> 190,6 |

## Reproduktionswert R(t) für München

Unser R(t) stabilisiert sich bei 0,75. 

Das R(t) vom StaBLab der LMU wurde aktualsiert (Nowcast Lauf vom 17.08.) und liegt jetzt bei 0,70.

| Datum | R(t) | LMU |
|---|---:|---:|
| 11.08. | 0,73 | 0,74 |
| 12.08. | 0,75 | 0,73 |
| 13.08. | 0,77 | 0,68 |
| 14.08. | 0,78 | 0,66 |
| 15.08. | 0,75 | 0,67 |
| 16.08. | 0,74 | 0,70 |
| 17.08. | 0,75 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt insgesamt wieder ein wenig: Normalbetten gehen von gestern 309 auf jetzt 301, IMC steigt von 9 auf 11, Intensiv sinkt von 32 auf 29. Damit insgesamt 341 Betten belegt, das sind 9 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 301 | -8 | 
| IMC | 11 | +2 | 
| Intensiv | 29 | -3 |
| SUMME | 341 | -9 |

Im Wochenvergleich sind das 49 Betten weniger. 

## Impfungen

Die Impfzahlen wurden um 16 Uhr doch noch aktualisiert und sind jetzt auf Stand 16.08. Damit verteilen sich die Impfungen auf 3 Tage und wenn man das umrechnet kommt man auf folgende Tageszahlen:

1. +27
2. +52
3. +264
4. +757

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 15.600 Tests/Tag mit einer Positivrate von 25,2%. Gegenüber den Zahlen von letzter Woche (20.930 Tests/tag mit 31,37% Positivrate) also deutlich weniger Tests, gepaart mit sinkender Positivrate.

15.600 Tests/Tag ist der niedrigste Stand seit Beginn der Pandemie. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.600 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 25,2%.](/images/2022/08/20220818-Test.png)