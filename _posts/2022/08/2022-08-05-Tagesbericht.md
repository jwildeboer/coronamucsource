---
layout: default
title: 2022-08-05 Tagesbericht
category: Tagesberichte
---

![2022-08-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern wieder etwas weniger, aber 9 Intensivbetten mehr, dritte zeigt Inzidenz, die sehr deutlich sinkt. Tag 2 unter Inzidenz 500.](/images/2022/08/20220805.png)

Das RKI meldet eine Inzidenz von 405,2 für München. Nur 592 Neue und 177 Nachmeldungen für die letzten 7 Tage, davon 161 für vorgestern.

Die gestern gemeldete Inzidenz steigt von 461,6 auf 473,5 und bleibt damit unter 500.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 28.07. | 712,1 -> 741,3 |
| 29.07. | 665,2 -> 678,5 |
| 30.07. | 664,7 -> 678,5 |
| 31.07. | 663,5 -> 677,7 |
| 01.08. | 607,6 -> 631,8 |
| 02.08. | 555,1 -> 573,2 |
| 03.08. | 461,6 -> 473,5 |

## Reproduktionswert R(t) für München

Da das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert, verwenden wir unser eigenes 7 Tage R. Das liegt heute bei 0,76.

Das R(t) vom StaBLab der LMU (Nowcast Lauf vom 04.08.) liegt bei 0,75.

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.07. | 0,91 | 0,87 |
| 30.07. | 0,87 | 0,79 |
| 31.07. | 0,84 | 0,74 |
| 01.08. | 0,82 | 0,73 |
| 02.08. | 0,81 | 0,74 |
| 03.08. | 0,78 | 0,75 |
| 04.08. | 0,76 | - |

## Krankenhaus

Weitere Abnahme aber auch Verschiebungen bei den Krankenhauszahlen heute. Weniger Normal-, mehr Intensivbetten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 353 | -18 | 
| IMC | 14 | +3 | 
| Intensiv | 37 | +9 |
| SUMME | 404 | -6 |

Im Wochenvergleich sind das trotzdem 67 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 03.08., sind weiter sehr niedrig. Die Sommerferien sind halt da.

1. +27
2. +15
3. +51
4. +201

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
