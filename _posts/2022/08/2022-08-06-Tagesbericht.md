---
layout: default
title: 2022-08-06 Tagesbericht
category: Tagesberichte
---

![2022-08-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, keine neuen Zahlen am Samstag, dritte zeigt Inzidenz, die sehr deutlich sinkt. Tag 3 unter Inzidenz 500.](/images/2022/08/20220806.png)

Das RKI meldet eine Inzidenz von 370,2 für München. 497 Neue, 165 Nachmeldungen, davon 141 für vorgestern.

Die gestern gemeldete Inzidenz steigt von 405,2 auf 415,9. Somit Tag 3 unter 500.

Leider auch 7 Tote mehr, damit insgesamt 22 Todesfälle in den letzten 7 Tagen.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 29.07. | 665,2 -> 678,7 |
| 30.07. | 664,7 -> 678,6 |
| 31.07. | 663,5 -> 677,9 |
| 01.08. | 607,6 -> 632,1 |
| 02.08. | 555,1 -> 573,8 |
| 03.08. | 461,6 -> 474,8 |
| 04.08. | 405,2 -> 415,9 |

## Reproduktionswert R(t) für München

Da das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert, verwenden wir unser eigenes 7 Tage R. Das liegt heute bei 0,75.

Das R(t) vom StaBLab der LMU (Nowcast Lauf vom 04.08.) liegt bei 0,75.

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.07. | 0,87 | 0,79 |
| 31.07. | 0,84 | 0,74 |
| 01.08. | 0,82 | 0,73 |
| 02.08. | 0,81 | 0,74 |
| 03.08. | 0,79 | 0,75 |
| 04.08. | 0,77 | - |
| 05.08. | 0,75 | - |

## Krankenhaus

Am Samstag keine neuen Krankenhauszahlen der Stadt München.

## Impfungen

Am Samstag keine neuen Impfzahlen der Stadt München.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
