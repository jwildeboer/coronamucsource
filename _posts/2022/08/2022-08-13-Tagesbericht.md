---
layout: default
title: 2022-08-13 Tagesbericht
category: Tagesberichte
---

![2022-08-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, Tendenz weiter stabil sinkend. Zweite zeigt Krankenbetten, wo am Samstag keine neuen Zahlen veröffentlicht werden, dritte zeigt Inzidenz, die weiter deutlich sinkt. Tag 3 unter Inzidenz 300.](/images/2022/08/20220813.png)

Das RKI meldet eine Inzidenz von 245,6, 496 neue und 85 Nachmeldungen, davon 66 für vorgestern. Die gestern gemeldete Inzidenz von 265,2 steigt damit auf 270,5.
<!--more-->

Leider wieder 3 neue Todesfälle, die 7-Tages-Summe ist jetzt 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 05.08. | 370,2 -> 394,0 |
| 06.08. | 369,7 -> 394,4 |
| 07.08. | 369,2 -> 394,2 |
| 08.08. | 329,2 -> 350,8 |
| 09.08. | 302,1 -> 313,1 |
| 10.08. | 276,7 -> 284,8 |
| 11.08. | 265,2 -> 270,5 |

Da am Wochenende weiterhin keine Zahlen aus Bayern an das RKI gemeldet werden, Montag auch nicht weil Feiertag in Bayern,  hier schon mal die Inzidenzen für Sonntag, Montag und Dienstag:

| Sonntag, 14.08. | 244,8 |
| Montag, 15.08. | 244,5 |
| Dienstag, 16.08. | 158,2 | 

Die (geschätzte) Zahl der Aktiven Fälle geht mit 9.335 unter 10.000. Das hatten wir zuletzt am 19.06. (9375).

## Reproduktionswert R(t) für München

Leichte Steigung bei unserem R(t), daß jetzt auf 0,75 geht. 

Das R(t) vom StaBLab der LMU (Nowcast-Lauf vom 12.08.) liegt bei 0,69.

| Datum | R(t) | LMU |
|---|---:|---:|
| 06.08. | 0,74 | 0,66 |
| 07.08. | 0,74 | 0,65 |
| 08.08. | 0,73 | 0,67 |
| 09.08. | 0,71 | 0,70 |
| 10.08. | 0,71 | 0,72 |
| 11.08. | 0,73 | 0,69 |
| 12.08. | 0,75 | ---- |

## Krankenhaus

Am Samstag keine neuen Krankenhauszahlen von der Stadt München. 

## Impfungen

Am Samstag keine neuen Impfzahlen von der Stadt München