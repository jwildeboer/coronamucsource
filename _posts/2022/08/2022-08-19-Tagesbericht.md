---
layout: default
title: 2022-08-19 Tagesbericht
category: Tagesberichte
---

![2022-08-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche in der Summe ähnlich. Zweite zeigt Krankenbetten, wieder insgesamt 8 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches sich gerade auf einem Plateau um 200 einpendelt wohl wegen Meldeverzug durch Wochenende/Feiertag.](/images/2022/08/20220819.png)

Das RKI meldet eine Inzidenz von 199,4 für München. 490 Neue, 133 Nachmeldungen. Die für gestern gemeldete Inzidenz von 195,8 erhöht sich auf 204,3. Da sind wohl weiterhin noch einige Nachmeldungen vom Wochenende und Feiertag enthalten.
<!--more-->

Keine neuen Todesfälle, die 7-Tages-Summe der Todesfälle sinkt auf 9. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle nur 2.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 11.08. | 265,2 -> 272,5 |
| 12.08. | 245,6 -> 255,2 |
| 13.08. | 244,8 -> 254,4 |
| 14.08. | 244,5 -> 254,1 |
| 15.08. | 158,2 -> 167,6 |
| 16.08. | 179,7 -> 191,4 |
| 17.08. | 195,8 -> 204,3 |

## Reproduktionswert R(t) für München

Unser R(t) steigt ein wenig auf jetzt 0,77. 

Das R(t) vom StaBLab der LMU wurde aktualsiert (Nowcast Lauf vom 18.08.) und liegt jetzt bei 0,7

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.08. | 0,75 | 0,71 |
| 13.08. | 0,77 | 0,68 |
| 14.08. | 0,78 | 0,68 |
| 15.08. | 0,75 | 0,71 |
| 16.08. | 0,74 | 0,75 |
| 17.08. | 0,75 | 0,77 |
| 18.08. | 0,77 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Normalbetten gehen von gestern 301 auf jetzt 298 (zum ersten mal seit dem 08.7. wieder unter 300), IMC sinkt von 11 auf 11, Intensiv sinkt von 29 auf 28. Damit insgesamt 333 Betten belegt, das sind 8 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 298 | -3 | 
| IMC | 8 | -3 | 
| Intensiv | 27 | -2 |
| SUMME | 333 | -8 |

Im Wochenvergleich sind das 49 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Ob die neue STIKO Empfehlung für Booster ab 60 Jahren daran was ändern wird? Wir werden es hoffentlich bald sehen.

1. +14
2. +18
3. +44
4. +76

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.