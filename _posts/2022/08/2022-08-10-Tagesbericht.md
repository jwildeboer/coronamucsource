---
layout: default
title: 2022-08-10 Tagesbericht
category: Tagesberichte
---

![2022-08-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern 16 mehr, Normal plus 13, IMC plus 3, Intensiv unverändert, dritte zeigt Inzidenz, die weiter sehr deutlich sinkt. Tag 5 unter Inzidenz 400.](/images/2022/08/20220810.png)

Das RKI meldet eine Inzidenz von 302,1, 656 neue und 292 Nachmeldungen für die letzten 7 Tage, davon 272 für vorgestern. Die gestern gemeldete Inzidenz steigt damit von 329,2 auf 348,7.
<!--more-->

Leider wieder 5 neue Todesfälle, die 7-Tages-Summe sinkt jedoch auf 16, da die 12 vom letzten Dienstag rausfallen.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 02.08. |  555,1 -> 575,0 |
| 03.08. |  461,6 -> 476,5 |
| 04.08. |  405,2 -> 419,4 |
| 05.08. |  370,2 -> 393,1 |
| 06.08. |  369,7 -> 393,4 |
| 07.08. |  369,2 -> 393,3 |
| 08.08. |  329,2 -> 348,7 |

Das LGL hatte gestern gemeldet, daß am Nachmittag nicht alle Zahlen korrekt an das RKI gingen. Sehr stark hat sich das anscheinend nicht ausgewirkt. Es gibt lediglich eine kleine Diskrepanz von 10 Fällen in den Berechnungen, die vermutlich damit zu erklären ist.

## Reproduktionswert R(t) für München

Das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) hat mal wieder einen aktuellen Nowcast und R(t) gemeldet:

Das R(t) vom StaBLab der LMU (Nowcast Lauf vom 09.08. liegt bei 0,64.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.08. | 0,79 | 0,79 |
| 04.08. | 0,77 | 0,76 |
| 05.08. | 0,76 | 0,71 |
| 06.08. | 0,74 | 0,66 |
| 07.08. | 0,74 | 0,64 |
| 08.08. | 0,72 | 0,66 |
| 09.08. | 0,71 | 0,69 |

## Krankenhaus

Heute wieder etwas größere Änderungen bei den Krankenhausbetten. 13 mehr auf Normalstation, 3 mehr auf IMC, Intensiv unverändert. Insegsamt 403 Betten belegt, 16 mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 353 | +13 | 
| IMC | 16 | +3 | 
| Intensiv | 34 | +0 |
| SUMME | 403 | +16 |

Im Wochenvergleich sind das nur noch 18 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 08.08., sind weiter sehr niedrig. Die Sommerferien sind halt da. Heute mal wieder nur Daten von einem Tag, dafür mit Nachmeldungen:

1. +58
2. +75
3. +400
4. +2.227

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
