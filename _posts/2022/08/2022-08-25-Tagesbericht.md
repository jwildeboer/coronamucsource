---
layout: default
title: 2022-08-25 Tagesbericht
category: Tagesberichte
---

![2022-08-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, insgesamt 17 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches um sich um 200 bewegt, mit unklarer Tendenz.](/images/2022/08/20220825.png)

Das RKI meldet eine Inzidenz von 192,3 für München. 390 Neue, 124  Nachmeldungen, davon 107 für vorgestern. Die gestern gemeldete Inzidenz von 212,1 erhöht sich auf 220,1.
<!--more-->

Leider auch wieder 3 neue Todesfälle, die 7-Tages-Summe der Todesfälle ist jetzt 16. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 0.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 17.08. | 195,8 -> 206,4 |
| 18.08. | 199,4 -> 210,1 | 
| 19.08. | 195,1 -> 206,9 | 
| 20.08. | 195,0 -> 206,9 | 
| 21.08. | 195,0 -> 207,0 | 
| 22.08. | 246,3 -> 255,5 |
| 23.08. | 212,1 -> 220,1 |

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf jetzt 1,04.

Das R(t) vom StaBLab der LMU wurde heute aktualsiert, aber nur bis zum Stand 2022-08-23. Das StaBLab/LMU R(t) liegt jetzt bei 0,88.

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.08. | 0,78 | 0,85 |
| 19.08. | 0,83 | 0,81 |
| 20.08. | 0,86 | 0,80 |
| 21.08. | 0,87 | 0,83 |
| 22.08. | 0,96 | 0,88 |
| 23.08. | 1,01 | --- |
| 24.08. | 1,04 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute deutlich: Normalbetten sinken von gestern 241 auf jetzt 230, IMC von 6 auf heute nur noch 2 und Intensivbetten gehen von 20 auf 18. Damit insgesamt 250 Betten belegt, das sind 17 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 230 | -11 | 
| IMC | 2 | -4 | 
| Intensiv | 18 | -2 |
| SUMME | 250 | -17 |

Im Wochenvergleich sind das sogar 91 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Und die etwas komische Korrektur bei den Zweitimpfungen bleibt weiterhin unerklärt. Am 19.08. laut Stadt 1.062.042 Zweitimpfungen, am 20.08. dann aber nur 1.062.013 Zweitimpfungen - also 29 weniger? 

1. +13
2. +19
3. +81
4. +293

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 14.825 Tests/Tag mit einer Positivrate von 22,1%. Gegenüber den Zahlen von letzter Woche (16.730 Tests/Tag mit 24,49% Positivrate) also weiter weniger Tests, gepaart mit leicht sinkender Positivrate.

14.825 Tests/Tag ist der niedrigste Stand seit Beginn der Pandemie. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 14.825 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 22,1%.](/images/2022/08/20220825-Test.png)