---
layout: default
title: 2022-08-20 Tagesbericht
category: Tagesberichte
---

![2022-08-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, weiter sinkend, Samstags aber keine neuen Zahlen, dritte zeigt Inzidenz, welches sich gerade auf einem Plateau um 200 einpendelt wohl immer noch wegen Meldeverzug durch Wochenende/Feiertag.](/images/2022/08/20220820.png)

Das RKI meldet eine Inzidenz von 195,1, 338 neue und 143 Nachmeldungen, davon 127 für vorgestern. Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von 199,4 auf 208,4.
<!--more-->

Leider wieder 5 neue Todesfälle, die 7-Tages-Summe der Todesfälle ist damit 11.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 12.08. | 245,6 -> 255,5 |
| 13.08. | 244,8 -> 254,7 |
| 14.08. | 244,5 -> 254,3 |
| 15.08. | 158,2 -> 167,9 |
| 16.08. | 179,7 -> 191,8 |
| 17.08. | 195,8 -> 205,1 |
| 18.08. | 199,4 -> 208,4 |

Da am Wochenende keine Zahlen vom LGL an das RKI gemeldet werden, können wir die Inzidenz für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 195,0 |
| Montag | 195,0 |

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf jetzt 0,82. 

Das R(t) vom StaBLab der LMU wurde aktualsiert (Nowcast Lauf vom 19.08.) und liegt jetzt bei 0,79.

| Datum | R(t) | LMU |
|---|---:|---:|
| 13.08. | 0,77 | 0,69 |
| 14.08. | 0,78 | 0,70 |
| 15.08. | 0,75 | 0,73 |
| 16.08. | 0,74 | 0,78 |
| 17.08. | 0,75 | 0,81 |
| 18.08. | 0,77 | 0,79 |
| 19.08. | 0,82 | --- |

## Krankenhaus

Am Samstag gibt es keine neuen Krankenhauszahlen der Stadt München. 

## Impfungen

Am Samstag gibt es keine neuen Impfzahlen der Stadt München.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.