---
layout: default
title: 2022-08-28 Tagesbericht
category: Tagesberichte
---

![2022-08-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, weiter sinkend aber da Sonntag, keine Aktualisierung, dritte zeigt Inzidenz, welches, mit wieder leicht sinkender Tendenz, unter 200 geht.](/images/2022/08/20220828.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 120 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 123, letzte Woche 161.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 675, gestern waren es 705, letzte Woche 776. 

## Reproduktionswert R(t) für München

Unser R(t) sinkt wieder, und liegt jetzt bei 0,96. 

Das R(t) vom StaBLab der LMU (Nowcast-Lauf vom 25.08.) liegt bei 1,01.
Unser R(t) steigtweiter auf jetzt 0,82. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.08. | 0,87 | 0,81 |
| 22.08. | 0,97 | 0,87 |
| 23.08. | 1,02 | 0,95 |
| 24.08. | 1,05 | 1,01 |
| 25.08. | 1,07 | --- |
| 26.08. | 0,99 | --- |
| 27.08. | 0,96 | --- |

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München