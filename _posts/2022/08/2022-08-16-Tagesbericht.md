---
layout: default
title: 2022-08-16 Tagesbericht
category: Tagesberichte
---

![2022-08-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, seit 3 Tagen keine neuen Zahlen. Zweite zeigt Krankenbetten, gegenüber Freitag 30 Betten weniger, Normal minus 32, IMC minus 3, Intensiv plus 5, dritte zeigt Inzidenz, die weiter sinkt aber auch hier fehlen Zahlen für 3 Tage.](/images/2022/08/20220816.png)

Das RKI meldet eine Inzidenz von 158,2. Da gestern Feiertag war, gibt es seit 3 Tagen keine neuen Zahlen oder Nachmeldungen. Somit handelt es sich effektiv um eine 4 Tages Inzidenz, die mit der Realität wenig zu tun hat. Heute wird wieder gemeldet, daher morgen dann "echte" Zahlen.
<!--more-->

## Reproduktionswert R(t) für München

Unser R(t) sinkt wieder auf 0,73 - auch wegen der fehlenden Zahlen. 

Das R(t) vom StaBLab der LMU wurde auch heute noch nicht aktualisiert, daher liegt es seit dem 12.08. bei 0,69.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.08. | 0,71 | 0,70 |
| 10.08. | 0,71 | 0,72 |
| 11.08. | 0,73 | 0,69 |
| 12.08. | 0,75 | --- |
| 13.08. | 0,77 | --- |
| 14.08. | 0,77 | --- |
| 15.08. | 0,73 | --- |

## Krankenhaus

Auch heute wieder etwas größere Änderungen bei den Krankenhausbetten. Diesmal sinkend: Im Vergleich zu den zuletzt an Freitag gemeldeten Zahlen gehen die Normalbetten von 345 auf jetzt 313, IMC Betten gehen von 8 auf 5 und Intensivbetten steigen von 29 auf 34.

Damit insgesamt 30 Betten weniger belegt als am Freitag. In der Tabelle haben wir das interpoliert und zeigen den Vergleich zu gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 313 | -8 | 
| IMC | 5 | -1 | 
| Intensiv | 34 | +1 |
| SUMME | 352 | -8 |

Im Wochenvergleich sind das 35 Betten weniger. 

## Impfungen

Die Impfzahlen wurden heute leider nicht aktualisiert, sond also weiterhin auf dem Stand von 10.08.

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
