---
layout: default
title: 2022-08-12 Tagesbericht
category: Tagesberichte
---

![2022-08-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, Tendenz weiter sinkend. Zweite zeigt Krankenbetten, gegenüber gestern 8 weniger, Normal minus 4, IMC minus 5, Intensiv plus 1, dritte zeigt Inzidenz, die weiter sehr deutlich sinkt. Tag 7 unter Inzidenz 400.](/images/2022/08/20220812.png)

Das RKI meldet eine Inzidenz von 265,2, 484 neue und 149 Nachmeldungen für die letzten 7 Tage, davon 98 für vorgestern. Die gestern gemeldete Inzidenz steigt damit von 276,7 auf 284,0.
<!--more-->

Leider wieder 6 neue Todesfälle, die 7-Tages-Summe steigt auf 34. So einen hohen Wert hatten wir zuletzt im Februar.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 04.08. |  405,2 -> 420,2 |
| 05.08. |  370,2 -> 394,0 |
| 06.08. |  369,7 -> 394,3 |
| 07.08. |  369,2 -> 394,2 |
| 08.08. |  329,2 -> 350,7 |
| 09.08. |  302,1 -> 312,9 |
| 10.08. |  276,7 -> 284,0 |

## Reproduktionswert R(t) für München

Ganz leichte Steigung bei unserem R(t), daß jetzt auf 0,73 geht. 

Das R(t) vom StaBLab der LMU (Nowcast-Lauf vom 10.08.) liegt bei 0,69.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.08. | 0,76 | 0,71 |
| 06.08. | 0,74 | 0,66 |
| 07.08. | 0,74 | 0,64 |
| 08.08. | 0,73 | 0,66 |
| 09.08. | 0,71 | 0,69 |
| 10.08. | 0,71 | 0,69 |
| 11.08. | 0,73 | ---- |

## Krankenhaus

Auch heute wieder etwas größere Änderungen bei den Krankenhausbetten. Diesmal sinkend: 4 weniger auf Normalstation, 5 weniger auf IMC, 1 mehr auf Intensiv. Insegsamt 382 Betten belegt, 8  weniger als gestern. Tag 2 unter 400.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 345 | -4 | 
| IMC | 8 | -5 | 
| Intensiv | 29 | +1 |
| SUMME | 382 | -8 |

Im Wochenvergleich sind das 22 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 10.08., sind weiter sehr niedrig. Die Sommerferien sind halt da.

1. +16
2. +12
3. +43
4. +117

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
