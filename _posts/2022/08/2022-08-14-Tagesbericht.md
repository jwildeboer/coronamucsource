---
layout: default
title: 2022-08-14 Tagesbericht
category: Tagesberichte
---

![2022-08-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche aber keine neuen Zahlen am Sonntag. Zweite zeigt Krankenbetten, keine neuen Zahlen am Sonntag, aber sinkt auch, dritte zeigt Inzidenz, die sehr deutlich sinkt aber auch ohne neue Zahlen weil Sonntag.](/images/2022/08/20220814.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister):

- Bayernweit gibt es heute 190 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 182, letzte Woche 227.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 803, gestern waren es 842. Damit seit 6 Tagen, auch mit Nachmeldungen, unter dem Grenzwert von 1200 für die "alte" gelbe Ampel.

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München