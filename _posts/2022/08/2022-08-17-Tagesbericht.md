---
layout: default
title: 2022-08-17 Tagesbericht
category: Tagesberichte
---

![2022-08-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, Tendenz weiter sinkend. Zweite zeigt Krankenbetten, wo sich im Vergleich zu gestern kaum Änderungen ergeben, dritte zeigt Inzidenz, die weiter deutlich sinkt.](/images/2022/08/20220817.png)

Das RKI meldet eine Inzidenz von 179,7 für Monchen. 984 Neue, 139 Nachmeldungen, davon 4 für Montag, 0 für Sonntag, 1 für Samstag und 104 für Freitag. Die für gestern gemeldete Inzidenz von 158,2 erhöht sich auf 166,8, für das letzte Wochenende und Feiertag von ca. 245 auf ca. 254.

<!--more-->

Leider auch 1 neuer Todesfall, die 7-Tages-Summe ist jetzt 25.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 09.08. | 302,1 -> 313,7 |
| 10.08. | 276,7 -> 285,7 |
| 11.08. | 265,2 -> 272,2 |
| 12.08. | 245,6 -> 254,3 |
| 13.08. | 244,8 -> 253,5 |
| 14.08. | 244,5 -> 253,2 |
| 15.08. | 158,2 -> 166,8 |

## Reproduktionswert R(t) für München

Unser R(t) sinkt leicht auf 0,73. 

Das R(t) vom StaBLab der LMU wurde auch heute noch nicht aktualisiert, daher liegt es seit dem letzten Lauf vom 12.08. bei 0,69.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.08. | 0,72 | 0,72 |
| 11.08. | 0,73 | 0,69 |
| 12.08. | 0,75 | --- |
| 13.08. | 0,77 | --- |
| 14.08. | 0,78 | --- |
| 15.08. | 0,75 | --- |
| 16.08. | 0,73 | --- |

## Krankenhaus

Heute kaum Änderungen in der Summe im Vergleich zu gestern, dafür einige Verschiebungen: Normalbetten gehen von gestern 313 auf jetzt 309, IMC steigen von 5 auf 9, Intensiv sinkt von 34 auf 32.

Damit insgesamt 2 Betten weniger belegt als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 309 | -4 | 
| IMC | 9 | +4 | 
| Intensiv | 32 | -2 |
| SUMME | 350 | -2 |

Im Wochenvergleich sind das 53 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 13.08., sind weiter sehr niedrig. Es wurden Zahlen für 3 Tage nachgemeldet. Wir haben das interpoliert und damit ergibt sich pro Tag:

1. +12
2. +11
3. +43
4. +96

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.