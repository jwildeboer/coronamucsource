---
layout: default
title: 2022-08-22 Wochenbericht KW33
category: Wochenberichte
---

![2022-08-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche aber keine neuen Zahlen Sonntag und heute. Zweite zeigt Krankenbetten, wo die Normalbetten deutlich zurückgegangen sind. Dritte zeigt Inzidenz, die sich um Inzidenz 200 stabilisiert.](/images/2022/08/20220822.png)

In der letzten Woche gab es insgesamt 2.902 neue COVID Fälle in München, das sind 23,3% weniger als in der Vorwoche (3.785). Leider aber auch 11 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (30).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 3.785 kontinuierlich zurück auf 2.902. Die Inzidenz verlor 59,3 Punkte in 7 Tagen. Die Welle klingt also weiter klar ab. Wegen der Sommerferien wird es aber wohl noch weniger Tests geben, die Dunkelziffer bleibt daher wohl hoch.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 14.08. | 0 | 3.785 | 245,3 |
| 15.08. | 5 | 2.498 | 167,9 |
| 16.08. | 1.149 | 2.854 | 191,8 |
| 17.08. | 794 | 3.052 | 205,1 |
| 18.08. | 616 | 3.102 | 208,4 |
| 19.08. | 338 | 2.903 | 195,1 |
| 20.08. | 0 | 2.902 | 195,0 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das war auch in der letzten Woche wieder stabil unter 1. 

Leider steigt es aber gerade wieder, so daß wir jetzt ein R(t) von 0,85 sehen. 

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf vom 19.08.) liegt bei 0,79.

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.08. | 0,75 | 0,73 |
| 16.08. | 0,74 | 0,78 |
| 17.08. | 0,75 | 0,81 |
| 18.08. | 0,77 | 0,79 |
| 19.08. | 0,82 | --- |
| 20.08. | 0,84 | --- |
| 21.08. | 0,85 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München verbessert sich die Lage bei den Normalbetten deutlich, bei IMC und Intensivbetten etwas zögerlicher. Wieder unter 300 Betten insgesamt belegt. Das hatten wir zuletzt am 07.06. mit 296.

Im Vergleich zu letzter Woche satte 85 Betten weniger belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 15.08. | 313 | 5 | 34 | 352 | -35 | 15 | 
| 16.08. | 309 | 9 | 32 | 350 | -53 | 20 |
| 17.08. | 301 | 11 | 29 | 341 | -49 | 18 |
| 18.08. | 298 | 8 | 27 | 333 | -49 | 17 |
| 19.08. | 280 | 8 | 26 | 314 | -61 | 19 |
| 20.08. | 262 | 8 | 25 | 295 | -73 | 19 |
| 21.08. | 243 | 8 | 24 | 275 | -85 | 19 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiterhin ziemlich gut aus. Seit 12 Tagen sind wir wieder klar unter der dem Grenzwert der alten gelben Ampel und die Zahlen sinken weiter kontinuierlich.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 15.08. | 822 | 184 |
| 16.08. | 831 | 207 |
| 17.08. | 937 | 232 |
| 18.08. | 875 | 242 |
| 19.08. | 811 | 251 |
| 20.08. | 776 | 276 |
| 21.08. | 749 | 299 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,89% | 75,33% |
| 2. | 71,36% | 75,22% |
| 3. | 47,84% | 58,81% |
| 4. | 5,87% | 5,87% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/08/KW33Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen Bayern

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca 15.600 Tests/Tag mit einer Positivrate von 25,2%. Gegenüber den Zahlen von letzter Woche (20.930 Tests/tag mit 31,37% Positivrate) also deutlich weniger Tests, gepaart mit sinkender Positivrate.

15.600 Tests/Tag ist der niedrigste Stand seit Beginn der Pandemie. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 15.600 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 25,2%.](/images/2022/08/20220818-Test.png)