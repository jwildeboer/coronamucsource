---
layout: default
title: 2022-08-23 Tagesbericht
category: Tagesberichte
---

![2022-08-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, insgesamt 2 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches gerade auf 246,3 springt.](/images/2022/08/20220823.png)

Das RKI meldet eine überraschend hohe Inzidenz von 246,3 für München. 602 Neue, 169  Nachmeldungen. Für Samstag und Sonntag jeweis 1 Nachmeldung, für letzten Freitag 149. Die für letzten Freitag gemeldete Inzidenz von 195,1 erhöht sich auf 206,1.
<!--more-->

Leider auch wieder 4 neue Todesfälle, die 7-Tages-Summe der Todesfälle steigt auf 15. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 15.08. | 158,2 -> 167,9 |
| 16.08. | 179,7 -> 192,1 |
| 17.08. | 195,8 -> 205,8 |
| 18.08. | 199,4 -> 209,4 | 
| 19.08. | 195,1 -> 206,1 | 
| 20.08. | 195,0 -> 206,1 | 
| 21.08. | 195,0 -> 206,2 | 

## Reproduktionswert R(t) für München

Unser R(t) steigt deutlich auf jetzt 0,96. 

Das R(t) vom StaBLab der LMU wurde heute noch nicht aktualsiert (letzter Nowcast Lauf war am 19.08.) und liegt deshalb weiter bei 0,79.

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.08. | 0,74 | 0,75 |
| 17.08. | 0,75 | 0,77 |
| 18.08. | 0,77 | 0,79 |
| 19.08. | 0,83 | --- |
| 20.08. | 0,86 | --- |
| 21.08. | 0,87 | --- |
| 22.08. | 0,96 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt insgesamt ein wenig: Normalbetten steigen von gestern 243 auf jetzt 247, IMC sinkt von 8 auf 6, Intensiv sinkt von 24 auf 20. Damit insgesamt 273 Betten belegt, das sind 2 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 247 | +4 | 
| IMC | 6 | -2 | 
| Intensiv | 20 | -4 |
| SUMME | 275 | -2 |

Im Wochenvergleich sind das 79 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Ob die neue STIKO Empfehlung für Booster ab 60 Jahren daran was ändern wird? Wir werden es hoffentlich bald sehen.

1. +12
2. +15
3. +53
4. +137

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.