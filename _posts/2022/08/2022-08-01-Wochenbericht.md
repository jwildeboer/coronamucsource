---
layout: default
title: 2022-08-01 Wochenbericht KW30
category: Wochenberichte
---

![2022-08-01 Dashboard mit den Zahlen für München. Am Montag keine neuen Fallzahlen. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, stabil mit leicht sinkender Tendenz, dritte zeigt Inzidenz, die weiter klar sinkt ](/images/2022/08/20220801.png)

In der letzten Woche gab es insgesamt 9.900 neue COVID Fälle in München, das sind 26% weniger als in der Vorwoche (13.459). Leider aber auch insgesamt 7 Tote mit COVID Nachweis in den letzten 7 Tagen.
<!--more-->

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berehchnen wir parallel "unser" R(t) weiter. Das rutschte diese Woche stabil unter 1. Das ist keine komplette Entwarnung (dazu wäre ein, über mehrere Tage stabiles R(t) um 0,6 erforderlich und nicht 0,9-0,8, die wir da grad sehen).

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Das sind keine besonders guten Voraussetzungen für die Herbst/Winterwelle, die mit Sicherheit kommen wird. Über zwei Jahre Pandemie und noch immer funktioniert nicht mal das Zahlen sammeln nur leidlich zuverlässig.

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 14.111 kontinuierlich zurück auf 9.892, und war damit zum ersten mal seit dem 30.06. wieder unter 10.000. Die Inzidenz verlor fast 300 Punkte in 7 Tagen. Die Welle klingt also klar ab. Wegen der Sommerferien wird es aber wohl noch weniger Tests gebe, die Dunkelziffer bleibt daher wohl hoch.

Wenn morgen die ersten neuen Zahlen der Woche kommen, könnte die Inzidenz schon unter 550 liegen. Wir werden es sehen.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 24.07. | 8 | 14.111 | 947,5 |
| 25.07. | 18 | 13.802 | 948,2 |
| 26.07. | 2599 | 12.848 | 927,4 | 
| 27.07. | 2194 | 12.186 | 863,3 |
| 28.07. | 2456 | 10.952 | 818,8 |
| 29.07. | 1574 | 9.900 | 735,9 |
| 30.07. | 1051 | 9.892 | 665,2 |

## Krankenhaus

Bei den Krankenhauszahlen in München wurde in der letzten Woche wohl auch die Spitze erreicht, die Zahlen sinken jedenfalls wieder ein wenig. Am Montag wurden insgesamt 464 Betten gemeldet, 409 Normal, 18 IMC und 37 Intensiv. Obwohl das klar weniger ist als Mitte letzter Woche, liegen wir damit immer noch 14 Betten über den Zahlen aus der Woche zuvor.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr> |
|---|--:|--:|--:|--:|--:|--:|
| 24.07. | 404 | 11 | 35 | 450 | +44 | 21 |
| 25.07. | 423 | 13 | 40 | 476 | +63 | 22 |
| 26.07. | 419 | 12 | 40 | 471 | +58 | 24 |
| 27.07. | 422 | 14 | 39 | 475 | +36 | 21 |
| 28.07. | 423 | 13 | 35 | 471 | +28 | 18 |
| 29.07. | 418 | 14 | 35 | 467 | +21 | 17 |
| 30.07. | 414 | 16 | 36 | 466 | +19 | 17 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine Zunahme bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,86% | 75,32% |
| 2. | 71,33% | 75,19% |
| 3. | 47,69% | 58,70% |
| 4. | 5,27% | 5,42% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/08/KW30Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsminiteriums](/images/2022/08/KW30FAQImpf.png)

## Tests (bayernweit)

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Wir testen mit ca 30% der maximalen Kapazität und einer weiter steigenden Positivrate von über 40%. Da es keine Zahlen zu Bürgertests gibt, kann man nur vermuten, daß deutlich zu wenig getestet wird und die Dunkelziffer daher hoch sein muss.

Auch hier wird sich in der nächsten Woche wohl der Einfluß der Sommerferien bemerkbar machen - weniger Tests. Ob die Positivrate dann aber niedriger wird, das zeigt sich am Donnerstag, wenn die neuen Zahlen vom LGL kommen.

![Bayernweite Tests - Anzahl Tests steigt leicht auf 320000 pro Tag, Positivrate steigt auf über 40%](/images/2022/07/20220728-Test.png)