---
layout: default
title: 2022-08-29 Wochenbericht KW34
category: Wochenberichte
---

![2022-08-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger, seit 2 Tagen keine neuen Zahlen da Wochenende. Zweite zeigt Krankenbetten, weiter etwas sinkend, dritte zeigt Inzidenz, welches, mit wieder leicht sinkender Tendenz, unter 200 geht.](/images/2022/08/20220829.png)

In der letzten Woche gab es insgesamt 2.736 neue COVID Fälle in München, das sind 11,2% weniger als in der Vorwoche (3.084). Leider aber auch 18 Tote mit COVID Nachweis in den letzten 7 Tagen, mehr als in der Vorwoche (11).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 3.084 zurück auf 2.736. Die Inzidenz verlor 23,4 Punkte in 7 Tagen. Der Ausreisser nach oben am Montag waren Nachwirkungen des Feiertages in der Woche zuvor. Die Welle klingt also weiter ab. Wegen der Sommerferien gibt es weiterhin weniger Tests geben, die Dunkelziffer bleibt daher wohl hoch.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 21.08. | 1 | 3.084 | 207,2 |
| 22.08. | 743 | 3.822 | 256,8 |
| 23.08. | 642 | 3.305 | 222,1 |
| 24.08. | 492 | 2.991 | 201,0 |
| 25.08. | 528 | 2.898 | 194,7 |
| 26.08. | 330 | 2.737 | 183,9 |
| 27.08. | 0 | 2.736 | 183,8 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das war in der letzten Woche kurz über 1, in den letzten 3 Tagen aber wieder unter 1. 

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf vom 25.08.) liegt bei 1,01.

| Datum | R(t) | LMU |
|---|---:|---:|
| 22.08. | 0,97 | 0,87 |
| 23.08. | 1,02 | 0,95 |
| 24.08. | 1,05 | 1,01 |
| 25.08. | 1,07 | --- |
| 26.08. | 0,99 | --- |
| 27.08. | 0,96 | --- |
| 28.08. | 0,94 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München verbessert sich die Lage weiter, bei IMC und Intensivbetten etwas zögerlicher. Seit3 Tagen weniger als 250 Betten insgesamt belegt. Das hatten wir zuletzt am 26.06. mit 242.

Im Vergleich zu letzter Woche immerhin noch 29 Betten weniger belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 22.08. | 247 | 6 | 20 | 273 | -79 | 75 |
| 23.08. | 241 | 6 | 20 | 267 | -83 | 84 |
| 24.08. | 230 | 2 | 16 | 250 | -91 | 89 |
| 25.08. | 224 | 5 | 22 | 251 | -82 | 90 |
| 26.08. | 222 | 6 | 21 | 249 | -65 | 97 |
| 27.08. | 221 | 7 | 20 | 248 | -47 | 97 |
| 28.08. | 219 | 8 | 19 | 246 | -29 | 97 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiterhin ziemlich gut aus. Seit dem 10.08. sind wir wieder unter dem Grenzwert der alten gelben Ampel und die Zahlen sinken weiter kontinuierlich.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 22.08. | 1.133 | 333 |
| 23.08. | 930 | 366 |
| 24.08. | 853 | 409 |
| 25.08. | 814 | 446 |
| 26.08. | 705 | 475 |
| 27.08. | 675 | 486 |
| 28.08. | 648 | 518 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,89% | 75,33% |
| 2. | 71,37% | 75,23% |
| 3. | 47,87% | 58,83% |
| 4. | 5,99% | 5,97% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/08/KW34Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

# Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca 14.825 Tests/Tag mit einer Positivrate von 22,1%. Gegenüber den Zahlen von letzter Woche (16.730 Tests/Tag mit 24,49% Positivrate) also weiter weniger Tests, gepaart mit leicht sinkender Positivrate.

14.825 Tests/Tag ist der niedrigste Stand seit Beginn der Pandemie. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir im Herbst/Winter wahrscheinlich auch brauchen werden.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 14.825 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt weiter auf jetzt 22,1%.](/images/2022/08/20220825-Test.png)