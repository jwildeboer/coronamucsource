---
layout: default
title: 2022-08-26 Tagesbericht
category: Tagesberichte
---

![2022-08-26 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, insgesamt 1 Bett mehr belegt als gestern, dritte zeigt Inzidenz, welches um sich um 200 bewegt, mit wieder leicht sinkender Tendenz.](/images/2022/08/20220826.png)

Das RKI meldet eine Inzidenz von 186,7 für München. 421 Neue, 120  Nachmeldungen, davon 101 für vorgestern. Die gestern gemeldete Inzidenz von 192,3 erhöht sich auf 200,1.
<!--more-->

Keine neuen Todesfälle, die 7-Tages-Summe der Todesfälle bleibt 16. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 0.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 18.08. | 199,4 -> 210,1 | 
| 19.08. | 195,1 -> 207,0 | 
| 20.08. | 195,0 -> 207,0 | 
| 21.08. | 195,0 -> 207,1 | 
| 22.08. | 246,3 -> 256,1 |
| 23.08. | 212,1 -> 221,3 |
| 24.08. | 192,3 -> 200,1 |

## Reproduktionswert R(t) für München

Unser R(t) steigt weiter auf jetzt 1,06.

Das R(t) vom StaBLab der LMU wurde heute nicht aktualsiert, bleibt daher auf dem  Stand vom 2022-08-23 bei 0,88.

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.08. | 0,83 | 0,81 |
| 20.08. | 0,86 | 0,80 |
| 21.08. | 0,87 | 0,83 |
| 22.08. | 0,96 | 0,88 |
| 23.08. | 1,02 | --- |
| 24.08. | 1,04 | --- |
| 25.08. | 1,06 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten bleibt nahezu unverändert, die Verteilung ändert sich jedoch: Normalbetten sinken von gestern 230 auf jetzt 226, IMC steigt von 2 auf 5 und Intensivbetten gehen von 18 wieder hoch auf 22. Damit insgesamt 251 Betten belegt, 1 Bett mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 224 | -6 | 
| IMC | 5 | +3 | 
| Intensiv | 22 | +4 |
| SUMME | 251 | -1 |

Im Wochenvergleich sind das trotzdem 82 Betten weniger. 

## Impfungen

Die Impfzahlen bleiben weiter sehr, sehr niedrig. Und die etwas komische Korrektur bei den Zweitimpfungen bleibt weiterhin unerklärt. Am 19.08. laut Stadt 1.062.042 Zweitimpfungen, am 20.08. dann aber nur 1.062.013 Zweitimpfungen - also 29 weniger? 

1. +16
2. +8
3. +48
4. +219

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.