---
layout: default
title: 2022-08-09 Tagesbericht
category: Tagesberichte
---

![2022-08-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern etwas mehr, Mormal plus 4, Intensiv plus 2, dritte zeigt Inzidenz, die weiter sehr deutlich sinkt. Tag 4 unter Inzidenz 400.](/images/2022/08/20220809.png)

Das RKI meldet eine Inzidenz von 329,2, 993 neue und 340 Nachmeldungen für die letzten 7 Tage, davon 5 für vorgestern, 12 für Samstag und 285 für Freitag. Die gestern gemeldete Inzidenz steigt damit von 369,2 auf 392,0.
<!--more-->

Leider auch 1 neu gemeldeter Todesfall, somit steigt die 7-Tages-Summe auf 23. So hoch war es zuletzt 02.05.2022 mit 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 01.08. |  607,6 -> 632,6 |
| 02.08. |  555,1 -> 574,7 |
| 03.08. |  461,6 -> 476,2 |
| 04.08. |  405,2 -> 418,7 |
| 05.08. |  370,2 -> 391,8 |
| 06.08. |  369,7 -> 392,2 |
| 07.08. |  369,2 -> 392,0 |

Das LGL meldet, daß gestern nicht alle Zahlen korrekt an das RKI gingen, daher morgen nachgemeldet wird und die Zahlen für Bayern von heute nicht unbedingt richtig auf dem RKI Dashboard vermeldet sind.

Wir schauen da morgen nochmal ganz genau nach.

## Reproduktionswert R(t) für München

Da das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert, verwenden wir unser eigenes 7 Tage R. Das liegt heute bei 0,72.

Das R(t) vom StaBLab der LMU (Nowcast Lauf vom 04.08., also vor 5 Tagen) liegt bei 0,75.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.08. | 0,81 | 0,74 |
| 03.08. | 0,78 | 0,75 |
| 04.08. | 0,77 | - |
| 05.08. | 0,76 | - |
| 06.08. | 0,74 | - |
| 07.08. | 0,73 | - |
| 08.08. | 0,72 | - |

## Krankenhaus

Minimale Änderungen bei den Krankenhausbetten. 4 mehr auf Normalstation, 2 mehr auf Intensiv, IMC unverändert. Insegsamt 387 Betten belegt, 6 mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 340 | +4 | 
| IMC | 13 | +0 | 
| Intensiv | 34 | +2 |
| SUMME | 387 | +6 |

Im Wochenvergleich sind das trotzdem 76 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 07.08., sind weiter sehr niedrig. Die Sommerferien sind halt da. Die neuen Zahlen umfassen drei Tage, daher entsprehend umgerechnet. Über das Wochenende täglich somit gerade mal:

1. +6
2. +6
3. +19
4. +48

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.
