---
layout: default
title: 2022-08-11 Tagesbericht
category: Tagesberichte
---

![2022-08-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, Tendenz weiter sinkend. Zweite zeigt Krankenbetten, gegenüber gestern 13 weniger, Normal minus 4, IMC minus 3, Intensiv minus 6, dritte zeigt Inzidenz, die weiter sehr deutlich sinkt. Tag 6 unter Inzidenz 400.](/images/2022/08/20220811.png)

Das RKI meldet eine Inzidenz von 276,6, 484 neue und 149 Nachmeldungen für die letzten 7 Tage, davon 125 für vorgestern. Die gestern gemeldete Inzidenz steigt damit von 302,1 auf 312,1.
<!--more-->

Leider wieder 15 neue Todesfälle, die 7-Tages-Summe steigt auf 28. So einen hohen Wert hatten wir zuletzt im April.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 03.08. |  461,6 -> 477,0 |
| 04.08. |  405,2 -> 419,9 |
| 05.08. |  370,2 -> 393,7 |
| 06.08. |  369,7 -> 394,0 |
| 07.08. |  369,2 -> 393,9 |
| 08.08. |  329,2 -> 350,4 |
| 09.08. |  302,1 -> 312,1 |


## Reproduktionswert R(t) für München

Das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) hat mal wieder einen aktuellen Nowcast und R(t) gemeldet:

Das R(t) vom StaBLab der LMU (Nowcast-Lauf vom 10.08.) liegt bei 0,69.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.08. | 0,79 | 0,79 |
| 04.08. | 0,77 | 0,76 |
| 05.08. | 0,76 | 0,71 |
| 06.08. | 0,74 | 0,66 |
| 07.08. | 0,74 | 0,64 |
| 08.08. | 0,72 | 0,66 |
| 09.08. | 0,71 | 0,69 |

## Krankenhaus

Auch heute wieder etwas größere Änderungen bei den Krankenhausbetten. Diesmal sinkend: 4 weniger auf Normalstation, 3 weniger auf IMC, 6 weniger auf Intensiv. Insegsamt 390 Betten belegt, 13 weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 349 | -4 | 
| IMC | 13 | -3 | 
| Intensiv | 28 | -6 |
| SUMME | 390 | -13 |

Im Wochenvergleich sind das nur noch 20 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 09.08., sind weiter sehr niedrig. Die Sommerferien sind halt da. Heute mal wieder nur Daten von einem Tag, dafür mit Nachmeldungen:

1. +18
2. +35
3. +95
4. +198

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 21.000 Tests/Tag mit einer Positivrate von 31,35%. Gegenüber den Zahlen von letzter Woche (27.300 Tests/tag mit 36,18% Positivrate) also deutlich weniger Tests, gepaart mit sinkender Positivrate.

Das war zu erwarten. Sommerferien bedeutet noch weniger Tests. Weniger als ein Viertel der vorhandenen Kapazitäten werden benutzt. Das die Positivrate wieder ein wenig weiter sinkt ist aber erfreulich.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Man erkennt: In der ersten Augustwoche sank die Positivrate von 36,1% auf 31,4%. Die Zahl der Tests pro Tag ist mit ca. 21.000 am Tag weit vom Maximum von 100.000 Tests/Tag entfernt.](/images/2022/08/20220811-Test.png)