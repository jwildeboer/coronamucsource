---
layout: default
title: 2022-08-04 Tagesbericht
category: Tagesberichte
---

![2022-08-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern wieder weniger. Satte 65 Betten weniger im Vergleich zur Vorwoche, dritte zeigt Inzidenz, die sehr deutlich sinkt. Tag 2 unter Inzidenz 600.](/images/2022/08/20220804.png)

Das RKI meldet eine Inzidenz von 461,6 für München. 828 Neue und 250 Nachmeldungen für die letzten 7 Tage, davon 222 für vorgestern. Die gestern gemeldete Inzidenz steigt von 555,1 auf 572,1 und bleibt damit unter 600.
<!--more-->

Leider auch 3 neue Todesfälle gemeldet, damit insgesamt 20 Tote in den letzten 7 Tagen.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 27.07. | 781,3 -> 821.9 |
| 28.07. | 712,1 -> 741,1 |
| 29.07. | 665,2 -> 678,2 |
| 30.07. | 664,7 -> 678,1 |
| 31.07. | 663,5 -> 677,4 |
| 01.08. | 607,6 -> 631,2 |
| 02.08. | 555,1 -> 572,1 |

## Reproduktionswert R(t) für München

Da das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert, verwenden wir unser eigenes 7 Tage R. Das liegt heute bei 0,78.

Das R(t) vom StaBLab der LMU (Nowcast Lauf vom 03.08.) liegt bei 0,75.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.07. | 0,96 | 0,96 |
| 29.07. | 0,91 | 0,88 |
| 30.07. | 0,87 | 0,80 |
| 31.07. | 0,84 | 0,75 |
| 01.08. | 0,82 | 0,74 |
| 02.08. | 0,81 | 0,75 |
| 03.08. | 0,78 | - |

## Krankenhaus

Weitere Verbesserung bei den Krankenhauszahlen heute. In allen Bereichen, aber am Deutlichsten bei den Intensivbetten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 371 | -3 | 
| IMC | 11 | -2 | 
| Intensiv | 28 | -6 |
| SUMME | 410 | -11 |

Im Wochenvergleich sind das 65 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 02.08., sind sehr, sehr niedrig. Ferieneffekt.

1. +22
2. +14
3. +60
4. +74

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca 26.500 Tests/Tag mit einer Positivrate von 37%. Gegenüber den Zahlen von letzter Woche (31.400 Tests/tag mit 43,7% Positivrate) also deutlich weniger Tests, gepaart mit sinkender Positivrate.

Die Zahlen der letzten Woche umfassen den Zeitraum vom 25.-31.07, daher noch kein Ferieneffekt enthalten.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Man erkennt: In der letzten Juliwoche sank die Positivrate von 43,7% auf 37%. Die Zahl der Tests pro Tag ist mit ca. 26.500 am Tag weit vom Maximum von 100.000 Tests/Tag entfernt.](/images/2022/08/20220804-Test.png)