---
layout: default
title: 2022-08-15 Wochenbericht KW32
category: Wochenberichte
---

![2022-08-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche aber keine neuen Zahlen Sonntag und heute. Zweite zeigt Krankenbetten, keine neuen Zahlen Sonntag und heute, aber sinkt auch, dritte zeigt Inzidenz, die sehr deutlich sinkt aber auch ohne neue Zahlen weil Sonntag und heute Feiertag.](/images/2022/08/20220815.png)

In der letzten Woche gab es insgesamt 3.643 neue COVID Fälle in München, das sind 37,9% weniger als in der Vorwoche (5.869). Leider aber auch insgesamt 30 Tote mit COVID Nachweis in den letzten 7 Tagen, wieder mehr als in der Vorwoche (22).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 5.869 kontinuierlich zurück auf 3.643. Die Inzidenz verlor fast 125 Punkte in 7 Tagen. Die Welle klingt also weiter klar ab. Wegen der Sommerferien wird es aber wohl noch weniger Tests geben, die Dunkelziffer bleibt daher wohl hoch.

Wegen dem heutigen Feiertag wird die Inzidenz morgen bei 158,2 liegen. Diese Zahl ist vieles, aber keine 7 Tages Inzidenz. Es sind nur Zahlen für 4 Tage vorhanden. Daher betrachten wir diese Inzidenz als Augenwischerei. Das RKI sollte sie gar nicht ausweisen. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 07.08. | 5 | 5.867 | 394,2 |
| 08.08. | 1.284 | 5.221 | 350,8 |
| 09.08. | 790 | 4.659 | 313,1 |
| 10.08. | 591 | 4.239 | 284,8 |
| 11.08. | 549 | 4.025 | 270,5 |
| 12.08. | 424 | 3.655 | 245,6 |
| 13.08. | 0 | 3.643 | 244,8 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berechnen wir parallel "unser" R(t) weiter. Das war in der letzten Woche stabil unter 1. Das ist keine komplette Entwarnung (dazu wäre ein, über mehrere Tage stabiles R(t) um 0,6 erforderlich aber 0,8 und darunter ist ein guter Wert.).

Unser R(t) steigt wieder langsam, das R(t) der LMU schwankt ein wenig, wurde aber auch seit dem 12.08. nicht mehr aktualisiert. Die versprochene tägliche Aktualisierung ist weit entfernt. Leider.

| Datum | R(t) | LMU |
|---|---:|---:|
| 07.08. | 0,74 | 0,65 |
| 08.08. | 0,73 | 0,67 |
| 09.08. | 0,71 | 0,70 |
| 10.08. | 0,71 | 0,72 |
| 11.08. | 0,73 | 0,69 |
| 12.08. | 0,75 | ---- |
| 13.08. | 0,77 | ---- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München stabilisiert sich die Lage. Die Zahlen sinken leider nicht so kräftig wie erhofft. Wwegen des Feiertages gibt es nur Zahlen bis letzten Freitag. Leider. Sobald morgen die neuen Zahlen von der Stadt München da sind, werden wir diese Tabelle ergänzen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 07.08. | 336 | 13 | 32 | 381 | -83 | 21 |
| 08.08. | 340 | 13 | 34 | 387 | -76 | 29 |
| 09.08. | 353 | 16 | 34 | 403 | -18 | 31 |
| 10.08. | 349 | 13 | 28 | 390 | -20 | 34 |
| 11.08. | 345 | 8 | 29 | 382 | -22 | 42 |
| 12.08. | 337 | 8 | 30 | 368 | -20 | 42 |
| 13.08. | 329 | 7 | 32 | 360 | -21 | 42 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine Zunahme bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 07.08. | 1.296 | 103|
| 08.08. | 1.175 | 113 |
| 09.08. | 1.101 | 116 |
| 10.08. | 1.036 | 129 |
| 11.08. | 951 | 151 |
| 12.08. | 842 | 164 |
| 13.08. | 803 | 168 |


## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,88% | 75,33% |
| 2. | 71,35% | 75,22% |
| 3. | 47,77% | 58,79% |
| 4. | 5,69% | 5,77% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/08/KW32Impf.png)

Ab dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben ab dann in München eine Impfquote unter 50%.

![FAQ des gesundheitsminiteriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen Bayern

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca 21.000 Tests/Tag mit einer Positivrate von 31,35%. Gegenüber den Zahlen von letzter Woche (27.300 Tests/tag mit 36,18% Positivrate) also deutlich weniger Tests, gepaart mit sinkender Positivrate.

Das war zu erwarten. Sommerferien bedeutet noch weniger Tests. Weniger als ein Viertel der vorhandenen Kapazitäten werden benutzt. Das die Positivrate wieder ein wenig weiter sinkt ist aber erfreulich.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Man erkennt: In der ersten Augustwoche sank die Positivrate von 36,1% auf 31,4%. Die Zahl der Tests pro Tag ist mit ca. 21.000 am Tag weit vom Maximum von 100.000 Tests/Tag entfernt.](/images/2022/08/20220811-Test.png)