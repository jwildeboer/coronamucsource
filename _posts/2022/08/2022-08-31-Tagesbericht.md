---
layout: default
title: 2022-08-31 Tagesbericht
category: Tagesberichte
---

![2022-08-31 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche vielleicht etwas höher. Zweite zeigt Krankenbetten, insgesamt 19 Betten weniger belegt als gestern, dritte zeigt Inzidenz, welches um sich um 190 bewegt, mit unklarer Tendenz.](/images/2022/08/20220831.png)

Das RKI meldet eine Inzidenz von 187,8 für München. 498 Neue, 142 Nachmeldungen, davon 134 für vorgestern. Die Inzidenz von gestern steigt durch die Nachmeldungen von 187,8 auf 198,0.
<!--more-->

Leider wieder 4 neue Todesfälle, die 7-Tages-Summe der Todesfälle bleibt damit 21. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 1.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 23.08. | 212,1 -> 223,0 | 
| 24.08. | 192,3 -> 202,1 | 
| 25.08. | 186,7 -> 196,3 | 
| 26.08. | 183,9 -> 188,5 | 
| 27.08. | 183,8 -> 188,5 | 
| 28.08. | 183,8 -> 188,4 | 
| 29.08. | 188,7 -> 198,0 | 

## Reproduktionswert R(t) für München

Unser R(t) sinkt leicht auf jetzt 0,91.

Das R(t) vom StaBLab der LMU wurde gestern aktualisiert. Dieser neue NowCast lauf vom 30.08. liefert ein R(t) von 0,87.

| Datum | R(t) | LMU |
|---|---:|---:|
| 24.08. | 1,05 | 1,05 |
| 25.08. | 1,07 | 1,07 |
| 26.08. | 1,00 | 1,00 |
| 27.08. | 0,96 | 0,91 |
| 28.08. | 0,95 | 0,86 |
| 29.08. | 0,92 | 0,87 |
| 30.08. | 0,91 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt wieder: Normalbetten sinken um 19 von gestern 228 auf jetzt 209, IMC sinkt von 11 auf 9 und Intensivbetten gehen von 17 auf 19. Damit insgesamt 237 Betten belegt, 19 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 209 | -19 | 
| IMC | 9 | -2 | 
| Intensiv | 19 | +2 |
| SUMME | 237 | -19 |

Im Wochenvergleich somit 30 Betten weniger. 

## Impfungen

Die Impfzahlen bewegen sich kaum, bis auf die 4. Impfungen, wo sich zumindest ein bisschen mehr tut. Da hat München heute die 90.000er Marke geschafft.

1. +9
2. +22
3. +160
4. +1117

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.