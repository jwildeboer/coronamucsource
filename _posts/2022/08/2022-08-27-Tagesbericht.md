---
layout: default
title: 2022-08-27 Tagesbericht
category: Tagesberichte
---

![2022-08-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger. Zweite zeigt Krankenbetten, weiter sinkend aber da Samstag, keinen Aktualisierung, dritte zeigt Inzidenz, welches, mit wieder leicht sinkender Tendenz, unter 200 geht.](/images/2022/08/20220827.png)

Das RKI meldet eine Inzidenz von 186,7 für München. 330 Neue, 124 Nachmeldungen, davon 107 für vorgestern. Die gestern gemeldete Inzidenz von 186,7 erhöht sich auf 194,7 und bleibt damit unter 200.
<!--more-->

Leider wieder 7 neue Todesfälle, die 7-Tages-Summe der Todesfälle geht auf 18. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle genau 0.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 19.08. | 195,1 -> 207,2 | 
| 20.08. | 195,0 -> 207,2 | 
| 21.08. | 195,0 -> 207,2 | 
| 22.08. | 246,3 -> 256,8 |
| 23.08. | 212,1 -> 222,1 |
| 24.08. | 192,3 -> 201,0 |
| 25.08. | 186,7 -> 194,7 |

Da am Wochenende keine Zahlen vom LGL an das RKI gemeldet werden, können wir die Inzidenz für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 183,8 |
| Montag | 183,8 |


## Reproduktionswert R(t) für München

Unser R(t) sinkt wieder ein wenig auf jetzt 0,99.

Das R(t) vom StaBLab der LMU wurde aktualsiert (Nowcast Lauf vom 25.08.) und liegt jetzt bei 1,01.

| Datum | R(t) | LMU |
|---|---:|---:|
| 20.08. | 0,86 | 0,78 |
| 21.08. | 0,87 | 0,81 |
| 22.08. | 0,97 | 0,87 |
| 23.08. | 1,02 | 0,95 |
| 24.08. | 1,05 | 1,01 |
| 25.08. | 1,07 | --- |
| 26.08. | 0,99 | --- |

## Krankenhaus

Am Samstag keine neuen Krankenhauszahlen von der Stadt München.

## Impfungen

Am Samstag keine neuen Impfzahlen von der Stadt München.