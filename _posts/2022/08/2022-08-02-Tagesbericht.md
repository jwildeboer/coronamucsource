---
layout: default
title: 2022-08-02 Tagesbericht
category: Tagesberichte
---

![2022-08-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern nahezu gleich, ein Bett weniger, dritte zeigt Inzidenz, die deutlich sinkt.](/images/2022/08/20220802.png)

Das RKI meldet eine Inzidenz von 607,6 für München. 1.612 neue und 165 Nachmeldungen für die letzten 7 Tage, davon 4 für Sonntag, 7 für Samstag und 103 für Freitag.

Die Inzidenzen vom Wochenende steigen damit für Samstag von 664,7 auf 675,3 und für Sonntag von 663,5 auf 674,4.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 25.07. | 876,7 -> 928,2 |
| 26.07. | 819,5 -> 864,7 |
| 27.07. | 781,3 -> 821.0 |
| 28.07. | 712,1 -> 739,4 |
| 29.07. | 665,2 -> 675,4 |
| 30.07. | 664,7 -> 675,3 |
| 31.07. | 663,5 -> 674,4 |

## Reproduktionswert R(t) für München

Da das StaBLab der LMU weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert, steigen wir auf unser eigenes 7 Tage R um. Das liegt heute bei 0,82.

| 26.07. | 1,04 |
| 27.07. | 1,01 |
| 28.07. | 0,96 |
| 29.07. | 0,91 |
| 30.07. | 0,87 |
| 31.07. | 0,84 |
| 01.08. | 0,82 |

## Krankenhaus

Die Krankenhaus-Belegung ist heute nahezu unverändert. Ein Normalbett weniger.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 408 | -1 | 
| IMC | 18 | +0 | 
| Intensiv | 37 | +0 |
| SUMME | 463 | -1 |

Im Wochenvergleich sind das 13 Betten weniger. Zum ersten mal seit dem 08.06. eine Abnahme bei der Bettenbelegung.

## Impfungen

Die Impfzahlen, Stand 30.07., wie gehabt: kaum Erst- und Zweitimpfungen. 

1. +21
2. +24
3. +111
4. +414

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.