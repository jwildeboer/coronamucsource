---
layout: default
title: 2022-08-07 Tagesbericht
category: Tagesberichte
---

![2022-08-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, deutlich weniger als letzte Woche. Zweite zeigt Krankenbetten, keine neuen Zahlen am Sonntag, aber sinkt auch, dritte zeigt Inzidenz, die sehr deutlich sinkt. Tag 4 unter Inzidenz 500](/images/2022/08/20220807.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI):

- Bayernweit gibt es 227 mit COVID positiven Patienten belegte Intensivbetten.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute 997, gestern 1040. Damit endlich unter dem Grenzwert von 1200 für die "alte" gelbe Ampel.

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München