---
layout: default
title: 2022-08-21 Tagesbericht
category: Tagesberichte
---

![2022-08-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche niedriger, am Sonntag keine neuen Zahlen.. Zweite zeigt Krankenbetten, weiter sinkend, Samstag und Sonntag aber keine neuen Zahlen, dritte zeigt Inzidenz, welches sich gerade auf einem Plateau um 200 einpendelt wohl immer noch wegen Meldeverzug durch Wochenende/Feiertag.](/images/2022/08/20220821.png)

Sonntag, daher keine neuen Fallzahlen, keine Nachmeldungen, keine Impfzahlen, keine Krankenhauszahlen für München und Bayern. 
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister):

- Bayernweit gibt es heute 161 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 149, letzte Woche 190.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 776, gestern waren es 811. Damit seit 12 Tagen, auch mit Nachmeldungen, unter dem Grenzwert von 1200 für die "alte" gelbe Ampel.

## Reproduktionswert R(t) für München

Weiter leichte Steigung bei unserem R(t), daß jetzt auf 0,84 geht. 

Das R(t) vom StaBLab der LMU (Nowcast-Lauf vom 19.08.) liegt bei 0,79.
Unser R(t) steigtweiter auf jetzt 0,82. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 14.08. | 0,78 | 0,70 |
| 15.08. | 0,75 | 0,73 |
| 16.08. | 0,74 | 0,78 |
| 17.08. | 0,75 | 0,81 |
| 18.08. | 0,77 | 0,79 |
| 19.08. | 0,82 | --- |
| 20.08. | 0,84 | --- |

## Krankenhaus

Am Sonntag keine Aktualisierung der Krankenhausbelegung in München

## Impfungen

Am Sonntag keine Aktualisierung der Impfzahlen in München