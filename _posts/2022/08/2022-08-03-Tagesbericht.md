---
layout: default
title: 2022-08-03 Tagesbericht
category: Tagesberichte
---

![2022-08-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern deutlich weniger,Satte 50 Betten weniger im Vergleich zur Vorwoche, dritte zeigt Inzidenz, die deutlich sinkt. Tag 5 unter Inzidenz 700.](/images/2022/08/20220803.png)

Das RKI meldet eine Inzidenz von 555,1 für München. 1.108 Neue und 326 Nachmeldungen für die letzten 7 Tage, davon 286 für vorgestern. Die gestern gemeldete Inzidenz steigt von 607,6 auf 629,1.
<!--more-->

Leider auch 12 neue Todesfälle gemeldet, damit insgesamt 17 Tote in den letzten 7 Tagen.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 26.07. | 819,5 -> 865,1 |
| 27.07. | 781,3 -> 821.5 |
| 28.07. | 712,1 -> 740,5 |
| 29.07. | 665,2 -> 677,5 |
| 30.07. | 664,7 -> 677,4 |
| 31.07. | 663,5 -> 676,7 |
| 01.08. | 607,6 -> 629,1 |

## Reproduktionswert R(t) für München

Da das [StaBLab der LMU](https://corona.stat.uni-muenchen.de/nowcast/) weiterhin sehr unregelmäßig den Nowcast und das R(t) aktualisiert (auch heute dort noch Stand 28. Juli), steigen wir auf unser eigenes 7 Tage R um. Das liegt heute bei 0,81.

| 27.07. | 1,01 |
| 28.07. | 0,96 |
| 29.07. | 0,91 |
| 30.07. | 0,87 |
| 31.07. | 0,84 |
| 01.08. | 0,82 |
| 02.08. | 0,81 |

## Krankenhaus

Deutliche Verbesserung bei den Krankenhauszahlen heute. In allen Bereichen, aber am Deutlichsten bei den Normalbetten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 374 | -34 | 
| IMC | 13 | -5 | 
| Intensiv | 34 | -3 |
| SUMME | 421 | -42 |

Im Wochenvergleich sind das 50 Betten weniger. 

## Impfungen

Die Impfzahlen, jetzt Stand 01.08., wie gehabt. Weiterhin kaum Erst- und Zweitimpfungen. 

1. +16
2. +41
3. +193
4. +1.426

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.