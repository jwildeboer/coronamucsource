---
layout: default
title: 2022-10-09 Tagesbericht
category: Tagesberichte
---

![2022-10-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher aber viele Nachmeldungen. Zweite zeigt Krankenbetten, die weiter steigen. Samstag und Sonntag aber keine Aktualisierung durch Stadt München. Dritte zeigt Inzidenz, die Tag 3 über 1.000 ist.](/images/2022/10/20221009.png)

Das RKI meldet eine Inzidenz von 1.026,5 für München. Da Sonntag, keine neuen Fallzahlen beim RKI, keine neuen Krankenhauszahlen der Stadt.
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 263 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 258, letzte Woche 211.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 1.918, gestern waren es 2.002, letzte Woche 2.014. 

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,15, also weiterhin exponentielles Wachstum. 

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 05.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,34.

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.10. | 1,66 | 1,51 |
| 03.10. | 1,51 | 1,45 |
| 04.10. | 1,36 | 1,40 |
| 05.10. | 1,26 | 1,34 |
| 06.10. | 1,19 | --- |
| 07.10. | 1,17 | --- |
| 08.10. | 1,15 | --- |

## Krankenhaus

Bis Montag keine neuen Krankenhauszahlen der Stadt München.

## Impfungen

Bis Montag keine neuen Impfzahlen der Stadt München.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 6 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 29,2% auf 1.130,7. Spitze bleibt Donnerstag, der 29.09. mit 182,3%. (Zahlen von gestern, da RKI Sonntag/Montag nicht aktualisiert)

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 46,81% auf jetzt 552. (Stand 2022-10-07, neue Zahlen erst am Montag)

![Wiesnbarometer 2022-10-08](/images/2022/10/20221008WB.png)