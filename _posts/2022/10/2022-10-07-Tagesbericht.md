---
layout: default
title: 2022-10-07 Tagesbericht
category: Tagesberichte
---

![2022-10-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher aber viele Nachmeldungen und es werden wohl noch mehr kommen. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die aufgrund der sehr vielen Nachmeldungen nunzum ersten mal seit dem 13.04. über 1.000 geht.](/images/2022/10/20221007.png)

Das RKI meldet eine Inzidenz von 1.057,3 für München. 3.429 Neue, 2.197(!) Nachmeldungen, davon 1.936 für vorgestern. Die gestern genannte Inzidenz von 813,7 erhöht sich auf 960,0. Somit an 4 von den letzten 7 Tagen eine Inzidenz über 900.
<!--more-->

Leider auch 3 neue Todesfälle. Damit jetzt insgesamt 604 COVID-Tote in 2022,in 2020 waren es 579. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 13.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 29.09. | 792,8 -> 822,1 |
| 30.09. | 834,3 -> 874,3 |
| 01.10. | 768,7 -> 912,3 |
| 02.10. | 766,8 -> 912,1 |
| 03.10. | 633,6 -> 783,3 |
| 04.10. | 708,9 -> 780,6 |
| 05.10. | 813,7 -> 960,0 |

Man sieht weiterhin Sprünge durch die verzögerten Nachmeldungen. Inzidenz über 900 am Samstag und Sonntag - und es fehlen unserer Einschätzung nach weiterhin viele Nachmeldungen. Frühestens näcshte Woche werden sich die Zahlen wohl stabilisieren.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortageswert in %.

| Kreis | 07.10 | 06.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.206,8 | 1.118,7 | +7,9% |
| LK Ebersberg | 1.265,2 | 1.157,3 | +9,3% |
| LK Fürstenfeldbruck | 1.333,6 | 1.210,5 | +10,2% |
| LK München | 907,3 | 886,4 | +2,4% |
| LK Starnberg | 1.084,5 |1.047,2 | +3,6% |

Auch hier darf man vermuten, daß noch Nachmeldungen ausstehen und erst in den nächsten Tagen in die Statistik eingehen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,18 und bleibt somit weiter knapp exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 05.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,34.

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.09. | 1,78 | 1,60 |
| 01.10. | 1,76 | 1,57 |
| 02.10. | 1,66 | 1,51 |
| 03.10. | 1,51 | 1,45 |
| 04.10. | 1,36 | 1,40 |
| 05.10. | 1,26 | 1,34 |
| 06.10. | 1,18 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter an: Normalbetten steigt um 2 von gestern 500 auf jetzt 502, IMC steigt deutlich von gestern 7 auf jetzt 13. Intensivbetten steigt von gestern 36 auf jetzt 37. Damit insgesamt 552 Betten belegt, 9 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 502 | +2 | 
| IMC | 13 | +6 | 
| Intensiv | 37 | +1 |
| SUMME | 552 | +9 |

Im Wochenvergleich sind das 176 Betten mehr. Einen derart schnellen Anstieg hatten wir noch nie. Bisheriger Rekord war der 27.01.2022 mit +136 im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr. Heute wurden die Zahlen für 3 Tage aktualisiert, somizt jetzt Stand 04.10.

1. +12
2. +5
3. +54
4. +351

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,09%, Bayern 58,88%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 4 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 28,6% auf 1.057,3. Spitze bisher war der Donnerstag letzter Woche mit 182,1%.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 46,81% auf jetzt 552.

![Wiesnbarometer 2022-10-06](/images/2022/10/20221007WB.png)