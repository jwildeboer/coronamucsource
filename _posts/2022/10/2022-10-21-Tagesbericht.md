---
layout: default
title: 2022-10-21 Tagesbericht
category: Tagesberichte
---

![2022-10-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 10 Tagen zügig sinkt.](/images/2022/10/20221021.png)

Das RKI meldet eine Inzidenz von 424,3 für München. 567 Neue, 196 Nachmeldungen, davon 170 für vorgestern. Die gestern genannte Inzidenz von 479,1 steigt damit auf 492,0. Somit an 7 von den letzten 7 Tagen eine Inzidenz unter 1.000, Tag 2 unter 500.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 22. Damit insgesamt 641 COVID-Tote in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 13.10. | 1.064,6 ->  1.086,4 |
| 14.10. | 964,4 ->  985,1 |
| 15.10. | 860,0 ->  945,7 |
| 16.10. | 707,9 ->  793,4 |
| 17.10. | 638,7 -> 660,3 |
| 18.10. | 544,1 -> 561,2 |
| 19.10. | 479,1 -> 492,0 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 21.10. | 20.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 601,5 | 681,3 | -11,7% |
| LK Ebersberg | 590,7 | 673,1 | -24,3% |
| LK Fürstenfeldbruck | 629,5 | 706,4 | -10,9% |
| LK München | 424,8 | 474,8 | -10,5% |
| LK Starnberg | 507,5 | 596,7 | -14,9% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,60 und ist somit seit 6 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 20.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und ist damit fast identisch zu unserem R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 14.10. | 1,10 | 1,20 |
| 15.10. | 0,98 | 1,09 |
| 16.10. | 0,89 | 0,92 |
| 17.10. | 0,79 | 0,77 |
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,64 | 0,63 |
| 20.10. | 0,60 | 0,59 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt ist weiterhin auf sehr hohem Niveau, sinkt aber weiter: Normalbetten sinkt um 35 von gestern 507 auf jetzt 472. IMC sinkt von gestern 12 auf jetzt 10. Intensivbetten sinkt von gestern 48 auf jetzt 43. Damit insgesamt 525 Betten belegt. Zu ersten Mal seit dem 04.10. wieder unter 500 Normalbetten.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 472 | -35 | 
| IMC | 10 | -2 | 
| Intensiv | 43 | -5 |
| SUMME | 525 | -42 |

Damit 87 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen. Heute wurden die Zahlen für 2 Tage aktualisiert, somit jetzt Stand 17.10.

1. +9
2. +3
3. +33
4. +445

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,17%, Bayern 58,99%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 18 nach der Wiesn. Die Münchner Inzidenz kollabiert nahezu im Vergleich zur Vorwoche um -60,9% auf 424,3.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche deutlich um -14,22% auf jetzt 525.

![Wiesnbarometer 2022-10-21](/images/2022/10/20221021WB.png)