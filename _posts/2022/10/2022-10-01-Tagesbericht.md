---
layout: default
title: 2022-10-01 Tagesbericht
category: Tagesberichte
---

![2022-10-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher aber in den letzten Tagen überraschen niedrige Fallzahlen. Zweite zeigt Krankenbetten, die weiter deutlich steigen, bis Dienstag aber nicht aktualisert werden. Dritte zeigt Inzidenz, die jetzt klar exponentiell steigt und auch heute über 800 liegt.](/images/2022/10/20221001.png)

Das RKI meldet eine Inzidenz von 834,3 für München. Nur 1.501 Neue, was nicht zum R(t) von 1,76 passt, nur 219 Nachmeldungen, davon 160 für vorgestern. Die gestern genannte Inzidenz von 792,8 erhöht sich auf 806,5. Tag 2 über Inzidenz 800. 
<!--more-->

Keine neuen Todesfälle gemeldet - die 7-Tage-Summe der Todesfälle ist 7. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 6.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 23.09. | 311,2 -> 329,3 |
| 24.09. | 311,1 -> 394,8 |
| 25.09. | 310,3 -> 395,8 |
| 26.09. | 424,9 -> 448,3 |
| 27.09. | 547,0 -> 590,8 |
| 28.09. | 695,8 -> 744,5 |
| 29.09. | 792,8 -> 806,5 | 

So schaut exponentielles Wachstum aus.

Es könnte sein, daß die Contact Tracer der Stadt an die Kapazitätsgrenze stoßen und daher jetzt wieder vermehrt Meldeverzögerungen kommen werden. Die Zahlen von heute lassen das vermuten.

Dazu dann das verlängerte Wochenende wo es wohl trotz der dramatischen Lage mit einem R(t) von über 1,7 bis Mittwoch keine neuen Fallzahlen und Inzidenz beim RKI geben wird - das ist schon eine gefährliche Mischung.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 01.10. | 30.09. | Änderung |
|:---|---:|
| LK Dachau | 871,7 | 777,7 | +12,1% |
| LK Ebersberg | 1.007,2 | 913,8 | +10,2% |
| LK Fürstenfeldbruck | 1.020,2 | 909,5 | +12,2% |
| LK München | 972,7 | 839,0 | +15,9% |
| LK Starnberg | 985,0 | 861,4 | +14,3% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,76 und steigt seit 24(!) Tagen, Tag 4 über 1,5, also klar exponentielles Wachstum. Warum dann die Fallzahlen sich trotzdem halbieren? Das kann eigentlich nur duch Kapazitätsprobleme bei der Fallbearbeitung erklärt werden.

Ein weiteres Münchner Wunder, wie letztes Jahr bei der IAA?

Das R(t) vom StaBLab der LMU wird voraussichtlich erst nach der Wiesn wieder aktualisiert - Grund sind "technische Probleme".

| Datum | R(t) | LMU |
|---|---:|---:|
| 24.09. | 1,28 | --- |
| 25.09. | 1,33 | --- |
| 26.09. | 1,39 | --- |
| 27.09. | 1,49 | --- |
| 28.09. | 1,59 | --- |
| 29.09. | 1,70 | --- |
| 30.09. | 1,76 | --- |

## Krankenhaus

Bis Dienstag keine neuen Krankenhauszahlen der Stadt München.

## Impfungen

Bis Dienstag keine neuen Impfzahlen der Stadt München.

## Änderung des Status "vollständig geimpft"

Ab heute gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 15 der Wiesn. Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 153,4%(!) auf heute 834,3. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 66,37% auf jetzt 376. Krankenhauszahlen werden erst am Dienstag wieder aktualisiert.

![Wiesnbarometer 2022-10-01](/images/2022/10/20221001WB.png)