---
layout: default
title: 2022-10-28 Tagesbericht
category: Tagesberichte
---

![2022-10-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und weiter sinkt.](/images/2022/10/20221028.png)

Das RKI meldet eine Inzidenz von 226,1 für München. 344 Neue, 141 Nachmeldungen, davon 102 für vorgestern. Die gestern geannte Inzidenz von 242,9 steigt durch die Nachmeldungen auf 251,2. Tag 3 unter Inzidenz 300.
<!--more-->

Es wurden 6 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 25. Damit insgesamt 666 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 20.10. | 424,3 -> 437,6 |
| 21.10. | 381,9 -> 392,3 |
| 22.10. | 316,7 -> 356,0 |
| 23.10. | 316,5 -> 355,8 |
| 24.10. | 302,6 -> 319,4 |
| 25.10. | 269,0 -> 281,0 |
| 26.10. | 242,9 -> 251,2 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 28.10. | 27.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 325,5 | 359,0 | -9,3% |
| LK Ebersberg | 262,2 | 289,1 | -9,3% |
| LK Freising | 263,9 | 293,7 | -10,1% |
| LK Fürstenfeldbruck | 328,9 | 366,9 | -10,4% |
| LK München | 233,0 | 259,0 | -10,0% |
| LK Starnberg | 247,2 | 272,8 | -9,4% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 3 Tagen wieder leicht auf jetzt 0,67 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 26.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,53 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.10. | 0,59 | 0,59 |
| 22.10. | 0,59 | 0,55 |
| 23.10. | 0,59 | 0,52 |
| 24.10. | 0,60 | 0,51 |
| 25.10. | 0,61 | 0,51 |
| 26.10. | 0,65 | 0,53 |
| 27.10. | 0,67 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Normalbetten sinkt um 5 Betten auf jetzt 390. IMC sinktt von gestern 15 auf jetzt 14. Intensivbetten sinkt deutlich von gestern 42 auf jetzt 36. Damit insgesamt 440 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 390 | -5 | 
| IMC | 14 | -1 | 
| Intensiv | 36 | -6 |
| SUMME | 440 | -12 |

Damit 85 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +4
2. +0
3. +119
4. +276

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,21%, Bayern 59,04%.

## Nach-Wiesnbarometer

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -48,3% auf 226,1. Damit wieder auf Vor-Wiesn-Niveau.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche um -16,19% auf jetzt 440.

![Wiesnbarometer 2022-10-28](/images/2022/10/20221028WB.png)