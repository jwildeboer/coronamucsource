---
layout: default
title: 2022-10-24 Wochenbericht KW42
category: Wochenberichte
---

![2022-10-24 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich aber Sonntag/Montag keine neue Zahlen vom RKI. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 13 Tagen zügig sinkt.](/images/2022/10/20221024.png)

In der letzten Woche gab es insgesamt 4.711 neue COVID Fälle in München, das sind satte 66,5% weniger als in der Vorwoche (14.074). Leider aber auch 26 Tote mit COVID Nachweis in den letzten 7 Tagen, 4 mehr als in der Vorwoche (22).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 11.809 stetig zurück auf 4.711. Die Inzidenz halbierte sich um stattliche 481,6 Punkte in 7 Tagen. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 16.09. | 2 | 11.809 | 793,8 |
| 17.09. | 1.248 | 9.838 | 661,3 |
| 18.09. | 1.263 | 8.364 | 562,2 |
| 19.09. | 954 | 7.343 | 493,6 |
| 20.09. | 692 | 6.459 | 434,2 |
| 21.09. | 552 | 5.681 | 381,9 |
| 22.10. | 0 | 4.711 | 316,7 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche und bleibt jetzt klar unter 1, durch die (noch fehlenden) Wochenendzahlen ist das aber wohl noch ein wenig nach unten verzerrt.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 20.10.) liegt bei 0,59.

| Datum | R(t) | LMU |
|---|---:|---:|
| 17.10. | 0,79 | 0,77 |
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,64 | 0,63 |
| 20.10. | 0,60 | 0,59 |
| 21.10. | 0,59 | --- |
| 22.10. | 0,58 | --- |
| 23.10. | 0,57 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende deutlich weniger Normalbetten aber nahezu unveränderte IMC/ICU Zahlen. Dennoch - klarer Rückgang bei den Krankenhauszahlen. An jedem Tag weniger als in der Vorwoche, heute sogar dreistellig.

Zum ersten Mal seit dem 3.10. liegt die Gesamtzahl der belegten Betten heute wieder unter 500.

Im Vergleich zum Vorjahrimmer noch ca. 4x mehr Betten belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 17.10. | 540 | 14 | 46 | 600 | -37 | 128 |
| 18.10. | 542 | 11 | 47 | 600 | -23 | 141 |
| 19.10. | 507 | 12 | 48 | 567 | -53 | 137 |
| 20.10. | 471 | 10 | 43 | 525 | -87 | 149 |
| 21.10. | 462 | 11 | 43 | 516 | -92 | 145 |
| 22.10. | 451 | 12 | 44 | 507 | -97 | 145 |
| 23.10. | 441 | 13 | 45 | 499 | -103 | 145 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiter schlimm aus. Seit dem 22.09. sind wir deutlich über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche, auch wenn die Zahlen jetzt langsam sinken. In der letzten Woche an 4 von 7 Tagen mehr als doppelte der alten gelben Ampel.

Bei den Intensivbetten sinkt die Sume von letzte Woche 323 auf jetzt 293.

Im Vergleich zum Vorjahr weiterhin klar mehr Betten belegt.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 17.10. | 3.024 | 615 |
| 18.10. | 2.901 | 662 |
| 19.10. | 2.659 | 754 |
| 20.10. | 2.402 | 813 |
| 21.10. | 2.111 | 849 |
| 22.10. | 2.008 | 909 |
| 23.10. | 1.923 | 947 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,97% | 75,17% |
| 2. | 71,44% | 75,08% |
| 3. | 48,17% | 59,00% |
| 4. | 8,34% | 8,82% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/10/KW42Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca. 26.923 Tests/Tag mit einer Positivrate von 40,82%. Gegenüber den Zahlen von letzter Woche (27.541 Tests/Tag mit 44,95% Positivrate) also wenger Tests, gepaart mit sinkdender Positivrate.

Aber auch 26.923 Tests/Tag sind weniger als 30% der Testkapazität. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für (PCR-Pool-)Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 26.923 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 40,82%.](/images/2022/10/20221020-Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Es gilt immer noch: BA.5 klar an der Spitze. BQ.1 ist da, und stieg zoemlich schnell aber seit 2 Wochen nicht mehr ganz so schnell. Andere Varianten tauchen hier und da auch auf, wachsen aber nicht.

| Variante | KW39 | KW40 | KW41 | KW42 |
|:--|--:|--:|--:|--:|
| Delta | 0.0% | 0.1% | 0.0% | 0.0% |
| Omikron BA.1 | 0.0% | 0.1% | 0.0% | 0.0% |
| Omikron BA.2 | 1.6% | 2.0% | 1.7% | 0.0% |
| Omikron BA.2.75 | 1.1% | 0.4% | 0.3% | 0.4% |
| Omikron BA.4 | 2.7% | 1.5% | 0.9% | 1.6% |
| Omikron BA.5 | 90.9% | 88.4% | 89.3% | 88.4% |
| Omikron BQ.1 | 2.9% | 7.0% | 7.1% | 8.8% |
| Andere | 0.7% | 0.5% | 0.6% | 0.8% |
| Sequenzierungen | 986 | 991 | 637 | 249 |

![VoC](/images/2022/10/KW42Var.png)