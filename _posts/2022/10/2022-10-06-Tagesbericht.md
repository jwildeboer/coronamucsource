---
layout: default
title: 2022-10-06 Tagesbericht
category: Tagesberichte
---

![2022-10-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher aber viele Nachmeldungen und es werden wohl noch mehr kommen. Zweite zeigt Krankenbetten, die weiter deutlich steigen, seit 7 Tagen jeweils über 55% mehr im Vergleich zur Vorwoche. Dritte zeigt Inzidenz, die aufgrund der Meldepause erst jetzt wieder über 800 geht.](/images/2022/10/20221006.png)

Das RKI meldet eine Inzidenz von 813,7 für München. 3.873 Neue, 843 Nachmeldungen, davon 617 für vorgestern. Die gestern genannte Inzidenz von 708,9 erhöht sich auf 764,0. Samstag und Sonntag gehen über 900.
<!--more-->

Leider auch 4 neue Todesfälle. Damit jetzt insgesamt 580 COVID-Tote in 2022,in 2020 waren es 579. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 11.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 28.09. | 695,8 -> 749,0 |
| 29.09. | 792,8 -> 821,1 |
| 30.09. | 834,3 -> 872,4 |
| 01.10. | 768,7 -> 909,4 |
| 02.10. | 766,8 -> 909,0 |
| 03.10. | 633,6 -> 779,7 |
| 04.10. | 708,9 -> 764,0 |

Man sieht weiterhin Sprünge durch die verzögerten Nachmeldungen. Inzidenz über 900 am Samstag und Sonntag - und es fehlen unserer Einschätzung nach weiterhin viele Nachmeldungen. Frühestens am Freitag werden sich die Zahlen wohl stabilisieren.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortageswert in %.

| Kreis | 06.10. | 05.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.118,7 | 905,1 | +13,6% |
| LK Ebersberg | 1.157,3 | 911,7 | +26,9% |
| LK Fürstenfeldbruck | 1.210,5 | 1.179,0 | +2,7% |
| LK München | 886,4 | 923,3 | -4,0% |
| LK Starnberg | 1.047,2 | 568,9 | +84,1% |

Auch hier darf man vermuten, daß noch viele Nachmeldungen ausstehen und erst in den nächsten Tagen in die Statistik eingehen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,23 und bleibt somit weiter exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.09. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,66.

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.09. | 1,70 | 1,65 |
| 30.09. | 1,78 | 1,66 |
| 01.10. | 1,75 | --- |
| 02.10. | 1,65 | --- |
| 03.10. | 1,50 | --- |
| 04.10. | 1,34 | --- |
| 05.10. | 1,23 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter an: Normalbetten steigt um 32 von gestern 468 auf jetzt 500 - der höchste Wert seit es die Krankenhauszahlen von der Stadt gibt (2020-09-30), IMC sinkt von gestern 18 auf jetzt 7. Intensivbetten bleiben gleich mit 36. Damit insgesamt 543 Betten belegt, 21 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 500 | +32 | 
| IMC | 7 | -11 | 
| Intensiv | 36 | +0 |
| SUMME | 543 | +21 |

Im Wochenvergleich sind das 200(!) Betten mehr. Einen derart schnellen Anstieg hatten wir noch nie. Bisheriger Rekord war der 27.01.2022 mit +136 im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr. Heute wurden die Zahlen für 3 Tage aktualisiert, somizt jetzt Stand 04.10.

1. +11
2. +23
3. +135
4. +1666

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,09%, Bayern 58,87%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca. 25.264 Tests/Tag mit einer Positivrate von 40,88%(!). Gegenüber den Zahlen von letzter Woche (18.663 Tests/Tag mit 27,58% Positivrate) also wieder mehr Tests, gepaart mit deutlich höherer Positivrate.

25.264 Tests/Tag während der Wiesn ist immer noch nur knapp 25% der Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 25.264 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate springt auf jetzt 40,88%.](/images/2022/10/20221006-Test.png)

## Wiesnbarometer

Tag 3 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche zwar nur um 8,6% auf 813,7, aber das liegt an den fehlenden Nachmeldungen. Spitze bisher war der Donnerstag letzter Woche mit 181,7%.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter stark um 58,31% auf jetzt 543.

![Wiesnbarometer 2022-10-06](/images/2022/10/20221006WB.png)