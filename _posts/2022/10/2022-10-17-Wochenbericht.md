---
layout: default
title: 2022-10-17 Wochenbericht KW41
category: Wochenberichte
---

![2022-10-17 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 6 Tagen sinkt.](/images/2022/10/20221017.png)

In der letzten Woche gab es insgesamt 15.272 neue COVID Fälle in München, das sind 12,4% mehr als in der Vorwoche (13.590). Leider auch 9 Tote mit COVID Nachweis in den letzten 7 Tagen, 2 mehr als in der Vorwoche (7).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 19.883 über den Spitzenwert der Wiesnwelle von 23.020 zurück auf 12.975. Die Inzidenz fiel um stattliche  476,5 Punkte in 7 Tagen, auch weil letztes Wochenende überraschend doch sher hohge Nachmeldungen kamen, die nun wieder aus der 7 Tage Summe herausgefallen sind. 

[Letzte Woche](/wochenberichte/2022-10-10) hatten wir da bereits eine Ahnung, als wir in den Wochenbericht schrieben: 

  "Sollten nicht noch überraschend viele (nach-)Meldungen auftauchen, so ist der Höhepunkt der Wiesn-Welle wohl in Sichtweite."

Es kamen sehr viele Nachmeldungen (wir schätzen insgesamt über 3.000) und damit hatten wir letzten Montag mit 1.547,3 wohl die Inzidenzspitze der Wiesnwelle erreicht. Diese Woche sollte die Inzidenz weiter sinken, aber nicht mehr so radikal. Und die Winterwelle kommt dann wohl auch noch dazu.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 09.09. | 2.264 | 19.883 | 1.336,5 |
| 10.09. | 3.208 | 23.020 | 1.547,3 |
| 11.09. | 2.720 | 22.720 | 1.527,2 |
| 12.09. | 1.952 | 18.711 | 1.257,7 |
| 13.09. | 1.541 | 16.074 | 1.080,5 |
| 14.09. | 1.110 | 14.348 | 964,4 |
| 15.10. | 0 | 12.975 | 860,0 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche und ist zwar jetzt klar unter 1, durch die (jetzt fehlenden) Wochenendzahlen ist das aber wohl noch ein wenig nach unten verzerrt.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 13.10.) liegt bei 1,34.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.10. | 1,28 | 1,19 |
| 11.10. | 1,33 | 1,22 |
| 12.10. | 1,32 | 1,28 |
| 13.10. | 1,24 | 1,32 |
| 14.10. | 1,09 | --- |
| 15.10. | 0,97 | --- |
| 16.10. | 0,87 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wenig Änderungen (-10) aber eine doch deutlcihe Steigerung bei den Intensivbetten (+9).

Seit nunmehr 7 Tagen liegt die Gesamtzahl der belegten Betten über 600 - das hatten wir, seit es die Zahlen von der Stadt (2020-09-30) gibt, noch nie. In 2020 hatten wir zwar auch mal 10 Tage über 600, damals aber deutlich mehr Intensivbetten und auch nie 7 Tage hintereinander.

Im Vergleich zum Vorjahr um 5x mehr Betten belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 10.10. | 590 | 17 | 30 | 637 | +159 | 146 |
| 11.10. | 573 | 16 | 34 | 623 | +101 | 139 |
| 12.10. | 577 | 16 | 27 | 620 | +77 | 136 |
| 13.10. | 562 | 16 | 34 | 612 | +60 | 129 |
| 14.10. | 556 | 14 | 38 | 608 | +48 | 119 |
| 15.10. | 549 | 13 | 42 | 604 | +37 | 119 |
| 16.10. | 543 | 12 | 47 | 602 | +26 | 119 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiter schlimm aus. Seit dem 22.09. sind wir deutlich über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche und die Zahlen steigen weiter. In der letzten Woche an 6 von 7 Tagen mehr als doppelte der alten gelben Ampel.

Bei den Intensivbetten stieg die Sume von letzte Woche 298 auf jetzt 323.

Im Vergleich zum Vorjahr nahezu eine vervierfachung der belegten Betten.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 10.10. | 3.415 | 512 |
| 11.10. | 3.052 | 522 |
| 12.10. | 2.862 | 527 |
| 13.10. | 2.781 | 550 |
| 14.10. | 2.554 | 581 |
| 15.10. | 2.430 | 593 |
| 16.10. | 2.334 | 608 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Landesweit gab es einen weiteren kleinen Schub bei den Viertimpfungen. Es scheint, daß die neuen Omikron-angepassten Booster ganz gut angenommen werden.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,96% | 75,16% |
| 2. | 71,44% | 75,08% |
| 3. | 48,13% | 58,95% |
| 4. | 8,17% | 8,15% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/10/KW41Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca. 27.029 Tests/Tag mit einer Positivrate von 44,92%. Gegenüber den Zahlen von letzter Woche (25.707 Tests/Tag mit 39,50% Positivrate) also wieder mehr Tests, gepaart mit weiter klar steigender Positivrate.

Aber auch 27.029 Tests/Tag sind weniger als 30% der Testkapazität. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für (PCR-Pool-)Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 25.264 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate springt auf jetzt 40,88%.](/images/2022/10/20221013-Test.png)

## Varianten

Die LMU betreibt jetzt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Aktuell immer noch ganz einfach: Aktuell alles BA.5. Aber da nur 12(!) Sequenzierungen für die KW 41 gemeldet wurden, ist das eher ohne Aussagekraft.

| Variante | KW38 | KW39 | KW40 | KW41 |
|:--|--:|--:|--:|--:|
| Andere | 0.2% | 1.0% | 0.0% | 0.0% |
| Omikron BA.2 | 2.1% | 3.2% | 0.0% | 0.0% |
| Omikron BA.4 | 3,3% | 2.6% | 2.1% | 0.0% |
| Omikron BA.5 | 94.4% | 93.2% | 97.9% | 100.0% |
| Sequenzierungen | 773 | 545 | 252 | 12 |

![VoC](/images/2022/10/KW41Var.png)

## Wiesnbarometer

Tag 14 nach der Wiesn. Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um kräftige 27,9% auf 964,4, aber auch da - Sondereffekt wegen (fehlender) Wochenenedzahlen. Spitze bleibt Donnerstag, der 29.09. mit 182,3%. (Zahlen von vorgestern, da RKI Sonntag/Montag nicht aktualisiert)

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 4,51% auf jetzt 602.

![Wiesnbarometer 2022-10-10](/images/2022/10/20221017WB.png)