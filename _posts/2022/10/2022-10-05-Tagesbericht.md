---
layout: default
title: 2022-10-05 Tagesbericht
category: Tagesberichte
---

![2022-10-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher aber viele Nachmeldungen und es werden wohl noch mehr kommen. Zweite zeigt Krankenbetten, die weiter deutlich steigen, seit 6 Tagen jeweils über 60% mehr im Vergleich zur Vorwoche. Dritte zeigt Inzidenz, die aufgrund der Meldepause zwichen 7-800 verharrt.](/images/2022/10/20221005.png)

Das RKI meldet eine Inzidenz von 708,9 für München. 2.176 Neue, 1.972 Nachmeldungen, davon 0 für Montag (Feiertag), 1 für Sonntag, satte 1.462 für Samstag und immerhin noch 324 für Freitag, 131 für Donnerstag. Die gestern genannte Inzidenz von 633,6 erhöht sich auf 765,3. 
<!--more-->

Seit 7 Tagen keine neuen Todesfälle gemeldet. Letztes Jahr um diese Zeit war die 7-Tages-Summe der Todesfälle 4.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 27.09. | 547,0 -> 591,5 |
| 28.09. | 695,8 -> 747,6 |
| 29.09. | 792,8 -> 818,3 |
| 30.09. | 834,3 -> 867,8 |
| 01.10. | 768,7 -> 900,4 |
| 02.10. | 766,8 -> 898,6 |
| 03.10. | 633,6 -> 765,3 |

Man sieht kräftige Sprünge durch die verzögerten Nachmeldungen. Inzidenz 900 am Samstag - und es fehlen unserer Einschätzung nach weiterhin viele Nachmeldungen. Frühestens am Freitag werden sich die Zahlen wohl stabilisieren.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Samstagswert in %.

| Kreis | 05.10. | 01.10. | Änderung |
|:---|---:|
| LK Dachau | 905,1 | 871,7 | +3,9% |
| LK Ebersberg | 911,7| 1.007,2 | -9,5% |
| LK Fürstenfeldbruck | 1.179,0 | 1.020,2 | +15,6% |
| LK München | 923,3 | 972,7 | -5,1% |
| LK Starnberg | 568,9 | 985,0 | -42,2% |

Auch hier darf man vermuten, daß noch viele Nachmeldungen ausstehen und erst in den nächsten Tagen in die Statistik eingehen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,34 und bleibt somit weiter exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.09. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,66.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.09. | 1,59 | 1,60 |
| 29.09. | 1,70 | 1,65 |
| 30.09. | 1,78 | 1,66 |
| 01.10. | 1,75 | --- |
| 02.10. | 1,65 | --- |
| 03.10. | 1,50 | --- |
| 04.10. | 1,34 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter stark an: Normalbetten steigt wieder um 42 von gestern 426 auf jetzt 468, IMC steigt von gestern 13 auf jetzt 18. Intensivbetten sinkt von gestern 39 auf jetzt 36. Damit insgesamt 522 Betten belegt, 44 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 468 | +42 | 
| IMC | 18 | +5 | 
| Intensiv | 36 | -3 |
| SUMME | 522 | +44 |

Im Wochenvergleich sind das 199(!) Betten mehr. Einen derart schnellen Anstieg hatten wir noch nie. Bisheriger Rekord war der 27.01.2022 mit +136 im Vergleich zur Vorwoche.

## Impfungen

Heute keine Aktualsierung der Impfzahlen von der Stadt München.

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,06%, Bayern 58,87%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 2 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche zwar nur um 19,9% auf 708,9, aber das liegt an den fehlenden Nachmeldungen. Spitze bisher war der Donnerstag letzter Woche mit 180,8%.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter stark um 61,61% auf jetzt 522.

![Wiesnbarometer 2022-10-05](/images/2022/10/20221005WB.png)