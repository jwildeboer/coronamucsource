---
layout: default
title: 2022-10-08 Tagesbericht
category: Tagesberichte
---

![2022-10-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher aber viele Nachmeldungen. Zweite zeigt Krankenbetten, die weiter steigen. Samstag und Sonntag aber keine Aktualisierung durch Stadt München. Dritte zeigt Inzidenz, die Tag 2 über 1.000 ist.](/images/2022/10/20221008.png)

Das RKI meldet eine Inzidenz von 1.130,7 für München. 2.228 Neue, 750 Nachmeldungen, davon 614 für vorgestern. Die gestern genannte Inzidenz von 1.057,3 erhöht sich auf 1.106,4. Tag 2 mit Inzidenz über 1.100.
<!--more-->

Leider auch 2 neue Todesfälle. Damit jetzt insgesamt 606 COVID-Tote in 2022, in 2020 waren es 579.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 30.09. | 834,3 -> 875,1 |
| 01.10. | 768,7 -> 913,5 |
| 02.10. | 766,8 -> 913,4 |
| 03.10. | 633,6 -> 784,5 |
| 04.10. | 708,9 -> 782,6 |
| 05.10. | 813,7 -> 968,2 |
| 06.10. | 1.057,3 -> 1.106,4 |

Man sieht weiterhin Sprünge durch die verzögerten Nachmeldungen. Inzidenz über 900 am Samstag und Sonntag - und es fehlen unserer Einschätzung nach weiterhin einige Nachmeldungen. Frühestens näcshte Woche werden sich die Zahlen wohl stabilisieren.

Da weiterhin am Wochenende keine Meldungen erfolgen und auch das RKI die Verarbeitung von Meldungen am Sochenende eingestellt hat, gibt es erst am Dienstag wieder neue Zahlen. Die Inzidenzen für Sonntag und Montag können wir daher schon jetzt berechnen:

| Sonntag, 09.10. | 1.026,5 |
| Montag, 10.10. | 1.024,7 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortageswert in %.

| Kreis | 08.10. | 07.10 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.225,5 | 1.206,8 | +1,6% |
| LK Ebersberg | 1.260,4 | 1.265,2 | -0,4% |
| LK Fürstenfeldbruck | 1.389,9 | 1.333,6 | +4,2% |
| LK München | 850,7 | 907,3 | -6,2% |
| LK Starnberg | 1.106,4 | 1.084,5 | +2,0% |

Auch hier darf man vermuten, daß noch ein paar Nachmeldungen ausstehen und erst in der nächsten Woche in die Statistik eingehen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,17 und bleibt somit weiter knapp exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 05.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,34.

| Datum | R(t) | LMU |
|---|---:|---:|
| 01.10. | 1,76 | 1,57 |
| 02.10. | 1,66 | 1,51 |
| 03.10. | 1,51 | 1,45 |
| 04.10. | 1,36 | 1,40 |
| 05.10. | 1,26 | 1,34 |
| 06.10. | 1,19 | --- |
| 07.10. | 1,17 | --- |

## Krankenhaus

Am Samstag keine Aktualisierung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Samstag keine Aktualisierung der Impfzahlen durch die Stadt München.

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,09%, Bayern 58,88%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 5 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 29,2% auf 1.130,7. Spitze bleibt Donnerstag, der 29.09. mit 182,3%.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 46,81% auf jetzt 552. (Stand 2022-10-07, neue Zahlen erst am Montag)

![Wiesnbarometer 2022-10-08](/images/2022/10/20221008WB.png)