---
layout: default
title: 2022-10-04 Wochenbericht KW39
category: Wochenberichte
---

![2022-10-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher aber seit 4 Tagen keine neuen Zahlen. Zweite zeigt Krankenbetten, die weiter stabil und deutlich steigen, Verdopplungsrate 10 Tage. Dritte zeigt Inzidenz, die aufgrund der Meldepause heute zwar sinkt, aber morgen wieder steigen wird.](/images/2022/10/20221004.png)

In der letzten Woche gab es insgesamt 11.436 neue COVID Fälle in München, das sind satte 94,4% mehr als in der Vorwoche (5.889). Exponentielles Wachstum. Leider auch 7 Tote mit COVID Nachweis in den letzten 7 Tagen, weniger als in der Vorwoche (11).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 5.889 hoch auf 11.436. Die Inzidenz stieg um satte 372,9 Punkte (nahezu Verdopplung) in 7 Tagen, 4 mal mehr als in der letzten Woche. Die Welle ist also exponentiell und wird in der laufenden Woche wohl ihre volle Wucht entfalten. Wenn sich die Zahlen in München an den Zahlen in anderen Orten mit Volksfesten halten, könnte sich die Inzidenz bis zum nächsten Wochenbericht wohl nochmal verdoppeln.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 25.09. | 28 | 5.889 | 395,8 |
| 26.09. | 1.982 | 6.670 | 448,3 |
| 27.09. | 3.007 | 8.789 | 590,8 |
| 28.09. | 3.094 | 11.076 | 744,5 |
| 29.09. | 1.824 | 11.999 | 806,5 |
| 30.09. | 1.501 | 12.412 | 834,3 |
| 01.10. | 0 | 11.436 | 768,7 |

## Reproduktionswert R(t) für München

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) hat angekündigt wegen "technischer Probleme" keinen Nowcast oder R(t) voraussichtlisch bis zum 5.10 berechnen zu können.

Daher berechnen wir parallel "unser" R(t) weiter. Das ist in der letzten Woche stabil weiter gewachsen und liegt jetzt bei 1,39 und damit klar im exponenziellem Bereich.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war am 21.09.) liegt bei 1,09.

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.09. | 1,39 | --- |
| 27.09. | 1,49 | --- |
| 28.09. | 1,59 | --- |
| 29.09. | 1,70 | --- |
| 30.09. | 1,76 | --- |
| 01.10. | 1,69 | --- |
| 02.10. | 1,55 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende und Feiertag eine heftige Steigung bei den Zahlen (+102), insbesondere bei den Normalbetten (+82). Seit 2 Tagen wieder über 400 Betten insgesamt belegt. Im Vergleich zur Vorwoche satte 181 Betten mehr, im Vergleich zum Vorjahr sogar 311 Betten mehr belegt.

Und die Zahlen werden in dieser Woche wohl noch weiter steigen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 26.09. | 260 | 13 | 24 | 297 | +68 | 141 |
| 27.09. | 285 | 15 | 23 | 323 | +104 | 143 |
| 28.09. | 307 | 12 | 24 | 343 | +119 | 141 |
| 29.09. | 344 | 9 | 23 | 376 | +150 | 149 |
| 30.09. | 364 | 10 | 27 | 401 | +162 | 141 |
| 01.10. | 384 | 11 | 31 | 426 | +172 | 141 |
| 02.10. | 405 | 12 | 35 | 452 | +184 | 141 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

Und wir gehen davon aus, daß bei einigen Patienten von der Wiesn auch eher zufällig ein positiver COVID Test auftauchte.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es jetzt schlimm aus. Seit dem 27.09. sind wir deutlich über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche und die Zahlen steigen stetig weiter.

Bei den Intensivbetten sprang die Sume von letzte Woche 171 auf jetzt 230.

Im Vergleich zum Vorjahr nahezu eine verdreifachung der belegten Betten.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 26.09. | 1.428 | 473 |
| 27.09. | 1.555 | 491 |
| 28.09. | 1.618 | 549 |
| 29.09. | 1.662 | 537 |
| 30.09. | 1.575 | 528 |
| 01.10. | 1.502 | 542 |
| 02.10. | 1.440 | 547 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Landesweit gab es einen weiteren Schub bei den Viertimpfungen. Es scheint, daß die neuen Omikron-angepassten Bosster ganz gut angenommen werden.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,95% | 75,15% |
| 2. | 71,42% | 75,06% |
| 3. | 48,06% | 58,86% |
| 4. | 7,25% | 7,07% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/10/KW39Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca. 17.930 Tests/Tag mit einer Positivrate von 28,34%. Gegenüber den Zahlen von letzter Woche (16.994 Tests/Tag mit 25,24% Positivrate) also wieder etwas mehr Tests, gepaart mit steigender Positivrate.

Aber auch 17.930 Tests/Tag sind wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 17.930 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 28,34%.](/images/2022/09/20220929-Test.png)

## Varianten

Die LMU betreibt jetzt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Aktuell eigentlich ganz einfach: Fast alles BA.5. Vereinzelt BA.2.75, noch kein BJ.1, BQ.1, B2.75.2.

Leider wurden die Zahlen für KW39 noch nicht veröffentlicht. Wir werden die Tabelle und Grafik aktualiseren sobald das passiert ist.

| Variante |	KW 35 |	KW 36 | KW 37 | KW38 |
|:--|--:|--:|--:|--:|
| Andere | 0.0 % | 1.0 % | 0.0 % | 0.0% |
| Omikron BA.2 | 1.0 % | 1.0 % | 0.0 % | 1.0% |
| Omikron BA.4 | 3.0 % | 3.0 % | 3.0 % | 1.0% |
| Omikron BA.5 | 95.0 % | 96.0 % | 97.0 % | 97.0% |

![VoC](/images/2022/09/KW38Var.png)

## Wiesnbarometer

Tag 1 nach der Wiesn. Die Münchner Inzidenz am Samstag steigt im Vergleich zur Vorwoche um satte 94,7%(!) auf 768,7 (2022-09-30). Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche sehr stark um 60,94% auf jetzt 478.

Bis Mittwoch keine neuen Fallzahlen und daher auch keine aktualisierten Inzidenzen wegen Meldepause am Wochenende und Feiertag.

![Wiesnbarometer 2022-10-04](/images/2022/10/20221004WB.png)