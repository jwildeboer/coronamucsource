---
layout: default
title: 2022-10-16 Tagesbericht
category: Tagesberichte
---

![2022-10-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Am Samstag und Sonntag aber keine Aktualiserung. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 5 Tagen sinkt.](/images/2022/10/20221016.png)

Sonntag, daher keine neuen Meldungen beim RKI. Die heute genannte Inzidenz von 860,0 für München ist daher mit Vorsicht zu betrachten. Erst am Dienstag wird es wieder eine "echte" neue Inzidenz geben.
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 301 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 303, letzte Woche 263. Damit lag der Wert an 6 der letzten 7 Tage über 300, das sind 50% der alten roten Ampel (600).
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 2.430, gestern waren es 2.554, letzte Woche 1.918. Seit dem 23.09. liegen wir damit klar über den alten Grenzwert der gelben Ampel (1.200).

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,97 und ist damit zwar unter 1, aber das kann sich ab Dienstag wieder ändern, wenn die neuen Meldungen beim RKI ankommen.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 13.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,32.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.10. | 1,21 | 1,21 |
| 10.10. | 1,28 | 1,19 |
| 11.10. | 1,33 | 1,22 |
| 12.10. | 1,32 | 1,28 |
| 13.10. | 1,23 | 1,32 |
| 14.10. | 1,09 | --- |
| 15.10. | 0,97 | --- |
## Krankenhaus

Am Sonntag keine Zahlen zur Krankenhausbelegung von der Stadt München.

## Impfungen

Am Sonntag keine neuen Impfzahlen von der Stadt München.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Da es keine neuen Meldungen beim RKI und keine neuen Krankenhauszahlen der Stadt gibt, kann das Wiesnbarometer heute nicht aktualisert werden. Morgen dann zwar auch noch keinen neuen Fallzahlen, aber es wird Krankenhauszahlen der Stadt geben.

![Wiesnbarometer 2022-10-15](/images/2022/10/20221015WB.png)