---
layout: default
title: 2022-10-30 Tagesbericht
category: Tagesberichte
---

![2022-10-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Samstag/Sonntag keine Aktualisierung. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und weiter sinkt.](/images/2022/10/20221030.png)

Sonntag. Keine Neu/Nachmeldungen beim RKI. Die heute genannte Inzidenz von 179,3 ist daher mit Vorsicht zu geniessen. Wegen Feiertag am Dienstag auch Mittwoch keine neue Inzidenz.

<!--more-->

Heute gibt es nur zwei neue Zahlen. Die 7 Tage Hospitalisierung bayernweit liegt jetzt bei 1.407 mit COVID-positiven Patienten belegeten Betten und damit weiter klar über den Grenzwert der alten gelben Ampel von 1.200. Seit nunmehr 39(!) Tagen.

Der zweite Wert ist die Belegung der Intensivbetten bayernweit mit COVID-positiven Patienten. Dieser Wert liegt heute bei 253 laut Intensivregister.

Weil auch morgen keine Neu/Nachmeldungen vom RKI verarbeitet werden, wird die Inzidenz dann auch 179,3 sein. Erst am Dienstag neue Zahlen. Und dann ist ja wieder Feiertag, also auch am Mittwoch keine neue Inzidenz.

## Umland

Stand 2022-10-29, wird erst Dienstag wieder aktualisiert wenn neue Zahlen beim RKI vorliegen.

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 29.10. | 28.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 265,0 | 325,5 | -18,6% |
| LK Ebersberg | 242,8 | 262,2 | -7,4% |
| LK Freising | 235,5 | 263,9 | -10,8% |
| LK Fürstenfeldbruck | 299,7 | 328,9 | -8,9% |
| LK München | 217,8 | 233,0 | -6,5% |
| LK Starnberg | 231,1 | 247,2 | -6,5% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 5 Tagen wieder leicht auf jetzt 0,69 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 27.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,57 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 23.10. | 0,59 | 0,52 |
| 24.10. | 0,60 | 0,51 |
| 25.10. | 0,61 | 0,51 |
| 26.10. | 0,65 | 0,53 |
| 27.10. | 0,67 | 0,57 |
| 28.10. | 0,69 | --- |
| 29.10. | 0,69 | --- |

## Krankenhaus

Am Sonntag gibt es keine aktualiserten Krankenhauszahlen der Stadt München.

## Impfungen

Am Sonntag gibt es keine aktualiserten Impfzahlen der Stadt München.

## Nach-Wiesnbarometer

Wegen fehlender Zahlen wird dieser Bereich auch erst am Montag aktulisiert, wenn neue Krankenhauszahlen vorliegen. Der Inzidenzteil erst am Dienstag.

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -46,9% auf 208,4. Damit wieder auf Vor-Wiesn-Niveau.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche um -16,19% auf jetzt 440. (Stand 2022-10-28)

![Wiesnbarometer 2022-10-29](/images/2022/10/20221029WB.png)