---
layout: default
title: 2022-10-22 Tagesbericht
category: Tagesberichte
---

![2022-10-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau auch klar sinken aber Samstag/Sonntag nicht aktualisiert werden. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 11 Tagen zügig sinkt.](/images/2022/10/20221022.png)

Das RKI meldet eine Inzidenz von 381,9 für München. 552 Neue, 153 Nachmeldungen, davon 125 für vorgestern. Die gestern genannte Inzidenz von 424,3 steigt damit auf 434,2. Somit Tag 3 mit Inzidenz unter 500.
<!--more-->

Es wurden 13 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 26. Damit insgesamt 654 COVID-Tote in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 14.10. | 964,4 ->  985,5 |
| 15.10. | 860,0 ->  946,0 |
| 16.10. | 707,9 ->  793,8 |
| 17.10. | 638,7 -> 661,3 |
| 18.10. | 544,1 -> 562,2 |
| 19.10. | 479,1 -> 493,6 |
| 20.10. | 424,3 -> 434,2 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 22.10. | 21.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 544,9 | 601,5 | -9,4% |
| LK Ebersberg | 555,5 | 590,7 | -6,0% |
| LK Freising | 431,1 | 458,8 | -6,0% |
| LK Fürstenfeldbruck | 589,3| 629,5 | -6,4% |
| LK München | 395,6 | 424,8 | -6,9% |
| LK Starnberg | 444,6 | 507,5 | -12,4% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,57 und ist somit seit 7 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 20.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und ist damit fast identisch zu unserem R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.10. | 0,98 | 1,09 |
| 16.10. | 0,89 | 0,92 |
| 17.10. | 0,79 | 0,77 |
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,64 | 0,63 |
| 20.10. | 0,60 | 0,59 |
| 21.10. | 0,59 | --- |

## Krankenhaus

Am Samstag keine neuen Zahlen zur Krankenhausbelegung von der Stadt München.

## Impfungen

Am Samstag keine neuen Impfzahlen von der Stadt München.

## Wiesnbarometer

Tag 19 nach der Wiesn. Die Münchner Inzidenz kollabiert nahezu im Vergleich zur Vorwoche um -61,3% auf 381,9.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche deutlich um -14,22% auf jetzt 525. (Stand Freitag, erst Montag wieder neue Zahlen.)

![Wiesnbarometer 2022-10-21](/images/2022/10/20221022WB.png)