---
layout: default
title: 2022-10-18 Tagesbericht
category: Tagesberichte
---

![2022-10-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 7 Tagen sinkt.](/images/2022/10/20221018.png)

Das RKI meldet eine Inzidenz von 638,7 für München. Nur 968 Neue, 1.253 Nachmeldungen, davon 1 für Sonntag, 954 für Samstag, 206 für Freitag. Die gestern genannte Inzidenz von 707,9 steigt damit auf790,0. Somit an 4 von den letzten 7 Tagen eine Inzidenz unetr 1.000.
<!--more-->

Keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 17. Damit weiter insgesamt 628 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 21.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 10.10. | 1.497,4 -> 1.548,7 |
| 11.10. | 1.481,3 ->  1.529,2 |
| 12.10. | 1.233,9 ->  1.260,3 |
| 13.10. | 1.064,6 ->  1.085,0 |
| 14.10. | 964,4 ->  982,7 |
| 15.10. | 860,0 ->  942,3 |
| 16.10. | 707,9 ->  790,0 |

Zumindest für Samstag noch mal viele Nachmeldungen, Sonntag bleibt aber nahezu leer.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstagswert** in %.

| Kreis | 18.10 | 15.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 883,9 | 1.566,4 | -43,6% |
| LK Ebersberg | 911,7 | 1.597,2 | -42,9% |
| LK Fürstenfeldbruck | 883,9 | 1.840,1 | -52,0% |
| LK München | 559,1 | 948,2 | -41,0% |
| LK Starnberg | 793,4 | 1.344,8 | -41,0% |

Die Welle kollabiert auch hier.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,79 und ist somit seit 3 Tagen nicht mehr exponentiell. Damit sind 33 Tage mit R(t) über 1,0 zu ende. In der Spitze hatte diese Welle ein R(t) von 1,79 am 30.09.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 14.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,23.

| Datum | R(t) | LMU |
|---|---:|---:|
| 11.10. | 1,33 | 1,14 |
| 12.10. | 1,32 | 1,19 |
| 13.10. | 1,24 | 1,24 |
| 14.10. | 1,10 | 1,23 |
| 15.10. | 0,98 | --- |
| 16.10. | 0,89 | --- |
| 17.10. | 0,79 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt bleibt auf sehr hoehm Niveau mehr oder weniger stabil: Normalbetten sinkt um 3 von gestern 543 auf jetzt 540. IMC steigt von gestern 12 auf jetzt 14. Intensivbetten sinkt von gestern 47 auf jetzt 46. Damit insgesamt 600 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 540 | -3 | 
| IMC | 14 | +2 | 
| Intensiv | 46 | -1 |
| SUMME | 600 | -2 |

Zum ersten mal seit dem 13.09. damit 37 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr. Heute wurden die Zahlen für 2 Tage aktualisiert, somit jetzt Stand 15.10.

1. +15
2. +12
3. +48
4. +415

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,14%, Bayern 58,96%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 15 nach der Wiesn. Die Münchner Inzidenz kollabiert nahezu im Vergleich zur Vorwoche um -58,8% auf 638,7.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche zum ersten mal seit dem 13.09.um -5,81% auf jetzt 600.

![Wiesnbarometer 2022-10-18](/images/2022/10/20221018WB.png)