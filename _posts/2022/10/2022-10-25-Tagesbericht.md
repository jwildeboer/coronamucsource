---
layout: default
title: 2022-10-25 Tagesbericht
category: Tagesberichte
---

![2022-10-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 12 Tagen zügig sinkt.](/images/2022/10/20221025.png)

Das RKI meldet eine Inzidenz von 302,6 für München. 493 Neue, 562 Nachmeldungen, davon 9 für vorgestern und 425 für Samstag. Die am Wochenende gemeldeten Inzidenzen steigen damit für Samstag von 316,7 auf 353,8 und für Sonntag von 316,5 auf 353,6. Tag 4 unter Inzidenz 400.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 28. Damit insgesamt 656 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 17.10. | 638,7 -> 661,9 |
| 18.10. | 544,1 -> 563,2 |
| 19.10. | 479,1 -> 494,9 |
| 20.10. | 424,3 -> 436,5 |
| 21.10. | 381,9 -> 390,5 |
| 22.10. | 316,7 -> 353,8 |
| 23.10. | 316,5 -> 353,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 25.10. | 22.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 454,2 | 544,9 | -16,6% |
| LK Ebersberg | 414,4 | 555,5 | -25,4% |
| LK Freising | 330,1 | 431,1 | -23,4% |
| LK Fürstenfeldbruck | 484,0 | 589,3 | -17,9% |
| LK München | 302,4 | 395,6 | -23,6% |
| LK Starnberg | 381,0 | 444,6 | -14,3% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt durch die Nachmeldungen wieder leicht auf jetzt 0,59 und ist somit seit 10 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 20.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und ist damit fast identisch zu unserem R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,64 | 0,63 |
| 20.10. | 0,60 | 0,59 |
| 21.10. | 0,59 | --- |
| 22.10. | 0,59 | --- |
| 23.10. | 0,59 | --- |
| 24.10. | 0,59 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt ist weiterhin auf hohem Niveau, sinkt aber weiter: Normalbetten sinkt um 15 von gestern 441 auf jetzt 426. IMC sinkt von gestern 13 auf jetzt 12. Intensivbetten sinkt von gestern 45 auf jetzt 43. Damit insgesamt 481 Betten belegt. Seit 2 Tagen  wieder unter 500 Normalbetten mit COVID-positiven Patienten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 426 | -15 | 
| IMC | 12 | -1 | 
| Intensiv | 43 | -2 |
| SUMME | 481 | -18 |

Damit 119 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen. Heute wurden die Zahlen für 3 Tage aktualisiert, somit jetzt Stand 23.10.

1. +10
2. +7
3. +33
4. +225

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,18%, Bayern 59,01%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Nach-Wiesnbarometer

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -54,3% auf 302,6.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche deutlich um -19,83% auf jetzt 499.

![Wiesnbarometer 2022-10-25](/images/2022/10/20221025WB.png)