---
layout: default
title: 2022-10-11 Tagesbericht
category: Tagesberichte
---

![2022-10-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher durch viele Nachmeldungen auch am Samstag und Sonntag. Zweite zeigt Krankenbetten, die weiter deutlich steigen. Dritte zeigt Inzidenz, die aufgrund der sehr vielen Nachmeldungen auf fast 1.500 geht.](/images/2022/10/20221011.png)

Das RKI meldet eine Inzidenz von 1.497,4(!) für München. 2.567 Neue, rekordverdächtige 4.569(!) Nachmeldungen, davon 2.220 für Sonntag, 1.537 für Samstag, 599 für Freitag. Die gestern genannte Inzidenz von 1.024,7 springt auf 1.329,6. Somit an 4 von den letzten 7 Tagen eine Inzidenz über 1.000.
<!--more-->

Leider auch 5 neue Todesfälle. Damit jetzt insgesamt 611 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 21.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 03.10. | 633,6 -> 786,0 |
| 04.10. | 708,9 -> 784,9 |
| 05.10. | 813,7 -> 973,2 |
| 06.10. | 1.057,3 -> 1.119,2 |
| 07.10. | 1.130,7 -> 1.183,4 |
| 08.10. | 1.026,5 -> 1.182,3 |
| 09.10. | 1.024,7 -> 1.329,6 |

Zum ersten mal seit Anfang Juni wieder eine hohe Zahl an Meldungen am Wochenende. Ob ab jetzt wieder tagesgenau (also nach Meldungseingang) nachgemeldet wird oder doch wieder am Montag alle Meldungen zusammen gemeldet werden - wir werden es nächste Woche sehen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstagswert** in %. Zweistellige Wachstumsraten über das Wochenende allerorten.

| Kreis | 11.10 | 08.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.566,4 | 1.225,5 | +27,8% |
| LK Ebersberg | 1.597,2 | 1.260,4 | +26,7% |
| LK Fürstenfeldbruck | 1.840,1 | 1.389,9 | +32,4% |
| LK München | 948,2 | 850,7 | +11,5% |
| LK Starnberg | 1.344,8 | 1.106,4 | +21,5% |

Auch hier darf man vermuten, daß noch Nachmeldungen ausstehen und erst in den nächsten Tagen in die Statistik eingehen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,27 und steigt seit 3 Tagen wieder an.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,42.

| Datum | R(t) | LMU |
|---|---:|---:|
| 04.10. | 1,37 | 1,56 |
| 05.10. | 1,26 | 1,53 |
| 06.10. | 1,20 | 1,49 |
| 07.10. | 1,18 | 1,42 |
| 08.10. | 1,19 | --- |
| 09.10. | 1,21 | --- |
| 10.10. | 1,27 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt steigt weiter deutlich an: Normalbetten steigt um 66 von gestern 524 auf jetzt 590, der klar höchste Wert seit die Stadt diese Zahl veröffetnlicht. IMC steigt von gestern 15 auf jetzt 17. Intensivbetten sinkt von gestern 37 auf jetzt 30. Damit insgesamt 637 Betten belegt, satte 61 Betten mehr als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 590 | +66 | 
| IMC | 17 | +2 | 
| Intensiv | 30 | -7 |
| SUMME | 637 | +61 |

Im Wochenvergleich sind das 159 Betten mehr. Seit 28 Tagen steigt damit die Krankenhausbelegung ununterbrochem im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr. Heute wurden die Zahlen für 3 Tage aktualisiert, somizt jetzt Stand 10.10.

1. +13
2. +6
3. +36
4. +253

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,10%, Bayern 58,90%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 8 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um deutliche 90,5% auf 1.497,4. Spitze bisher war der Donnerstag letzter Woche mit 182,5%.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 33,26% auf jetzt 637.

![Wiesnbarometer 2022-10-06](/images/2022/10/20221011WB.png)