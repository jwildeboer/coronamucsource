---
layout: default
title: 2022-10-31 Wochenbericht KW43
category: Wochenberichte
---

![2022-10-31 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Dritte zeigt Inzidenz, die weiter sinkt.](/images/2022/10/20221031.png)

In der letzten Woche gab es insgesamt nur 2.668 neue COVID Fälle in München, das sind satte 58,9% weniger als in der Vorwoche (5.300). Leider aber auch 19 Tote mit COVID Nachweis in den letzten 7 Tagen, 7 weniger als in der Vorwoche (26).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 5.289 stetig zurück auf 2.668. Die Inzidenz halbierte sich wieder nahezu, wie schon in der letzten Woche. 137,2 Punkte weniger. Eine erstaunliche Leistung. München gehört jetzt bundesweit zu den Top 3 der Kreise mit der **niedrigesten** Inzidenz. 

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 23.10. | 0 | 5.298 | 316,5 |
| 24.10. | 714 | 4.756 | 302,6 |
| 25.10. | 702 | 4.187 | 269,0 |
| 26.10. | 525 | 3.751 | 242,9 |
| 27.10. | 465 | 3.497 | 226,1 |
| 28.10. | 262 | 3.100 | 208,4 |
| 29.10. | 0 | 2.668 | 179,3 |

## Reproduktionswert R(t) für München

Unser R(t) stieg in der letzten Woche ein wenig, bleibt aber klar unter 1, durch die (noch fehlenden) Wochenendzahlen ist das aber wohl noch ein wenig nach unten verzerrt.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 27.10.) liegt bei 0,57.

| Datum | R(t) | LMU |
|---|---:|---:|
| 24.10. | 0,60 | 0,52 |
| 25.10. | 0,61 | 0,53 |
| 26.10. | 0,65 | 0,55 |
| 27.10. | 0,67 | 0,57 |
| 28.10. | 0,69 | --- |
| 29.10. | 0,69 | --- |
| 30.10. | 0,69 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder weniger Normal- und Intensivbetten aber nahezu unveränderte IMC Zahlen. Dennoch - klarer Rückgang bei den Krankenhauszahlen. An jedem Tag deutlich weniger als in der Vorwoche.

Im Vergleich zum Vorjahrimmer noch ca. 2,5x mehr Betten belegt.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 24.10. | 426 | 12 | 43 | 481 | -119 | 143 |
| 25.10. | 426 | 14 | 46 | 486 | -114 | 154 |
| 26.10. | 395 | 15 | 42 | 452 | -115 | 163 |
| 27.10. | 390 | 14 | 36 | 440 | -85 | 167 |
| 28.10. | 383 | 13 | 36 | 432 | -84 | 174 |
| 29.10. | 376 | 12 | 35 | 423 | -84 | 174 |
| 30.10. | 369 | 12 | 34 | 415 | -84 | 174 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es langsam weniger schlimm aus. Seit dem 22.09. sind wir klar über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche, auch wenn die Zahlen weiter sinken. In der letzten Woche an 4 von 7 Tagen mehr als doppelte der alten gelben Ampel.

Bei den Intensivbetten sinkt die Sume von letzte Woche 323 auf jetzt 293.

Im Vergleich zum Vorjahr fast Gleichstand. Im letzten Jahr fing um diese Zeit die große Herbstwelle an.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 24.10. | 2.396 | 984 |
| 25.10. | 2.133 | 999 |
| 26.10. | 1.946 | 1.030 |
| 27.10. | 1.721 | 1.071 |
| 28.10. | 1.485 | 1.138 |
| 29.10. | 1.407 | 1.186 |
| 30.10. | 1.363 | 1.250 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,97% | 75,18% |
| 2. | 71,45% | 75,09% |
| 3. | 48,21% | 59,05% |
| 4. | 9,29% | 9,45% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/10/KW43Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 21.416 Tests/Tag mit einer Positivrate von 32,42%. Gegenüber den Zahlen von letzter Woche (27.124 Tests/Tag mit 40,65% Positivrate) also klar weniger Tests, gepaart mit sinkdender Positivrate.

Aber 21.416 Tests/Tag sind deutlich weniger als 30% der vorhandenen Testkapazität. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für (PCR-Pool-)Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 21.416 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf jetzt 32,42%.](/images/2022/10/20221027Test.png)

## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. BA.5 herrscht zwar noch vor, hat aber kräftig Anteil verloren. Weniger an BQ.1, deutlich mehr an BF.7.

| Variante | KW39 | KW40 | KW41 | KW42 |
|:--|--:|--:|--:|--:|
| Omikron BA.1 | 0.0% | 0.1% | 0.0% | 0.0% |
| Omikron BA.2 | 1.6% | 2.1% | 2.0% | 2.8% |
| Omikron BA.2.75 | 1.1% | 0.3% | 0.4% | 0.3% |
| Omikron BA.4 | 2.7% | 1.5% | 1.4% | 2.8% |
| Omikron BA.5 | 90.9% | 79.3% | 73.3% | 67.2% |
| Omikron BF.7 | --- | 9.9% | 15.2% | 18.0% |
| Omikron BQ.1 | 2.9% | 6.3% | 7.2% | 7.3% |
| Andere | 0.7% | 0.5% | 0.4% | 1.6% |
| Sequenzierungen | 986 | 1.149 | 902 | 317 |

![VoC](/images/2022/10/KW43Var.png)