---
layout: default
title: 2022-10-02 Tagesbericht
category: Tagesberichte
---

![2022-10-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche exponentiell höher aber in den letzten Tagen überraschen niedrige Fallzahlen. Zweite zeigt Krankenbetten, die weiter deutlich steigen, bis Dienstag aber nicht aktualisiert werden. Dritte zeigt Inzidenz, die jetzt klar exponentiell steigt und auch heute über 800 liegt.](/images/2022/10/20221002.png)

Das RKI meldet eine Inzidenz von 768,7 für München. Da Sonntag, keine neuen Fallzahlen beim RKI, keine neuen Krankenhauszahlen der Stadt.
<!--more-->

Die einzigen neuen Zahlen (vom RKI und Intensivregister) für Bayern:

- Bayernweit gibt es heute 196 mit COVID positiven Patienten belegte Intensivbetten, gestern waren es 183, letzte Woche 135.
- Die 7 Tages Summe der COVID-Positiv Hospitalisierten, bayernweit. Heute sind es 1.502, gestern waren es 1.575, letzte Woche 901. 

Es könnte sein, daß die Contact Tracer der Stadt an die Kapazitätsgrenze stoßen und daher in der nächsten Woche wieder vermehrt Meldeverzögerungen kommen werden. Die Zahlen lassen das vermuten.

Dazu dann das verlängerte Wochenende wo es wohl trotz der dramatischen Lage mit einem R(t) von über 1,6 bis Mittwoch keine neuen Fallzahlen und Inzidenz beim RKI geben wird - das ist schon eine gefährliche Mischung.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,69, Tag 5 über 1,5, also klar exponentielles Wachstum. Warum dann die Fallzahlen sich trotzdem halbieren? Das kann eigentlich nur duch Kapazitätsprobleme bei der Fallbearbeitung erklärt werden.

Ein weiteres Münchner Wunder, wie letztes Jahr bei der IAA?

Das R(t) vom StaBLab der LMU wird voraussichtlich erst nach der Wiesn wieder aktualisiert - Grund sind "technische Probleme".

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.09. | 1,33 | --- |
| 26.09. | 1,39 | --- |
| 27.09. | 1,49 | --- |
| 28.09. | 1,59 | --- |
| 29.09. | 1,70 | --- |
| 30.09. | 1,76 | --- |
| 01.10. | 1,69 | --- |

## Krankenhaus

Bis Dienstag keine neuen Krankenhauszahlen der Stadt München.

## Impfungen

Bis Dienstag keine neuen Impfzahlen der Stadt München.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 16 der Wiesn. Bis Dienstag keine neuen Krankenhauszahlen, bis Mittowch keine neue Inzidenz. Daher unverändert: Die Münchner Inzidenz springt im Vergleich zur Vorwoche um 153,4%(!) auf 834,3 am Freitag. Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche kräftig um 66,37% auf 376 am Freitag.

![Wiesnbarometer 2022-10-01](/images/2022/10/20221001WB.png)