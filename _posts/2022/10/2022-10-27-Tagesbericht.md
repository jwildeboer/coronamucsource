---
layout: default
title: 2022-10-27 Tagesbericht
category: Tagesberichte
---

![2022-10-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 14 Tagen zügig sinkt.](/images/2022/10/20221027.png)

Das RKI meldet eine Inzidenz von 242,9 für München. 415 Neue, 159 Nachmeldungen, davon 137 für vorgestern. Die gestern geannte Inzidenz von 269,0 steigt durch die Nachmeldungen auf 279,4. Tag 2 unter Inzidenz 300.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle bleibt bei 23. Damit insgesamt 660 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 12.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 19.10. | 479,1 -> 495,1 |
| 20.10. | 424,3 -> 437,0 |
| 21.10. | 381,9 -> 391,3 |
| 22.10. | 316,7 -> 354,8 |
| 23.10. | 316,5 -> 354,7 |
| 24.10. | 302,6 -> 317,9 |
| 24.10. | 269,0 -> 279,4 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 27.10. | 26.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 359,0 | 395,0 | -9,1% |
| LK Ebersberg | 289,1 | 354,9 | -18,5% |
| LK Freising | 293,7 | 316,3 | -7,1% |
| LK Fürstenfeldbruck | 366,9 | 417,2 | -12,1% |
| LK München | 259,0 | 283,0 | -8,5% |
| LK Starnberg | 272,8 | 324,7 | -16,0% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 3 Tagen wieder leicht auf jetzt 0,64 und ist trotzdem seit 12 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 26.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,53 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 20.10. | 0,60 | 0,63 |
| 21.10. | 0,59 | 0,59 |
| 22.10. | 0,59 | 0,55 |
| 23.10. | 0,59 | 0,52 |
| 24.10. | 0,60 | 0,51 |
| 25.10. | 0,61 | 0,51 |
| 26.10. | 0,64 | 0,53 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt weiter: Normalbetten sinkt deutlich um 31 Betten auf jetzt 395. Unter 400 Normalbetten hatten wir zuletzt am 01.10. IMC steigt von gestern 14 auf jetzt 15. Intensivbetten sinkt von gestern 46 auf jetzt 42. Damit insgesamt 452 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 395 | -31 | 
| IMC | 15 | +1 | 
| Intensiv | 42 | -4 |
| SUMME | 452 | -34 |

Damit 115 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen.

1. +12
2. +6
3. +39
4. +366

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,20%, Bayern 59,03%.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 21.416 Tests/Tag mit einer Positivrate von 32,42%. Gegenüber den Zahlen von letzter Woche (27.124 Tests/Tag mit 40,65% Positivrate) also weniger Tests, gepaart mit niedriger Positivrate.

21.416 Tests/Tag ist klar weniger als 25% der vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 21.416 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt auf 32,42%.](/images/2022/10/20221027Test.png)

## Nach-Wiesnbarometer

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -51,0% auf 242,9. Damit wieder auf Vor-Wiesn-Niveau.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche deutlich um -20,28% auf jetzt 452.

![Wiesnbarometer 2022-10-27](/images/2022/10/20221027WB.png)