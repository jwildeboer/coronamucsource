---
layout: default
title: 2022-10-20 Tagesbericht
category: Tagesberichte
---

![2022-10-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau langsam sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 9 Tagen zügig sinkt.](/images/2022/10/20221020.png)

Das RKI meldet eine Inzidenz von 479,1 für München. 776 Neue, 236 Nachmeldungen, davon 194 für vorgestern. Die gestern genannte Inzidenz von 544,1 steigt damit auf 559,6. Somit an 6 von den letzten 7 Tagen eine Inzidenz unter 1.000, Tag 1 unter 500.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 21. Damit insgesamt 637 COVID-Tote in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 12.10. | 1.233,9 ->  1.261,4 |
| 13.10. | 1.064,6 ->  1.086,2 |
| 14.10. | 964,4 ->  984,8 |
| 15.10. | 860,0 ->  945,2 |
| 16.10. | 707,9 ->  793,0 |
| 17.10. | 638,7 -> 659,7 |
| 18.10. | 544,1 -> 559,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 20.10. | 19.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 681,3 | 786,1 | -13,3% |
| LK Ebersberg | 673,1 | 753,3 | -10,6% |
| LK Fürstenfeldbruck | 706,4 | 734,7 | -3,9% |
| LK München | 474,8 | 521,4 | -8,9% |
| LK Starnberg | 596,7 | 653,0 | -8,6% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,63 und ist somit seit 5 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 19.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,62 und ist damit fast identisch zu unserem R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 13.10. | 1,24 | 1,22 |
| 14.10. | 1,10 | 1,21 |
| 15.10. | 0,98 | 1,10 |
| 16.10. | 0,89 | 0,92 |
| 17.10. | 0,79 | 0,77 |
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,63 | 0,62 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt ist weiterhin auf sehr hohem Niveau, sinkt jetzt aber wieder: Normalbetten sinkt um satte 35 von gestern 542 auf jetzt 507. IMC steigt von gestern 11 auf jetzt 12. Intensivbetten steigt von gestern 47 auf jetzt 48. Damit insgesamt 567 Betten belegt. Zu ersten Mal seit dem 09.10. wieder unter 600.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 507 | -35 | 
| IMC | 12 | +1 | 
| Intensiv | 48 | +1 |
| SUMME | 567 | -33 |

Damit 53 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen. Heute wurden die Zahlen für 2 Tage aktualisiert, somit jetzt Stand 17.10.

1. +12
2. +15
3. +61
4. +510

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,16%, Bayern 58,98%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 17 nach der Wiesn. Die Münchner Inzidenz kollabiert nahezu im Vergleich zur Vorwoche um -62,0% auf 479,1.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche um -8,55% auf jetzt 567.

![Wiesnbarometer 2022-10-20](/images/2022/10/20221020WB.png)