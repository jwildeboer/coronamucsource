---
layout: default
title: 2022-10-23 Tagesbericht
category: Tagesberichte
---

![2022-10-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau auch klar sinken aber Samstag/Sonntag nicht aktualisiert werden. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 12 Tagen zügig sinkt.](/images/2022/10/20221023.png)

Sonntag, somit keine neuen Meldungen beim RKI verarbeitet. Damit sinkt die Inzidenz zwar auf 316,7, aber die Aussagekraft ist gering. 
<!--more-->

Wie immer am Sonntag, nut 2 wirklich neue Zahlen greifbar:

Die 7 Tage Hospitalisierung bayernweit liegt mit 2.008 seit nunmehr 30(!) Tagen über dem Grenzwert der alten gelben Ampel von 1.200. Mit 3.712 wurde am 10.10. der höchste Wert seit der Beginn der Pandemie gemessen.

Aktuell sind 286 Intensivbetten in Bayern mit COVID-positiven Patienten belegt. In 2022 gab es keine großen Spitzen wie noch in 2021 (Omikron und Impfung sei dank) aber wir waren dieses Jahr dafür nie unter 100 belegten Intensivbetten.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,56 und ist somit seit 8 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 20.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,59 und ist damit fast identisch zu unserem R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.10. | 0,89 | 0,92 |
| 17.10. | 0,79 | 0,77 |
| 18.10. | 0,70 | 0,68 |
| 19.10. | 0,64 | 0,63 |
| 20.10. | 0,60 | 0,59 |
| 21.10. | 0,59 | --- |
| 22.10. | 0,58 | --- |

## Krankenhaus

Am Sonntag keine neuen Zahlen zur Krankenhausbelegung von der Stadt München.

## Impfungen

Am Sonntag keine neuen Impfzahlen von der Stadt München.
