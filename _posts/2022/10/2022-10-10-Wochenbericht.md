---
layout: default
title: 2022-10-10 Wochenbericht KW40
category: Wochenberichte
---

![2022-10-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher. Zweite zeigt Krankenbetten, die weiter steigen, wenn auch langsamer. Dritte zeigt Inzidenz, die Tag 4 über 1.000 ist.](/images/2022/10/20221010.png)

In der letzten Woche gab es insgesamt 15.272 neue COVID Fälle in München, das sind 12,4% mehr als in der Vorwoche (13.590). Leider auch 9 Tote mit COVID Nachweis in den letzten 7 Tagen, 2 mehr als in der Vorwoche (7).
<!--more-->

## Fallzahlen

In der letzten Woche ging die 7 Tages Summe von 13.589 hoch auf 15.272. Die Inzidenz stieg um 113,1 Punkte in 7 Tagen. Sollten nicht noch überraschend viele (nach-)Meldungen auftauchen, so ist der Höhepunkt der Wiesn-Welle wohl in Sichtweite.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 02.10. | 27 | 13.589 | 913,4 |
| 03.10. | 71 | 11.671 | 784,5 |
| 04.10. | 3.003 | 11.643 | 782,6 |
| 05.10. | 5.901 | 14.404 | 968,2 |
| 06.10. | 4.042 | 16.460 | 1.106,4 |
| 07.10. | 2.228 | 16.821 | 1.130,7 |
| 08.10. | 0 | 15.272 | 1.026,5 |

## Reproduktionswert R(t) für München

Unser R(t) war in der letzten Woche stabil weiter über 1 und damit exponentiell, sinkt aber auf jetzt 1,13.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war am 06.10) liegt bei 1,34.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.10. | 1,51 | 1,45 |
| 04.10. | 1,36 | 1,40 |
| 05.10. | 1,26 | 1,34 |
| 06.10. | 1,19 | --- |
| 07.10. | 1,17 | --- |
| 08.10. | 1,15 | --- |
| 09.10. | 1,13 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende wieder weitere Steigerungen bei den Zahlen (+16), insbesondere bei den Normalbetten (+15). Seit 6 Tagen wieder über 500 Betten insgesamt belegt. Im Vergleich zur Vorwoche 124 Betten mehr, im Vergleich zum Vorjahr sogar 430(!) Betten mehr belegt.

Normalbetten über 500, wie wir es seit 5 Tagen sehen - das gab es noch nie seit diese Zahlen veröffentlicht werden.

Und die Zahlen werden in dieser Woche wohl noch weiter steigen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 03.10. | 426 | 13 | 39 | 478 | +181 | 135 |
| 04.10. | 468 | 18 | 36 | 522 | +199 | 144 |
| 05.10. | 500 | 7 | 36 | 543 | +200 | 129 |
| 06.10. | 502 | 13 | 37 | 552 | +176 | 131 |
| 07.10. | 509 | 14 | 37 | 560 | +159 | 135 |
| 08.10. | 516 | 14 | 37 | 567 | +141 | 135 |
| 09.10. | 524 | 15 | 37 | 576 | +124 | 146 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und auch dort schaut es weiter schlimm aus. Seit dem 22.09. sind wir deutlich über dem Grenzwert der alten gelben Ampel von 1.200 Neuaufnahmen pro Woche und die Zahlen steigen weiter.

Bei den Intensivbetten sprang die Sume von letzte Woche 211 auf jetzt 306.

Im Vergleich zum Vorjahr nahezu eine vervierfachung der belegten Betten.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 03.10. | 1.590 | 545 |
| 04.10. | 2.002 | 521 |
| 05.10. | 2.170 | 478 |
| 06.10. | 2.148 | 484 |
| 07.10. | 2.002 | 498 |
| 08.10. | 1.918 | 494 |
| 09.10. | 1.839 | 502 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter  klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Landesweit gab es einen weiteren kleinen Schub bei den Viertimpfungen. Es scheint, daß die neuen Omikron-angepassten Bosster ganz gut angenommen werden.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,95% | 75,15% |
| 2. | 71,43% | 75,07% |
| 3. | 48,09% | 58,90% |
| 4. | 7,63% | 7,48% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/10/KW40Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München jetzt eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei ca. 25.264 Tests/Tag mit einer Positivrate von 40,88%. Gegenüber den Zahlen von letzter Woche (18.663 Tests/Tag mit 27,58% Positivrate) also wieder mehr Tests, gepaart mit deutlich steigender Positivrate.

Aber auch 25.264 Tests/Tag sind wenig. Das Maximum liegt bei 90.000 Tests/Tag und es gibt genug Kapazitäten für über 100.000 Tests/Tag in Bayern. Die wir jetzt, im Herbst und dann im Winter auch nutzen sollten. Insbesondere für Tests an den Schulen.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 25.264 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate springt auf jetzt 40,88%.](/images/2022/10/20221006-Test.png)

## Varianten

Die LMU betreibt jetzt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Aktuell immer noch ganz einfach: Fast alles BA.5. Vereinzelt BA.2.75, noch kein BJ.1, BQ.1, B2.75.2, auch wenn in Österreich schon vermehrt neue Varianten auftreten.

| Variante | KW 37 | KW38 | KW39 | KW40 |
|:--|--:|--:|--:|--:|
| Andere | 0.5% | 0.2% | 1.0% | 0.0% |
| Omikron BA.2 | 1.5% | 2.1% | 3.2% | 0.0% |
| Omikron BA.4 | 3.8% | 3,3% | 2.6% | 2.1% |
| Omikron BA.5 | 93.8% | 94.4% | 93.2% | 97.9% |

![VoC](/images/2022/10/KW40Var.png)

## Wiesnbarometer

Tag 7 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um 29,2% auf 1.130,7. Spitze bleibt Donnerstag, der 29.09. mit 182,3%. (Zahlen von vorgestern, da RKI Sonntag/Montag nicht aktualisiert)

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 29,38% auf jetzt 576.

![Wiesnbarometer 2022-10-10](/images/2022/10/20221010WB.png)