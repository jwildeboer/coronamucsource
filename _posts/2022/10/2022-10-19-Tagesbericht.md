---
layout: default
title: 2022-10-19 Tagesbericht
category: Tagesberichte
---

![2022-10-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 8 Tagen sinkt.](/images/2022/10/20221019.png)

Das RKI meldet eine Inzidenz von 544,1 für München. 1.054 Neue, 285 Nachmeldungen, davon 246 für vorgestern. Die gestern genannte Inzidenz von 638,7 steigt damit auf 657,1. Somit an 5 von den letzten 7 Tagen eine Inzidenz unter 1.000.
<!--more-->

Leider auch 9 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 25. Damit insgesamt 637 COVID-Tote in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 10.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 11.10. | 1.481,3 ->  1.529,8 |
| 12.10. | 1.233,9 ->  1.261,1 |
| 13.10. | 1.064,6 ->  1.085,8 |
| 14.10. | 964,4 ->  984,1 |
| 15.10. | 860,0 ->  944,3 |
| 16.10. | 707,9 ->  792,0 |
| 17.10. | 638,7 -> 657,1 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 19.10. | 18.10 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 786,1 | 883,9 | -11,1% |
| LK Ebersberg | 753,3 | 911,7 | -17,4% |
| LK Fürstenfeldbruck | 734,7 | 883,9 | -16,9% |
| LK München | 521,4 | 559,1 | -6,7% |
| LK Starnberg | 653,0 | 793,4 | -17,7% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 0,70 und ist somit seit 4 Tagen nicht mehr exponentiell. Damit sind 33 Tage mit R(t) über 1,0 beendet. In der Spitze hatte diese Welle ein R(t) von 1,79 am 30.09.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 18.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,67.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.10. | 1,32 | 1,16 |
| 13.10. | 1,24 | 1,20 |
| 14.10. | 1,10 | 1,19 |
| 15.10. | 0,98 | 1,08 |
| 16.10. | 0,89 | 0,91 |
| 17.10. | 0,79 | 0,76 |
| 18.10. | 0,70 | 0,67 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt bleibt auf sehr hohem Niveau mehr oder weniger stabil: Normalbetten steigt um 2 von gestern 540 auf jetzt 542. IMC sinkt von gestern 14 auf jetzt 11. Intensivbetten steigt von gestern 46 auf jetzt 47. Damit insgesamt weiterhin 600 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 542 | +2 | 
| IMC | 11 | -3 | 
| Intensiv | 47 | +1 |
| SUMME | 600 | +0 |

Damit 23 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen. Heute wurden die Zahlen für 2 Tage aktualisiert, somit jetzt Stand 17.10.

1. +22
2. +30
3. +169
4. +143

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,16%, Bayern 58,97%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 16 nach der Wiesn. Die Münchner Inzidenz kollabiert nahezu im Vergleich zur Vorwoche um -64,4% auf 544,1.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche zum zweiten mal seit dem 13.09.um -3,69% auf jetzt 600.

![Wiesnbarometer 2022-10-19](/images/2022/10/20221019WB.png)