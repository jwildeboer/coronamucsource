---
layout: default
title: 2022-10-26 Tagesbericht
category: Tagesberichte
---

![2022-10-26 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 13 Tagen zügig sinkt.](/images/2022/10/20221026.png)

Das RKI meldet eine Inzidenz von 269,0 für München. 561 Neue, 238 Nachmeldungen, davon 200 für vorgestern. Die gestern geannte Inzidenz von 302,6 steigt durch die Nachmeldungen auf 316,6. Tag 5 unter Inzidenz 400.
<!--more-->

Es wurden 4 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle geht auf 23. Damit insgesamt 660 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 8.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 18.10. | 544,1 -> 563,4 |
| 19.10. | 479,1 -> 495,1 |
| 20.10. | 424,3 -> 436,8 |
| 21.10. | 381,9 -> 391,1 |
| 22.10. | 316,7 -> 354,5 |
| 23.10. | 316,5 -> 354,4 |
| 24.10. | 302,6 -> 316,6

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 26.10. | 25.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 395,0 | 454,2 | -13.0% |
| LK Ebersberg | 354,9 | 414,4 | -14,4% |
| LK Freising | 316,3 | 330,1 | -4,2% |
| LK Fürstenfeldbruck | 417,2 | 484,0 | -13,8% |
| LK München | 283,0 | 302,4 | -6,4% |
| LK Starnberg | 324,7 | 381,0 | -14,8% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 2 Tagen wieder leicht auf jetzt 0,61 und ist somit seit 11 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 24.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,50 und ist damit (noch) deutlich niedriger als unser R(t).

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.10. | 0,64 | 0,66 |
| 20.10. | 0,60 | 0,63 |
| 21.10. | 0,59 | 0,59 |
| 22.10. | 0,59 | 0,54 |
| 23.10. | 0,59 | 0,51 |
| 24.10. | 0,60 | 0,50 |
| 25.10. | 0,61 | 0,50 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt ist weiterhin auf hohem Niveau, heute eine kleine Steigung: Normalbetten bleibt auf 426. IMC steigt von gestern 12 auf jetzt 14. Intensivbetten steigt von gestern 43 auf jetzt 46. Damit insgesamt 486 Betten belegt. Seit 3 Tagen  wieder unter 500 Normalbetten mit COVID-positiven Patienten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 426 | +0 | 
| IMC | 14 | +2 | 
| Intensiv | 46 | +3 |
| SUMME | 486 | +5 |

Damit 114 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Aber auch die Viertimpfungen werden weniger häufig angenommen. Heute wurden bei den Viertimpfungen wohl sehr viele Nachmeldungen eingearbeitet. Von den 17.969 Viertimpfungen in ganz Bayern scheinen 12.700 nur aus München zu sein. Erstaunlich.

1. +27
2. +39
3. +391
4. +12.700

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,18%, Bayern 59,01%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Nach-Wiesnbarometer

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -52,3% auf 269,0. Damit wieder auf Vor-Wiesn-Niveau.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche deutlich um -19,0% auf jetzt 486.

![Wiesnbarometer 2022-10-26](/images/2022/10/20221026WB.png)