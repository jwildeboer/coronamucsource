---
layout: default
title: 2022-10-15 Tagesbericht
category: Tagesberichte
---

![2022-10-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Am Samstag und Sonntag aber keine Aktualiserung.  Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und seit 4 Tagen sinkt.](/images/2022/10/20221015.png)

Das RKI meldet eine Inzidenz von 964,4 für München. 1.110 Neue, 244 Nachmeldungen, davon 191 für vorgestern. Die gestern genannte Inzidenz von 1.064,6 erhöht sich auf 1.080,5. Nach 9 Tagen wieder unter 1.000.
<!--more-->

Leider auch 9 neue Todesfälle. Damit jetzt insgesamt 628 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. 7 Tage Summe der Todesfälle jetzt 22. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 14.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 07.10. | 1.130,7 -> 1.186,3 |
| 08.10. | 1.026,5 -> 1.186,2 |
| 09.10. | 1.024,7 -> 1.336,5 |
| 10.10. | 1.497,4 -> 1.547,3 |
| 11.10. | 1.481,3 -> 1.527,2 |
| 12.10. | 1.233,9 -> 1.257,7 |
| 13.10. | 1.064,6 -> 1.080,5 |

Da am Wochenende keine neuen Zahlen vom RKI verarbeitet werden, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| Sonntag, 16.10. | 860,0 |
| Montag, 17.10. | 707,9 |

Dieses schnelle Absinken der Inzidenz macht sich natürlich gut am Montag in den Nachrichten - "München halbiert Inzidenz in 5 Tagen!", ist aber nur ein Artefakt weil letzten Montag überraschend doch Zahlen für Samstag und Sonntag nachgemeldet werden, die jetzt eben wieder aus der 7 Tage Summe herausfallen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu gestern in %. Jetzt überall sinkende Inzidenzen.

| Kreis | 15.10. | 14.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau |1.019,6 | 1.072,4 | -4,9% |
| LK Ebersberg | 1.016,9 | 1.098,5 | -7,4% |
| LK Fürstenfeldbruck | 1.127,3 | 1.269,1 | -11,2% |
| LK München | 734,6 | 835,0 | -12,0% |
| LK Starnberg | 891,4 | 949,9 | -6,2% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,09 und sinkt damit zwar seit 3 Tagen, bleibt aber noch knapp über 1 und somit exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 13.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,32.

| Datum | R(t) | LMU |
|---|---:|---:|
| 08.10. | 1,19 | 1,28 |
| 09.10. | 1,21 | 1,21 |
| 10.10. | 1,28 | 1,19 |
| 11.10. | 1,33 | 1,22 |
| 12.10. | 1,32 | 1,28 |
| 13.10. | 1,23 | 1,32 |
| 14.10. | 1,09 | --- |

## Krankenhaus

Am Samstag keine Zahlen zur Krankenhausbelegung von der Stadt München.

## Impfungen

Am Samstag keine neuen Impfzahlen von der Stadt München.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 12 nach der Wiesn. Die Münchner Inzidenz **sinkt** im Vergleich zur Vorwoche jetzt klar um 18,7% auf 946,4.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 10,87% auf jetzt 612. Bis Montag keine Aktualiserung dieser Zahlen.

![Wiesnbarometer 2022-10-15](/images/2022/10/20221015WB.png)