---
layout: default
title: 2022-10-14 Tagesbericht
category: Tagesberichte
---

![2022-10-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen ein wenig. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Dritte zeigt Inzidenz, die anscheinend den Höhepunkt überschritten hat.](/images/2022/10/20221014.png)

Das RKI meldet eine Inzidenz von 1.064,6 für München. 1.662 Neue, 316 Nachmeldungen, davon 246 für vorgestern. Die gestern genannte Inzidenz von 1.233,9 erhöht sich auf 1.254,6. Somit 8 Tagen eine Inzidenz über 1.000. 2 Tage sogar über 1.500.
<!--more-->

Leider auch 3 neue Todesfälle. Damit jetzt insgesamt 619 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. 7 Tage Summe der Todesfälle jetzt 15. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 14.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 06.10. | 1.057,3 -> 1.121,3 |
| 07.10. | 1.130,7 -> 1.185,9 |
| 08.10. | 1.026,5 -> 1.185,9 |
| 09.10. | 1.024,7 -> 1.336,0 |
| 10.10. | 1.497,4 -> 1.546,5 |
| 11.10. | 1.481,3 -> 1.525,6 |
| 12.10. | 1.233,9 -> 1.254,6 |

Zum ersten mal seit Anfang Juni wieder eine hohe Zahl an Meldungen am Wochenende. Ob ab jetzt wieder tagesgenau (also nach Meldungseingang) nachgemeldet wird oder doch wieder am Montag alle Meldungen zusammen gemeldet werden - wir werden es nächste Woche sehen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zu gestern in %. Jetzt überall klar sinkende Inzidenzen.

| Kreis | 14.10. | 13.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.072,4 | 1.221,6 | -12,2% |
| LK Ebersberg | 1.098,5 | 1.253,4 | -12,4% |
| LK Fürstenfeldbruck | 1.269,1 | 1.473,6 | -13,9% |
| LK München | 835,0 | 874,1 | -4,5% |
| LK Starnberg | 949,9 | 1.075,0 | -11,6% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,23 und sinkt damit zwar seit 3 Tagen, bleibt aber weiter klar über 1 und somit exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 13.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,32.

| Datum | R(t) | LMU |
|---|---:|---:|
| 07.10. | 1,18 | 1,36 |
| 08.10. | 1,19 | 1,28 |
| 09.10. | 1,21 | 1,21 |
| 10.10. | 1,28 | 1,19 |
| 11.10. | 1,33 | 1,22 |
| 12.10. | 1,32 | 1,28 |
| 13.10. | 1,23 | 1,32 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute wieder ein wenig, bleibt aber auf sehr hohem Niveau: Normalbetten sinkt um 15 von gestern 577 auf jetzt 562 (Freitagseffekt). IMC bleibt mit 16 gleich. Intensivbetten steigt wieder von gestern 27 auf jetzt 34. Damit insgesamt 612 Betten belegt, 8 Betten weniger als gestern. Neue Zahlen erst wieder am Montag.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 562 | -15 | 
| IMC | 16 | +0 | 
| Intensiv | 34 | +7 |
| SUMME | 612 | -8 |

Im Wochenvergleich sind das 60 Betten mehr. Seit 31 Tagen steigt damit die Krankenhausbelegung ununterbrochen im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr.

1. +19
2. +7
3. +36
4. +355

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,13%, Bayern 58,93%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 11 nach der Wiesn. Die Münchner Inzidenz **sinkt** im Vergleich zur Vorwoche um 5,1% auf 1.064,6. Das erste Mal seit dem 12.09. das diese Zahl sinkt.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 10,87% auf jetzt 612.

![Wiesnbarometer 2022-10-14](/images/2022/10/20221014WB.png)