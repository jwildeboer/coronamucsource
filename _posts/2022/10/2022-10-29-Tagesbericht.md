---
layout: default
title: 2022-10-29 Tagesbericht
category: Tagesberichte
---

![2022-10-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sinken die Zahlen deutlich. Zweite zeigt Krankenbetten, die jetzt von einem sehr hohem Niveau kommend auch klar sinken, Samstag/Sonntag keine Aktualisierung. Dritte zeigt Inzidenz, die den Höhepunkt überschritten hat und weiter sinkt.](/images/2022/10/20221029.png)

Das RKI meldet eine Inzidenz von 208,1 für München. 262 Neue, 140 Nachmeldungen, davon 121 für vorgestern. Die gestern geannte Inzidenz von 226,1 steigt durch die Nachmeldungen auf 235,1. Tag 4 unter Inzidenz 300.
<!--more-->

Es wurden 7 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 19. Damit insgesamt 673 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 15.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 21.10. | 381,9 -> 392,6 |
| 22.10. | 316,7 -> 356,0 |
| 23.10. | 316,5 -> 356,1 |
| 24.10. | 302,6 -> 319,7 |
| 25.10. | 269,0 -> 281,4 |
| 26.10. | 242,9 -> 252,1 |
| 27.10. | 226,1 -> 235,1 |

Da Sonntag und Montag keine neuen Fälle an das RKI (nach-)gemeldet werden, können wir schon jetzt die Inzidenz für Sonntag und Montag berechnen:

| Sonntag | 179,3 |
| Montag | 179,3 | 

Also kann die Stadtpolitik am Montag den zweiten Tag unter Inzidenz 200 feiern ;)

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 29.10. | 28.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 265,0 | 325,5 | -18,6% |
| LK Ebersberg | 242,8 | 262,2 | -7,4% |
| LK Freising | 235,5 | 263,9 | -10,8% |
| LK Fürstenfeldbruck | 299,7 | 328,9 | -8,9% |
| LK München | 217,8 | 233,0 | -6,5% |
| LK Starnberg | 231,1 | 247,2 | -6,5% |

Überall sinkende Inzidenzen.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 5 Tagen wieder leicht auf jetzt 0,69 und ist trotzdem seit 13 Tagen nicht mehr exponentiell.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 27.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,57 und steigt damit auch wieder etwas an.

| Datum | R(t) | LMU |
|---|---:|---:|
| 22.10. | 0,59 | 0,55 |
| 23.10. | 0,59 | 0,52 |
| 24.10. | 0,60 | 0,51 |
| 25.10. | 0,61 | 0,51 |
| 26.10. | 0,65 | 0,53 |
| 27.10. | 0,67 | 0,57 |
| 28.10. | 0,69 | --- |

## Krankenhaus

Am Sanstag gibt es keine aktualiserten Krankenhauszahlen der Stadt München.

## Impfungen

Am Sanstag gibt es keine aktualiserten Impfzahlen der Stadt München.

## Nach-Wiesnbarometer

Die Münchner Inzidenz sinkt im Vergleich zur Vorwoche um -46,9% auf 208,4. Damit wieder auf Vor-Wiesn-Niveau.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern sinkt im Vergleich zur Vorwoche um -16,19% auf jetzt 440. (Stand 2022-10-28)

![Wiesnbarometer 2022-10-29](/images/2022/10/20221029WB.png)