---
layout: default
title: 2022-10-13 Tagesbericht
category: Tagesberichte
---

![2022-10-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche leicht höher. Zweite zeigt Krankenbetten, die sich auf sehr hohem Niveau stabilisieren. Dritte zeigt Inzidenz, die anscheinend den Höhepunkt überschritten hat.](/images/2022/10/20221013.png)

Das RKI meldet eine Inzidenz von 1.233,9 für München. 1.681 Neue,  602 Nachmeldungen, davon 461 für vorgestern. Die gestern genannte Inzidenz von 1.481,3 erhöht sich auf 1.521,2. Somit an 6 von den letzten 7 Tagen eine Inzidenz über 1.000. 2 Tage sogar über 1.500.
<!--more-->

Leider auch 4 neue Todesfälle. Damit jetzt insgesamt 616 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. 7 Tage Summe der Todesfälle jetzt 15. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 14.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 05.10. | 813,7 -> 974,0 |
| 06.10. | 1.057,3 -> 1.120,9 |
| 07.10. | 1.130,7 -> 1.185,4 |
| 08.10. | 1.026,5 -> 1.185,4 |
| 09.10. | 1.024,7 -> 1.335,4 |
| 10.10. | 1.497,4 -> 1.544,5 |
| 11.10. | 1.481,3 -> 1.521,2 |

Zum ersten mal seit Anfang Juni wieder eine hohe Zahl an Meldungen am Wochenende. Ob ab jetzt wieder tagesgenau (also nach Meldungseingang) nachgemeldet wird oder doch wieder am Montag alle Meldungen zusammen gemeldet werden - wir werden es nächste Woche sehen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum gestern in %. Bis auf LK Fürstenfeldbrück überall klar sinkende Inzidenzen.

| Kreis | 13.10. | 12.10. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.221,6 | 1.408,8 | -13,3% |
| LK Ebersberg | 1.253,4 | 1.507,3 | -16,8% |
| LK Fürstenfeldbruck | 1.473,6 | 1.462,6 | +0,8% |
| LK München | 874,1 | 913,3 | -4,3% |
| LK Starnberg | 1.075,0 | 1.399,7 | -23,2% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,31 und sinkt damit ein klein wenig.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 10.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,32.

| Datum | R(t) | LMU |
|---|---:|---:|
| 06.10. | 1,20 | 1,49 |
| 07.10. | 1,18 | 1,45 |
| 08.10. | 1,19 | 1,38 |
| 09.10. | 1,21 | 1,32 |
| 10.10. | 1,28 | 1,29 |
| 11.10. | 1,33 | 1,32 |
| 12.10. | 1,31 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute minimal, bleibt aber auf sehr hohem Niveau: Normalbetten steigt um 4 von gestern 573 auf jetzt 577. IMC bleibt mit 16 gleich. Intensivbetten sinkt von gestern 34 auf jetzt 27. Damit insgesamt 620 Betten belegt, 3 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 577 | +4 | 
| IMC | 16 | +0 | 
| Intensiv | 27 | -7 |
| SUMME | 620 | -3 |

Im Wochenvergleich sind das 77 Betten mehr. Seit 30 Tagen steigt damit die Krankenhausbelegung ununterbrochen im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr.

1. +15
2. +19
3. +39
4. +518

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,13%, Bayern 58,91%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei ca. 27.029 Tests/Tag mit einer Positivrate von 44,92%(!). Gegenüber den Zahlen von letzter Woche (25.707 Tests/Tag mit 39,50% Positivrate) also wieder etwas mehr Tests, gepaart mit deutlich höherer Positivrate.

27.029 Tests/Tag während einer solchen Welle ist immer noch weniger als 30% der vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 27.029 weiterhin niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate springt auf jetzt 44,92%.](/images/2022/10/20221013-Test.png)

## Wiesnbarometer

Tag 10 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um "nur" noch 26,7% auf 1.233,9.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 14,18% auf jetzt 620.

![Wiesnbarometer 2022-10-13](/images/2022/10/20221013WB.png)