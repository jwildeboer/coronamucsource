---
layout: default
title: 2022-10-12 Tagesbericht
category: Tagesberichte
---

![2022-10-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche höher durch viele Nachmeldungen auch am Samstag und Sonntag. Zweite zeigt Krankenbetten, die weiter steigen, auch wenn heute minimal weniger als gestern. Dritte zeigt Inzidenz, die aufgrund der sehr vielen Nachmeldungen zum zweiten mal auf fast 1.500 geht.](/images/2022/10/20221012.png)

Das RKI meldet eine Inzidenz von 1.481,3 für München. 2.213 Neue, 575 Nachmeldungen, davon 522 für vorgestern. Die gestern genannte Inzidenz von 1.497,4 steigt auf 1.535,4. Somit an 6 von den letzten 7 Tagen eine Inzidenz über 1.000.
<!--more-->

Leider auch 1 neuer Todesfall. Damit jetzt insgesamt 612 COVID-Tote in 2022, in 2020 waren es 579., 2021 sogar 1.050. 7 Tage Summe der Todesfälle jetzt 15. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 16.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 04.10. | 708,9 -> 785,0 |
| 05.10. | 813,7 -> 973,6 |
| 06.10. | 1.057,3 -> 1.120,0 |
| 07.10. | 1.130,7 -> 1.184,4 |
| 08.10. | 1.026,5 -> 1.184,0 |
| 09.10. | 1.024,7 -> 1.332,7 |
| 10.10. | 1.497,4 -> 1.535,4 |

Zum ersten mal seit Anfang Juni wieder eine hohe Zahl an Meldungen am Wochenende. Ob ab jetzt wieder tagesgenau (also nach Meldungseingang) nachgemeldet wird oder doch wieder am Montag alle Meldungen zusammen gemeldet werden - wir werden es nächste Woche sehen.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum gestern in %. Bis auf LK Starnberg sinkende Inzidenzen.

| Kreis | 12.10. | 11.10 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 1.408,8 | 1.566,4 | -10,1% |
| LK Ebersberg | 1.507,3 | 1.597,2 | -5,6% |
| LK Fürstenfeldbruck | 1.462,6 | 1.840,1 | -20,5% |
| LK München | 913,3 | 948,2 | -3,7% |
| LK Starnberg | 1.399,7 | 1.344,8 | +4,1% |

## Reproduktionswert R(t) für München

Unser R(t) liegt jetzt bei 1,32 und steigt seit 4 Tagen wieder an.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.10. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,42.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.10. | 1,26 | 1,53 |
| 06.10. | 1,20 | 1,49 |
| 07.10. | 1,18 | 1,42 |
| 08.10. | 1,19 | --- |
| 09.10. | 1,21 | --- |
| 10.10. | 1,27 | --- |
| 11.10. | 1,32 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten insgesamt sinkt heute ein wenig, bleibt aber auf sehr hohem Niveau: Normalbetten sinkt um 17 von gestern 590 auf jetzt 573. IMC sinkt von gestern 17 auf jetzt 16. Intensivbetten steigt von gestern 30 auf jetzt 34. Damit insgesamt 623 Betten belegt, 14 Betten weniger als gestern.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 573 | -17 | 
| IMC | 16 | -1 | 
| Intensiv | 34 | +4 |
| SUMME | 623 | -14 |

Im Wochenvergleich sind das 101 Betten mehr. Seit 29 Tagen steigt damit die Krankenhausbelegung ununterbrochen im Vergleich zur Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Viertimpfungen ein wenig mehr. Heute wurden die Nachmeldungen der letzten 7 Tage durch Arztptaxen etc. eingearbeitet, daher die hohen Zahlen.

1. +57
2. +44
3. +383
4. +6066

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,12%, Bayern 58,90%.

## Änderung des Status "vollständig geimpft"

Seit dem 1. Oktober gilt man [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben jetzt in München eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Wiesnbarometer

Tag 9 nach der Wiesn. Die Münchner Inzidenz steigt im Vergleich zur Vorwoche um deutliche 88,7% auf 1.481,3.

Die Zahl der mit COVID-positiven Patienten belegten Betten in den Münchner Krankenhäusern steigt im Vergleich zur Vorwoche weiter um 19,35% auf jetzt 623.

![Wiesnbarometer 2022-10-12](/images/2022/10/20221012WB.png)