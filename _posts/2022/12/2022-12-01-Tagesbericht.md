---
layout: default
title: 2022-12-01 Tagesbericht
category: Tagesberichte
---

![2022-12-01 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht noch knapp über 1. Zweite zeigt Krankenbetten, die heute weiter steigen. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert.](/images/2022/12/20221201.png)

Das RKI meldet eine Inzidenz von 98,5 für München. 213 Neue, 97 Nachmeldungen, davon 93 für vorgestern. Durch Nachmeldungen erhöht sich die gestern genannte Inzidenz von 96,1 auf 102,6. Wir bleiben also mit einem Tag Verzögerung seit 8 Tagen über Inzidenz 100.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt 738 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 32.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 23.11. | 92,7 -> 103,4 |
| 24.11. | 98,9 -> 106,1 |
| 25.11. | 101,0 -> 102,5 |
| 26.11. | 99,8 -> 102,5 |
| 27.11. | 99,8 -> 102,5 |
| 28.11. | 97,9 -> 104,1 |
| 29.11. | 96,1 -> 102,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 01.12. | 30.11. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 133,2 | 128,0 | +4,1% |
| LK Ebersberg | 91,3 | 98,2 | -7,0% |
| LK Freising | 70,7 | 72,9 | -3,0% |
| LK Fürstenfeldbruck | 115,3 | 115,3 | +/-0,0% |
| LK München | 94,9 | 92,0 | +3,2% |
| LK Starnberg | 89,9 | 88,5 | +1,6% |

Uneinheitlich, aber stabil.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 5 Tagen über 1 und damit über der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 23.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,92.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 24.11. | 1,01 | 1,04 |
| 25.11. | 1,04 | 1,05 |
| 26.11. | 1,06 | 1,02 |
| 27.11. | 1,06 | 0,99 |
| 28.11. | 1,05 | 0,98 |
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,02 | 1,04 |

[19:56] Die neuen Zahlen wurden vom StaBLab veröffentlicht.

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt langsam weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 3 von 203 auf 206. IMC bleibt auf 10. Intensivbetten steigt um 1 von 16 auf 17. Damit insgesamt 233 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 206 | +3 | 
| IMC | 10 | +/-0 | 
| Intensiv | 17 | +1 |
| SUMME | 233 | +4 |

Damit insgesamt 26 Betten **mehr** belegt als in der Vorwoche. An 5 der letzten 7 Tage damit mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +6 | 73,00% | +81 | 75,19% |
| 2. | +6 | 71,47% | +115 | 75,11% |
| 3. | +18 | 48,33% | +481 | 59,17% |
| 4. | +217 | 11,41% | +6.886 | 11,08% |
| 5. | --- | --- | +1.408 | 0,62% |
| 6. | --- | --- | +59 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,33%, Bayern 59,17%.

Das RKI gibt auch Zahlen zur fünften und sechsten Impfung (also 3. und 4. Boosterimpfung) auf Landesebene an. Aktuell 81.120 5. Impfungen in Bayern, das entpricht einer Impfquote von 0,62%. Dazu 3.676 6. Impfungen, das entspricht einer Impfquote von 0,03%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 13.640 Tests/Tag mit einer Positivrate von 13,18%. Gegenüber den Zahlen von letzter Woche (14.631 Tests/Tag mit 12,05% Positivrate) also wieder weniger Tests, gepaart mit minimal steigender Positivrate.

13.640 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.640 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt minimal auf 13,18%.](/images/2022/12/20221201Test.png)
