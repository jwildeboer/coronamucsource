---
layout: default
title: 2022-12-23 Tagesbericht
category: Tagesberichte
---

![2022-12-23 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert liegt bei 1,05. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 3 Tagen über 130 liegt, und langsam aber stetig steigt.](/images/2022/12/20221223.png)

Das RKI meldet eine Inzidenz von 138,7 für München. 283 Neue, 60 Nachmeldungen, davon 55 für vorgestern. Durch die Nachmeldungen erhöht sich die gestern genannte Inzidenz von 136,0 auf 139,8. Tag 3 mit Inzidenz über 130.
<!--more-->

Es wurden 5 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 13. Damit insgesamt 773 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 17.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 15.12. | 122,9 -> 126,7 |
| 16.12. | 122,1 -> 125,2 |
| 17.12. | 122,1 -> 125,2 |
| 18.12. | 122,1 -> 125,2 |
| 19.12. | 122,7 -> 129,6 |
| 20.12. | 128,5 -> 133.6 |
| 21.12. | 136,0 -> 139,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 23.12. | 22.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 167,3 | 128,7 | +30,0% |
| LK Ebersberg | 99,6 | 98,2 | +1,4% |
| LK Freising | 78,4 | 74,0 | +5,9% |
| LK Fürstenfeldbruck | 143,7 | 146,9 | -2,2% |
| LK München | 180,1 | 166,1 | +8,4% |
| LK Starnberg | 109,7 | 116,3 | -5,7% |

Sehr uneinheitlich. Dachau macht große Sprünge, da scheint es echte Probleme mit der Datenübermittlung zu geben.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 27 Tagen über 1, heute ist der R(t) Wert 1,05 - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zum Stand 21.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,05 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 16.12. | 1,07 | 1,11 |
| 17.12. | 1,06 | 1,04 |
| 18.12. | 1,05 | 0,99 |
| 19.12. | 1,04 | 0,98 |
| 20.12. | 1,04 | 1,01 |
| 21.12. | 1,04 | 1,05 |
| 22.12. | 1,05 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute wieder weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 13 von 399 auf 412. IMC springt um 5 von gestern 9 auf jetzt 14. Intensivbetten unverändert auf 29. Damit insgesamt 455 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 412 | +13 | 
| IMC | 14 | +5 | 
| Intensiv | 29 | +/-0 |
| SUMME | 455 | +18 |

Damit insgesamt 111 Betten **mehr** belegt als in der Vorwoche. Jetzt 24 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Bei den Impfzahlen tut sich wenig. Leider.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +5 | 73,01% | +56 | 75,20% |
| 2. | +4 | 71,48% | +71 | 75,12% |
| 3. | +15 | 48,38% | +322 | 59,22% |
| 4. | +92 | 12,09% | +3.139 | 11,68% |
| 5. | --- | --- | +654 | 0,74% |
| 6. | --- | --- | +19 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.