---
layout: default
title: 2022-12-16 Tagesbericht
category: Tagesberichte
---

![2022-12-16 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert steigt auf 1,09. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 4 Tagen über 120 liegt.](/images/2022/12/20221216.png)

Das RKI meldet eine Inzidenz von 122,9 für München. 245 Neue, 32 Nachmeldungen, davon 29 für Vorgestern. Durch de Nachmeldungen erhöht sich die gestern genannte Inzidenz von 123,9 auf 125,9. Tag 4 über Inzidenz 120.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 760 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 61.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 08.12. | 107,5 -> 110,8 |
| 09.12. | 110,8 -> 117,0 |
| 10.12. | 110,8 -> 117,0 |
| 11.12. | 110,8 -> 117,0 |
| 12.12. | 119,6 -> 123,3 |
| 13.12. | 120,2 -> 124,0 |
| 14.12. | 123,9 -> 125,9 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 16.12. | 15.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 137,7 | 146,0 | -5,7% |
| LK Ebersberg | 87,2 | 89,2 | -2,2% |
| LK Freising | 74,5 | 66,2 | +12,5% |
| LK Fürstenfeldbruck | 148,7 | 143,7 | +3,5% |
| LK München | 130,9 | 121,2 | +8,0% |
| LK Starnberg | 128,0 | 124,3 | +3,0% |

Leicht steigende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 11 Tagen stetig weiter und hat jetzt 1,08 erreicht - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,03.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber frühestens am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in der letzten Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 09.12. | 1,04 | --- |
| 10.12. | 1,05 | --- |
| 11.12. | 1,05 | --- |
| 12.12. | 1,07 | --- |
| 13.12. | 1,08 | --- |
| 14.12. | 1,08 | --- |
| 15.12. | 1,09 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter und bleibt in Summe zum 3. Tag in Folge klar über 300: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 18 von 290 auf 308. IMC ist unverändert 8. Intensivbetten steigt um 3 von gestern 25 auf 28. Damit insgesamt 344 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 308 | +18 | 
| IMC | 8 | +/-0 | 
| Intensiv | 28 | +3 |
| SUMME | 344 | +21 |

Damit insgesamt 49 Betten **mehr** belegt als in der Vorwoche. Jetzt 16 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Weiterhin kaum Impfungen in München. Auch bayernweit niedrige Zahlen.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +10 | 73,00% | +29 | 75,20% |
| 2. | +7 | 71,48% | +65 | 75,12% |
| 3. | +8 | 48,36% | +361 | 59,20% |
| 4. | +95 | 11,88% | +5.650 | 11,55% |
| 5. | --- | --- | +1.196 | 0,71% |
| 6. | --- | --- | +25 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,36%, Bayern 59,20%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.