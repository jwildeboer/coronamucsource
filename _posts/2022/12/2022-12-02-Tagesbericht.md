---
layout: default
title: 2022-12-02 Tagesbericht
category: Tagesberichte
---

![2022-12-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht auf genau 1,00. Zweite zeigt Krankenbetten, die heute deutlich steigen. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert.](/images/2022/12/20221202.png)

Das RKI meldet eine Inzidenz von 98,0 für München. 207 Neue, 60 Nachmeldungen, davon 55 für vorgestern. Durch Nachmeldungen erhöht sich die gestern genannte Inzidenz von 98,5 auf 102,5. Wir bleiben also mit einem Tag Verzögerung seit 9 Tagen über Inzidenz 100. Plateau.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 5. Damit insgesamt 739 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 33.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 24.11. | 98,9 -> 106,1 |
| 25.11. | 101,0 -> 102,5 |
| 26.11. | 99,8 -> 102,5 |
| 27.11. | 99,8 -> 102,5 |
| 28.11. | 97,9 -> 104,2 |
| 29.11. | 96,1 -> 102,8 |
| 30.11. | 98,5 -> 102,5 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 02.12. | 01.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 127,4 | 133,2 | -4,4% |
| LK Ebersberg | 92,7 | 91,3 | +1,5% |
| LK Freising | 74,5 | 70,7 | +5,4% |
| LK Fürstenfeldbruck | 117,1 | 115,3 | +1,6% |
| LK München | 93,2 | 94,9 | -1,8% |
| LK Starnberg | 87,8 | 89,9 | -1,2% |

Uneinheitlich, aber stabil.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 6 Tagen über 1 und damit über der Grenze zum exponenziellem Wachstum, sinkt heute aber auf genau 1,00.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 25.11. | 1,04 | 1,05 |
| 26.11. | 1,06 | 1,02 |
| 27.11. | 1,06 | 0,99 |
| 28.11. | 1,05 | 0,98 |
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,00 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute weiter, was am Freitag eher ungewöhnlich ist: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 15 von 206 auf 221. IMC bleibt auf 10. Intensivbetten sinkt um 3 von 17 auf 14. Damit insgesamt 245 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 221 | +15 | 
| IMC | 10 | +/-0 | 
| Intensiv | 14 | -3 |
| SUMME | 245 | +12 |

Damit insgesamt 35 Betten **mehr** belegt als in der Vorwoche. An 5 der letzten 7 Tage damit mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +8 | 73,00% | +44 | 75,20% |
| 2. | +4 | 71,47% | +83 | 75,11% |
| 3. | +19 | 48,33% | +520 | 59,17% |
| 4. | +148 | 11,42% | +7.869 | 11,14% |
| 5. | --- | --- | +1.611 | 0,63% |
| 6. | --- | --- | +30 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,33%, Bayern 59,17%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.
