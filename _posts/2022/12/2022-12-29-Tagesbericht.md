---
layout: default
title: 2022-12-29 Tagesbericht
category: Tagesberichte
---

![2022-12-29 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ein Vergleich mit der Vorwoche ist noch nicht möglich, der R(t) Wert sinkt auf 0,98. Zweite zeigt Krankenbetten, die über die Feiertage gesunken sind, heute leicht steigen. Dritte zeigt Inzidenz, die zwar sinkt, aber wegen Feiertage verzerrt ist.](/images/2022/12/20221229.png)

Das RKI meldet eine Inzidenz von 121,7 für München. 325 Neue, 123 Nachmeldungen, davon 122 für vorgestern Die gestern genannte Inzidenz von 121,4 erhöht sich durch Nachmeldungen auf 129,7. Tag 2 über Inzidenz 120.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 21.12. | 136,0 -> 140,4 |
| 22.12. | 138,7 -> 144,8 |
| 23.12. | 145,5 -> 150,4 |
| 24.12. | 145,5 -> 150,4 |
| 25.12. | 145,5 -> 150,4 |
| 26.12. | 104,3 -> 109,1 |
| 27.12. | 121,4 -> 129,7 |

 Erst in der 2. Januarwoche werden die Zahlen wieder wirklich zuverlässig - sofern es dann noch aktuelle Zahlen geben wird.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 28.12. | 24.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 158,3 | 173,0 | -8,5% |
| LK Ebersberg | 70,6 | 90,6 | -22,1% |
| LK Freising | 80,6 | 86,1 | -6,4% |
| LK Fürstenfeldbruck | 137,7 | 145,9 | -5,6% |
| LK München | 120,1 | 170,1 | -29,4% |
| LK Starnberg | 91,4 | 112,6 | -18,8% |

Feiertagsknick, allerorten.

## Reproduktionswert R(t) für München

Unser R(t) stand seit 31 Tagen über 1, heute geht der R(t) Wert auf 0,98 und damit knapp unter 1.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,96 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 22.12. | 1,06 | 1,10 |
| 23.12. | 1,08 | 1,07 |
| 24.12. | 1,09 | 0,99 |
| 25.12. | 1,10 | 0,95 |
| 26.12. | 1,06 | 0,94 |
| 27.12. | 1,03 | 0,95 |
| 28.12. | 0,98 | 0,96 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 10 von 346 auf 356. IMC bleibt unverändert auf 19. Intensivbetten steigt um 4 von gestern 30 auf jetzt 34. Damit insgesamt 409 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 356 | +10 | 
| IMC | 19 | */-0 | 
| Intensiv | 34 | +4 |
| SUMME | 409 | +14 |

Damit insgesamt 28 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Bei den Impfzahlen tut sich wenig. Leider. 

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +1 | 73,01% | +22 | 75,20% |
| 2. | +4 | 71,48% | +42 | 75,13% |
| 3. | +9 | 48,38% | +135 | 59,22% |
| 4. | +44 | 12,11% | +1.450 | 11,72% |
| 5. | --- | --- | +345 | 0,75% |
| 6. | --- | --- | +15 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 11.519 Tests/Tag mit einer Positivrate von 17,73%. Gegenüber den Zahlen von letzter Woche (11.629 Tests/Tag mit 13,80% Positivrate) also weiter weniger Tests, gepaart mit steigender Positivrate.

11.519 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Seit 4 Wochen schaffen wir aber nich nichtmal 100.000 Test pro **Woche**.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 11.519 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt auf jetzt 17,73%.](/images/2022/12/20221229Test.png)