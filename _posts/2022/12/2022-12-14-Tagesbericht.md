---
layout: default
title: 2022-12-14 Tagesbericht
category: Tagesberichte
---

![2022-12-14 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert steigt auf 1,08. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 2 Tagen über 120 liegt.](/images/2022/12/20221214.png)

Das RKI meldet eine Inzidenz von 120,2 für München. 347 Neue, 60 Nachmeldungen, davon 54 für Vorgestern. Durch de Nachmeldungen erhöht sich die gestern genannte Inzidenz von 119,6 auf 123,3. Tag 2 über Inzidenz 120.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 757 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 64.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 06.12. | 99,5 -> 106,8 |
| 07.12. | 104,3 -> 110,2 |
| 08.12. | 107,5 -> 110,8 |
| 09.12. | 110,8 -> 117,0 |
| 10.12. | 110,8 -> 117,0 |
| 11.12. | 110,8 -> 117,0 |
| 12.12. | 119,6 -> 123,3 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 14.12 | 13.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 123,5 | 124,2 | -0,1% |
| LK Ebersberg | 98,9 | 92,0 | +7,5% |
| LK Freising | 69,0 | 66,8 | +3,3% |
| LK Fürstenfeldbruck | 140,5 | 134,0 | +4,9% |
| LK München | 115,5 | 115,5 | +/-0,0% |
| LK Starnberg | 118,5 | 111,9 | +5,9% |

Leicht steigende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 11 Tagen stetig weiter und hat jetzt 1,08 erreicht - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,03.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in der letzten Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 07.12. | 1,02 | --- |
| 08.12. | 1,02 | --- |
| 09.12. | 1,04 | --- |
| 10.12. | 1,05 | --- |
| 11.12. | 1,05 | --- |
| 12.12. | 1,07 | --- |
| 13.12. | 1,08 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter und bleibt in Summe zum 2. Tag in Folge über 300: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 4 von 276 auf 280. IMC steigt um 3 von gestern 6 auf 9. Intensivbetten steigt um 2 von gestern 20 auf 22. Damit insgesamt 311 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 280 | +4 | 
| IMC | 9 | +3 | 
| Intensiv | 22 | +2 |
| SUMME | 311 | +9 |

Damit insgesamt 49 Betten **mehr** belegt als in der Vorwoche. Jetzt 15 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Heute ist wieder Nachmeldetag bei den Impfzahlen. Änderungen bei der Impfquote, wenn überhaupt, dann nur in den Nachkommastellen sichtbar.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +42 | 73,00% | +68 | 75,20% |
| 2. | +22 | 71,48% | +63 | 75,12% |
| 3. | +126 | 48,35% | +354 | 59,20% |
| 4. | +2.578 | 11,86% | +5.338 | 11,47% |
| 5. | --- | --- | +1.072 | 0,70% |
| 6. | --- | --- | +24 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,35%, Bayern 59,20%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.