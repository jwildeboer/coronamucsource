---
layout: default
title: 2022-12-10 Tagesbericht
category: Tagesberichte
---

![2022-12-10 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher und anders verteilt, der R(t) Wert steht bei 1,03. Zweite zeigt Krankenbetten, die in der letzten Woche deutlich gestiegen sind, am Wochenende aber nicht aktualisiert werden. Dritte zeigt Inzidenz, die sich von 100 weg nach oben bewegt.](/images/2022/12/20221210.png)

Das RKI meldet eine Inzidenz von 110,8 für München. 207 Neue, 39 Nachmeldungen, davon 35 für vorgestern. Die gestern genannte Inzidenz von 107,5 steigt dadurch auf 110,1. Tag 2 über Inzidenz 110.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 10. Damit insgesamt 755 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 38.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 02.12. | 102,8 -> 107,1 |
| 03.12. | 101,6 -> 105,9 |
| 04.12. | 101,6 -> 105,9 |
| 05.12. | 94,8 -> 100,7 |
| 06.12. | 99,5 -> 106,5 |
| 07.12. | 104,3 -> 109,8 |
| 08.12. | 107,5 -> 110,1 |

Da am Wochenende keine Meldungen an das RKI gehen, können wir die Inzidenzen für Sonntag und Montag schon jetzt berechnen:

| Sonntag | 110,8 |
| Montag | 110,8 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 10.12. | 09.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 117,1 | 123,5 | -5,2% |
| LK Ebersberg | 100,3 | 98,2 | +2,1% |
| LK Freising | 64,0 | 63,5 | +0,8% |
| LK Fürstenfeldbruck | 124,9 | 125,8 | -0,7% |
| LK München | 122,1 | 120,3 | +1,5% |
| LK Starnberg | 106,8 | 114,8 | -7,0% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 12 Tagen auf, bzw. knapp über 1 und damit an der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in dieser Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 04.12. | 1,01 | 0,94 |
| 05.12. | 1,01 | 0,95 |
| 05.12. | 1,01 | 0,98 |
| 06.12. | 1,01 | 1,03 |
| 07.12. | 1,02 | --- |
| 08.12. | 1,02 | --- |
| 09.12. | 1,03 | --- |

## Krankenhaus

Am Samstag gibt es keine Aktualisierung der Krankenhauszahlen von der Stadt München.

Bayernweit gibt es immerhin noch die 7 Tage Hospitalisierung, die heute mit 1.002 zu ersten Mal seit dem 08.11. wieder über 1.000 liegt, wenn man sich nur die LGL Zahlen anschaut. Wenn man die um Nachmeldungen korrigierten Zahlen der RKI nimmt, dann sind wir bayernweit seit dem 18.09. kontinuierlich über 1.000.

## Impfungen

Am Samstag gibt es keine Aktualisierung der Impfzahlen von der Stadt München. Aber für Bayern schon.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,00% | +120 | 75,20% |
| 2. | --- | 71,47% | +142 | 75,12% |
| 3. | --- | 48,34% | +353 | 59,19% |
| 4. | --- | 11,66% | +4.869 | 11,40% |
| 5. | --- | --- | +972 | 0,68% |
| 6. | --- | --- | +29 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,19%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.