---
layout: default
title: 2022-12-04 Tagesbericht
category: Tagesberichte
---

![2022-12-04 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht auf 0,99. Zweite zeigt Krankenbetten, die in den letzten Tagen wieder gestiegen sind. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert. Am Sonntag und Montag keine Aktualisierung der Zahlen durch das RKI.](/images/2022/12/20221204.png)

Das RKI meldet, wie vorherberechnet, eine Inzidenz von 101,6 für München. Erst am Dienstag gibt es wieder neue Zahlen vom RKI. Tag 12 mit Inzidenz über 100. Plateau.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert, erst am Dienstag neue Zahlen):

| 25.11. | 101,0 -> 102,5 |
| 26.11. | 99,8 -> 102,5 |
| 27.11. | 99,8 -> 102,5 |
| 28.11. | 97,9 -> 104,2 |
| 29.11. | 96,1 -> 102,9 |
| 30.11. | 98,5 -> 102,7 |
| 01.12. | 98,0 -> 102,4 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Montag schon heute berechnen:

| Montag | 101,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Erst am Dienstag neue Zahlen.

| Kreis | 03.12. | 02.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,7 | 127,4 | +1,0% |
| LK Ebersberg | 88,5 | 92,7 | -4,5% |
| LK Freising | 72,3 | 74,5 | -2,9% |
| LK Fürstenfeldbruck | 118,0 | 117,1 | +0,7% |
| LK München | 94,9 | 93,2 | +1,8% |
| LK Starnberg | 92,9 | 87,8 | +5,8% |

Uneinheitlich, aber stabil.

## Reproduktionswert R(t) für München

Unser R(t) geht nach 8 Tagen über 1 heute wieder auf 0,99.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 27.11. | 1,06 | 0,99 |
| 28.11. | 1,05 | 0,98 |
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,01 | --- |
| 02.12. | 1,00 | --- |
| 03.12. | 0,99 | --- |

## Krankenhaus

Am Samstag erfolgt keine Aktualisierung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Samstag erfolgt keine Aktualisierung der Impfzahlen durch die Stadt München.