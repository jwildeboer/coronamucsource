---
layout: default
title: 2022-12-20 Tagesbericht
category: Tagesberichte
---

![2022-12-20 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert sinkt auf 1,03. Zweite zeigt Krankenbetten, die kräftig steigen. Dritte zeigt Inzidenz, die seit 8 Tagen über 120 liegt.](/images/2022/12/20221220.png)

Das RKI meldet eine Inzidenz von 122,7 für München. 511 Neue, 47 Nachmeldungen, davon 0 für das vergangene Wochenende, 38 für Freitag. Die Inzidenzen für Freitag bis Sonntag steigen damit einheitlich von 122,1 auf 125,0. Die Krankenhauszahlen steigen stark an.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt 764 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 40.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 12.12. | 119,6 -> 123,5 |
| 13.12. | 120,2 -> 124,1 |
| 14.12. | 123,9 -> 125,9 |
| 15.12. | 122,9 -> 126,6 |
| 16.12. | 122,1 -> 125,0 |
| 17.12. | 122,1 -> 125,0 |
| 18.12. | 122,1 -> 125,0 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 20.12. | 17.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 110,6 | 139,0 | -20,4% |
| LK Ebersberg | 103,8 | 85,1 | +22,0% |
| LK Freising | 85,0 | 75,6 | +12,4% |
| LK Fürstenfeldbruck | 151,4 | 152,8 | -0,9% |
| LK München | 142,4 | 130,6 | +9,0% |
| LK Starnberg | 135,3 | 134,6 | +0,5% |

Überwiegend steigende Tendenz, bis auf Dachau, die nur 2 Neue gemeldet haben. Da war also was kaputt.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 26 Tagen über 1, heute ist der R(t) Wert 1,03 - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt am Samstag zum Stand 14.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,10.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Samstag veröffentlicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.12. | 1,07 | 1,03 |
| 13.12. | 1,08 | 1,06 |
| 14.12. | 1,09 | 1,10 |
| 15.12. | 1,09 | --- |
| 16.12. | 1,07 | --- |
| 17.12. | 1,06 | --- |
| 18.12. | 1,05 | --- |
| 19.12. | 1,03 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten macht einen kräftigen Sprung nach oben und ist in Summe über 400 - das hatten wir zuletzt am 01.11.: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 45(!) von 326 auf 371. IMC steigt um 2 von gestern 10 auf jetzt 12. Intensivbetten bleiben unverändert auf 29. Damit insgesamt 412 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 371 | +45 | 
| IMC | 12 | +2 | 
| Intensiv | 29 | +/-0 |
| SUMME | 412 | +47 |

Damit insgesamt 110(!) Betten **mehr** belegt als in der Vorwoche. Jetzt 21 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Weiterhin kaum Impfungen in München. Auch bayernweit niedrige Zahlen.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +3 | 73,00% | +27 | 75,20% |
| 2. | +5 | 71,48% | +53 | 75,12% |
| 3. | +10 | 48,36% | +151 | 59,21% |
| 4. | +76 | 11,90% | +2.051 | 11,61% |
| 5. | --- | --- | +528 | 0,73% |
| 6. | --- | --- | +10 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,36%, Bayern 59,21%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.