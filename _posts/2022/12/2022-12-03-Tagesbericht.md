---
layout: default
title: 2022-12-03 Tagesbericht
category: Tagesberichte
---

![2022-12-03 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht auf genau 1,00. Zweite zeigt Krankenbetten, die in den letzten Tagen wieder gestiegen sind. Dritte zeigt Inzidenz, die sich momentan um 100 stabilisiert.](/images/2022/12/20221203.png)

Das RKI meldet eine Inzidenz von 102,8 für München. 145 Neue, 70 Nachmeldungen, davon 63 für vorgestern. Durch Nachmeldungen erhöht sich die gestern genannte Inzidenz von 98,0 auf 102,4. Tag 11 mit Inzidenz über 100. Plateau.
<!--more-->

Es wurden 6 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 9. Damit insgesamt 745 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 33.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 25.11. | 101,0 -> 102,5 |
| 26.11. | 99,8 -> 102,5 |
| 27.11. | 99,8 -> 102,5 |
| 28.11. | 97,9 -> 104,2 |
| 29.11. | 96,1 -> 102,9 |
| 30.11. | 98,5 -> 102,7 |
| 01.12. | 98,0 -> 102,4 |

Da am Wochenende keine Zahlen an das RKI gemeldet werden, können wir die Inzidenzen für Sonntag und Montag schon heute berechnen:

| Sonntag | 101,6 |
| Montag | 101,6 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 03.12. | 02.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,7 | 127,4 | +1,0% |
| LK Ebersberg | 88,5 | 92,7 | -4,5% |
| LK Freising | 72,3 | 74,5 | -2,9% |
| LK Fürstenfeldbruck | 118,0 | 117,1 | +0,7% |
| LK München | 94,9 | 93,2 | +1,8% |
| LK Starnberg | 92,9 | 87,8 | +5,8% |

Uneinheitlich, aber stabil.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 6 Tagen über 1 und damit über der Grenze zum exponenziellem Wachstum, sinkt heute aber auf genau 1,00.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 26.11. | 1,06 | 1,02 |
| 27.11. | 1,06 | 0,99 |
| 28.11. | 1,05 | 0,98 |
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,01 | --- |
| 02.12. | 1,00 | --- |

## Krankenhaus

Am Samstag erfolgt keine Aktualisierung der Krankenhauszahlen durch die Stadt München.

## Impfungen

Am Samstag erfolgt keine Aktualisierung der Impfzahlen durch die Stadt München.