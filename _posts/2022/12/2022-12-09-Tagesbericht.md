---
layout: default
title: 2022-12-09 Tagesbericht
category: Tagesberichte
---

![2022-12-09 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher und anders verteilt, der R(t) Wert steht bei 1,02. Zweite zeigt Krankenbetten, die heute deutlich gestiegen sind. Dritte zeigt Inzidenz, die sich von 100 weg nach oben bewegt.](/images/2022/12/20221209.png)

Das RKI meldet eine Inzidenz von 107,5 für München. 251 Neue, 80 Nachmeldungen, davon 76 für vorgestern. Die gestern genannte Inzidenz von 104,3 steigt dadurch auf 109,5. Der Trend zeigt nach oben.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 13. Damit insgesamt 752 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 30.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 01.12. | 98,0 -> 103,2 |
| 02.12. | 102,8 -> 107,0 |
| 03.12. | 101,6 -> 105,8 |
| 04.12. | 101,6 -> 105,8 |
| 05.12. | 94,8 -> 100,6 |
| 06.12. | 99,5 -> 106,4 |
| 07.12. | 104,3 -> 109,5 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 09.12. | 08.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 123,5 | 107,4 | +15,0% |
| LK Ebersberg | 98,2 | 103,8 | -5,4% |
| LK Freising | 63,5 | 68,5 | -7,3% |
| LK Fürstenfeldbruck | 125,8 | 127,6 | -1,4% |
| LK München | 120,3 | 118,9 | +1,2% |
| LK Starnberg | 114,8 | 106,0 | +8,3% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 12 Tagen auf, bzw. knapp über 1 und damit an der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. Oder auch gar nicht, wie in dieser Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 03.12. | 1,01 | --- |
| 04.12. | 1,01 | --- |
| 05.12. | 1,01 | --- |
| 05.12. | 1,01 | --- |
| 06.12. | 1,01 | --- |
| 07.12. | 1,01 | --- |
| 08.12. | 1,02 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter, was für ein Freitag eher ungewöhnlich ist, jetzt aber in der zweiten Woche auch passiert: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 23 von 241 auf 264. IMC steigt um 2 von gestern 9 auf 11.. Intensivbetten steigt um 6 von 14 auf 20. Damit insgesamt 295 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 264 | +23 | 
| IMC | 11 | +2 | 
| Intensiv | 20 | +6 |
| SUMME | 295 | +31 |

Damit insgesamt 50 Betten **mehr** belegt als in der Vorwoche. Jetzt 10 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +4 | 73,00% | +105 | 75,20% |
| 2. | +6 | 71,47% | +97 | 75,12% |
| 3. | +19 | 48,34% | +495 | 59,19% |
| 4. | +130 | 11,66% | +6.759 | 11,36% |
| 5. | --- | --- | +1.390 | 0,67% |
| 6. | --- | --- | +38 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,19%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.