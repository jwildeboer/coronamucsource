---
layout: default
title: 2022-12-21 Tagesbericht
category: Tagesberichte
---

![2022-12-21 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich abet etwas höher, der R(t) Wert bleibt bei 1,03. Zweite zeigt Krankenbetten, die weiter hoch bleiben. Dritte zeigt Inzidenz, die seit 9 Tagen über 120 liegt, aber nur langsam steigt.](/images/2022/12/20221221.png)

Das RKI meldet eine Inzidenz von 128,5 für München. 392 Neue, 98 Nachmeldungen, davon 96 für vorgestern. Durch die Nachmeldungen erhöht sich die gestern genannte Inzidenz von 122,7 auf 129,2. Tag 9 mit Inzidenz über 120.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 765 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 34.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 13.12. | 120,2 -> 124,1 |
| 14.12. | 123,9 -> 125,9 |
| 15.12. | 122,9 -> 126,6 |
| 16.12. | 122,1 -> 125,1 |
| 17.12. | 122,1 -> 125,1 |
| 18.12. | 122,1 -> 125,1 |
| 19.12. | 122,7 -> 129,2 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 21.12. | 20.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 147,3 | 110,6 | +33,2% |
| LK Ebersberg | 97,5 | 103,8 | -6,1% |
| LK Freising | 77,8 | 85,0 | -8,5% |
| LK Fürstenfeldbruck | 135,4 | 151,4 | -10,6% |
| LK München | 172,1 | 142,4 | +20,9% |
| LK Starnberg | 127,2 | 135,3 | -6,0% |

Heute ein etwas chaotisches Bild. Dachau hat nachgemeldet, der LK München macht einen unerwarteten Satz nach oben, ansonsten eher sinkende Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 26 Tagen über 1, heute ist der R(t) Wert 1,03 - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt am Samstag zum Stand 14.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,10.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Samstag veröffentlicht.

| Datum | R(t) | LMU |
|---|---:|---:|
| 13.12. | 1,08 | 1,06 |
| 14.12. | 1,09 | 1,10 |
| 15.12. | 1,09 | --- |
| 16.12. | 1,07 | --- |
| 17.12. | 1,06 | --- |
| 18.12. | 1,05 | --- |
| 19.12. | 1,03 | --- |
| 20.12. | 1,03 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten bleibt auch heute in Summe über 400 - das hatten wir zuletzt am 01.11.: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 3 von 371 auf 374. IMC sinkt um 3 von gestern 12 auf jetzt 9. Intensivbetten sinkt um 3 von gestern 29 auf jetzt 26. Damit insgesamt 409 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 374 | +3 | 
| IMC | 9 | -3 | 
| Intensiv | 26 | -3 |
| SUMME | 409 | -3 |

Damit insgesamt 98 Betten **mehr** belegt als in der Vorwoche. Jetzt 22 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Heute ist wieder Nachmeldetag, daher die etwas höheren Zahlen für München - insgesamt trotzdem kaum Fortschritt. Knapp 30 Tage haben wir gebraucht um bei den 4./5./6. Impfungen von 11% auf 12% zu gehen.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +16 | 73,01% | +27 | 75,20% |
| 2. | +28 | 71,48% | +47 | 75,12% |
| 3. | +208 | 48,37% | +215 | 59,21% |
| 4. | +2.566 | 12,07% | +3.205 | 11,63% |
| 5. | --- | --- | +778 | 0,73% |
| 6. | --- | --- | +20 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,37%, Bayern 59,21%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.