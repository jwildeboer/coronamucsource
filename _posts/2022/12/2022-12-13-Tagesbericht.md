---
layout: default
title: 2022-12-13 Tagesbericht
category: Tagesberichte
---

![2022-12-13 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher, der R(t) Wert steht bei 1,07. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die sich von 100 weg nach oben bewegt.](/images/2022/12/20221213.png)

Das RKI meldet eine Inzidenz von 119,6 für München. 489 Neue, 94 Nachmeldungen, davon 81 für letzten Freitag und 0 für Samstag und Sonntag. Durch die Nachmeldungen erhöhen sich die Inzidenzen von Freitag, Samstag und Sonntag von jeweils 110,8 auf 116,7.
<!--more-->

Es wurden 2 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 9. Damit insgesamt 757 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 45.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 05.12. | 94,8 -> 100,8 |
| 06.12. | 99,5 -> 106,6 |
| 07.12. | 104,3 -> 110,0 |
| 08.12. | 107,5 -> 110,6 |
| 09.12. | 110,8 -> 116,7 |
| 10.12. | 110,8 -> 116,7 |
| 11.12. | 110,8 -> 116,7 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 13.12. | 10.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 124,2 | 117,1 | +6,1% |
| LK Ebersberg | 92,0 | 100,3 | -8,3% |
| LK Freising | 66,8 | 64,0 | +4,4% |
| LK Fürstenfeldbruck | 134,0 | 124,9 | +7,3% |
| LK München | 115,5 | 122,1 | -5,4% |
| LK Starnberg | 111,9 | 106,8 | +4,8% |

Weiterhin uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 10 Tagen stetig weiter und hat jetzt 1,07 erreicht - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,03.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in der letzten Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 06.12. | 1,01 | 1,03 |
| 07.12. | 1,02 | --- |
| 08.12. | 1,02 | --- |
| 09.12. | 1,04 | --- |
| 10.12. | 1,05 | --- |
| 11.12. | 1,05 | --- |
| 12.12. | 1,07 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter und ist in Summe zum ersten Mal seit dem 7.11. wieder über 300: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 8 von 268 auf 276. IMC sinkt um 3 von gestern 9 auf 6. Intensivbetten steigt um 3 von gestern 17 auf 20. Damit insgesamt 302 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 276 | +8 | 
| IMC | 6 | -3 | 
| Intensiv | 20 | +3 |
| SUMME | 302 | +8 |

Damit insgesamt 54 Betten **mehr** belegt als in der Vorwoche. Jetzt 14 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Die Impfzahlen für München wurden heute nicht aktualisiert.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,00% | +19 | 75,20% |
| 2. | --- | 71,47% | +32 | 75,12% |
| 3. | --- | 48,34% | +259 | 59,20% |
| 4. | --- | 11,67% | +2.547 | 11,43% |
| 5. | --- | --- | +663 | 0,69% |
| 6. | --- | --- | +8 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,20%.

Die Stadt München scheint die Viert-m Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.