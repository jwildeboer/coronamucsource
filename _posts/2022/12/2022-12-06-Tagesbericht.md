---
layout: default
title: 2022-12-06 Tagesbericht
category: Tagesberichte
---

![2022-12-06 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen niedriger, der R(t) Wert steht auf genau 1,00. Zweite zeigt Krankenbetten, die heute en wenig sinken. Dritte zeigt Inzidenz, die sich um 100 stabilisiert hat.](/images/2022/12/20221206.png)

Das RKI meldet eine Inzidenz von 94,8 für München. 361 Neue, 66 Nachmeldungen, davon 0 für Sa/So, 49 für Freitag. Die Inzidenzen erhöhen sich durch Machmeldungen wie folgt: Samstag und Sonntag gehen jeweils von 101,6 auf 105,7, Freitag geht 102,8 auf 106,9.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle steigt auf 12. Damit insgesamt 748 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 28.11. | 97,9 -> 104,3 |
| 29.11. | 96,1 -> 103,0 |
| 30.11. | 98,5 -> 102,9 |
| 01.12. | 98,0 -> 103,2 |
| 02.12. | 102,8 -> 106,9 |
| 03.12. | 101,6 -> 105,7 |
| 04.12. | 101,6 -> 105,7 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum **Samstag** in %.

| Kreis | 06.12. | 02.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 124,2 |  127,4 | -2,5% |
| LK Ebersberg | 101,0 | 92,7 | +9,0% |
| LK Freising | 63,5 | 74,5 | -14,8% |
| LK Fürstenfeldbruck | 122,6 | 117,1 | +4,7% |
| LK München | 97,5 | 93,2 | +4,6% |
| LK Starnberg | 99,5 | 87,8 | +13,3% |

Uneinheitlich, aber weiterhin stabil.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 12 Tagen knapp über 1 und damit über der Grenze zum exponenziellem Wachstum, sinkt heute aber auf genau 1,00.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,01 | --- |
| 02.12. | 1,00 | --- |
| 03.12. | 1,01 | --- |
| 04.12. | 1,01 | --- |
| 05.12. | 1,00 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute ein wenig: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 2 von 228 auf 226. IMC steigt um 1 von 8 auf 9. Intensivbetten sinkt um 2 von 15 auf 13. Damit insgesamt 248 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 226 | -2 | 
| IMC | 9 | +1 | 
| Intensiv | 13 | -2 |
| SUMME | 248 | -3 |

Damit insgesamt 29 Betten **mehr** belegt als in der Vorwoche. Damit 7 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +9 | 73,00% | +29 | 75,20% |
| 2. | +6 | 71,47% | +30 | 75,12% |
| 3. | +11 | 48,33% | +262 | 59,18% |
| 4. | +85 | 11,45% | +3.308 | 11,22% |
| 5. | --- | --- | +803 | 0,64% |
| 6. | --- | --- | +13 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,33%, Bayern 59,18%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.
