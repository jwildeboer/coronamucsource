---
layout: default
title: 2022-12-15 Tagesbericht
category: Tagesberichte
---

![2022-12-15 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert steigt auf 1,08. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 3 Tagen über 120 liegt.](/images/2022/12/20221215.png)

Das RKI meldet eine Inzidenz von 123,9 für München. 319 Neue, 61 Nachmeldungen, davon 55 für Vorgestern. Durch de Nachmeldungen erhöht sich die gestern genannte Inzidenz von 120,2 auf 123,9. Tag 3 über Inzidenz 120.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 7. Damit insgesamt 757 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 61.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 07.12. | 104,3 -> 110,2 |
| 08.12. | 107,5 -> 110,8 |
| 09.12. | 110,8 -> 117,0 |
| 10.12. | 110,8 -> 117,0 |
| 11.12. | 110,8 -> 117,0 |
| 12.12. | 119,6 -> 123,3 |
| 13.12. | 120,2 -> 123,9 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 15.12. | 14.12 | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 146,0 | 123,5 | +18,2% |
| LK Ebersberg | 89,2 | 98,9 | -9,8% |
| LK Freising | 66,2 | 69,0 | -4,1% |
| LK Fürstenfeldbruck | 143,7 | 140,5 | +2,3% |
| LK München | 121,2 | 115,5 | +4,9% |
| LK Starnberg | 124,3 | 118,5 | +4,9% |

Leicht steigende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steigt seit 11 Tagen stetig weiter und hat jetzt 1,08 erreicht - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 07.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,03.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber frühestens am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in der letzten Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 08.12. | 1,02 | --- |
| 09.12. | 1,04 | --- |
| 10.12. | 1,05 | --- |
| 11.12. | 1,05 | --- |
| 12.12. | 1,07 | --- |
| 13.12. | 1,08 | --- |
| 14.12. | 1,08 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter und bleibt in Summe zum 3. Tag in Folge über 300: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 10 von 280 auf 290. IMC sinkt um 1 von gestern 9 auf 8. Intensivbetten steigt um 3 von gestern 22 auf 25. Damit insgesamt 323 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 290 | +10 | 
| IMC | 8 | -1 | 
| Intensiv | 25 | +3 |
| SUMME | 323 | +12 |

Damit insgesamt 59 Betten **mehr** belegt als in der Vorwoche. Jetzt 16 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Heute ist wieder Nachmeldetag bei den Impfzahlen. Änderungen bei der Impfquote, wenn überhaupt, dann nur in den Nachkommastellen sichtbar.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +42 | 73,00% | +90 | 75,20% |
| 2. | +22 | 71,48% | +95 | 75,12% |
| 3. | +126 | 48,35% | +319 | 59,20% |
| 4. | +2.578 | 11,86% | +4.788 | 11,51% |
| 5. | --- | --- | +1.106 | 0,70% |
| 6. | --- | --- | +30 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,35%, Bayern 59,20%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 12.904 Tests/Tag mit einer Positivrate von 12,50%. Gegenüber den Zahlen von letzter Woche (13.714 Tests/Tag mit 11,72% Positivrate) also weiter weniger Tests, gepaart mit leicht steigender Positivrate.

12.904 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 12.904 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt ein wenig auf jetzt 12,50%.](/images/2022/12/20221215Test.png)