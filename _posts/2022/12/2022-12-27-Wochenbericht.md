---
layout: default
title: 2022-12-27 Wochenbericht KW51
category: Wochenberichte
---

![2022-12-27 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert bleibt knapp über 1. Zweite zeigt Krankenbetten, die nach den Feiertagen etwas sinken. Dritte zeigt Inzidenz, die wegen fehlender Meldungen deutlich zu niedrig ist.](/images/2022/12/20221227.png)

In der letzten Woche gab es insgesamt 2.164 neue COVID Fälle in München. Das sind 16,2% mehr als in der Vorwoche (1.863). Leider auch 13 Tote mit COVID Nachweis in den letzten 7 Tagen, höher als in der Vorwoche (8).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe langsam aber stetig von 1.863 auf 2.164. Die um Nachmeldungen korrigierte Inzidenz ging genauso langsam aber stetig von 125,2 auf 145,5.

Eine richtige Welle ist es bei den Inzidenzen also nicht, in den Krankenhäusern aber schon. Eher eine "versteckte Welle".

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 18.12. | 0 | 1.863 | 125,2 |
| 19.12. | 613 | 1.930 | 129,7 |
| 20.12. | 464 | 1.991 | 133,8 |
| 21.12. | 441 | 2.085 | 140,1 |
| 22.12. | 361 | 2.145 | 144,2 |
| 23.12. | 285 | 2.164 | 145,5 |
| 24.12. | 0 | 2.164 | 145,5 |

## Reproduktionswert R(t) für München

Unser R(t) war während der ganzen letzten Woche über 1 und stand gestern bei 1,09.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 21.12.) liegt bei 1,05. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 19.12. | 1,04 | 0,98 |
| 20.12. | 1,04 | 1,01 |
| 21.12. | 1,04 | 1,05 |
| 22.12. | 1,05 | --- |
| 23.12. | 1,07 | --- |
| 24.12. | 1,08 | --- |
| 25.12. | 1,09 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über die Feiertage klar weniger belegte Normalbetten. IMC und Intensiv aber sind gestiegen. Wegen der Feiertage sind auf Normalstation wohl noch Nachmeldungen zu erwarten. An 7 von 7 Tagen war die Summe klar höher als in der Vorwoche und immer über 400 Betten insgesamt.

Im Vorjahr endete um diese Zeit die Herbstwelle - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 19.12. | 371 | 12 | 29 | 412 | +110 | 352 |
| 20.12. | 374 | 9 | 26 | 409 | +98 | 354 |
| 21.12. | 399 | 9 | 29 | 437 | +114 | 334 |
| 22.12. | 412 | 14 | 29 | 455 | +111 | 320 |
| 23.12. | 398 | 16 | 30 | 444 | +94 | 310 |
| 24.12. | 384 | 19 | 30 | 433 | +76 | 301 |
| 25.12. | 370 | 21 | 31 | 422 | +57 | 290 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt eher schlechter aus. In der ganzen letzten Woche waren wir mit den korrigierten Zahlen über dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten steigt die Summe von letzte Woche 138 auf jetzt 173.

Im Vergleich zum Vorjahr deutlich höhere Zahlen. Im letzten Jahr klang um diese Zeit die Herbstwelle ab, die wir dieses Jahr nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 19.12. | 1.654 | 1.171 |
| 20.12. | 1.689 | 1.109 |
| 21.12. | 1.660 | 1.020 |
| 22.12. | 1.613 | 944 |
| 23.12. | 1.503 | 899 |
| 24.12. | 1.463 | 833 |
| 25.12. | 1.409 | 756 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen. Änderungen gegenüber der Vorwoche nur im Bereich der Nachkommastellen.

Das RKI gibt jetzt auch Zahlen für die 5te und 6te Impfung an, die Stadt München fasst die 4., 5. und 6. Impfung zusammen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 73,01% | 75,20% |
| 2. | 71,48% | 75,12% |
| 3. | 48,38% | 59,22% |
| 4. | 12,10% | 11,69% |
| 5. | --- | 0,75% |
| 6. | --- | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/12/KW51Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Laut LGL sind wir jetzt bayernweit bei nur 11.479 Tests/Tag mit einer Positivrate von 13,91%. Gegenüber den Zahlen von letzter Woche (13.150 Tests/Tag mit 12,44% Positivrate) also weiter weniger Tests, gepaart mit leicht steigender Positivrate.

11.479 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Seit 3 Wochen schaffen wir aber nich nichtmal 100.000 Test pro **Woche**.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 11.479 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt ein wenig auf jetzt 13,91%.](/images/2022/12/20221222Test.png)
 
## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter auf jetzt 29.4%, BQ.1 scheint mit 38.1% das Rennen zu machen. Aber BF.7 liegt auch um 20%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

| Variante | KW47 | KW48 | KW49 | KW50 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 1.1% | 0.9% | 0.7% | 0.0% |
| Omikron BA.2.75 | 2.1% | 2.5% | 3.6% | 3.4% |
| Omikron BN.1 | 4.2% | 5.1% | 6.2% | 7.1% |
| Omikron BA.4 | 0.9% | 0.7% | 0.0% | 0.4% |
| Omikron BA.5 | 41.0% | 35.6% | 29.8% | 26.9% |
| Omikron BF.7 | 25.0% | 24.4% | 21.8% | 18.1% |
| Omikron BQ.1 | 23.7% | 28,2% | 32.6% | 41.2% |
| Andere | 2.1% | 2.6% | 5.4% | 2.9% |
| Sequenzierungen | 761 | 680 | 611 | 238 | 

![VoC](/images/2022/12/KW51Var.png)