---
layout: default
title: 2022-12-07 Tagesbericht
category: Tagesberichte
---

![2022-12-07 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht auf genau 1,00. Zweite zeigt Krankenbetten, die heute wieder gestiegen sind. Dritte zeigt Inzidenz, die sich um 100 stabilisiert hat.](/images/2022/12/20221207.png)

Das RKI meldet eine Inzidenz von 99,5 für München. 300 Neue, 82 Nachmeldungen, davon 76 vor vorgestern. Die gestern genannte Inzidenz von 98,4 steigt dadurch auf 100,0. Und auch morgen wird sich die Inzidenz wohl wieder auf knapp über 100 korrigieren. Wir bleiben auf einem Plateau.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 11. Damit insgesamt 749 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 29.11. | 96,1 -> 103,0 |
| 30.11. | 98,5 -> 102,9 |
| 01.12. | 98,0 -> 103,2 |
| 02.12. | 102,8 -> 106,9 |
| 03.12. | 101,6 -> 105,7 |
| 04.12. | 101,6 -> 105,7 |
| 05.12. | 94,8 -> 100,0 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 07.12. | 06.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 111,9 | 124,2 | -9,9% |
| LK Ebersberg | 93,4 | 101,0 | -7,5% |
| LK Freising | 59,1 | 63,5 | -6,9% |
| LK Fürstenfeldbruck | 125,4 | 122,6 | +2,3% |
| LK München | 99,2 | 97,5 | +1,7% |
| LK Starnberg | 100,9 | 99,5 | +1,4% |

Uneinheitlich, aber weiterhin stabil.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 12 Tagen auf, bzw. knapp über 1 und damit an der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,01 | --- |
| 02.12. | 1,00 | --- |
| 03.12. | 1,01 | --- |
| 04.12. | 1,01 | --- |
| 05.12. | 1,01 | --- |
| 05.12. | 1,00 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt wieder: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 12 von 226 auf 238. IMC bleibt auf 9. Intensivbetten steigt um 2 von 13 auf 15. Damit insgesamt 262 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 238 | +12 | 
| IMC | 9 | +/-0 | 
| Intensiv | 15 | +2 |
| SUMME | 262 | +143 |

Damit insgesamt 33 Betten **mehr** belegt als in der Vorwoche. Jetzt 8 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +8 | 73,00% | +63 | 75,20% |
| 2. | +11 | 71,47% | +60 | 75,12% |
| 3. | +132 | 48,34% | +421 | 59,18% |
| 4. | +2.931 | 11,64% | +5.820 | 11,27% |
| 5. | --- | --- | +1.213 | 0,65% |
| 6. | --- | --- | +62 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,18%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.
