---
layout: default
title: 2022-12-25 Tagesbericht
category: Tagesberichte
---

![2022-12-25 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert liegt bei 1,08. Zweite zeigt Krankenbetten, die weiter steigen, aber über die Feiertage kommen nur wenige neue Zahlen. Dritte zeigt Inzidenz, die seit 4 Tagen über 140 liegt, und stetig steigt.](/images/2022/12/20221225.png)

Das RKI meldet, wie vorhergesagt, eine Inzidenz von 145,5 für München. Bis Dienstag werden keine Fallzahlen an das RKI gemeldet bzw. dort ausgewertet. Erst am Mittwoch wieder neue Zahlen vom RKI.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert), Stand 23.12., Aktualisierung erst am Mittwoch, dem 28.12.:

| 16.12. | 122,1 -> 125,2 |
| 17.12. | 122,1 -> 125,2 |
| 18.12. | 122,1 -> 125,2 |
| 19.12. | 122,7 -> 129,7 |
| 20.12. | 128,5 -> 133.8 |
| 21.12. | 136,0 -> 140,1 |
| 22.12. | 138,7 -> 144,2 |

Erst am Dienstag werden wieder Zahlen an das RKI gemeldet, eine "echte" neue Inzidenz somit erst am Mittwoch. Die Inzidenz am Dienstag wird also nur Zahlen für 4 Tage umfassen und deshalb niedrig erscheinen. Aber das täuscht. Bei einem R(t), daß stabil über 1 liegt und langsam wächst, werden die Zahlen steigen. Erst in der 2. Januarwoche werden die Zahlen wieder zuverlässig.

Immerhin wissen wir schon heute welche Inzidenzen in den nächsten Tagen genannt werden:

| Montag, 26.12 | 144,5 |
| Dienstag, 27.12 | 104,3 |


## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Bis Mittwoch, 28.12. keine Aktualisierung möglich.

| Kreis | 24.12. | 23.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 173,0 | 167,3 | +3,4% |
| LK Ebersberg | 90,6 | 99,6 | -9,0% |
| LK Freising | 86,1 | 78,4 | +9,8% |
| LK Fürstenfeldbruck | 145,9 | 143,7 | +1,5% |
| LK München | 170,1 | 180,1 | -5,6% |
| LK Starnberg | 112,6 | 109,7 | +2,6% |

Weiter uneinheitlich.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 28 Tagen über 1, heute steigt der R(t) Wert auf 1,08 - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zum Stand 21.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,05 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 18.12. | 1,05 | 0,99 |
| 19.12. | 1,04 | 0,98 |
| 20.12. | 1,04 | 1,01 |
| 21.12. | 1,04 | 1,05 |
| 22.12. | 1,05 | --- |
| 23.12. | 1,07 | --- |
| 24.12. | 1,08 | --- |

## Krankenhaus

Am Samstag und über die Feiertage gibt es keine neuen Krankenhauszahlen von der Stadt München.

Die einzig neuen Zahlen heute sind die 7 Tage Hospitalisierung in ganz Bayern (1.463, letzte Woche: 1.280) und bayernweit belegte Intensivbetten (147, letzte Woche 141).

Seit nunmehr 21 Tagen liegt die 7 Tage Hospitalisierung in Bayern damit über den alten Grenzwert der gelben Ampel von 1.200.

Der Jahresvergleich zeigt deutlich, wie wir in 2022 zwar keine richtigen Spitzen bei der Intensivbettenbelegung hatten, dafür aber auf Normalstation sehr viel mehr los war.

![Jahresvergleich 2021/2022 Hospitalisierung Bayern mit COVID positiven Patienten](/images/2022/12/20221225KHB.png)

## Impfungen

Am Samstag und über die Feiertage gibt es keine neuen Impfzahlen von der Stadt München.
