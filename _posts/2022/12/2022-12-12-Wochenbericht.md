---
layout: default
title: 2022-12-12 Wochenbericht KW49
category: Wochenberichte
---

![2022-12-12 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher und anders verteilt, der R(t) Wert steht bei 1,03. Zweite zeigt Krankenbetten, die in der letzten Woche deutlich gestiegen sind. Dritte zeigt Inzidenz, die sich von 100 weg nach oben bewegt, aber erst morgen neue Zahlen enthält.](/images/2022/12/20221212.png)

In der letzten Woche gab es insgesamt 1.648 neue COVID Fälle in München. Das sind 4,6% mehr als in der Vorwoche (1.575). Leider aber auch 10 Tote mit COVID Nachweis in den letzten 7 Tagen, ähnlich wie in der Vorwoche (9).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe langsam aber stetig auf 1.648. Die um Nachmeldungen korrigierte Inzidenz ging genauso langsam aber stetig auf 110,8.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 04.12. | 0 | 1.575 | 105,9 |
| 05.12. | 445 | 1.498 | 100,7 |
| 06.12. | 393 | 1.585 | 106,5 |
| 07.12. | 318 | 1.633 | 109,8 |
| 08.12. | 285 | 1.638 | 110,1 |
| 09.12. | 207 | 1.648 | 110,8 |
| 10.12. | 0 | 1.648 | 110,8 |

## Reproduktionswert R(t) für München

Unser R(t) war während der ganzen letzten Woche über 1 und steht heute bei 1,03.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 07.12.) liegt bei 1,03. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.12. | 1,01 | 0,95 |
| 06.12. | 1,01 | 0,98 |
| 07.12. | 1,02 | 1,03 |
| 08.12. | 1,02 | --- |
| 09.12. | 1,03 | --- |
| 10.12. | 1,03 | --- |
| 11.12. | 1,03 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende etwas mehr Normalbetten. IMC und Intensiv etwas niedriger. An 7 von 7 Tagen war die Summe klar höher als in der Vorwoche und immer über 200, jetzt fast 300 Betten insgesamt.

Im Vorjahr schlug um diese Zeit die Herbstwelle zu - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 05.12. | 226 | 9 | 13 | 248 | +29 | 477 |
| 06.12. | 238 | 9 | 15 | 262 | +33 | 467 |
| 07.12. | 241 | 9 | 14 | 264 | +31 | 461 |
| 08.12. | 264 | 11 | 20 | 295 | +50 | 454 |
| 09.12. | 265 | 10 | 19 | 294 | +48 | 448 |
| 10.12. | 267 | 9 | 18 | 294 | +46 | 446 |
| 11.12. | 268 | 9 | 17 | 294 | +43 | 445 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt etwas besser aus. Seit dem 08.11. sind wir auch mit korrigierten Zahlen (knapp) unter dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten steigt die Summe von letzte Woche 125 auf jetzt 129.

Im Vergleich zum Vorjahr deutlich bessere Zahlen. Im letzten Jahr klang um diese Zeit die Herbstwelle ab, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 05.11. | 1.180 | 2.208 |
| 06.11. | 1.176 | 2.119 |
| 07.11. | 1.142 | 2.071 |
| 08.12. | 1.069 | 1.972 |
| 09.12. | 1.002 | 1.887 |
| 10.12. | 965 | 1.785 |
| 11.12. | 928 | 1.743 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Das RKI gibt jetzt auch Zahlen für die 5te und 6te Impfung an, die Stadt München fasst die 4., 5. und 6. Impfung zusammen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 73,00% | 75,20% |
| 2. | 71,47% | 75,12% |
| 3. | 48,34% | 59,19% |
| 4. | 11,67% | 11,41% |
| 5. | --- | 0,68% |
| 6. | --- | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/12/KW49Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 13.315 Tests/Tag mit einer Positivrate von 11,22%. Gegenüber den Zahlen von letzter Woche (14.468 Tests/Tag mit 12,79% Positivrate) also weiter weniger Tests, gepaart mit sinkender Positivrate.

13.315 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.315 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt ein wenig auf jetzt 11,22%.](/images/2022/12/20221208Test.png)
 
## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter, aber es setzt sich noch keine neue Variante deutlich durch. BN.1, BQ.1, und  BF.7 machen zusammen jetzt um 60% aus. Wobei in der KW49 bisher nur 28 Proben ausgewertet wurden.

| Variante | KW 46 | KW47 | KW48 | KW49 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 1.6% | 0.9% | 0.4% | 0.0% |
| Omikron BA.2.75 | 2.1% | 2.2% | 2.7% | 3.6% |
| Omikron BN.1 | 2.8% | 4,3% | 5.6% | 7.1% |
| Omikron BA.4 | 1.6% | 1.0% | 0.6% | 0.0% |
| Omikron BA.5 | 45.3% | 41.4% | 35.1% | 32.1% |
| Omikron BF.7 | 24.4% | 24.6% | 27.4% | 28.6% |
| Omikron BQ.1 | 20.7% | 23.1% | 26.8% | 21.4% |
| Andere | 1.2% | 2.4% | 1.3% | 7.1% |
| Sequenzierungen | 426 | 667 | 478 | 28 | 

![VoC](/images/2022/12/KW49Var.png)