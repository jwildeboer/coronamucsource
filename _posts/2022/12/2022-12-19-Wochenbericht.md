---
layout: default
title: 2022-12-19 Wochenbericht KW50
category: Wochenberichte
---

![2022-12-19 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert sinkt ein wenig auf 1,04. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 7 Tagen über 120 liegt.](/images/2022/12/20221219.png)

In der letzten Woche gab es insgesamt 1.648 neue COVID Fälle in München. Das sind 4,6% mehr als in der Vorwoche (1.575). Leider aber auch 10 Tote mit COVID Nachweis in den letzten 7 Tagen, ähnlich wie in der Vorwoche (9).
<!--more-->

## Fallzahlen

In der letzten Woche stieg die 7 Tages Summe langsam aber stetig von 1.742 auf 1.816. Die um Nachmeldungen korrigierte Inzidenz ging genauso langsam aber stetig von 117,1 auf 122,1.

Eine richtige Welle ist es bei den Inzidenzen also nicht, in den Krankenhäusern aber schon. Es ist eine "versteckte Welle".

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 11.12. | 0 | 1.742 | 117,1 |
| 12.12. | 544 | 1.835 | 123,3 |
| 13.12. | 402 | 1.844 | 123,9 |
| 14.12. | 347 | 1.871 | 125,8 |
| 15.12. | 295 | 1.877 | 126,2 |
| 16.12. | 228 | 1.816 | 122,1 |
| 17.12. | 0 | 1.816 | 122,1 |

## Reproduktionswert R(t) für München

Unser R(t) war während der ganzen letzten Woche über 1 und steht heute bei 1,04.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 14.12.) liegt bei 1,10. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen natürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 12.12. | 1,07 | 1,03 |
| 13.12. | 1,08 | 1,05 |
| 14.12. | 1,09 | 1,10 |
| 15.12. | 1,09 | --- |
| 16.12. | 1,07 | --- |
| 17.12. | 1,05 | --- |
| 18.12. | 1,05 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende klar mehr Normalbetten. IMC und Intensiv nahezu unverändert. An 7 von 7 Tagen war die Summe klar höher als in der Vorwoche und immer über 300 Betten insgesamt.

Im Vorjahr endete um diese Zeit die Herbstwelle - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 12.12. | 276 | 6 | 20 | 302 | +54 | 443 |
| 13.12. | 280 | 9 | 22 | 311 | +49 | 425 |
| 14.12. | 290 | 8 | 25 | 323 | +59 | 413 |
| 15.12. | 308 | 8 | 28 | 344 | +49 | 399 |
| 16.12. | 314 | 8 | 28 | 350 | +56 | 379 |
| 17.12. | 320 | 9 | 28 | 357 | +63 | 370 |
| 18.12. | 326 | 10 | 29 | 365 | +71 | 365 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt eher schlechter aus. In der ganzen letzten Woche waren wir mit den korrigierten Zahlen über dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten steigt die Summe von letzte Woche 129 auf jetzt 138.

Im Vergleich zum Vorjahr nahezu identische Zahlen. Im letzten Jahr klang um diese Zeit die Herbstwelle ab, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 12.12. | 1.263 | 1.686 |
| 13.12. | 1.310 | 1.629 |
| 14.12. | 1.353 | 1.528 |
| 15.12. | 1.399 | 1.422 |
| 16.12. | 1.315 | 1.335 |
| 17.12. | 1.280 | 1.279 |
| 18.12. | 1.248 | 1.226 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen. Änderungen gegenüber der Vorwoche nur im Bereich der Nachkommastellen.

Das RKI gibt jetzt auch Zahlen für die 5te und 6te Impfung an, die Stadt München fasst die 4., 5. und 6. Impfung zusammen.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 73,00% | 75,20% |
| 2. | 71,48% | 75,12% |
| 3. | 48,36% | 59,21% |
| 4. | 11,88% | 11,59% |
| 5. | --- | 0,72% |
| 6. | --- | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/12/KW50Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 12.904 Tests/Tag mit einer Positivrate von 12,50%. Gegenüber den Zahlen von letzter Woche (13.714 Tests/Tag mit 11,72% Positivrate) also weiter weniger Tests, gepaart mit leicht steigender Positivrate.

12.904 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 12.904 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt ein wenig auf jetzt 12,50%.](/images/2022/12/20221215Test.png)
 
## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter auf jetzt 29.4%, BQ.1 scheint mit 38.1% das Rennen zu machen. Aber BF.7 liegt auch um 20%. Alles BA.5 Abkömmlinge, daher scheint die Boosterimpfung mit dem bivalentem Impfstoff gegen BA.4/5 weiterhin eine sehr gute Empfehlung zu bleiben.

| Variante | KW47 | KW48 | KW49 | KW50 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 1.1% | 0.9% | 0.8% | 0.0% |
| Omikron BA.2.75 | 2.1% | 2.6% | 3.2% | 3.1% |
| Omikron BN.1 | 4.2% | 5.3% | 5.6% | 7.7% |
| Omikron BA.4 | 0.9% | 0.5% | 0.0% | 0.5% |
| Omikron BA.5 | 41.0% | 35.4% | 30.4% | 29.4% |
| Omikron BF.7 | 25.0% | 24.9% | 21.6% | 18.6% |
| Omikron BQ.1 | 23.7% | 27.9% | 33.2% | 38.1% |
| Andere | 2.1% | 2.6% | 5.3% | 2.6% |
| Sequenzierungen | 761 | 666 | 533 | 194 | 

![VoC](/images/2022/12/KW50Var.png)