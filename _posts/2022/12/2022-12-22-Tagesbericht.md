---
layout: default
title: 2022-12-22 Tagesbericht
category: Tagesberichte
---

![2022-12-22 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher, der R(t) Wert liegt bei 1,04. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 9 Tagen über 120 liegt, und langsam aber stetig steigt.](/images/2022/12/20221222.png)

Das RKI meldet eine Inzidenz von 136,0 für München. 384 Neue, 78 Nachmeldungen, davon 70 für vorgestern. Durch die Nachmeldungen erhöht sich die gestern genannte Inzidenz von 128,5 auf 133,5. Tag 2 mit Inzidenz über 130.
<!--more-->

Es wurden 3 neue Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 11. Damit insgesamt 768 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 20.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 14.12. | 123,9 -> 125,9 |
| 15.12. | 122,9 -> 126,6 |
| 16.12. | 122,1 -> 125,1 |
| 17.12. | 122,1 -> 125,1 |
| 18.12. | 122,1 -> 125,1 |
| 19.12. | 122,7 -> 129,5 |
| 20.12. | 128,5 -> 133.5 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 22.12. | 21.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 128,7 | 147,3 | -12,6% |
| LK Ebersberg | 98,2 | 97,5 | +0,7% |
| LK Freising | 74,0 | 77,8 | -4,9% |
| LK Fürstenfeldbruck | 146,9 | 135,4 | +8,5% |
| LK München | 166,1 | 172,1 | -3,5% |
| LK Starnberg | 116,3 | 127,2 | -8,6% |

Insgesamt leicht sinkende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 27 Tagen über 1, heute ist der R(t) Wert 1,04 - damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde heute zum Stand 21.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,05 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 15.12. | 1,09 | 1,15 |
| 16.12. | 1,07 | 1,11 |
| 17.12. | 1,06 | 1,04 |
| 18.12. | 1,05 | 0,99 |
| 19.12. | 1,04 | 0,98 |
| 20.12. | 1,04 | 1,01 |
| 21.12. | 1,04 | 1,05 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute wieder weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 25 von 374 auf 399. IMC unverändert auf 9. Intensivbetten steigt um 3 von gestern 26 auf jetzt 29. Damit insgesamt 437 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 399 | +25 | 
| IMC | 9 | +/-0 | 
| Intensiv | 29 | +3 |
| SUMME | 437 | +28 |

Damit insgesamt 114 Betten **mehr** belegt als in der Vorwoche. Jetzt 23 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Bei den Impfzahlen tut sich wenig. Leider.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +6 | 73,01% | +46 | 75,20% |
| 2. | +8 | 71,48% | +103 | 75,12% |
| 3. | +16 | 48,37% | +245 | 59,21% |
| 4. | +152 | 12,08% | +2.974 | 11,65% |
| 5. | --- | --- | +726 | 0,74% |
| 6. | --- | --- | +84 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,37%, Bayern 59,21%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 11.479 Tests/Tag mit einer Positivrate von 13,91%. Gegenüber den Zahlen von letzter Woche (13.150 Tests/Tag mit 12,44% Positivrate) also weiter weniger Tests, gepaart mit leicht steigender Positivrate.

11.479 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern. Seit 3 Wochen schaffen wir aber nich nichtmal 100.000 Test pro **Woche**.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 11.479 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt ein wenig auf jetzt 13,91%.](/images/2022/12/20221222Test.png)