---
layout: default
title: 2022-12-11 Tagesbericht
category: Tagesberichte
---

![2022-12-11 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen etwas höher und anders verteilt, der R(t) Wert steht bei 1,03. Zweite zeigt Krankenbetten, die in der letzten Woche deutlich gestiegen sind, am Wochenende aber nicht aktualisiert werden. Dritte zeigt Inzidenz, die sich von 100 weg nach oben bewegt.](/images/2022/12/20221211.png)

Das RKI meldet, wie vorausberechnet, eine Inzidenz von 110,8 für München. Sonntag, daher keine neuen Fallzahlen oder Nachmeldungen der Stadt. Keine Krankenhaus- oder Impfzahlen.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert, Stand 2022-11-10, nächste Aktualisierung erst am Dienstag wenn es neue Zahlen vom RKI gibt):

| 02.12. | 102,8 -> 107,1 |
| 03.12. | 101,6 -> 105,9 |
| 04.12. | 101,6 -> 105,9 |
| 05.12. | 94,8 -> 100,7 |
| 06.12. | 99,5 -> 106,5 |
| 07.12. | 104,3 -> 109,8 |
| 08.12. | 107,5 -> 110,1 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.
Stand 2022-11-10, nächste Aktualisierung erst am Dienstag wenn es neue Zahlen vom RKI gibt.

| Kreis | 10.12. | 09.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 117,1 | 123,5 | -5,2% |
| LK Ebersberg | 100,3 | 98,2 | +2,1% |
| LK Freising | 64,0 | 63,5 | +0,8% |
| LK Fürstenfeldbruck | 124,9 | 125,8 | -0,7% |
| LK München | 122,1 | 120,3 | +1,5% |
| LK Starnberg | 106,8 | 114,8 | -7,0% |

Uneinheitliches Bild.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 17 Tagen auf, bzw. knapp über 1 und damit an der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt gestern zum Stand 07.12. aktualisiert, der letzte Nowcast Lauf lieferte auch ein R(t) von 1,03.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in dieser Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 05.12. | 1,01 | 0,95 |
| 05.12. | 1,01 | 0,98 |
| 06.12. | 1,01 | 1,03 |
| 07.12. | 1,02 | --- |
| 08.12. | 1,02 | --- |
| 09.12. | 1,03 | --- |
| 10.12. | 1,03 | --- |

## Krankenhaus

Am Samstag gibt es keine Aktualisierung der Krankenhauszahlen von der Stadt München.

Bayernweit gibt es immerhin noch die 7 Tage Hospitalisierung, die heute mit 965 weiterhin hoch bleibt. Insgesamt 114 Intensivbetten bayernweit mit COVID-positiven Patienten belegt, davon 45 invasiv beatmet.

## Impfungen

Am Sonntag gibt es keine Aktualisierung der Impfzahlen. Erst am Montag wieder neue Zahlen von der Stadt und LGL.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,00% | +120 | 75,20% |
| 2. | --- | 71,47% | +142 | 75,12% |
| 3. | --- | 48,34% | +353 | 59,19% |
| 4. | --- | 11,66% | +4.869 | 11,40% |
| 5. | --- | --- | +972 | 0,68% |
| 6. | --- | --- | +29 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,19%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.