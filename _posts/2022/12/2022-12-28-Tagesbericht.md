---
layout: default
title: 2022-12-28 Tagesbericht
category: Tagesberichte
---

![2022-12-28 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, ein Vergleich mit der Vorwoche ist noch nicht möglich, der R(t) Wert sinkt auf 1,02. Zweite zeigt Krankenbetten, die über die Feiertage gesunken sind. Dritte zeigt Inzidenz, die zwar sinkt, aber wgen Feiertage verzerrt ist.](/images/2022/12/20221228.png)

Das RKI meldet eine Inzidenz von 121,4 für München. 648 Neue, 73 Nachmeldungen, davon 64 für letzten Freitag. Die gestern genannte Inzidenz von 104,3 erhöht sich durch Nachmeldungen auf 109,0. Die Inzidenz der Feiertage steigt von 145,5 auf 150,3.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 20.12. | 128,5 -> 133.9 |
| 21.12. | 136,0 -> 140,4 |
| 22.12. | 138,7 -> 144,7 |
| 23.12. | 145,5 -> 150,3 |
| 24.12. | 145,5 -> 150,3 |
| 25.12. | 145,5 -> 150,3 |
| 26.12. | 104,3 -> 109,0 |

Die Inzidenz heute umfasst nur Zahlen für 4 Tage. Bei einem R(t), daß stabil über 1 liegt, werden die Zahlen steigen. Erst in der 2. Januarwoche werden die Zahlen wieder wirklich zuverlässig - sofern es dann noch aktuelle Zahlen geben wird.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Samstag** in %.

| Kreis | 28.12. | 24.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 158,3 | 173,0 | -8,5% |
| LK Ebersberg | 70,6 | 90,6 | -22,1% |
| LK Freising | 80,6 | 86,1 | -6,4% |
| LK Fürstenfeldbruck | 137,7 | 145,9 | -5,6% |
| LK München | 120,1 | 170,1 | -29,4% |
| LK Starnberg | 91,4 | 112,6 | -18,8% |

Feiertagsknick, allerorten.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 31 Tagen über 1, heute steht der R(t) Wert auf 1,02 - damit stabil im unteren exponenziellem Bereich und heute wegen Feiertage etwas unterbewertet.

Das R(t) vom StaBLab der LMU wurde zum Stand 21.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,05 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 21.12. | 1,04 | 1,05 |
| 22.12. | 1,06 | --- |
| 23.12. | 1,08 | --- |
| 24.12. | 1,09 | --- |
| 25.12. | 1,10 | --- |
| 26.12. | 1,06 | --- |
| 27.12. | 1,02 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten sinkt heute weiter: Gegenüber gestern sinkt die Zahl der belegten Normalbetten um 8 von 354 auf 346. IMC sinkt um 4 von gestern 23 auf jetzt 19. Intensivbetten sinkt um 2 von gestern 32 auf jetzt 30. Damit insgesamt 395 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 346 | -8 | 
| IMC | 19 | -4 | 
| Intensiv | 30 | -2 |
| SUMME | 395 | -14 |

Damit insgesamt 14 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Bei den Impfzahlen tut sich wenig. Leider. Und noch hat die Stadt die Impfzahlen nicht aktualisiert, immer noch Stand 23.12.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,01% | +33 | 75,20% |
| 2. | --- | 71,48% | +40 | 75,13% |
| 3. | --- | 48,38% | +173 | 59,22% |
| 4. | --- | 12,10% | +1.628 | 11,71% |
| 5. | --- | --- | +341 | 0,75% |
| 6. | --- | --- | +10 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.