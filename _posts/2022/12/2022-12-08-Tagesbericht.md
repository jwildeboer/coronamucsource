---
layout: default
title: 2022-12-08 Tagesbericht
category: Tagesberichte
---

![2022-12-08 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich, der R(t) Wert steht minimal über 1,00. Zweite zeigt Krankenbetten, die heute auch wieder ein wenig gestiegen sind. Dritte zeigt Inzidenz, die sich um 100 stabilisiert hat.](/images/2022/12/20221208.png)

Das RKI meldet eine Inzidenz von 104,3 für München. 240 Neue, 106 Nachmeldungen, davon 93 für vorgestern. Die gestern genannte Inzidenz von 99,5 steigt dadurch auf 106,3. Wir bleiben auf einem Plateau, aber die Zahlen tendieren ein wenig nach oben.
<!--more-->

Es wurde 1 neuer Todesfall gemeldet. Die 7 Tage Summe der Todesfälle ist 12. Damit insgesamt 750 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 19.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 30.11. | 98,5 -> 102,9 |
| 01.12. | 98,0 -> 103,3 |
| 02.12. | 102,8 -> 107,1 |
| 03.12. | 101,6 -> 105,9 |
| 04.12. | 101,6 -> 105,9 |
| 05.12. | 94,8 -> 100,6 |
| 06.12. | 99,5 -> 106,3 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %.

| Kreis | 08.12. | 07.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 107,4 | 111,9 | -4,0% |
| LK Ebersberg | 103,8 | 93,4 | +11,1% |
| LK Freising | 68,5 | 59,1 | +15,9% |
| LK Fürstenfeldbruck | 127,6 | 125,4 | +1,8% |
| LK München | 118,9 | 99,2 | +19,9% |
| LK Starnberg | 106,0 | 100,9 | +5,1% |

Leicht steigende Tendenz, bis auf Dachau.

## Reproduktionswert R(t) für München

Unser R(t) steht seit 12 Tagen auf, bzw. knapp über 1 und damit an der Grenze zum exponenziellem Wachstum.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 30.11. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,04.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber erst am Donnerstag nachmittag veröffentlicht. 

| Datum | R(t) | LMU |
|---|---:|---:|
| 02.12. | 1,01 | --- |
| 03.12. | 1,01 | --- |
| 04.12. | 1,01 | --- |
| 05.12. | 1,01 | --- |
| 05.12. | 1,01 | --- |
| 06.12. | 1,01 | --- |
| 07.12. | 1,01 | --- |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt weiter: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 3 von 238 auf 241. IMC bleibt auf 9. Intensivbetten sinkt um 1 von 15 auf 14. Damit insgesamt 264 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 241 | +3 | 
| IMC | 9 | +/-0 | 
| Intensiv | 14 | -1 |
| SUMME | 264 | +2 |

Damit insgesamt 31 Betten **mehr** belegt als in der Vorwoche. Jetzt 9 Tage in Folge mehr Betten belegt als in der Vorwoche.

## Impfungen

Erst-, Zweit- und Drittimpfungen sind weiterhin kaum vorhanden. Wir haben die Impfzahlen in eine Tabelle umgewandelt, mit dan Zahlen für ganz Bayern zum Vergleich.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +1 | 73,00% | +38 | 75,20% |
| 2. | +7 | 71,47% | +64 | 75,12% |
| 3. | +17 | 48,34% | +372 | 59,19% |
| 4. | +177 | 11,66% | +5.499 | 11,31% |
| 5. | --- | --- | +1.331 | 0,66% |
| 6. | --- | --- | +75 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,34%, Bayern 59,19%.

Die Stadt München scheint die Viert- und Fünftimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.

## Testzahlen

Heute gab es auch das wöchentliche Update der Testzahlen. Laut LGL sind wir jetzt bayernweit bei nur 13.315 Tests/Tag mit einer Positivrate von 11,22%. Gegenüber den Zahlen von letzter Woche (14.468 Tests/Tag mit 12,79% Positivrate) also weiter weniger Tests, gepaart mit sinkender Positivrate.

13.315 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.315 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate sinkt ein wenig auf jetzt 11,22%.](/images/2022/12/20221208Test.png)