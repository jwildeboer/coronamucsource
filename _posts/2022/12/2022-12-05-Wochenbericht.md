---
layout: default
title: 2022-12-05 Wochenbericht KW48
category: Wochenberichte
---

![2022-12-05 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen ähnlich. Zweite zeigt Krankenbetten, die insgesamt weiter steigen. Dritte zeigt Inzidenz, die um 100 verharrt.](/images/2022/12/20221205.png)

In der letzten Woche gab es insgesamt 1.511 neue COVID Fälle in München. Das sind 0,9% weniger als in der Vorwoche (1.525). Leider aber auch 9 Tote mit COVID Nachweis in den letzten 7 Tagen, ähnlich wie in der Vorwoche (8).
<!--more-->

## Fallzahlen

In der letzten Woche blieb die 7 Tages Summe mehr oder weniger konstant um 1530. Die Inzidenz schwankte nur wenig und blieb um 102.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|---:|---:|---:|
| 27.11. | 0 | 1.525 | 102,5 |
| 28.11. | 521 | 1.550 | 104,2 |
| 29.11. | 306 | 1.531 | 102,0 |
| 30.11. | 270 | 1.528 | 102,7 |
| 01.12. | 269 | 1.523 | 102,4 |
| 02.12. | 145 | 1.529 | 102,8 |
| 03.12. | 0 | 1.511 | 101,6 |

## Reproduktionswert R(t) für München

Unser R(t) sank in der letzten Woche von 1,05 auf jetzt 0,99 und ist damit seit 2 Tagen sehr knapp unter 1.

Das R(t) vom StaBLab der LMU (letzter Nowcast-Lauf war für den 30.11.) liegt bei 1,04. Das StaBLab der LMU veröffentlicht nur noch am Mittwoch die R(t) Zahlen. Wir berechnen netürlich trotzdem täglich unser R(t) weiter.

| Datum | R(t) | LMU |
|---|---:|---:|
| 28.11. | 1,05 | 0,98 |
| 29.11. | 1,04 | 1,01 |
| 30.11. | 1,03 | 1,04 |
| 01.12. | 1,01 | --- |
| 02.12. | 1,00 | --- |
| 03.12. | 0,99 | --- |
| 04.12. | 0,99 | --- |

## Krankenhauszahlen München

Bei den Krankenhauszahlen in München gab es über das Wochenende etwas mehr Normalbetten. IMC und Intensiv nahezu gleich. An 6 von 7 Tagen war die Summe klar höher als in der Vorwoche.

Im Vorjahr schlug um diese Zeit die Herbstwelle zu - diese Welle scheint sich dieses Jahr nicht zu wiederholen.

| Datum | Normal | IMC | ICU | Summe | Differenz<br>Vorwoche | Summe<br>Vorjahr |
|---|--:|--:|--:|--:|--:|--:|
| 28.11. | 194 | 8 | 17 | 219 | -3 | 474 |
| 29.11. | 203 | 10 | 16 | 229 | +17 | 474 |
| 30.11. | 206 | 10 | 17 | 233 | +26 | 456 |
| 01.12. | 221 | 10 | 14 | 245 | +35 | 454 |
| 02.12. | 223 | 9 | 14 | 246 | +37 | 460 |
| 03.12. | 225 | 9 | 14 | 248 | +40 | 466 |
| 04.12. | 228 | 8 | 15 | 251 | +43 | 472 |

Was man bei den Krankenhauszahlen immer berücksichtigen sollte - München bietet Maximalversorgung. Daher ist es ziemlich normal, daß im Verlauf einer Welle, und immer auch zum Ende der Welle, die wirklich schweren Fälle aus dem (weiten) Umland nach München verlegt werden. Eine vergleichsweise hohe Belegung bei den Intensivbetten ist daher nichts besonderes. Das ist (leider) zu erwarten.

## Krankenhauszahlen Bayern

Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die bayernweite 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert. Mittlerweile haben wir da direkt beim RKI die fehlenden Daten gefunden.

Und dort schaut es insgesamt etwas besser aus. Seit dem 08.11. sind wir auch mit korrigierten Zahlen wieder unter dem alten Grenzwert der gelben Ampel von 1.200 belegten Betten bayernweit.

Bei den Intensivbetten steigt die Summe von letzte Woche 120 auf jetzt 125.

Im Vergleich zum Vorjahr deutlich bessere Zahlen. Im letzten Jahr lief um diese Zeit die große Herbstwelle, die wir dieses Jahr (noch) nicht sehen.

| Datum | 7 Tage<br>Hosp | 2021<br>z. Vgl |
|---|---:|---:|
| 28.11. | 1.110 | 2.569 |
| 29.11. | 1.080 | 2.517 |
| 30.11. | 1.068 | 2.417 |
| 01.12. | 994 | 2.371 |
| 02.12. | 915 | 2.320 |
| 03.12. | 880 | 2.282 |
| 04.12. | 861 | 2.232 |

## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind weiter klar unter den Zahlen für ganz Bayern. Kaum Erst-, Zweit- oder Drittimpfungen, ein wenig mehr vierte Impfungen.

Das RKI gibt jetzt auch Zahlen für die 5te Impfung an, die Stadt München nch nicht.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 73,00% | 75,20% |
| 2. | 71,47% | 75,12% |
| 3. | 48,33% | 59,18% |
| 4. | 11,43% | 11,20% |
| 5. | --- | 0,64% |
| 6. | --- | 0,03% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/2022/12/KW48Impf.png)

Seit dem 1. Oktober gilt man übrigens [nur mit dritter/Booster-Impfung](https://www.bundesgesundheitsministerium.de/coronavirus/faq-covid-19-impfung.html) als vollständig geimpft. Das heisst, wir haben in München weiterhin eine Impfquote unter 50%.

![FAQ des gesundheitsministeriums](/images/2022/08/KW30FAQImpf.png)

## Testzahlen

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Laut LGL sind wir jetzt bayernweit bei nur 13.640 Tests/Tag mit einer Positivrate von 13,18%. Gegenüber den Zahlen von letzter Woche (14.631 Tests/Tag mit 12,05% Positivrate) also wieder weniger Tests, gepaart mit minimal steigender Positivrate.

13.640 Tests/Tag ist weniger als 15% der maximal vorhandenen Kapazität. Das bisherige Maximum liegt bei 90.000 Tests/Tag und es gäbe genug Kapazitäten für über 100.000 PCR-Tests/Tag in Bayern.

![Grafik mit durchgeführten PCR Tests pro Tag in Bayern seit Juni 2020. Die Zahl der Tests pro Tag ist mit ca. 13.640 sehr niedrig und weit vom Maximum von ca. 100.000 Tests/Tag entfernt. Die Positivrate steigt minimal auf 13,18%.](/images/2022/12/20221201Test.png)


## Varianten

Die LMU betreibt ein [Projekt](https://www.bay-voc.lmu.de/news.xhtml) zur Variantenbeobachtung in Bayern. Der BA.5 Anteil sinkt weiter, aber es setzt sich noch keine neue Variante deutlich durch. BN.1, BQ.1, und  BF.7 machen zusammen jetzt mehr als 60% aus. Wobei in der KW48 bisher nur 93 Proben ausgewertet wurden.

| Variante | KW 45 | KW 46 | KW47 | KW48 |
|:--|--:|--:|--:|--:|
| Omikron BA.2 | 1.9% | 1.6% | 0.9% | 0.0% |
| Omikron BA.2.75 | 0.2% | 1.1% | 2.0% | 3.2% |
| Omikron BN.1 | 2.6% | 1.1% | 3.2% | 11.8% |
| Omikron BA.4 | 1.2% | 1.9% | 1.2% | 0.0% |
| Omikron BA.5 | 48.7% | 45.3% | 42.9% | 34.4% |
| Omikron BF.7 | 23.0% | 25.5% | 25.1% | 30.1% |
| Omikron BQ.1 | 19.2% | 20.9% | 21.9% | 20.4% |
| Andere | 3.2% | 1.4% | 2.8% | 0.0% |
| Sequenzierungen | 569 | 369 | 562 | 93 | 

![VoC](/images/2022/12/KW48Var.png)