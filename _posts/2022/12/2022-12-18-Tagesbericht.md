---
layout: default
title: 2022-12-18 Tagesbericht
category: Tagesberichte
---

![2022-12-18 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, im Vergleich zur Vorwoche sind die Zahlen höher, der R(t) Wert sinkt ein wenig auf 1,05. Zweite zeigt Krankenbetten, die weiter steigen. Dritte zeigt Inzidenz, die seit 6 Tagen über 120 liegt.](/images/2022/12/20221218.png)

Das RKI meldet eine Inzidenz von 122,1 für München. Sonntag, daher keine neuen Fallzahlen vom RKI, keine neuen Krankenhaus- oder Impfzahlen von der Stadt München. Tag 6 über Inzidenz 120.
<!--more-->

Es wurden keine neuen Todesfälle gemeldet. Die 7 Tage Summe der Todesfälle ist 8. Damit insgesamt 763 COVID-Tote bis jetzt in 2022, in 2020 waren es 579, 2021 sogar 1.050. Letztes Jahr um diese Zeit war die 7-Tage-Summe der Todesfälle 50.

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert) bleibt bis Dienstag unverändert:

| 09.12. | 110,8 -> 117,1 |
| 10.12. | 110,8 -> 117,1 |
| 11.12. | 110,8 -> 117,1 |
| 12.12. | 119,6 -> 123,3 |
| 13.12. | 120,2 -> 123,9 |
| 14.12. | 123,9 -> 125,8 |
| 15.12. | 122,9 -> 126,2 |

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum Vortag in %. Bis Dienstag keine neuen Zahlen.

| Kreis | 16.12. | 15.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 137,7 | 146,0 | -5,7% |
| LK Ebersberg | 87,2 | 89,2 | -2,2% |
| LK Freising | 74,5 | 66,2 | +12,5% |
| LK Fürstenfeldbruck | 148,7 | 143,7 | +3,5% |
| LK München | 130,9 | 121,2 | +8,0% |
| LK Starnberg | 128,0 | 124,3 | +3,0% |

Leicht steigende Tendenz.

## Reproduktionswert R(t) für München

Unser R(t) sinkt nach 11 Tagen Steigung heute minimal auf 1,05 (Wochenendeffekt) - und bleibt damit stabil im unteren exponenziellem Bereich.

Das R(t) vom StaBLab der LMU wurde zuletzt zum Stand 14.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 1,10.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch, anscheinend werden die neuen Zahlen aber frühestens am Donnerstag nachmittag veröffentlicht. Oder auch erst am Samstag, wie in dieser und der letzten Woche.

| Datum | R(t) | LMU |
|---|---:|---:|
| 10.12. | 1,05 | 1,05 |
| 11.12. | 1,05 | 1,03 |
| 12.12. | 1,07 | 1,03 |
| 13.12. | 1,08 | 1,06 |
| 14.12. | 1,08 | 1,10 |
| 15.12. | 1,09 | --- |
| 16.12. | 1,07 | --- |

## Krankenhaus

Am Sonntag meldet die Stadt München keine neuen Krankenhauszahlen.

## Impfungen

Am Sonntag melden weder Stadt München noch RKI neue Impfzahlen. Morgen erst wieder neue Zahlen.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | --- | 73,00% | +51 | 75,20% |
| 2. | --- | 71,48% | +86 | 75,12% |
| 3. | --- | 48,36% | +438 | 59,21% |
| 4. | --- | 11,88% | +4.355 | 11,58% |
| 5. | --- | --- | +881 | 0,72% |
| 6. | --- | --- | +30 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,36%, Bayern 59,20%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.