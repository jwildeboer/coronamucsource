---
layout: default
title: 2022-12-30 Tagesbericht
category: Tagesberichte
---

![2022-12-30 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, etwas niedriger als in der Vorwoche, der R(t) Wert sinkt auf 0,93. Zweite zeigt Krankenbetten, die über die Feiertage gesunken sind, heute leicht steigen. Dritte zeigt Inzidenz, die zwar sinkt, aber wegen Feiertage verzerrt ist.](/images/2022/12/20221230.png)

Das RKI meldet eine Inzidenz von 116,8 für München. 244 Neue, 51 Nachmeldungen, davon 35 für vorgestern Die gestern genannte Inzidenz von 121,7 erhöht sich durch Nachmeldungen auf 125,1. Tag 2 mit R(t) unter 1.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 22.12. | 138,7 -> 144,9 |
| 23.12. | 145,5 -> 150,5 |
| 24.12. | 145,5 -> 150,5 |
| 25.12. | 145,5 -> 150,5 |
| 26.12. | 104,3 -> 109,2 |
| 27.12. | 121,4 -> 130,7 |
| 28.12. | 121,7 -> 125,1 |

 Erst in der 2. Januarwoche werden die Zahlen wieder wirklich zuverlässig - sofern es dann noch aktuelle Zahlen geben wird.

## Umland

Inzidenz im Umland von München. Jeweils mit Änderung zum letzten **Mittwoch** in %.

| Kreis | 30.12. | 28.12. | Änderung |
|:---|---:|---:|---:|
| LK Dachau | 119,0 | 158,3 | -24,8% |
| LK Ebersberg | 100,3 | 70,6 | +42,1% |
| LK Freising | 81,7 | 80,6 | +1,4% |
| LK Fürstenfeldbruck | 142,3 | 137,7 | +3,3% |
| LK München | 123,2 | 120,1 | +2,6% |
| LK Starnberg | 95,8 | 91,4 | +4,8% |

In Ebersberg ungewöhlich hohe Zahlen.

## Reproduktionswert R(t) für München

Unser R(t) stand seit 31 Tagen über 1, heute geht der R(t) Wert auf 0,93 und damit knapp unter 1.

Das R(t) vom StaBLab der LMU wurde zum Stand 28.12. aktualisiert, der letzte Nowcast Lauf lieferte ein R(t) von 0,96 für München.

Die LMU aktualisiert die R(t) Berechnung für München nur noch einmal wöchentlich am Mittwoch.

| Datum | R(t) | LMU |
|---|---:|---:|
| 23.12. | 1,08 | 1,07 |
| 24.12. | 1,09 | 0,99 |
| 25.12. | 1,10 | 0,95 |
| 26.12. | 1,06 | 0,94 |
| 27.12. | 1,03 | 0,95 |
| 28.12. | 0,98 | 0,96 |
| 29.12. | 0,98 | 0,96 |

## Krankenhaus

Die Zahl der belegten Krankenhausbetten steigt heute stark an: Gegenüber gestern steigt die Zahl der belegten Normalbetten um 38(!) von 356 auf 394. IMC sinkt um 3 von gestern 19 auf heute 16. Intensivbetten sinkt um 1 von gestern 34 auf jetzt 33. Damit insgesamt 443 Betten belegt.

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation | 394 | +38 | 
| IMC | 16 | -3 | 
| Intensiv | 33 | -1 |
| SUMME | 443 | +34 |

Damit insgesamt 12 Betten **weniger** belegt als in der Vorwoche.

## Impfungen

Bei den Impfzahlen tut sich wenig. Leider.

| Impfung | München | Quote | Bayern | Quote |
|---:|---:|---:|---:|---:|
| 1. | +4 | 73,01% | +20 | 75,20% |
| 2. | +4 | 71,48% | +36 | 75,13% |
| 3. | +9 | 48,38% | +146 | 59,22% |
| 4. | +160 | 12,12% | +1.528 | 11,73% |
| 5. | --- | --- | +359 | 0,75% |
| 6. | --- | --- | +12 | 0,03% |

Wir bleiben in München weiter klar unter dem Landesdurchschnitt, besonders deutlich bei den Drittimpfungen: München 48,38%, Bayern 59,22%.

Die Stadt München scheint die Viert-, Fünft- und Sechstimpfungen als "weitere Auffrischimpfungen" zusammenzufassen und nicht getrennt anzugeben.