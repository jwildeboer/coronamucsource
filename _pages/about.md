---
title: "Impressum"
layout: default
excerpt: "Information about this page, GDPR/DSGVO compliance information, Impressum"
permalink: /impressum/
---

## Über CoronaMUC.de

Diese Seite veröffentlicht Zahlen und Daten zu COVID/Corona in München und Bayern. Es wird keine Gewähr für die Korrektheit der Zahlen übernommen.

## Impressum

**Angaben gemäß § 5 TMG  
Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV**

Jan Wildeboer  
Heilwigstraße 91  
81827 München  
GERMANY

### Kontakt:

E-Mail: info@coronamuc.de

### Datenschutzerklärung (DSGVO)

Auf dieser Webseite werden keine personenbezogenen Daten erhoben, gespeichert oder verarbeitet. Es werden keine Cookies verwendet.

### Verweis auf Dritte

Diese Website verwendet die [GoatCounter](https://www.goatcounter.com) Analytics Plattform. GoatCounter erfasst keine personenbezogenen Daten. Die GoatCounter Privacy Policy ist [hier](https://www.goatcounter.com/help/privacy) hinterlegt.

Diese Website wird auf [<img width="20" height="20" src="/images/codeberg-logo_icon_blue.svg" style="vertical-align: sub;"> Codeberg](https://codeberg.org) als [<img width="20" height="20" src="/images/codeberg-logo_icon_blue.svg" style="vertical-align: sub;"> Codeberg Pages](https://pages.codeberg.org) gehostet. Die Codeberg Privacy Policy ist [hier](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md) hinterlegt.
