---
title: "Quellen"
layout: default
permalink: /sources/
---

## Verwendete Quellen

Die hier präsentierten Zahlen und Graphen speisen sich aus folgenden Quellen, die alle öffentlich zugänglich sind:

**Ab 2023-06-02**

Am 2. Juni 2023 wurde die Aktualisierung des Corona-Dashboards und des COVID-19 Datenhubs eingestellt. Damit haben wir unsere Quellen umgestellt, damit wir weiterhin, wie gewohnt, Daten liefern können. Ob und wie lange diese Quellen gepflegt werden, wird sich zeigen.

- [RKI SARS-CoV-2 Archiv](https://github.com/robert-koch-institut/SARS-CoV-2-Infektionen_in_Deutschland_Archiv/tree/main/Archiv) RKI Daten zu allen Fällen in Deutschland, hier können wir täglich die Summe der Toten für München ermitteln.
- [RKI COVID-19 7 Tage Inzidenz](https://github.com/robert-koch-institut/COVID-19_7-Tage-Inzidenz_in_Deutschland) Aus den Daten für Bundesländer ermitteln wir die bayernweite 7-Tage-Inzidenz für die Dunkelzifferberechnung. Aus den Daten per Landkreis die Fallzahlen, Nachmeldungen und Korrekturen für München. Neue Zahlen gibt es Di-Sa. Nach Feiertagen und am Sonntag und Montag erfolgt wegen Meldepause keine Aktualisierung.
- [RKI Hospitalisierung](https://github.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland) Di-Sa holen wir aus der adjustierten Datei die Daten zur bayernweiten 7-Tage-Hospitaliserung, die Korrekturen und NowCast Zahlen. So-Mo zumindest die 7-Tage-Hospitaliserung für Bayern, am Wochenende wird die adjustierte Datei nicht aktualisiert.
- [RKI Impfzahlen](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquoten-Tab.html) Jeden zweiten Dienstag im Monat aktualisierte Impfzahlen für Bayern.
- [Intensivregister Ländertabelle](https://www.intensivregister.de/#/aktuelle-lage/laendertabelle) Durch COVID-Positiv getestete Patienten belegte Intensivbetten, bayernweit, mit Unterscheidung nach invasiv beatmet oder nicht.
- [Intensivregister Kreiskarte](https://www.intensivregister.de/#/aktuelle-lage/kartenansichten) Durch COVID-Positiv getestete Patienten belegte Intensivbetten in München, mit Unterscheidung nach invasiv beatmet oder nicht.
- [Bay-VOC Abwasserwerte](https://www.bay-voc.lmu.de/abwassermonitoring) Abwasserbelastung in München und Variantendaten aus der Abwasseranalyse.

**Bis 2023-06-02**

- RKI - Robert Koch Institut
  - [Quelle 1 für Zahl der Genesenen](https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/45258e51f57d43efb612f700a876ae8f_0/explore?filters=eyJMYW5ka3JlaXMiOlsiU0sgTfxuY2hlbiJdfQ%3D%3D) Täglich aktualisiert, leider mit Lücken, daher muß man den 2020-02-29, 2020-03-01, 2020-03-07, 2020-03-08, 2020-06-01, 2020-12-25, 2022-06-11 jedes mal manuell mit 0 einfügen.
  - [Quelle 2 für Fallzahlen](https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/6d78eb3b86ad4466a8e264aa2e32a2e4_0/explore?filters=eyJCdW5kZXNsYW5kSWQiOls5LDldLCJBZG1Vbml0SWQiOls5MTYyLDkxNjJdfQ%3D%3D) Täglich aktualisiert, aber ohne neue Zahlen am Sonntag und Montag, da am Wochenende vom LGL keine Zahlen an das RKI übermittelt werden.
  - [Quelle 3 für Impfzahlen](https://github.com/robert-koch-institut/COVID-19-Impfungen_in_Deutschland), bayernweit. Montag bis Samstag aktualisiert. Mit Auslaufen der Impfverordnung am 08.04.2023 keine Updates mehr.
  - [Quelle 4 für Hospitalisierung](https://github.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland), bayernweit, täglich aktualisiert.
- [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm#) Bis 2023-04-01 für IVENA Zahl zur Gesamtbettenbelegung in Bayern. Bis 2023-03-01  auch die Testzahlen. Beide Werte werden nicht mehr aktualisiert seitens LGL.
- [Stadt München](https://stadt.muenchen.de/infos/corona-fallzahlen-muenchen.html) für Hospitalisierungs- und Impfzahlen für München. Aktualisierung Montag bis Freitag. Seit 2023-03-06 keine neuen Zahlen mehr, da die Stadt München die Veröffentlichung eingestellt hat.
- [LMU StaBLab](https://corona.stat.uni-muenchen.de/nowcast/) Für Nowcast, Reproduktionswert R(t) und detaillierte Zahlen zur Krankenbettenbelegung bayernweit. Soll Dienstag - Freitag täglich aktualisiert werden, in Wirklichkeit aber unregelmäßig. Seit 2022-12-28 keine neuen Zahlen mehr, da die LMU die Veröffentlichung eingestellt hat.
- [Bay-VOC](https://www.bay-voc.lmu.de/surveillance.xhtml) Für die SARSCoV2 Variantenstatistik und Abwasserwerte im Wochenbericht. Soll wöchentlich aktualisiert werden, läuft aber deutlich nach.

## Berechung Reproduktionswert R(t)

Wir berechnen unser 4- und 7-Tages R(t) wie folgt:

- 4 Tages R(t): Summe der 7 Tages Summen der letzten 4 Tage, dividiert durch Summe der 7 Tages Summen der 4 Tage davor.
- 7 Tages R(t): Summe der 7 Tages Summen der letzten 7 Tage, dividiert durch Summe der 7 Tages Summen der letzten 7 Tage, aber um 3 Tage verschoben.

Diese Berechnungsart basiert auf der RKI Formel, die man [hier](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/R-Wert-Erlaeuterung.pdf?__blob=publicationFile) findet, kommt aber ohne Nowcast aus. Wir haben unsere Ergebnisse mit den offiziellen R(t) Werten verglichen und unsere Reproduktionswerte liegen erstaunlich nahe an den RKI Werten, daher halten wir die Methode für akzeptabel.

## Schätzung Dunkelziffer

### Abschätzung mit IFR (Infection Fatality Rate) von angenommenen 0,14%:

- Anzahl Todesfälle pro Kalenderjahr dividiert durch Anzahl Infektionen pro Kaldenrjahr ergibt naive IFR.
- Naive IFR dividiert durch angenommene IFR von 0,14% ergibt Faktor für Dunkelziffer

![DUnkelziffer über IFR](/images/CalcIFR.png)

### Abschätzung mit Krankenhauszahlen und Inzidenz:

- Summe der belegten Intensivbetten bayernweit heute, dividiert durch Summe der belegten Intensvbetten von vor 365 Tagen
- Das Ergebnis mit der korrigierten Inzidenz für Bayern von vor 365 Tagen multiplizieren (somit eine Schätzung wie hoch die aktuelle Inzidenz sein müsste basierend auf Intensivbettenbelegung)
- Das Ergebnis dividieren durch die tagesaktuelle Inzidenz für Bayern (und damit den Faktor erhalten)
- Und zum Schluss das Ergebnis mal 0,8 als Korrekturfaktor für höheren Impfungs/Immunisierungsgrad

![Dunkelziffer über Hospitalisierung und Inzidenz](/images/CalcHosp.png)

### Abschätzung mit Abwasserwerten

- Aktuelle Abwasserwerte nehmen und Durchschnitt der Trendlinienwerte der letzten 4 Messungen nehmen.
- Ein Jahr zurückschauen und Abwasserwerte von damals nehmen, wieder Durchschnitt der Trendlinienwerte der letzten 4 Messungen von damals nehmen.
- Inzidenz von vor einem Jahr nehmen.
- Dann damalige Inzidenz durch damaligem Abwasserwert dividieren.
- Wir erhalten einen Faktor, der besagt wieviel Abwasserpunkte wieviel Inzidenz bedeuten.
- Aktuellen Abwasserwert nehmen und mit diesem Faktor multiplizieren, ergibt geschätzte Inzidenz heute.
- Diesen Wert durch aktuelle RKI Inzidenz dividieren ergibt Dunkelzifferfaktor

Beispiel: Durchschnitt der letzten 4 Trendwerte vom 2023-08-02: 6,31. Durchschnitt der letzten 4 Trendwerte am 2022-08-01: 27,54. Inzidenz am 2022-08-08: ca. 380. 380 dividiert durch 27,54 = 13,8. 6,31 * 13,8 = 87,1. 87,1/2,4=36. Somit ist Dunkelzifferfaktor Abwasserbasiert 36. Geschätzte Dunkelziffer im August 2022 war um 2, also 36x2=DZF von 72.

Im Ergebnis sehr grobe Werte, die aber eine Abschätzung zulassen, wie hoch der Faktor für die Dunkelziffer ist. Alle Berechnungsarten sind in unserem Spreadsheet enthalten und werden daher täglich aktualisiert.

## Download und Weiterverwendung

Diese Quellen werden täglich in [diesem Spreadsheet](https://www.icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona) zusammengeführt und von da als CSV exportiert und in [diesem Repository](https://codeberg.org/jwildeboer/MUCNumbers) mit Archiv veröffentlicht. Zusätzlich spiegeln wir dieses Repository auch auf [GitHub](https://github.com/jwildeboer/MUCNumbers).

**Alle Daten stehen jedem frei zur beliebigen Verwendung zur Verfügung. Trotz größter Sorgfalt können wir für die Korrektheit der Daten keine Gewähr übernehmen.**

Für unsere Zahlen haben wir selbstverständlich auch eine DOI um als zitierfähige Quelle zu gelten:

[![DOI](https://zenodo.org/badge/507026399.svg)](https://zenodo.org/badge/latestdoi/507026399)