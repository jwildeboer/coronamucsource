## Build Status

[![Build Status](https://ci.codeberg.org/api/badges/jwildeboer/coronamucsource/status.svg)](https://ci.codeberg.org/jwildeboer/coronamucsource)

# coronamucsource

Numbers, graphs and insights for the Corona pandemic in Munich. This is the Jekyll source that gets rendered into the website. The output is then uploaded and made available at https://codeberg.org/jwildeboer/coronamuc which serves the website https://coronamuc.de itself.
