---
layout: default
title: 2022-07-25 Wochenbericht KW29
category: Wochenberichte
---

![2022-07-25 Dashboard mit Balkendiagrammen für Neue Fälle, ohne Zahlen für die letzten zwei Tage, Inzidenz von 903,6, R(t) von 1,10, insgesamt 450 Krankenhausbetten belegt](/images/2022/07/20220725.png)

In der letzten Woche gab es insgesamt 13.459 neue COVID Fälle in München, das sind 7,5% mehr als in der Vorwoche (12.521). Leider auch insgesamt 13 Tote mit COVID Nachweis in den letzten 7 Tagen.
<!--more-->

Die [LMU](https://corona.stat.uni-muenchen.de/nowcast/) liefert weiterhin eher unregelmäßig neue Zahlen, was schade ist - ein tägliches R(t) wäre hilfreich. Daher berehchnen wir parallel "unser" R(t) weiter. Auch beim [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm) kommen gewisse Zahlen, wie die 7 Tages Hospitaliserung, nicht wie versprochen täglich. Auch da gibt es Lücken, Sonntags wird seit Wochen nicht mehr aktualisiert.

Und das bei deutlich steigenden Zahlen. Das sind keine guten Voraussetzungen für die Herbst/Winterwelle, die mit Sicherheit kommen wird. Über zwei Jahre Pandemie und noch immer funktioniert nicht mal das Zahlen sammeln zuverlässig.

## Fallzahlen

In der letzten Woche war die 7 Tages Summe immer über 12.000, mit 14.187 in der Sptze am Donnerstag. An 3 von 7 Tagen war die Tageszahl der neuen Fälle sogar über 3.000. Solche Zahlen hatten wir zuletzt in der ersten Aprilwoche.

| Datum | Tageszahl | 7 Tages<br>Summe | Korr.<br>Inzidenz |
|---|--:|---:|---:|
| 17.07. | 7 | 12.471 | 838,0 |
| 18.07. | 2889 | 12.755 | 857,1 |
| 19.07. | 3121 | 12.981 | 872,3 |
| 20.07. | 3091 | 13.624 | 915,5 |
| 21.07. | 2734 | 14.187 | 953,3 |
| 22.07. | 1612 | 13.459 | 904,4 |
| 23.07. | 0 | 13.454 | 904,0 |

## Krankenhaus

Auch bei den Krankenhauszahlen in München ging es in der letzten Woche weiter nach oben. Sowohl auf Normal- als auch auf Intensivstationen. 

| Datum | Normal | IMC | ICU | Summe | Änderung zu<br>Vorwoche |
|---|--:|--:|--:|--:|--:|
| 17.07. | 364 | 17 | 25 | 406 | +59 |
| 18.07. | 372 | 9 | 32 | 413 | +55 |
| 19.07. | 372 | 9 | 32 | 413 | +59 |
| 20.07. | 391 | 10 | 38 | 439 | +95 |
| 21.07. | 397 | 10 | 36 | 443 | +91 |
| 22.07. | 399 | 11 | 36 | 446 | +77 |
| 23.07. | 401 | 11 | 35 |447 | +59 | 
 
## Impfungen

Die Impfzahlen in München bleiben weiterhin zu niedrig. Die Impfquoten sind leider immer noch klar unter den Zahlen für ganz Bayern.

| Impfung | München | Bayern |
|---|--:|--:|
| 1. | 72,85% | 75,31% |
| 2. | 71,31% | 75,18% |
| 3. | 47,63% | 56,85% |
| 4. | 5,01% | 5,16% |

![Balken- und Kurvendiagramm der Impfzahlen in München das zeigt, daß in 2022 die Impfquoten sehr langsam bis kaum steigen](/images/KW29Impf.png)

## Tests (bayernweit)

Immer am Donnerstag werden die Testzahlen vom LGL aktualisiert. Dieser Wochentakt macht es schwierig Trends zu erkennen. Wir testen mt ca 30% der maximalen Kapazität und einer Positivrate von über 40%. Da es keine Zahlen zu Bürgertests gibt, kann man nur vermuten, daß deutlich zu wenig getestet wird und die Dunkelziffer daher hoch sein muss.

![Bayernweite Tests - Anzahl Tests steigt leicht auf 320000 pro Tag, Positivrate steigt auf über 40%](/images/2022/07/20220721-Test.png)