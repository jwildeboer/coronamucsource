---
layout: default
title: 2022-09-04 Tagesbericht
category: Tagesberichte
---

![2022-08-02 Dashboard mit den Zahlen für München. Drei Balkengrafiken. Erste zeigt Neu- und Nachmeldungen, weiter klar weniger als letzte Woche. Zweite zeigt Krankenbetten, gegenüber gestern leichte Abnahme um 4 Betten, dritte zeigt Inzidenz, die deutlich sinkt.](/images/2022/08/20220802.png)

Nur ein Test, ob der Upload funktioniert. Das RKI meldet eine Inzidenz von X für München. X neue und X Nachmeldungen für die letzten 7 Tage, davon X für vorgestern.

Die gestern gemeldete Inzidenz steigt mit den Nachmeldungen von X auf X.
<!--more-->

Die Entwicklung der Inzidenzen der letzten Tage (gemeldet -> aktualisiert):

| 25.07. | 876,7 -> 928,2 |
| 26.07. | 819,5 -> 864,7 |
| 27.07. | 781,3 -> 821.0 |
| 28.07. | 712,1 -> 739,4 |
| 29.07. | 665,2 -> 675,4 |
| 30.07. | 664,7 -> 675,3 |
| 31.07. | 663,5 -> 674,4 |

## Krankenhaus

Die Krankenhaus-Belegung ist heute wieder leicht gesunken, hauptsächlich bei den Intensivbetten (-4). 

| Art | Gesamt | Änderung |
|---|---:|---:|
| Normalstation |  | + | 
| IMC | | + | 
| Intensiv | | + |
| SUMME | | + |

Im Wochenvergleich entspricht das weiterhin einem Plus von 28 Betten.

## Impfungen

Die Impfzahlen, Stand 27.07., wie gehabt. Kaum Erst- und Zweitimpfungen. 

1. +
2. +
3. +
4. +

Wir bleiben in München weiterhin deutlich unter dem Landesdurchschnitt.